<?php
	include('../admin/conf.php');
	require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

	$id = ($_GET['id']) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
	$cant = ($_GET['cant']) ? mysqli_real_escape_string($conexion, $_GET['cant']) : 0;

	$precio = getPrecio($id);

	if(!tieneDescuento($id)){
		$pd = consulta_bd("precio_cantidad","productos_detalles","id = $id","");
		if($pd[0][0]){
			$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $cant AND productos_detall_id = $id","rango DESC");
			if($precios_cantidad){
				$pc = $precios_cantidad[0];
				$rango 		= $pc[1];
				$descuento 	= $pc[2];
				$precio = $precio - ($precio * ($descuento / 100));
			}else{
				$precio = getPrecio($id);
			}	
		}
	}
	



?>

<div class="price ohidden">
     
    <span>$<?php echo number_format($precio,0,",","."); ?></span>
    
    <span>IVA Incluido</span>
</div>

<?php if (tieneDescuento($id)): ?>
	<div class="discount">
	    Antes $<?php echo number_format(getPrecioNormal($id),0,",","."); ?>
	</div>
<?php endif ?>