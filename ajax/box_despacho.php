<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$comuna_id = (isset($_GET['comuna'])) ? mysqli_real_escape_string($conexion, $_GET['comuna']) : 0;

?>

<div class="bx_despacho mtop-20 ohidden">
    <div class="title_box">Tipo de despacho</div>
	
	<?php if ($comuna_id == 0): ?>
		<span class="text_despacho">Seleccione una comuna para ver las opciones de despacho</span>
	<?php elseif($comuna_id == 999): ?>
		<span class="text_despacho">Retiro en tienda</span>
	<?php else: 
		$valor_despacho = valorDespacho($comuna_id); ?>
		<div class="group_radio_despacho">
	        <input type="radio" name="tipodespacho" value="1" id="despachoNormal" checked>
	        <?php if (!is_numeric($valor_despacho)): ?>
	        	<label for="despachoNormal" style="height: 15px;margin-top: 5px;">Normal: <span>Por pagar</span> <small>de 3 a 5 días</small></label>
	        <?php else: ?>
	        	<label for="despachoNormal" style="height: 15px;margin-top: 5px;">Normal: <span>$<?= number_format($valor_despacho, 0, ',', '.') ?></span> <small>de 3 a 5 días</small></label>
	        <?php endif ?>
	    </div>

	    <!-- <div class="group_radio_despacho">
	        <input type="radio" name="tipodespacho" value="1" id="despachoExpress">
	        <label for="despachoExpress" style="height: 15px;margin-top: 5px;">Express: <span>$10.000</span> <small>Durante el día</small></label>
	    </div> -->
	<?php endif ?>
</div>