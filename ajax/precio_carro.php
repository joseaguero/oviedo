<?php 

include('../admin/conf.php');
include('../admin/includes/tienda/cart/inc/functions.inc.php');

$comuna_id = (isset($_GET['comuna'])) ? mysqli_real_escape_string($conexion, $_GET['comuna']) : 0;

?>

<div class="totales_xs">
    <span class="tituloxs">Total:</span>
    <div class="precioxs">$<?= number_format(get_precio_total($comuna_id, 0), 0, ',', '.') ?></div>
</div>