<?php 

include('../admin/conf.php');
date_default_timezone_set('America/Santiago');

$fecha_hoy = date('Y-m-d H:i:s', time());

$cliente_id = $_COOKIE['usuario_id'];
$region 	= mysqli_real_escape_string($conexion, $_POST['region']);
$comuna 	= mysqli_real_escape_string($conexion, $_POST['comuna']);
$calle 		= mysqli_real_escape_string($conexion, $_POST['calle']);
$numero 	= mysqli_real_escape_string($conexion, $_POST['numero']);
$piso 		= mysqli_real_escape_string($conexion, $_POST['piso']);
$alias 		= mysqli_real_escape_string($conexion, $_POST['alias']);
$principal 	= mysqli_real_escape_string($conexion, $_POST['principal']);

if ($principal == "true") {
	$principal_chk = 1;
}else{
	$principal_chk = 0;
}

if ($region != 0 AND $comuna != 0 AND trim($calle) != '' AND trim($numero) AND trim($alias)) {
	if ($principal == "true") {
		$update_principal = update_bd('clientes_direcciones', 'principal = 0', 'principal = 1');
	}

	$insert = insert_bd('clientes_direcciones', 'nombre, cliente_id, region_id, comuna_id, calle, numero, piso, principal', "'$alias', $cliente_id, $region, $comuna, '$calle', '$numero', '$piso', $principal_chk");

	if ($insert) {
		$out['status'] = 'success';
		$out['message'] = 'Dirección agregada correctamente';
	}else{
		$out['status'] = 'error';
		$out['message'] = 'Error al crear la dirección';
	}

}else{
	$out['status'] = 'error';
	$out['message'] = 'Debes completar todos los campos requeridos';
}

echo json_encode($out);


?>