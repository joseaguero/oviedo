<?php 

include('../admin/conf.php');

$region_id = mysqli_real_escape_string($conexion, $_POST['id']);

$comunas = consulta_bd('c.id, c.nombre', 'comunas c JOIN despachos d ON c.id = d.comuna_id', "c.region_id = $region_id AND d.a > 0", 'c.nombre asc');

if (is_array($comunas)) {
	$i = 0;
	$out['status'] = 'success';
	foreach ($comunas as $c) {
		$out['comunas'][$i]['id'] = $c[0];
		$out['comunas'][$i]['nombre'] = $c[1];

		$i++;
	}
}else{
	$out['status'] = 'error';
}

echo json_encode($out);

?>