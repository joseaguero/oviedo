<?php 
	include("../admin/conf.php");

	//include("../funciones.php");
	$nombre = (isset($_POST["nombre"])) ? ucwords(strtolower(mysqli_real_escape_string($conexion, $_POST["nombre"]))) : 0;
	$correo = (isset($_POST["email-registro"])) ? mysqli_real_escape_string($conexion, $_POST["email-registro"]) : 0;
	$correo_re = (isset($_POST["email-re"])) ? mysqli_real_escape_string($conexion, $_POST["email-re"]) : 0;
	$pass = (isset($_POST["password"])) ? mysqli_real_escape_string($conexion, $_POST["password"]) : 0;	
	$pass_re = (isset($_POST["password-re"])) ? mysqli_real_escape_string($conexion, $_POST["password-re"]) : 0;

		
	if ($correo == $correo_re) {
		
		if ($pass == $pass_re) {
			$pass_encrypted = md5($pass);
			$rand = rand(1000, 9999);
		    $password_salt = md5($rand);
		    $password = hash('md5',$pass_encrypted.$password_salt);
			
			$existe = consulta_bd("id","clientes","email='$correo'","");
			$cant = mysqli_affected_rows($conexion);
			if($cant > 0){
				$error = "Correo ya se encuentra registrado";
				header("Location: {$url_base}login?a=2&msje=$error");
				die();
			}

			$keys = "nombre, email, fecha_creacion, clave, password_salt, activo";
			$vals = "'$nombre', '$correo', NOW(), '$password', '$password_salt', 1";
			
			$insert = insert_bd("clientes", $keys, $vals);
			if($insert){
				$error = "Tu cuenta fue creada, por favor inicia sesión.";
				header("Location: {$url_base}login?a=1&msje=$error");
				die();
			} else{
				$error = "No fue posible crear tu cuenta en este momento, inténtalo más tarde.";
				header("Location: {$url_base}login?a=2&msje=$error");
				die();
			}
		}else{
			$error = "Las contraseñas no son iguales";
			header("Location: {$url_base}login?a=2&msje=$error");
			die();
		}

	}else{
		$error = "Los correos no son iguales";
		header("Location: {$url_base}login?a=2&msje=$error");
		die();
	}
	
?>