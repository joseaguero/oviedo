<?php 
	include("../admin/conf.php");
	
	$correo = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
	$clave = (isset($_POST['clave'])) ? mysqli_real_escape_string($conexion, $_POST['clave']) : 0;
	$recordarCuenta = (isset($_POST['recordarCuenta'])) ? mysqli_real_escape_string($conexion, $_POST['recordarCuenta']) : 0;

	$origen = (isset($_POST['origen'])) ? mysqli_real_escape_string($conexion, $_POST['origen']) : 0;
	
	$pass_hash =  md5($clave);
	$pass_encrypted = consulta_bd("password_salt", "clientes", "email = '$correo'", "");
	$cantClientes = mysqli_affected_rows($conexion);
	if($cantClientes == 0){
		$error = "Su correo no se encuentra registrado";
		if($origen === 'carro'){
			header("Location: ../identificacion?a=2&msje=$error");
			} else {
				header("Location: ../login?a=2&msje=$error");
				}
		
		die();
		}
	
	$password_final = hash('md5',$pass_hash.$pass_encrypted[0][0]);
	$enter  = consulta_bd("id, nombre", "clientes", "clave ='$password_final' and email ='$correo'", "");
	$cant = mysqli_affected_rows($conexion);
	
	if(count($enter) > 0){
		//guardo los datos en una cookie y la contraseña codificada en base 64
		if($recordarCuenta == true){
			setcookie("nombreUsuario", "$correo", time() + (365 * 24 * 60 * 60), "/");
			setcookie("claveUsuario", base64_encode($clave), time() + (365 * 24 * 60 * 60), "/");
			} else {
			setcookie('nombreUsuario', null, -1, '/');
			setcookie('claveUsuario', null, -1, '/');
			}
		//guardo los datos en una cookie
		$_SESSION["usuario"] = $enter[0][0];
		setcookie("usuario_id", $enter[0][0], time() + (365 * 24 * 60 * 60), "/");
		if($origen === 'carro'){
			header("Location: ../envio-y-pago");
			} else {
				header("Location: ../cuenta/mis-datos?a=1&msje=Bienvenido ".$enter[0][1]);
				}
		die();
	} else {
		$error = "No fue posible acceder a su cuenta.";
		if($origen == 'carro'){
			header("Location: ../identificacion?a=2&msje=$error");
			} else {
				header("Location: ../login?a=2&msje=$error");
				}
		die();
	}	
?>
