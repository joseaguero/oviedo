<?php 

include('../admin/conf.php');

$producto_id 	= mysqli_real_escape_string($conexion, $_POST['producto_id']);
$cliente_id 	= $_COOKIE['usuario_id'];
$where			= mysqli_real_escape_string($conexion, $_POST['where']);

$id = consulta_bd('id', 'productos_guardados', "producto_id = $producto_id AND cliente_id = $cliente_id", '');

$del = del_bd('productos_guardados', $id[0][0]);

if ($del) {
	if ($where == 'guardados') {
		$_SESSION['flash']['status']	= 'success';
		$_SESSION['flash']['message']	= 'Producto eliminado de favoritos';
	}elseif ($where == 'dash'){
		$_SESSION['flash']['status']	= 'success';
		$_SESSION['flash']['message']	= 'Producto agregado al carro';
	}
	
	echo 1;
}else{
	echo 2;
}

?>