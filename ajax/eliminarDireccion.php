<?php 

include('../admin/conf.php');

$id = mysqli_real_escape_string($conexion, $_POST['id']);

$del = del_bd('clientes_direcciones', $id);

if ($del) {
	$out['status'] = 'success';
	$out['message'] = 'Dirección eliminada correctamente';
}else{
	$out['status'] = 'error';
	$out['message'] = 'Error al eliminar la dirección';
}

echo json_encode($out);

?>