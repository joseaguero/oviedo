<?php 

include(dirname(__FILE__).'/../admin/conf.php');
date_default_timezone_set('America/Santiago');

// ini_set('max_execution_time', 0);
// ini_set('default_socket_timeout', 1000);

$query = consulta_bd('id, sku, producto_id', 'productos_detalles', '', 'id asc');
$fechaHoy = date('Y-m-d H:i:s', time());

$contador = 0;

$stock = 0;
try {
	$publicado = 0;

	//$client = new SoapClient("http://wsjce.isprd.cl/Servicio.asmx?WSDL",
	$client = new SoapClient("http://200.42.190.41/WSOviedo/Oviedo.svc?singleWsdl",
    //$client = new SoapClient("http://190.153.252.244:81/wsjce/Servicio.asmx?WSDL",
	  array("trace" => 1,'soap_version' => SOAP_1_1,'style' => SOAP_DOCUMENT,'encoding' => SOAP_LITERAL,'cache_wsdl' => WSDL_CACHE_NONE));

	//$param = array("CodigoProducto" => $CodigoProducto);
	$param = array("SQLKey" => '3H32YY01CC');
 	$result = $client->Productos_Ver($param)->Productos_VerResult;

	if(is_array($result->ClsProducto)){
		$result = $result->ClsProducto;
	} else {
		$result = array($result->ClsProducto);
	}

	foreach ($query as $producto) {
		
		$sku_query = $producto[1];
		$producto_madre = $producto[2];
		$id = $producto[0];
		$stock = 0;

		foreach ($result as $aux) {

			
			if ($sku_query == $aux->Codigo_Tecnico) {

				$bodega_id = $aux->IdBodega;
				$precio = round($aux->ValorNeto * 1.19);

				if ((int)substr($precio, -3) < 500) {
					$precio = (int)substr($precio, 0 , -3)."490";
				}else{
					$precio = (int)substr($precio, 0 , -3)."990";
				}

				if ($bodega_id == 20 OR $bodega_id == 18 OR $bodega_id == 8 OR $bodega_id == 4 OR $bodega_id == 19 OR $bodega_id == 25) {
					$stock += $aux->Stock;
				}
				
			}

		}

		if ($stock > 0 AND $precio > 0) {
			$publicado = 1;
		}

		update_bd('productos_detalles', "publicado = $publicado, stock = $stock, precio = $precio, fecha_modificacion = '$fechaHoy'", "id = $id");
		update_bd('productos', "publicado = $publicado, fecha_modificacion = '$fechaHoy'", "id = $producto_madre");

		$out[$contador]['publicado'] = $publicado;
		$out[$contador]['sku'] = $sku_query;
		$out[$contador]['precio'] = $precio;
		$out[$contador]['stock'] = $stock;

	 	$contador++;

	}


} catch (Exception $e) {
	/*echo '<pre>';
	var_dump($e);
	echo '</pre>';
	trigger_error($e->getMessage(), E_USER_WARNING);*/
	
	// echo 'Excepción capturada';
	// echo $sku . ' | ERROR <br>';
}

echo '<pre>';
print_r($out);
echo '</pre>';

?>