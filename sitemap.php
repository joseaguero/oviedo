<?php
include('admin/conf.php');
header("Content-type: text/xml");
?>
<?='<?xml version="1.0" encoding="utf-8"?>'?>
<?="<?xml-stylesheet type='text/xsl' href='".$url_base."includes/template_sitemap.xsl'?>"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url>
  <loc><?=$url_base?></loc>
</url>

<url>
  <loc><?=$url_base?>como-comprar</loc>
</url>

<url>
  <loc><?=$url_base?>tiendas</loc>
</url>

<url>
  <loc><?=$url_base?>politicas-de-privacidad</loc>
</url>

<url>
  <loc><?=$url_base?>terminos-y-condiciones</loc>
</url>

<url>
  <loc><?=$url_base?>politicas-de-calidad</loc>
</url>

<url>
  <loc><?=$url_base?>marcas</loc>
</url>

<url>
  <loc><?=$url_base?>ofertas</loc>
</url>

<?php 

$lineas = consulta_bd('id, nombre, fecha_modificacion', 'lineas', 'publicado = 1', '');

if (is_array($lineas)) {
  foreach ($lineas as $linea) {
    if($linea[2] != null && $linea[2] != '') {
      $fecha_modificacion = new Datetime($linea[2]);
      $fecha_modificacion->setTimezone('America/Santiago');
      $fecha_mod = '<lastmod>'.$fecha_modificacion->format('Y-m-d H:i:sP').'</lastmod>';
    }else{
      $fecha_mod = '';
    }

    echo 
    '<url>
      <loc>'.$url_base.'lineas/'.$linea[0].'/'.url_amigables($linea[1]).'</loc>'
      .$fecha_mod.'<changefreq>daily</changefreq>
      <priority>0.8</priority>
    </url>';
  }
}

$categorias = consulta_bd('id, nombre, fecha_modificacion', 'categorias', 'publicada = 1', '');

if (is_array($categorias)) {
  foreach ($categorias as $categoria) {
    if($categoria[2] != null && $categoria[2] != '') {
      $fecha_modificacion = new Datetime($categoria[2]);
      $fecha_modificacion->setTimezone('America/Santiago');
      $fecha_mod = '<lastmod>'.$fecha_modificacion->format('Y-m-d H:i:sP').'</lastmod>';
    }else{
      $fecha_mod = '';
    }

    echo 
    '<url>
      <loc>'.$url_base.'categorias/'.$categoria[0].'/'.url_amigables($categoria[1]).'</loc>'
      .$fecha_mod.'<changefreq>daily</changefreq>
      <priority>0.8</priority>
    </url>';
  }
}

?>

<?php

$productos = consulta_bd('id, nombre, fecha_modificacion', 'productos', 'publicado = 1', '');

if (is_array($productos)) {
  foreach ($productos as $producto) {
    if($producto[2] != null && $producto[2] != '') {
      $fecha_modificacion = new Datetime($producto[2]);
      $fecha_modificacion->setTimezone('America/Santiago');
      $fecha_mod = '<lastmod>'.$fecha_modificacion->format('Y-m-d H:i:sP').'</lastmod>';
    }else{
      $fecha_mod = '';
    }

    echo 
    '<url>
      <loc>'.$url_base.'ficha/'.$producto[0].'/'.url_amigables($producto[1]).'</loc>'
      .$fecha_mod.'<changefreq>daily</changefreq>
      <priority>0.8</priority>
    </url>';
  }
}

?>
</urlset>
