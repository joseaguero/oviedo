$(function(){
	
	 /*Menu responsive */
	 $('.toggle-btn').click(function(){
		$('.filter-btn').toggleClass('open');
	
	 });
	 $('.filter-btn a').click(function(){
		$('.filter-btn').removeClass('open');
	 });

	$('.slider-ficha-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-ficha-nav',
		infinite: false,
		responsive: [
		    {
		      breakpoint: 860,
		      settings: {
		        arrows: true,
		        'prevArrow': '<button type="button" class="slick-prev"><img src="img/arrow-ficha-prev.jpg" /></button>',
				'nextArrow': '<button type="button" class="slick-next"><img src="img/arrow-ficha-next.jpg" /></button>',
		      }
		    },
		  ]
	});

	$('.slider-ficha-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.slider-ficha-for',
		dots: false,
		focusOnSelect: true,
		vertical: true,
		arrows: false,
		infinite: false,
		responsive: [
		    {
		      breakpoint: 860,
		      settings: {
		        vertical: false,
		        arrows: true
		      }
		    },
		  ]
	});

	/*$('.grid-categorias').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		infinite: true
	});*/
	 
	$('.hamburger').click(function () {
		$('.hamburger').toggleClass('open');
		$('.contMenu').slideToggle(100);
	});

	var min_valor_filtros = parseInt($('.min-filtro').attr('data-info'));
	var max_valor_filtros = $('.precio_filtro').attr('data-max');
	var max_filtro_actual = $('.precio_filtro').attr('data-max-actual');

	$( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: max_valor_filtros,
      step: 1000,
      values: [ min_valor_filtros, max_filtro_actual ],
      slide: function( event, ui ) {
      	$('.min-filtro').html('$' + number_format(ui.values[0]), 0);
      	$('.max-filtro').html('$' + number_format(ui.values[1]), 0);

      	$('.min-filtro').attr('data-valor', ui.values[0]);
      	$('.max-filtro').attr('data-valor', ui.values[1]);
      },
      change: function( event, ui ) {
      	var chk_cat = $('input[name="chk_cat"]:checked'),
			chk_marcas = $('input[name="chk_marcas"]:checked'),
			min_price = $('.min-filtro').attr('data-valor'),
			max_price = $('.max-filtro').attr('data-valor'),
			chk_fglobal = $('input[name="chk_fglobal"]:checked'),
			orden = $('.orderby').attr('data-order'),
			link = '';

		var ref = $('.filtros_col').attr('data-ref');

		var str_cat = '?cat=',
			str_marcas = '&marca=',
			str_fglobal = '&filtros=',
			str_min = '&desde=',
			str_max = '&hasta=';

		if (ref == 'busqueda') {
			str_cat = '&cat=';
		}

		if (ref == 'busqueda') {
			chk_cat.each(function(index, el) {
				if (str_cat == '&cat=') {
					str_cat += $(this).val();
				}else{
					str_cat += '-' + $(this).val();
				}
			});
		}else{
			chk_cat.each(function(index, el) {
				if (str_cat == '?cat=') {
					str_cat += $(this).val();
				}else{
					str_cat += '-' + $(this).val();
				}
			});
		}
		

		if (ref == 'cat') {
			str_marcas = '?marca=';
			chk_marcas.each(function(index, el) {
				if (str_marcas == '?marca=') {
					str_marcas += $(this).val();
				}else{
					str_marcas += '-' + $(this).val();
				}
			});
		}else{
			chk_marcas.each(function(index, el) {
				if (str_marcas == '&marca=') {
					str_marcas += $(this).val();
				}else{
					str_marcas += '-' + $(this).val();
				}
			});
		}

		chk_fglobal.each(function(index, el) {
			if (str_fglobal == '&filtros=') {
				str_fglobal += $(this).val();
			}else{
				str_fglobal += '-' + $(this).val();
			}
		});

		str_min += min_price;
		str_max += max_price;

		if (ref == 'cat') {
			link = str_marcas + str_fglobal + str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'lineas'){
			link = str_cat + str_marcas + str_fglobal + str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'marcas'){
			link = str_cat + str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'busqueda'){
			var busqueda = getParameterByName('buscar');
			link = '?buscar=' + busqueda + str_cat + str_marcas + str_min + str_max + '&orden=' + orden + '&page=1';
		}else if(ref == 'ofertas'){
			link = str_cat + str_marcas + str_min + str_max + '&orden=' + orden + '&page=1';
		}

		var	url_actual = window.location.pathname;

		location.href = url_actual + link;
      }
    });

    $('.min-filtro').html('$' + number_format($( "#slider-range" ).slider( "values", 0 )), 0);
    $('.max-filtro').html('$' + number_format($( "#slider-range" ).slider( "values", 1 )), 0);

    $('.min-filtro').attr('data-valor', $( "#slider-range" ).slider( "values", 0 ));
    $('.max-filtro').attr('data-valor', $( "#slider-range" ).slider( "values", 1 ));
	
	$(".movil").click(function(){
		$("ul.sub").hide();
		console.log("sdfsdfsfd");
		$(this).parent().find("ul.sub").show(100);
		});
		
	$(".abrirFooter").click(function(){
		$(".contrespFooter").hide(100);
		$(this).parent().parent().find(".contrespFooter").show(1000);
		});
	 /*Menu responsive */
	
	/*Buscador responsive*/ 
	$('.iconoBuscador').click(function () {
		$('.buscador').slideToggle();
	});
	
    /*Botonera responsive de identificacion*/
    $(".contBotonesIdentificacionResponsive a").click(function(){
			$(".contBotonesIdentificacionResponsive a").removeClass("active");
			$(this).addClass("active");
			$(".cont50Identificacion").fadeOut(100);
			var tabs = $(this).attr("rel");
			$("#" + tabs).fadeIn(100);
		});
    
    
    /*BOTONERAS ENVIO Y PAGO RESPONSIVE*/
	$(".contBotoneraEnvioYPago a").click(function(){
		$(".contBotoneraEnvioYPago a").removeClass("activo");
		$(this).addClass("activo");
		var tab = $(this).attr("rel");
		$(".contPaso3Movil").fadeOut(100);
		$("#"+tab).fadeIn(100);
		if(tab == "misDatos"){
			$("#facturaMovil").fadeIn(100);
			}
		
		});
	/*BOTONERAS ENVIO Y PAGO RESPONSIVE*/
    
    
	$('#btnNewsletter').click(function(){
		var correo = $('#newsletter').val();
		if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            swal({
			  icon: 'error',
			  text: 'El correo electrónico introducido no es correcto',
			  button: 'ACEPTAR'
			});
			return false;
        } else {
			$.ajax({
				url      : "ajax/newsletter.php",
				type     : "post",
				async    : true,
				data 	 : {correo : correo},
				success  : function(resp){
					console.log(resp);
					if(resp == 1){
						swal({
						  icon: 'success',
						  text: 'Correo ingresado exitosamente',
						  button: 'ACEPTAR'
						});
					}
					else if(resp == 2){
						swal({
						  icon: 'error',
						  text: 'tu correo ya se encuentra en nuestros registros',
						  button: 'ACEPTAR'
						});
					};

					$('#newsletter').val('');
				}
			});
		};
	});
/*
Agregar correo al newsletter -------------------------	----------------------	----------------------	
*/
	

/*
GUARDAR NUEVA DIRECCION
*/
$("#guardarDireccion").click(function(){
	var nombre = $("#nombre").val();
	var region = $("#region").val();
	var comuna = $("#comuna").val();
	var localidad = $("#localidad").val();
	var calle = $("#calle").val();
	var numero = $("#numero").val();
	
	console.log(region);
	console.log(comuna);
	console.log(localidad);
	if(nombre != "" && region != "" && comuna != "" && localidad != "" && calle != "" && numero != ""){
		$("#formNuevaDireccion").submit();
		} else {
			console.log("Faltan campos por completar.");
			swal({
			  type: 'error',
			  text: 'Faltan campos por completar.'
			});
		}
	
});


/*Eliminar direcciones guardadas*/	
$(".deleteDireccion").click(function(){
	var id = $(this).attr("rel");
	console.log(id);
	$.ajax({
		url      : "ajax/eliminaDireccion.php",
		type     : "post",
		async    : true,
		data 	 : {id : id},
		success  : function(resp){
			console.log(resp);
			if(resp == 1){
				console.log("dirección eliminada");
				swal({
					  type: 'success',
					  text: 'Dirección eliminada.'
					});
					$("#dir_"+id).hide(300);
				
				} else {
					console.log("No fue posible eliminar dirección.");
						swal({
						  type: 'error',
						  text: '"No fue posible eliminar dirección.'
						});
					}
		}
	});

});
	
	
	

	
	

	
	
	/*
Registro
*/
	$("#btnRegistro").click(function(){
		var nombre = $("#nombre").val();
		var apellido = $('#apellido').val();
		var rut = $("#Rut").val();
		var email = $("#email").val();
		var clave = $("#clave").val();
		var clave2 = $("#clave2").val();

		
		
		if(nombre != '' && email != '' && clave != '' && clave2 !='' && apellido != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				swal('','Email invalido','error'); 
			} else {
				/* Correo valido */
				if(clave === clave2){
					/* Envio el formulario */
					$("#formularioRegistro").submit();
				} else {
					/* Claves no coinciden */
					//alertify.error("Claves no coinciden");
					swal('','Claves no coinciden','error'); 
					$("#clave").addClass("campo_vacio");
					$("#clave2").addClass("campo_vacio");
				};
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			swal('','Faltan campos por completar','error'); 
			if(nombre == '' ){
				$("#nombre").addClass("campo_vacio");
			} else {
				$("#nombre").removeClass("campo_vacio");
			};

			if(apellido == '' ){
				$("#apellido").addClass("campo_vacio");
			} else {
				$("#apellido").removeClass("campo_vacio");
			};
			
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(rut == '' ){
				$("#Rut").addClass("campo_vacio");
			} else {
				$("#Rut").removeClass("campo_vacio");
			};
			
			if(clave == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
			
			if(clave2 == '' ){
				$("#clave2").addClass("campo_vacio");
			} else {
				$("#clave2").removeClass("campo_vacio");
			};

			
			
		};
		
	});/*fin registro paso 1*/
	
	
	
	
	/*Actualizar */
	$("#btnActualizar").click(function(){
		var nombre = $("#nombre").val();
		var apellido = $('#apellido').val();
		var Rut = $("#Rut").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var clave = $("#clave").val();
		var clave2 = $("#clave2").val();

		
		if(clave == clave2){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				swal('','Email incorrecto','error'); 
				} else {
					$("#formularioActualizar").submit();
					}
			
			} else if(clave == "" && clave2 == ""){
				if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
					swal('','Email incorrecto','error'); 
					} else {
						$("#formularioActualizar").submit();
						}
			} else {
					swal('','Claves no coinciden','error'); 
				}
				
	});/*fin actualizar1*/
	
	
	
	
	
	
	
	
	/*Inicio de session*/
	$("#inicioSesion").click(function(){
		console.log("entra");
		var email = $("#email").val();
		var password = $("#clave").val();
		
		
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				swal('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$("#formularioLogin").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			swal('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
	/*Inicio de session carro de compras*/
	$("#inicioSesionIdentificacion").click(function(){
		
		var email = $("#email").val();
		var password = $("#clave").val();
		
		console.log(password);
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				swal('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$(".formInicioSesion").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			swal('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
/*Recuperar clave ----------------------------------------------------------------------------------------------------*/
	$('#recuperarClave').click(function(){
		//alert("sdfdsf");
		var correo = $('#email').val();
		if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            swal('','El correo electrónico introducido no es correcto.','error'); 
			return false;
        } else {
			console.log(correo);
			$("#formRecuperarClave").submit();
		};
	});/*fin funcion*/
/*Recuperar clave ----------------------------------------------------------------------------------------------------*/
	
	
	/* Encvio formulario para crear cuenta a partir de los datos obtenidos de una compra exitosa*/
	$(".btnCuentaExito").click(function(){
		$("#crearCuentaDesdeExito").submit();
		});
	
});







	
	
	/*
Validar rut
*/
	
$("#Rut").Rut({
   on_error: function(){ 
   				//swal(type:'error', text:'');
				swal('Error','Rut incorrecto','error'); 
				$("#Rut").addClass("campo_vacio");
				$("#Rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#Rut").removeClass("campo_vacio");} 
});
$("#rut_retiro, #rut_empresa").Rut({
   on_error: function(){ 
   				//swal(type:'error', text:'');
				swal('Error','Rut incorrecto','error'); 
				$("#Rut").addClass("campo_vacio");
				$("#Rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#Rut").removeClass("campo_vacio");} 
});
$("#RutFactura").Rut({
   on_error: function(){ 
   				alertify.error('El rut ingresado es incorrecto'); 
				$("#RutFactura").addClass("incompleto");
				$("#RutFactura").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#RutFactura").removeClass("incompleto");} 
   
});

$('input[name="rut"]').Rut({
	on_error: function(){ 
		alertify.error('El rut ingresado es incorrecto'); 
		$('input[name="rut"]').addClass("incompleto");
		$('input[name="rut"]').val("");
	},
	format_on: 'keyup'
})
	
$('.countdown').each(function(){
	var $this = $(this);
	var date = $this.attr('rel');
	var countDownDate = new Date(date).getTime();
	
	var x = setInterval(function() {
		var now = new Date().getTime();
		var distance = countDownDate - now;
		
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		// $this.empty();

		hours = (hours > 9) ? hours : '0'+hours;
		seconds = (seconds > 9) ? seconds : '0'+seconds;
		minutes = (minutes > 9) ? minutes : '0'+minutes;

		$this.find('.box[data-rel="hour"]').html(hours);
		$this.find('.box[data-rel="minute"]').html(minutes);
		$this.find('.box[data-rel="second"]').html(seconds);
		
		/*if(days == 1){
			$this.append(days + " día " + hours + " Horas ");
		}else if(days > 0){
			$this.append(days + " días " + hours + " Horas ");
		}else if(hours == 1){
			$this.append(hours + " Hora " + minutes + " minutos");
		}else if(hours > 0){
			$this.append(hours + " Horas " + minutes + " minutos");
		}else{
			$this.append(minutes + " Minutos " + seconds + "s");	
		}*/
		
		if (distance < 0) {
			$this.empty();
			$this.append("TERMINADA");
		}
	}, 1000);
});

$('.timer').each(function(){
	var $this = $(this);
	var date = $this.attr('data-fecha');
	var countDownDate = new Date(date).getTime();
	
	var x = setInterval(function() {
		var now = new Date().getTime();
		var distance = countDownDate - now;
		
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		// $this.empty();

		hours = (hours > 9) ? hours : '0'+hours;
		seconds = (seconds > 9) ? seconds : '0'+seconds;
		minutes = (minutes > 9) ? minutes : '0'+minutes;

		$this.find('.box_time[data-hour="hora"] > .time').html(hours);
		$this.find('.box_time[data-hour="minuto"] > .time').html(minutes);
		$this.find('.box_time[data-hour="segundos"] > .time').html(seconds);

		// $this.append(hours + ':' + minutes + ':' + seconds);
		
		if (distance < 0) {
			$this.find('.box_time[data-hour="hora"] > .time').html('00');
			$this.find('.box_time[data-hour="minuto"] > .time').html('00');
			$this.find('.box_time[data-hour="segundos"] > .time').html('00');
		}
	}, 1000);
})

$('#spinner').on( "spinstop", function(e){
	var cant 	= $(this).val();
	var id 	= $(this).parent().parent().attr('rel');

	$('.precio-ficha').load('ajax/precio_ficha.php?cant='+cant+'&id='+id);

	//console.log('cant: ' + cant + ' id: ' + id);
});

var popup = setInterval(IsVisible, 3000);

function IsVisible(){
	if ($('.popup-news').is(':visible')){
		$(".bg-popup").click(function() {
			var nombre_popup = $('.bg-popup').attr('data-cliente');
			$('.bg-popup').fadeOut();
		    $('.popup-news').fadeOut();
		    Cookies.set(nombre_popup,'Newsletter (No Suscrito)',{expires: 14});
		});
	}
	clearInterval(popup);
}

$('.popup-news').click(function (e) {
    e.stopPropagation();
});
$(".popup-news .contenido > .close").on('click', function(e){
	var nombre_popup = $('.bg-popup').attr('data-cliente');
	Cookies.set(nombre_popup,'Newsletter (No Suscrito)',{expires: 14});
	$('.bg-popup').fadeOut();
    $('.popup-news').fadeOut();
})

$('#suscribe-newsletter').on('click', function(e){

	e.preventDefault();
	var nombre = $('#nombre-suscribe').val();
	var email = $('#email-suscribe').val();
	var nombre_popup = $('.bg-popup').attr('data-cliente');

	if (nombre.trim() != '' && email.trim() != '') {
		$.ajax({
			url: 'ajax/popup.php',
			type: 'post',
			data: {nombre: nombre, email: email},
			beforeSend: function(){
				$('#suscribe-newsletter').html('ESPERE POR FAVOR...');
			},
			success: function(res){
				Cookies.set(nombre_popup,'Newsletter Suscrito',{expires: 90});
				$('#suscribe-newsletter').html('SUSCRIBIRME');
				$('.bg-popup').fadeOut();
	    		$('.popup-news').fadeOut();
	    		swal('','Gracias por suscribirte.','success');
			}
		})
	}else{
		swal('','Debe completar todos los campos', 'error');
	}
})

$('.acordeon_ficha .box .title').on('click', function(){

	var body = $(this).parent().find('.body');	

	$('.acordeon_ficha .box .body').slideUp();

	if (body.is(':visible')) {
		body.slideUp();
	}else{
		body.slideDown();
	}

})

$('.button_view').on('click', function(){

	var content = $(this).parent().parent().parent().find('.content-detalles');

	if (!content.is(':visible')) {
		content.slideDown();
		$(this).html('Ocultar');
	}else{
		content.slideUp();
		$(this).html('Ver');
	}

})


$('.content_tabs_carro .tab_carro').on('click', function(){

	$this = $(this);

	if ($this.attr('data-op') == 1) {
		$this.removeClass('disabled_left');
		$('#retiroClick').attr('data-active', false);
		$('#despachoClick').attr('data-active', true);
		$('.content_tabs_carro .tab_carro[data-op="2"]').addClass('disabled_right');
		$('.box_retiro_c').fadeOut(200, function(){
			$('.box_despacho_c').fadeIn(200);
			$('.direcciones_ajx').fadeIn(200);

			if ($('.actions_row_principal').attr('data-id') == undefined) {
				var comuna_id = $('#comuna_checkout').val();
				$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna_id);
				$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna_id);
				$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna_id);
			}else{
				var comuna_id = $('.actions_row_principal').attr('data-id');
				$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna_id);
				$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna_id);
				$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna_id);
			}
		});

		$('#despacho_retiro').val(1);

		

	}else{
		$this.removeClass('disabled_right');
		$('#despachoClick').attr('data-active', false);
		$('#retiroClick').attr('data-active', true);
		$('.content_tabs_carro .tab_carro[data-op="1"]').addClass('disabled_left');

		$('.box_despacho_c').fadeOut(200, function(){
			$('.box_retiro_c').fadeIn(200);
			$('.direcciones_ajx').fadeOut(200);
		});

		$('#despacho_retiro').val(2);

		$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna=999');
		$('.ajx_despacho').load('ajax/box_despacho.php?comuna=999');
		$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna=0');

	}

})

function selectDirChk(comuna_id, direccion_id, ts){

	$.ajax({
		url: 'ajax/cambiarPrincipal.php',
		type: 'post',
		data: { id: direccion_id }
	}).done(function(res){

		$(ts).parent().fadeOut('fast', function(){
			$('.datos_ajx').load(" .content_datos");
			$('.direcciones_ajx').load(" .content_direcciones");

			$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna_id);
			$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna_id);
			$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna_id);
		});
		

	}).fail(function(res){
		console.log(res);
	})

}

$('.btnValidar').on('click', function(){
	var email = $('.emailIdent').val();

	if (email.trim() != '') {
		if (validarEmail(email)) {
			$('.btnValidar').html('<i class="far fa-check-circle" style="font-size:18px;"></i>');
			$('.emailIdent').attr('data-valid', true);
			$('.boton_pago a').html('<span>Comprar productos</span>');
			$('.boton_pago a').addClass('btnActivoPago');
			$('.content_tabs_carro .disabled_checkout').fadeOut();
			$('.content_factura .disabled_checkout').fadeOut();
			$('.box_ident .disabled_checkout').fadeIn();
		}else{
			swal('', 'Correo ingresado incorrecto', 'error');
		}
	}else{
		swal('', 'Debes ingresar una dirección', 'error');
	}

})

$('.boton_pago a').on('click', function(){

	var email 			= $('.emailIdent'),
		nombre 			= $('#nombre_checkout'),
		region 			= $('#region_checkout'),
		comuna 			= $('#comuna_checkout'),
		calle 			= $('#calle_checkout'),
		numero 			= $('#numero_cheackout'),
		sucursal 		= $('#sucursal_checkout'),
		quien_retira 	= $('#quien_retira'),
		rut_retiro 		= $('#rut_retiro'),
		telefono		= $('#telefono_checkout'),
		factura 		= $('input[name="factura"]').val(),
		nombre_r		= $('#nombrer_checkout');

	var terminos = $('#terminos');

	var despacho = $('#despachoClick').attr('data-active'),
		retiro = $('#retiroClick').attr('data-active');

	if (despacho == "true") {

		if (email.val().trim != '' && email.attr('data-valid')) {

			if (nombre.val().trim() != '' && region.val() != 0 && comuna.val() != 0 && calle.val().trim() != '' && numero.val().trim() != '') {

				if (factura == 0) {

					if (telefono.val().trim() != '') {
						var re = /^([0-9]){8,12}$/;

						if (re.test(telefono.val())) {
							if (terminos.is(':checked')) {
								$('#checkout').attr('data-valid', true);
								$('#checkout').submit();
							}else{
								swal('', 'Debes aceptar los términos y condiciones', 'warning');
								$('#terminos').css('border', '1px solid red');
								$('.lblTerminos').css('color', 'red');
							}
						}else{
							swal('', 'Número ingresado incorrecto', 'error');
						}

					}else{
						if (terminos.is(':checked')) {
							$('#checkout').attr('data-valid', true);
							$('#checkout').submit();
						}else{
							swal('', 'Debes aceptar los términos y condiciones', 'warning');
							$('#terminos').css('border', '1px solid red');
							$('.lblTerminos').css('color', 'red');
						}
					}

				}else{

					var giro 				= $('#giro_cheackout').val(),
						razon_social 		= $('#razon_social_cheackout').val(),
						rut_empresa 		= $('#rut_empresa').val(),
						direccion_factura 	= $('#direccion_empresa').val(),
						nombre_factura 		= $('#nombre_factura').val(),
						telefono_factura 	= $('#telefono_factura').val();

					if (giro.trim() != '' && razon_social.trim() != '' && rut_empresa.trim() != '' && direccion_factura.trim() != '' && nombre_factura.trim() != '' && telefono_factura.trim() != '') {

						if (telefono.val().trim() != '') {
							var re = /^([0-9]){8,12}$/;

							if (re.test(telefono.val())) {
								if (terminos.is(':checked')) {
									$('#checkout').attr('data-valid', true);
									$('#checkout').submit();
								}else{
									swal('', 'Debes aceptar los términos y condiciones', 'warning');
									$('#terminos').css('border', '1px solid red');
									$('.lblTerminos').css('color', 'red');
								}
							}else{
								swal('', 'Número ingresado incorrecto', 'error');
							}

						}else{
							if (terminos.is(':checked')) {
								$('#checkout').attr('data-valid', true);
								$('#checkout').submit();
							}else{
								swal('', 'Debes aceptar los términos y condiciones', 'warning');
								$('#terminos').css('border', '1px solid red');
								$('.lblTerminos').css('color', 'red');
							}
						}

					}else{
						swal('', 'Debes completar los campos obligatorios', 'error');
					}

				}
				
				
			}else{
				swal('', 'Debes completar los campos obligatorios', 'error');
			}

		}else{

			swal('', 'Error al validar el correo', 'error');

		}

	}else if(retiro == "true"){
		if (sucursal.val() != 0 && quien_retira.val().trim() != '' && rut_retiro.val().trim() != '' && nombre_r.val().trim() != '') {

			if (factura == 0) {
				if (terminos.is(':checked')) {
					$('#checkout').attr('data-valid', true);
					$('#checkout').submit();
				}else{
					swal('', 'Debes aceptar los términos y condiciones', 'warning');
					$('#terminos').css('border', '1px solid red');
					$('.lblTerminos').css('color', 'red');
				}
			}else{
				var giro = $('#giro_cheackout').val(),
					razon_social 		= $('#razon_social_cheackout').val(),
					rut_empresa 		= $('#rut_empresa').val(),
					direccion_factura 	= $('#direccion_empresa').val(),
					nombre_factura 		= $('#nombre_factura').val(),
					telefono_factura 	= $('#telefono_factura').val();

				if (giro.trim() != '' && razon_social.trim() != '' && rut_empresa.trim() != '' && direccion_factura.trim() != '' && nombre_factura.trim() != '' && telefono_factura.trim() != '') {
					if (terminos.is(':checked')) {
						$('#checkout').attr('data-valid', true);
						$('#checkout').submit();
					}else{
						swal('', 'Debes aceptar los términos y condiciones', 'warning');
						$('#terminos').css('border', '1px solid red');
						$('.lblTerminos').css('color', 'red');
					}		
				}else{
					swal('', 'Debes completar los campos obligatorios', 'error');
				}

			}

		}else{
			swal('', 'Debes completar todos los campos', 'error');
		}
	}

})

$('.boton_pago_session').on('click', function(){

	var principal = $('.direccion_prl').length, // Para ver si hay una principal agregada y no validad los radio
		terminos = $('#terminos'),
		despacho = $('#despachoClick').attr('data-active'),
		retiro = $('#retiroClick').attr('data-active'),
		chkDirecciones = $('input[name="row_select_dir"]:checked').val(),
		factura = $('input[name="factura"]').val();

	if ( despacho == "true" ) {

		if (factura == 0) {

			if ( principal == 1 ) {
				if ( terminos.is(':checked') ) {
					$('#checkout').attr('data-valid', true);
					$('#checkout').submit();
				}else{
					swal('', 'Debes aceptar los términos y condiciones', 'warning');
					$('#terminos').css('border', '1px solid red');
					$('.lblTerminos').css('color', 'red');
				}
			}else{
				if (chkDirecciones != undefined) {
					if ( terminos.is(':checked') ) {
						$('#checkout').attr('data-valid', true);
						$('#checkout').submit();
					}else{
						swal('', 'Debes aceptar los términos y condiciones', 'warning');
						$('#terminos').css('border', '1px solid red');
						$('.lblTerminos').css('color', 'red');
					}
				}else{
					swal('', 'Debes seleccionar una dirección', 'error');
				}
			}

		}else{

			var giro = $('#giro_cheackout').val(),
				razon_social 		= $('#razon_social_cheackout').val(),
				rut_empresa 		= $('#rut_empresa').val(),
				direccion_factura 	= $('#direccion_empresa').val(),
				nombre_factura 		= $('#nombre_factura').val(),
				telefono_factura 	= $('#telefono_factura').val();

			if (giro.trim() != '' && razon_social.trim() != '' && rut_empresa.trim() != '' && direccion_factura.trim() != '' && nombre_factura.trim() != '' && telefono_factura.trim() != '') {

				if ( principal == 1 ) {
					if ( terminos.is(':checked') ) {
						$('#checkout').attr('data-valid', true);
						$('#checkout').submit();
					}else{
						swal('', 'Debes aceptar los términos y condiciones', 'warning');
						$('#terminos').css('border', '1px solid red');
						$('.lblTerminos').css('color', 'red');
					}
				}else{
					if (chkDirecciones != undefined) {
						if ( terminos.is(':checked') ) {
							$('#checkout').attr('data-valid', true);
							$('#checkout').submit();
						}else{
							swal('', 'Debes aceptar los términos y condiciones', 'warning');
							$('#terminos').css('border', '1px solid red');
							$('.lblTerminos').css('color', 'red');
						}
					}else{
						swal('', 'Debes seleccionar una dirección', 'error');
					}
				}

			}else{
				swal('', 'Debes completar todos los campos', 'error');
			}

		}
		
	}else if(retiro == "true"){
		var sucursal = $('#sucursal_checkout').val(),
			quien_retira = $('#quien_retira').val(),
			rut_retiro = $('#rut_retiro').val();

		if (sucursal != 0 && quien_retira.trim() != '' && rut_retiro.trim() != '') {
			if (factura == 0) {
				if ( terminos.is(':checked') ) {
					$('#checkout').attr('data-valid', true);
					$('#checkout').submit();
				}else{
					swal('', 'Debes aceptar los términos y condiciones', 'warning');
					$('#terminos').css('border', '1px solid red');
					$('.lblTerminos').css('color', 'red');
				}
			}else{

				var giro = $('#giro_cheackout').val(),
					razon_social = $('#razon_social_cheackout').val(),
					rut_empresa = $('#rut_empresa').val(),
					direccion_factura 	= $('#direccion_empresa').val(),
					nombre_factura 		= $('#nombre_factura').val(),
					telefono_factura 	= $('#telefono_factura').val();

				if (giro.trim() != '' && razon_social.trim() != '' && rut_empresa.trim() != '' && direccion_factura.trim() != '' && nombre_factura.trim() != '' && telefono_factura.trim() != '') {
					if ( terminos.is(':checked') ) {
						$('#checkout').attr('data-valid', true);
						$('#checkout').submit();
					}else{
						swal('', 'Debes aceptar los términos y condiciones', 'warning');
						$('#terminos').css('border', '1px solid red');
						$('.lblTerminos').css('color', 'red');
					}
				}else{
					swal('', 'Debes completar todos los campos', 'error');
				}

			}
			
		}else{
			swal('', 'Debes completar todos los campos', 'error');
		}

	}

})

$('#region_checkout').on('change', function(){
	var id = $(this).val();

	$.ajax({
		url: 'ajax/get_comunas.php',
		type: 'post',
		dataType: 'json',
		data: {id: id},
		beforeSend: function(){
			$('.cont_loading').fadeIn(100);
		}
	}).done(function(res){
		$('.cont_loading').fadeOut(100);
		$('#comuna_checkout').html('<option value="0">Seleccionar comuna</option>');

		if (res['status'] == 'success') {

			for (var i = 0; i < res['comunas'].length; i++) {

				$('#comuna_checkout').append('<option value="'+res["comunas"][i]["id"]+'">'+res["comunas"][i]["nombre"]+'</option>');
			
			}

		}

	}).fail(function(res){
		$('.cont_loading').fadeOut(100);
		console.log(res);
	})

	$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna=0');
	$('.ajx_despacho').load('ajax/box_despacho.php?comuna=0');
	$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna=0');
})

$('#comuna_checkout').on('change', function(){

	var id = $(this).val();

	$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+id);
	$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+id);
	$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+id);

})

$('#region_dir').on('change', function(){
	var id = $(this).val();

	$.ajax({
		url: 'ajax/get_comunas.php',
		type: 'post',
		dataType: 'json',
		data: {id: id},
		beforeSend: function(){
			$('.cont_loading').fadeIn(100);
		}
	}).done(function(res){
		$('.cont_loading').fadeOut(100);
		$('#comuna_dir').html('<option value="0">Seleccionar comuna</option>');

		if (res['status'] == 'success') {

			for (var i = 0; i < res['comunas'].length; i++) {

				$('#comuna_dir').append('<option value="'+res["comunas"][i]["id"]+'">'+res["comunas"][i]["nombre"]+'</option>');
			
			}

		}

	}).fail(function(res){
		$('.cont_loading').fadeOut(100);
		console.log(res);
	})
})

function addFade(){
	$('.bg_checkout').fadeIn();
	$('.box_agregar_direccion').fadeIn();

	$('.btnAdd').css('display', 'block');
	$('.btnEdit').css('display', 'none');
}

function cancelAdd(){
	$('.bg_checkout').fadeOut();
	$('.box_agregar_direccion').fadeOut();

	resetear_campos();
}

$('.bg_checkout').on('click', function(){
	$('.bg_checkout').fadeOut();
	$('.box_agregar_direccion').fadeOut();

	resetear_campos();
})

function delDir(id, ts){
	$('.bg_popup').fadeIn();
	$('.popup_acciones').fadeIn();
	$('.confirmPop').attr('data-id', id);
}

function editDirOne(id, principal){
	$('.bg_checkout').fadeIn();
	$('.box_agregar_direccion').fadeIn();

	$('.btnAdd').css('display', 'none');
	$('.btnEdit').css('display', 'block');

	$.ajax({
		url: 'ajax/getDireccion.php',
		type: 'post',
		dataType: 'json',
		data: {id: id}
	})
	.done(function(res) {
		// console.log(res);

		$('#comuna_dir').html('<option value="0">Seleccionar comuna</option>');
		for (var i = 0; i < res['comunas'].length; i++) {
			$('#comuna_dir').append('<option value="'+res["comunas"][i][0]+'">'+res["comunas"][i][1]+'</option>');
		}

		$('#region_dir').val( res['direccion'][0][2] );
		$('#comuna_dir').val( res['direccion'][0][3] );
		$('#calle_dir').val( res['direccion'][0][5] );
		$('#numero_dir').val( res['direccion'][0][4] );
		$('#piso_dir').val( res['direccion'][0][6] );
		$('#alias_dir').val( res['direccion'][0][1] );

		if (res['direccion'][0][7] == 1) {
			$('#chkPrincipal').attr('checked', true);
		}else{
			$('#chkPrincipal').attr('checked', false);
		}
		
	})
	.fail(function(res) {
		console.log(res);
	});
	

	$('.btnEdit').attr('data-id', id);
}

$('.bg_popup, .cancelPop').on('click', function(){
	$('.bg_popup').fadeOut();
	$('.popup_acciones').fadeOut();
})

$('.confirmPop').on('click', function(){
	var data_id = $(this).attr('data-id'),
		where = $(this).attr('data-where');

	$.ajax({
		url: 'ajax/eliminarDireccion.php',
		type: 'post',
		dataType: 'json',
		data: {id: data_id},
	})
	.done(function(res) {

		if (where == 'cart') {
			if(res['status'] == 'success'){
				$('.datos_ajx').load(" .content_datos");
				$('.direcciones_ajx').load(" .content_direcciones");
			}

			$('.bg_popup').fadeOut();
			$('.popup_acciones').fadeOut();
			$('.confirmPop').attr('data-id', 0);


			swal('', res["message"], res["status"]);
		}else{
			if (res['status'] == 'success') {
				location.reload();
			}else{
				swal('', res["message"], res["status"]);
			}
		}

	})
	.fail(function(res) {
		console.log(res);
	});
	
})

function addDir(){

	var region 	= $('#region_dir').val(),
		comuna 	= $('#comuna_dir').val(),
		calle 	= $('#calle_dir').val(),
		numero  = $('#numero_dir').val(),
		piso = $('#piso_dir').val(),
		alias = $('#alias_dir').val()
		principal = $('#chkPrincipal'),
		comuna_id = $('.actions_row_principal').attr('data-id');

	var principal_chk = $('#chkPrincipal').is(':checked');

	if (region != 0 && comuna != 0 && calle.trim() != '' && numero.trim() != '' && alias.trim() != '') {
		$.ajax({
			url: 'ajax/agregarDireccion.php',
			type: 'POST',
			dataType: 'json',
			data: {region: region, comuna: comuna, calle: calle, numero: numero, piso: piso, alias: alias, principal: principal_chk}
		}).done(function(res) {

			if (res['status'] == 'success') {
				$('.datos_ajx').load(" .content_datos", function(){
					if (principal_chk) {
						$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna);
						$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna);
						$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna);
					}else{
						if ($('.actions_row_principal').attr('data-id') != undefined) {
							$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna_id);
							$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna_id);
							$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna_id);
						}else{
							$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna=0');
							$('.ajx_despacho').load('ajax/box_despacho.php?comuna=0');
							$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna=0');
						}
					}
				});
				$('.direcciones_ajx').load(" .content_direcciones");
			}

			$('.bg_checkout').fadeOut();
			$('.box_agregar_direccion').fadeOut();

			swal('', res["message"], res["status"]);

			resetear_campos();

		}).fail(function(res) {
			console.log(res);
		});
	}else{
		swal('', 'Debes completar los campos requeridos', 'error');
	}
}

function editDir(){
	var region 	= $('#region_dir').val(),
		comuna 	= $('#comuna_dir').val(),
		calle 	= $('#calle_dir').val(),
		numero  = $('#numero_dir').val(),
		piso = $('#piso_dir').val(),
		alias = $('#alias_dir').val()
		principal = $('#chkPrincipal'),
		id = $('.btnEdit').attr('data-id'),
		comuna_id = $('.actions_row_principal').attr('data-id');

	var principal_chk = $('#chkPrincipal').is(':checked');

	if (region != 0 && comuna != 0 && calle.trim() != '' && numero.trim() != '' && alias.trim() != '') {
		$.ajax({
			url: 'ajax/editarDireccion.php',
			type: 'POST',
			dataType: 'json',
			data: {region: region, comuna: comuna, calle: calle, numero: numero, piso: piso, alias: alias, principal: principal_chk, id: id}
		}).done(function(res) {

			if (res['status'] == 'success') {
				$('.datos_ajx').load(" .content_datos", function(){
					if (principal_chk) {
						$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna);
						$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna);
						$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna);
					}else{
						if ($('.actions_row_principal').attr('data-id') != undefined) {
							$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna='+comuna_id);
							$('.ajx_despacho').load('ajax/box_despacho.php?comuna='+comuna_id);
							$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna='+comuna_id);
						}else{
							$('.contCartCompraRapida').load('ajax/resumenCompra.php?comuna=0');
							$('.ajx_despacho').load('ajax/box_despacho.php?comuna=0');
							$('.ajx_precio_xs').load('ajax/precio_carro.php?comuna=0');
						}
					}
				});

				$('.direcciones_ajx').load(" .content_direcciones");
			}

			$('.bg_checkout').fadeOut();
			$('.box_agregar_direccion').fadeOut();

			swal('', res["message"], res["status"]);

			resetear_campos();

		}).fail(function(res) {
			console.log(res);
		});
	}else{
		swal('', 'Debes completar los campos requeridos', 'error');
	}
}

function resetear_campos(){
	$('#region_dir').val(0);
	$('#comuna_dir').val(0);
	$('#calle_dir').val("");
	$('#numero_dir').val("");
	$('#piso_dir').val("");
	$('#alias_dir').val("");
	$('#chkPrincipal').attr('checked', false);
}

function validarEmail(valor) {
	emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	if (emailRegex.test(valor)){
		return true;
	}else {
		return false;
	}
}
function validarFono(valor){
	var re = /^([0-9]){8,12}$/;
	
	if (re.test(valor)) {
		return true;
	}else{
		return false;
	}
}

$('.cambiar_password').on('click', function(){
	$('.bg_password').fadeIn();
	$('.content_password').fadeIn();
})

$('.bg_password').on('click', function(){
	$('.bg_password').fadeOut();
	$('.content_password').fadeOut();
})

$('.btnUpdatePasswordPost').on('click', function(){

	var actual = $('input[name="actual_password"]').val(),
		nueva = $('input[name="nueva_password"]').val(),
		nueva_re = $('input[name="r_nueva_password"]').val();

	if (actual.trim() != '' && nueva.trim() != '' && nueva_re.trim() != '') {

		if (nueva.trim() == nueva_re.trim()) {

			$.ajax({
				url: 'ajax/cambiarPassword.php',
				type: 'post',
				data: { actual: actual, nueva: nueva, nueva_re: nueva_re }
			}).done(function(res){

				if (res == 1) {

					$('.bg_password').fadeOut('fast');
					$('.content_password').fadeOut('fast', function(){
						swal('', 'Contraseña cambiada correctamente', 'success');
					})

				}else if(res == 2){ // Clave actual mala
					swal('', 'Constraseña actual ingresada no coincide', 'error');
				}else if(res == 3){ // Nuevas no coinciden
					swal('', 'Las nuevas contraseñas no coinciden', 'error');
				}else if(res == 4){ // No logueado
					swal('', 'Usuario no logueado', 'error');
				}else if(res == 0){ // Error update
					swal('', 'Hubo un error al actualizar la contraseña', 'error');
				}

			}).fail(function(res){
				console.log(res);
			})

		}else{
			swal('', 'Las contraseñas no coinciden', 'error');
		}

	}else{
		swal('', 'Debes completar todos los campos', 'error');
	}

})

$('.box_buttons .update').on('click', function(){
	var nombre = $('input[name="nombre"]').val(),
		email = $('input[name="email"]').val(),
		telefono = $('input[name="telefono"]').val();

	if (nombre.trim() != '' && email.trim() != '' && telefono.trim() != '') {

		if (validarEmail(email.trim())) {

			var re = /^([0-9]){9,12}$/;

			if (re.test(telefono)) {

				$.ajax({
					url: 'ajax/actualizarDatos.php',
					type: 'post',
					dataType: 'json',
					data: { nombre: nombre, email: email, telefono: telefono }
				}).done(function(res){

					if (res['status'] == 'success') {
						swal('', 'Cuenta actualizada correctamente', 'success');
					}else{
						swal('', 'Error al actualizar la cuenta', 'error');
					}

				}).fail(function(res){
					console.log(res);
				});

			}else{
				swal('', 'Teléfono ingresado es incorrecto', 'error');
			}

		}else{
			swal('', 'Correo ingresado es incorrecto', 'error');
		}

	}else{
		swal('', 'Debes completar todos los campos', 'error');
	}
})

$('.delDir').on('click', function(){

	var id = $(this).attr('data-id');

	$('.bg_popup').fadeIn();
	$('.popup_acciones').fadeIn();
	$('.confirmPop').attr('data-id', id);
})

$('.btnAddDash').on('click', function(){
	var region 	= $('#region_dir').val(),
		comuna 	= $('#comuna_dir').val(),
		calle 	= $('#calle_dir').val(),
		numero  = $('#numero_dir').val(),
		piso = $('#piso_dir').val(),
		alias = $('#alias_dir').val()
		principal = $('#chkPrincipal');

	var principal_chk = $('#chkPrincipal').is(':checked');

	if (region != 0 && comuna != 0 && calle.trim() != '' && numero.trim() != '' && alias.trim() != '') {
		$.ajax({
			url: 'ajax/agregarDireccion.php',
			type: 'POST',
			dataType: 'json',
			data: {region: region, comuna: comuna, calle: calle, numero: numero, piso: piso, alias: alias, principal: principal_chk}
		}).done(function(res) {

			if (res['status'] == 'success') {
				location.href = "cuenta/mis-direcciones";
			}else{
				swal('', 'Error al agregar la dirección', 'error');
			}

		}).fail(function(res) {
			console.log(res);
		});
	}else{
		swal('', 'Debes completar los campos requeridos', 'error');
	}
})

$('.btnEditDash').on('click', function(){
	var region 	= $('#region_dir').val(),
		comuna 	= $('#comuna_dir').val(),
		calle 	= $('#calle_dir').val(),
		numero  = $('#numero_dir').val(),
		piso = $('#piso_dir').val(),
		alias = $('#alias_dir').val()
		principal = $('#chkPrincipal'),
		id = $(this).attr('data-id');

	var principal_chk = $('#chkPrincipal').is(':checked');

	if (region != 0 && comuna != 0 && calle.trim() != '' && numero.trim() != '' && alias.trim() != '') {
		$.ajax({
			url: 'ajax/editarDireccion.php',
			type: 'POST',
			dataType: 'json',
			data: {region: region, comuna: comuna, calle: calle, numero: numero, piso: piso, alias: alias, principal: principal_chk, id: id}
		}).done(function(res) {

			swal('', res["message"], res["status"]);

		}).fail(function(res) {
			console.log(res);
		});
	}else{
		swal('', 'Debes completar los campos requeridos', 'error');
	}
})

$('.wishlist').on('click', function(){
	var id = $(this).attr('data-id'),
		action = $(this).attr('data-action');

	$.ajax({
		url: 'ajax/wishlist.php',
		type: 'post',
		data: {producto_id: id, action: action},
	})
	.done(function(res) {
		if (res == 1 || res == 4) {
			location.reload();
		}else if(res == 2){
			swal('', 'Debes ingresar a tu cuenta para agregar el producto a la lista de deseos.', 'error');
		}else if(res == 3){
			swal('', 'Error al eliminar el producto a la lista de deseados', 'error');
		}else{
			swal('', 'Error al agregar el producto a la lista de deseados', 'error');
		}
	})
	.fail(function(res) {
		console.log(res);
	});
	
})

function moveToCart(id, where){
	var cantidad = 1,
		cantidadActual = parseInt($('.cantidad_carro').html());

	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_validar_stock.php',
		data: {id:id, qty:cantidad},
		cache: false,
		success:function(resp) {
			if(resp >= 0){
				$.ajax({
					type: 'GET',
					url: 'tienda/addCarro.php',
					data: {id:id, action:"add", qty:cantidad},
					cache: false,
					success:function() {
						
						$.ajax({
							url: 'ajax/eliminarFavorito.php',
							type: 'post',
							data: {producto_id: id, where: where}
						})
						.done(function(res) {

							if (res == 1) {
								if (where == 'dash') {
									location.reload();
								}

								$('.cantidad_carro').html(cantidadActual+cantidad);
							}else{
								swal('', 'Error al mover producto al carro', 'error');
							}
						})
						.fail(function(res) {
							console.log(res);
						});
						

					}
				});
			}else{
				swal({
					  type: 'error',
					  title: 'Stock insuficiente',
					  text: 'No fue posible agregar tu producto por falta de stock'
					})
			};
		}
	});

}

$('.eliminarGuardado').on('click', function(){
	var producto_id = $(this).attr('data-id');

	$.ajax({
		url: 'ajax/eliminarFavorito.php',
		type: 'post',
		data: {producto_id: producto_id, where: 'guardados'}
	})
	.done(function(res) {

		if (res == 1) {
			location.reload();
		}else{
			swal('', 'Error al mover producto al carro', 'error');
		}

	})
	.fail(function(res) {
		console.log(res);
	});

})

$('.volver_a_comprar').on('click', function(){
	var id = $(this).attr('data-id'),
		cantidadActual = parseInt($('.cantidad_carro').html());
	$.ajax({
		type: 'POST',
		url: 'ajax/ajax_validar_stock.php',
		data: {id:id, qty:1},
		cache: false,
		success:function(resp) {
			if(resp >= 0){
				$.ajax({
					type: 'GET',
					url: 'tienda/addCarro.php',
					data: {id:id, action:"add", qty:1},
					cache: false,
					success:function() {
						
						swal('', 'Producto agregado al carro', 'success');
						$('.cantidad_carro').html(cantidadActual+1);

					}
				});
			}else{
				swal({
					  type: 'error',
					  title: 'Stock insuficiente',
					  text: 'No fue posible agregar tu producto por falta de stock'
					})
			};
		}
	});
})

$('.cantidad_ficha').on('click', function(){

	$(this).parent().parent().find('.box_select_stock').toggle();

})

$('.btnFactura').on('click', function(){
	$('.form_factura').fadeToggle();

	var hidden = $('input[name="factura"]').val();

	if (hidden == 0) {
		$('input[name="factura"]').val(1);
	}else{
		$('input[name="factura"]').val(0);
	}

})

function openSelect(obj){
	$(obj).parent().parent().find('.box_select_stock').toggle();
}

$('.box_main_qty .row_stock').on('click', function(){

	var data_value  = $(this).attr('data-val'),
		id 			= $('.box_qty').attr('data-id');

	if (data_value != 'add') {
		$('.cantidad_ficha').val(data_value);
		$('.box_price_ajx').load('ajax/precio_ficha.php?cant='+data_value+'&id='+id);
	}else{
		// Más de 10 productos
		$('.cantidad_ficha').val('');
		$('.cantidad_ficha').removeAttr('readonly');
		$('.cantidad_ficha').focus();
	}

	$('.box_select_stock').toggle();

})

$('.box_main_qty .row_stock_ficha').on('click', function(){

	var data_value  = $(this).attr('data-val'),
		id 			= $('.box_qty').attr('data-id');

	if (data_value != 'add') {
		$('.cantidad_ficha').val(data_value);
		$('.box_price_ajx').load('ajax/precio_ficha.php?cant='+data_value+'&id='+id);
	}else{
		// Más de 10 productos
		$('.cantidad_ficha').val('');
		$('.cantidad_ficha').removeAttr('readonly');
		$('.cantidad_ficha').focus();
		$('.cantidad_ficha').attr('onblur', 'blurSelectFicha(this)');
	}

	$('.box_select_stock').toggle();

})

function blurSelectFicha(obj){
	var valor  = $(obj).val(),
		id 	= $(obj).parent().attr('data-id');

	if (valor.trim() != '') {
		$.ajax({
			url: 'ajax/ajax_validar_stock.php',
			type: 'post',
			data: {id: id, qty: valor}
		}).done(function(res){
			if (res > 0) {
				$('.cantidad_ficha').val(valor);
				$('.box_price_ajx').load('ajax/precio_ficha.php?cant='+valor+'&id='+id);
			}else{
				swal('', 'Stock insuficiente', 'error');
			}
		}).fail(function(res){
			console.log(res);
		})

		$(obj).removeAttr('onblur');
	}

}

function changeSelect(obj){
	var data_value  = $(obj).attr('data-val'),
		id 			= $(obj).parent().parent().attr('data-id');

	if (data_value != 'add') {
		$(obj).parent().parent().find('.cantidadProducto').val(data_value);

		$.ajax({
			type: "GET",
			url: "tienda/addCarro.php",
			data: {id:id, action:"delete"},
			cache: false,
			 success:function() {
				
				$.ajax({
					type: 'GET',
					url: 'tienda/addCarro.php',
					data: {id:id, action:"add", qty:data_value},
					cache: false,
					 success:function(resp) {

					 	$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
						$("#contFilasCarro").load("tienda/showCart.php");
						$("#contProductosGuardados").load("tienda/saveForLater.php");

					}
				});

			},
			error: function() {
				$(".cont_loading").fadeOut(100);
			}
		});

	}else{
		// Más de 10 productos
		$(obj).parent().parent().find('.cantidadProducto').val('');
		$(obj).parent().parent().find('.cantidadProducto').removeAttr('readonly');
		$(obj).parent().parent().find('.cantidadProducto').attr('onblur', 'blurSelect(this)');
		$(obj).parent().parent().find('.cantidadProducto').focus();
	}

	$(obj).parent().toggle();

}

function blurSelect(obj){
	var valor  = $(obj).val(),
		id 	= $(obj).parent().attr('data-id');

	if (valor.trim() != '') {
		$.ajax({
			url: 'ajax/ajax_validar_stock.php',
			type: 'post',
			data: {id: id, qty: valor}
		}).done(function(res){

			if (res > 0) {

				$.ajax({
					type: "GET",
					url: "tienda/addCarro.php",
					data: {id:id, action:"delete"},
					cache: false,
					success:function() {
						
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id, action:"add", qty:valor},
							cache: false,
							 success:function(resp) {

							 	$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
								$("#contFilasCarro").load("tienda/showCart.php");
								$("#contProductosGuardados").load("tienda/saveForLater.php");

							}
						});

					},
					error: function() {
						$(".cont_loading").fadeOut(100);
					}
				});

				$(obj).removeAttr('onblur');

			}else{
				swal('', 'Stock insuficiente', 'error');
				$(obj).val('');
			}

		}).fail(function(res){
			console.log(res);
		})
	}

}

$('.box_filtro > .box_check .row').on('click', function(){

	var chk_cat = $('input[name="chk_cat"]:checked'),
		chk_marcas = $('input[name="chk_marcas"]:checked'),
		min_price = $('.min-filtro').attr('data-valor'),
		max_price = $('.max-filtro').attr('data-valor'),
		chk_fglobal = $('input[name="chk_fglobal"]:checked'),
		orden = $('.orderby').attr('data-order'),
		link = '',
		ref = $('.filtros_col').attr('data-ref');

	var str_cat = '?cat=',
		str_fglobal = '&filtros=',
		str_min = '&desde=',
		str_max = '&hasta=';

	if (ref == 'lineas') {
		str_marcas = '&marca='
		chk_marcas.each(function(index, el) {
			if (str_marcas == '&marca=') {
				str_marcas += $(this).val();
			}else{
				str_marcas += '-' + $(this).val();
			}
		});
	}else if(ref == 'cat'){
		str_marcas = '?marca=';

		chk_marcas.each(function(index, el) {
			if (str_marcas == '?marca=') {
				str_marcas += $(this).val();
			}else{
				str_marcas += '-' + $(this).val();
			}
		});
	}

	chk_cat.each(function(index, el) {
		if (str_cat == '?cat=') {
			str_cat += $(this).val();
		}else{
			str_cat += '-' + $(this).val();
		}
	});

	chk_fglobal.each(function(index, el) {
		if (str_fglobal == '&filtros=') {
			str_fglobal += $(this).val();
		}else{
			str_fglobal += '-' + $(this).val();
		}
	});

	var check_desde = getParameterByName('desde'),
		check_hasta = getParameterByName('hasta');
	
	if (check_desde) {
		if (check_desde != '') {
			if (parseInt(check_hasta) > 0) {

				str_min += min_price;
				str_max += max_price;

			}else{
				str_min = '&desde=0';
				str_max = '&hasta=0';
			}
		}else{
			str_min = '&desde=0';
			str_max = '&hasta=0';
		}
	}else{
		str_min = '&desde=0';
		str_max = '&hasta=0';
	}

	if (ref == 'lineas') {
		link = str_cat + str_marcas + str_fglobal + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'cat'){
		link = str_marcas + str_fglobal + str_min + str_max + '&orden=' + orden + '&page=1';
	}
	
	var	url_actual = window.location.pathname;

	location.href = url_actual + link;

})

$('.aplicado').on('click', function(){
	var action = $(this).attr('data-action'),
		ref = $('.filtros_col').attr('data-ref');

	var chk_cat = $('input[name="chk_cat"]:checked'),
		chk_marcas = $('input[name="chk_marcas"]:checked'),
		min_price = $('.min-filtro').attr('data-valor'),
		max_price = $('.max-filtro').attr('data-valor'),
		chk_fglobal = $('input[name="chk_fglobal"]:checked'),
		orden = $('.orderby').attr('data-order'),
		link = '',
		precio_max = $('.max-filtro').attr('data-info');

	var str_cat = '',
		str_marcas = '',
		str_fglobal = '',
		str_min = '&desde=',
		str_max = '&hasta=';

	chk_cat.each(function(index, el) {
		if (str_cat == '') {
			str_cat += $(this).val();
		}else{
			str_cat += '-' + $(this).val();
		}
	});

	chk_marcas.each(function(index, el) {
		if (str_marcas == '') {
			str_marcas += $(this).val();
		}else{
			str_marcas += '-' + $(this).val();
		}
	});

	chk_fglobal.each(function(index, el) {
		if (str_fglobal == '') {
			str_fglobal += $(this).val();
		}else{
			str_fglobal += '-' + $(this).val();
		}
	});

	if (precio_max > 0) {
		str_min += min_price;
		str_max += max_price;
	}else{
		str_min += "0";
		str_max += "0";
	}
	

	if (action != 'precio') {
		var id = $(this).attr('data-id');

		var params = getParameterByName(action),
			split = params.split('-');

		for (var i = 0; i < split.length; i++) {
			if (id == split[i]) {
				split.splice(i, 1);
			}
		}

		if (action == 'cat') {
			str_cat = split.join('-');
		}else if(action == 'marca'){
			str_marcas = split.join('-');
		}else if(action == 'filtros'){
			str_fglobal = split.join('-');
		}
	}else{

		str_min = 0;
		str_max = 0;

	}

	if (ref == 'lineas') {
		link = '?cat=' + str_cat + '&marca=' + str_marcas + '&filtros=' + str_fglobal + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'cat'){
		link = '?marca=' + str_marcas + '&filtros=' + str_fglobal + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'marcas'){
		link = '?cat=' + str_cat + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'busqueda'){
		var busqueda = getParameterByName('buscar');

		link = '?buscar=' + busqueda + '&cat=' + str_cat + '&marca=' + str_marcas + str_min + str_max + '&orden=' + orden + '&page=1';

	}
	
	var	url_actual = window.location.pathname;

	location.href = url_actual + link;

})

$('.aplicado_otros').on('click', function(){
	var action = $(this).attr('data-action'),
		ref = $('.filtros_col').attr('data-ref');

	var chk_cat = $('input[name="chk_cat"]:checked'),
		chk_marcas = $('input[name="chk_marcas"]:checked'),
		min_price = $('.min-filtro').attr('data-valor'),
		max_price = $('.max-filtro').attr('data-valor'),
		orden = $('.orderby').attr('data-order'),
		link = '',
		precio_max = $('.max-filtro').attr('data-info');

	var str_cat = '',
		str_marcas = '',
		str_min = '&desde=',
		str_max = '&hasta=';

	chk_cat.each(function(index, el) {
		if (str_cat == '') {
			str_cat += $(this).val();
		}else{
			str_cat += '-' + $(this).val();
		}
	});

	chk_marcas.each(function(index, el) {
		if (str_marcas == '') {
			str_marcas += $(this).val();
		}else{
			str_marcas += '-' + $(this).val();
		}
	});

	if (precio_max > 0) {
		str_min += min_price;
		str_max += max_price;
	}else{
		str_min += "0";
		str_max += "0";
	}
	

	if (action != 'precio') {
		var id = $(this).attr('data-id');

		var params = getParameterByName(action),
			split = params.split('-');

		for (var i = 0; i < split.length; i++) {
			if (id == split[i]) {
				split.splice(i, 1);
			}
		}

		if (action == 'cat') {
			str_cat = split.join('-');
		}else if(action == 'marca'){
			str_marcas = split.join('-');
		}else if(action == 'filtros'){
			str_fglobal = split.join('-');
		}
	}else{

		str_min = 0;
		str_max = 0;

	}

	if (ref == 'busqueda') {
		var busqueda = getParameterByName('buscar');
		link = '?buscar=' + busqueda + '&cat=' + str_cat + '&marca=' + str_marcas + '&desde=' + str_min + '&hasta=' + str_max + '&orden=' + orden + '&page=1';
	}else{
		link = '?cat=' + str_cat + '&marca=' + str_marcas + '&desde=' + str_min + '&hasta=' + str_max + '&orden=' + orden + '&page=1';
	}
	

	
	var	url_actual = window.location.pathname;

	location.href = url_actual + link;

})

$('.box_filtro > .box_check .row_otro').on('click', function(){

	var chk_cat = $('input[name="chk_cat"]:checked'),
		chk_marcas = $('input[name="chk_marcas"]:checked'),
		min_price = $('.min-filtro').attr('data-valor'),
		max_price = $('.max-filtro').attr('data-valor'),
		orden = $('.orderby').attr('data-order'),
		link = '',
		ref = $('.filtros_col').attr('data-ref');

	var str_cat = '?cat=',
		str_fglobal = '&filtros=',
		str_min = '&desde=',
		str_max = '&hasta=';

	if (ref == 'busqueda') {
		str_cat = '&cat=';
	}

	str_marcas = '&marca='
	chk_marcas.each(function(index, el) {
		if (str_marcas == '&marca=') {
			str_marcas += $(this).val();
		}else{
			str_marcas += '-' + $(this).val();
		}
	});

	if (ref == 'busqueda') {
		chk_cat.each(function(index, el) {
			if (str_cat == '&cat=') {
				str_cat += $(this).val();
			}else{
				str_cat += '-' + $(this).val();
			}
		});
	}else{
		chk_cat.each(function(index, el) {
			if (str_cat == '?cat=') {
				str_cat += $(this).val();
			}else{
				str_cat += '-' + $(this).val();
			}
		});
	}
	
	var check_desde = getParameterByName('desde'),
		check_hasta = getParameterByName('hasta');
	
	if (check_desde) {
		if (check_desde != '') {
			if (parseInt(check_hasta) > 0) {

				str_min += min_price;
				str_max += max_price;

			}else{
				str_min = '&desde=0';
				str_max = '&hasta=0';
			}
		}else{
			str_min = '&desde=0';
			str_max = '&hasta=0';
		}
	}else{
		str_min = '&desde=0';
		str_max = '&hasta=0';
	}

	if (ref == 'ofertas') {
		link = str_cat + str_marcas + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'marcas'){
		link = str_cat + str_min + str_max + '&orden=' + orden + '&page=1';
	}else if(ref == 'busqueda'){
		var busqueda = getParameterByName('buscar');

		link = '?buscar=' + busqueda + str_cat + str_marcas + str_min + str_max + '&orden=' + orden + '&page=1';

	}
	
	var	url_actual = window.location.pathname;

	console.log(url_actual + link);

	location.href = url_actual + link;

})

$('.btnRecuperarClave').on('click', function(){
	$('.bg_popup_login').fadeIn();
	$('.content_popup_login').fadeIn();
})

$('.bg_popup_login').on('click', function(){
	$('.bg_popup_login').fadeOut();
	$('.content_popup_login').fadeOut();
})

$('.restablecerPass').on('click', function(){
	var email = $('.emailRec').val();

	if (email.trim() != '') {
		if (validarEmail(email)) {
			$.ajax({
				url: 'ajax/recuperar_clave.php',
				type: 'post',
				data: { email: email }
			}).done(function(res){
				if (res == 1) {
					$('.bg_popup_login').fadeOut();
					$('.content_popup_login').fadeOut();
					swal('', 'Nueva clave enviada correctamente', 'success');
				}else if(res == 2){
					swal('', 'No fue posible enviar una nueva clave', 'error');
				}else if(res == 3){
					swal('', 'Usuario no registrado', 'error');
				}
			}).fail(function(res){
				console.log(res);
			})
		}else{
			swal('', 'Correo ingresado es incorrecto', 'error');
		}
	}else{
		swal('', 'Debes ingresar un correo', 'error');
	}

})

$('#formRegistroPagLogin').on('submit', function(){

	var nombre = $(this).find('input[name="nombre"]').val(),
		email = $(this).find('input[name="email-registro"]').val(),
		email_re = $(this).find('input[name="email-re"]').val(),
		password = $(this).find('input[name="password"]').val(),
		password_re = $(this).find('input[name="password-re"]').val();

	if (nombre.trim() != '' && email.trim() != '' && email_re.trim() != '' && password.trim() != '' && password_re.trim()) {

		if (validarEmail(email)) {
			if (email.trim() == email_re.trim()) {
				if (password.trim() == password_re.trim()) {
					$('#formRegistroPagLogin').submit();
				}else{
					swal('', 'Contraseñas ingresadas no coinciden', 'error');
				}
			}else{
				swal('', 'Correos ingresados no coinciden', 'error');
			}
		}else{
			swal('', 'Email ingresado no es correcto', 'error');
		}

	}else{
		swal('', 'Debes completar todos los campos', 'error');
	}

	return false;

})

$('.not-user').on('click', function(){
	swal('', 'Debes ingresar a tu cuenta para ver los productos guardados.', 'error');
})

$('.btn_menu').on('click', function(){
	$('#main_menu').fadeToggle();
})

$('.btnNext').on('click', function(){
	$(this).parent().parent().find('.submenu').fadeIn();
})

$('.title_submenu').on('click', function(){
	$('#main_menu').scrollTop(0);
	$('.submenu').fadeOut();

})

$('.btnXs i').on('click', function(){
	$(this).parent().parent().find('.list_footer').slideToggle();
})

$('.orderby .title-xs').on('click', function(){
	$('.bg-order').fadeIn();
	$('.orderby-xs').fadeIn(10);
	$('.orderby-xs').animate({
		'bottom': '0',
	}, 300);
})

$('.bg-order').on('click', function(){
	$('.bg-order').fadeOut();
	$('.orderby-xs').animate({
		'bottom': '-100%',
	}, 300, function(){
		$('.orderby-xs').fadeOut(10);
	});
})

$('.tabs_login .bx').on('click', function(){
	var action = $(this).attr('data-action');

	if (action == 1) {
		$('.center-login .col[data-tab="2"]').fadeOut(200, function(){

			$('.center-login .col[data-tab="1"]').fadeIn(200);
			$('.tabs_login .bx[data-action="2"]').removeClass('active');
			$('.tabs_login .bx[data-action="1"]').addClass('active');

		})
	}else{
		$('.center-login .col[data-tab="1"]').fadeOut(200, function(){

			$('.center-login .col[data-tab="2"]').fadeIn(200);
			$('.tabs_login .bx[data-action="1"]').removeClass('active');
			$('.tabs_login .bx[data-action="2"]').addClass('active');

		})
	}

})

$('.btn_dash_xs').on('click', function(){
	$('.menuMiCuenta').slideToggle();
})

$('.button_filtros_xs').on('click', function(){
	$('.container_grid > .col:first-child').fadeIn();
	$('.bg_filtros_xs').fadeIn();
})

$('.bg_filtros_xs').on('click', function(){
	$('.container_grid > .col:first-child').fadeOut();
	$('.bg_filtros_xs').fadeOut();
})

$('.isearch-xs').on('click', function(){
	$('.search_xs').fadeIn();
	$('.campo_buscador').focus();
})

$('.close_search_xs').on('click', function(){
	$('.search_xs').fadeOut();
})

function addToWish(id, ts){

	var action = $(ts).attr('data-action');

	$.ajax({
		url: 'ajax/wishlist.php',
		type: 'post',
		data: {producto_id: id, action: action},
	})
	.done(function(res) {
		if (res == 1) {
			$(ts).html('<i class="fas fa-heart"></i>');
			$(ts).attr('data-action', 'remove');
		}else if(res == 2){
			swal('', 'Debes ingresar a tu cuenta para realizar la acción', 'error');
		}else if(res == 3){
			swal('', 'Error al eliminar el producto a la lista de deseados', 'error');
		}else if(res == 4){
			$(ts).html('<i class="far fa-heart"></i>');
			$(ts).attr('data-action', 'add');
		}else{
			swal('', 'Error al agregar el producto a la lista de deseados', 'error');
		}

		$.ajax({
			url: 'ajax/getQtySave.php',
			type: 'get',
		})
		.done(function(res) {
			$('.qty_save').html(' <span class="qty_save">Productos guardados ('+res+')</span>');
		})
		.fail(function(res) {
			console.log(res);
		});
		
	})
	.fail(function(res) {
		console.log(res);
	});

}

function justNumbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 8) || (keynum == 46))
    return true;
     
    return /\d/.test(String.fromCharCode(keynum));
}

function number_format(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    return amount_parts.join('.');
}

function validarEmail(valor) {
	emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	if (emailRegex.test(valor)){
		return true;
	}else {
		return false;
	}
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}