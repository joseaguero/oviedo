$(function(){
	/*Agrego desde el fixed o desde el boton*/
	$('#btn_agregarCarro, #btnAgregarFixed').click(function(){
		var id_pro = $(this).attr('rel');
		var cantidad = parseInt($(".cantidad_ficha").val());
		var cantActual = $(".cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				beforeSend: function(){
					$(".cont_loading").fadeIn(200);
				},
				success:function(resp) {
					console.log("resp stock: "+resp);
					if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"add", qty:cantidad},
							cache: false,
							 success:function(resp) {
								 console.log("respuesta agregar"+resp);
								var cantTotal = parseInt(cantActual) + parseInt(cantidad);
								$(".totalHeader").load("tienda/totalCart.php");
								$(".cantItems").load("tienda/qtyTotal.php"); 
								$.ajax({
									type: 'GET',
									url: 'pags/popUpAddToCart.php',
									data: {id:id_pro, qty:cantidad},
									cache: false,
									 success:function(resp){
										 $(".cont_loading").fadeOut(200);
										 $("#popUp").html(resp);
										 $(".fondoPopUp").fadeIn(100);
									 }
								 });
							}
						});
					}else{
						$(".cont_loading").fadeOut(200);
						swal({
							  type: 'error',
							  title: 'Stock insuficiente',
							  text: 'No fue posible agregar tu producto por falta de stock'
							})
					};
				}
			});
		
	});
	
	
	
	
	/*agrega desde mis pedidos*/
	
	$('.btnComprarAhora').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = parseInt(1);
		var cantActual = $(".cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				success:function(resp) {
					console.log("resp stock: "+resp);
					if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"add", qty:cantidad},
							cache: false,
							 success:function(resp) {
								 console.log("respuesta agregar"+resp);
								var cantTotal = parseInt(cantActual) + parseInt(cantidad);
								$(".totalHeader").load("tienda/totalCart.php");
								$(".cantItems").load("tienda/qtyTotal.php"); 
								$.ajax({
									type: 'GET',
									url: 'pags/popUpAddToCart.php',
									data: {id:id_pro, qty:cantidad},
									cache: false,
									 success:function(resp){
										 $(".cont_loading").fadeOut(200);
										 $("#popUp").html(resp);
										 $(".fondoPopUp").fadeIn(100);
									 }
								 });
							}
						});
					}else{
						$(".cont_loading").fadeOut(200);
						swal({
							  type: 'error',
							  title: 'Stock insuficiente',
							  text: 'No fue posible agregar tu producto por falta de stock'
							})
					};
				}
			});
		
	});
	
	
	
	
	
	
});