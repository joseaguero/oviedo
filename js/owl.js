$(function(){
	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:20,
	    nav:true,
	    responsive : {
	    	0: {
	    		items: 2,
	    	},
	    	460: {
	    		items: 3,
	    	},
		    580 : {
		        items: 4,
		    },
		    890 : {
		        items: 6,
		    }
		}
	})
})