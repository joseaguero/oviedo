$(function(){
	$(".btnValidarCodigo").on('click', function(){
        var codigo = $('.descuento_box').val();

        var comuna_id;
        
        var retiro 		= $('#retiroClick').attr('data-active'),
        	despacho 	= $('#despachoClick').attr('data-active');

        if (retiro == true) {
        	retiro = 1;
        	comuna_id = 0;
        }else if(despacho == true){
        	retiro = 0;
        	comuna_id = $('#comuna_checkout').val();
        }

		$("#loadingCheckDescuento").fadeIn(100);
		$.ajax({
			url         : "tienda/ajax_codigo_descuento.php",
			type        : "post",
			async       : true,
			data		: {codigo:codigo},
			cache: false,
			success     : function(resp){
				if(resp == 1){
					$("#loadingCheckDescuento").fadeOut(100);
					$(".descuento_box").attr('disabled', "disabled");
                    $("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro);
                    
				} else {
					$("#loadingCheckDescuento").fadeOut(100);
					$("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro);
				};
			}
		});
    });
});