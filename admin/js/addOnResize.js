$.fn.addOnResizeEvent = function(custom_options) {
 
	var options = {
		timeInterval : 100,
		forceIE : false
	}
 
	$.extend( custom_options, options );
 
	// Soporte nativo IE
	if ($.browser.ie && !options.forceIE) return;
 
	return this.each( function() {
 
		var target = $(this);
		var lw, lh;
 
		var interval = setInterval( function() {
 
			var w = target.width(),
				h = target.height();
 
			if (!lw == undefined) lw = w;
			if (!lh == undefined) lh = h;
 
			if ( lw != w || lh != h )
			{
				lw = w;
				lh = h;
				target.trigger( "resize" );
			} 
 
		}, options.timeInterval );
 
	} );
 
};