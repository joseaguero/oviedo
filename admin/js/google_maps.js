// JavaScript Document
		
$(function() {
	initialize();
});			

var geocoder;
var map;


function codeAddress() {
	var address = document.getElementById("address").value;
	geocoder.geocode( { 'address': address}, function(results, status) {
	  if (status == google.maps.GeocoderStatus.OK) {
	    var marker = new google.maps.Marker({
	        map: map, 
	        position: results[0].geometry.location
	    });
	    map.setCenter(results[0].geometry.location);

	  } else {
	    alert("Geocode was not successful for the following reason: " + status);
	  }
	});
}

function initialize() {
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(-33.4469119561203, -70.6239366531372);
	var myOptions = {
	  zoom: 16,
	  center: latlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
}

	