<?php

/********************************************************\
|  Moldeable CMS - Instalador.		            		 |
|  Fecha Modificación: 24/09/2018		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/
	
include('conf.php');

$die_start = "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Error de instalaci&oacute;n:</span> <br /><br />";
$die_end = "</p>";

function create_dir($dir) {
	if (!file_exists("../".$dir."/"))
	{
 		$mk = mkdir("../".$dir); 
 		if (!$mk) 
		return false;
	}
	return true;
}

//Creación de directorios
/*
if (fileperms('../') == 16895)
{
	if (!create_dir("imagenes")) {
		die ("Error al crear directorio de im&aacute;genes");
	}
	if (!create_dir("docs")) {
		die ("Error al crear directorio de documentos");
	}
}
else
{
	echo fileperms('../');
	die($die_start."Directorio ra&iacute;z no dispone de permisos de escritura. Debe dar permisos de escritura al directorio ra&iacute;z para que el sistema cree los directorios necesarios. Una vez instalado el sistema debe cambiar los permisos de la carpeta nuevamente.".mysqli_error($conexion)."$die_end");
}
*/


/*
$dropTablas = "
DROP TABLE IF EXISTS `lineas`;
DROP TABLE IF EXISTS `categorias`;
DROP TABLE IF EXISTS `subcategorias`;
DROP TABLE IF EXISTS `categorias_productos`;
DROP TABLE IF EXISTS `marcas`;
DROP TABLE IF EXISTS `productos`;
DROP TABLE IF EXISTS `productos_detalles`;
DROP TABLE IF EXISTS `img_productos`;
DROP TABLE IF EXISTS `pedidos`;
DROP TABLE IF EXISTS `productos_pedidos`;
DROP TABLE IF EXISTS `estados`;
DROP TABLE IF EXISTS `descuento_opciones`;
DROP TABLE IF EXISTS `codigo_descuento`;
DROP TABLE IF EXISTS `newsletter`;
DROP TABLE IF EXISTS `regiones`;
DROP TABLE IF EXISTS `ciudades`;
DROP TABLE IF EXISTS `comunas`;
DROP TABLE IF EXISTS `localidades`;
DROP TABLE IF EXISTS `clientes_direcciones`;
DROP TABLE IF EXISTS `clientes`;
DROP TABLE IF EXISTS `posicion_productos`;";

$run_dropTablas = mysqli_query($conexion, $dropTablas);
if (!$run_dropTablas)
{
	die($die_start."Error al eliminar tablas: ".mysqli_error($conexion)."$die_end");
}
*/


//Tabla lineas
$lineas = "CREATE TABLE `lineas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `publicado` int(1) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_lineas = mysqli_query($conexion, $lineas);

if (!$run_lineas)
{
	die($die_start."Error al crear tabla lineas: ".mysqli_error($conexion)."$die_end");
}


//Tabla categorias
$categorias = "CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publicada` int(1) DEFAULT NULL,
  `linea_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT '',
  `descuento` int(11) DEFAULT '0',
  `posicion` int(11) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `banner_movil` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_categorias = mysqli_query($conexion, $categorias);

if (!$run_categorias)
{
	die($die_start."Error al crear tabla Categorias: ".mysqli_error($conexion)."$die_end");
}

//Tabla Subcategorias
$subcategorias = "CREATE TABLE `subcategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_subcategorias = mysqli_query($conexion, $subcategorias);

if (!$run_subcategorias)
{
	die($die_start."Error al crear tabla Subcategorias: ".mysqli_error($conexion)."$die_end");
}


//Tabla categorias_productos
$lineas_productos = "CREATE TABLE `lineas_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linea_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `subcategoria_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_lineas_productos = mysqli_query($conexion, $lineas_productos);

if (!$run_lineas_productos)
{
	die($die_start."Error al crear tabla lineas_productos: ".mysqli_error($conexion)."$die_end");
}

//Tabla marcas
$marcas = "CREATE TABLE `marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `imagen2` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_marcas = mysqli_query($conexion, $marcas);

if (!$run_marcas)
{
	die($die_start."Error al crear tabla marcas: ".mysqli_error($conexion)."$die_end");
}



//Tabla newsletter
$newsletter = "CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_newsletter = mysqli_query($conexion, $newsletter);

if (!$run_newsletter)
{
	die($die_start."Error al crear tabla Newsletter: ".mysqli_error($conexion)."$die_end");
}



//Tabla productos
$productos = "CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publicado` int(1) DEFAULT NULL,
  `cyber` int(1) DEFAULT '0',
  `recomendado` int(1) DEFAULT NULL,
  `pack` int(1) DEFAULT NULL,
  `codigos` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `ficha_tecnica` varchar(255) DEFAULT NULL,
  `thumbs` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `descripcion_seo` text,
  `keywords` text,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `vacio` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_productos = mysqli_query($conexion, $productos);

if (!$run_productos)
{
	die($die_start."Error al crear tabla productos: ".mysqli_error($conexion)."$die_end");
}



//Tabla productos_detalles
$productos_detalles = "CREATE TABLE `productos_detalles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `publicado` int(1) DEFAULT NULL,
  `precio_cantidad` int(1) DEFAULT '0',
  `nombre` varchar(255) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `tamaño_id` int(11) DEFAULT '0',
  `ancho` int(5) DEFAULT NULL,
  `alto` int(5) DEFAULT NULL,
  `largo` int(5) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `descuento` int(11) DEFAULT NULL,
  `precio_cyber` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT '0',
  `stock_reserva` int(4) DEFAULT '0',
  `venta_minima` int(4) DEFAULT '1',
  `oferta_tiempo_activa` int(1) DEFAULT NULL,
  `oferta_tiempo_descuento` int(11) DEFAULT NULL,
  `oferta_tiempo_hasta` datetime DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_productos_detalles = mysqli_query($conexion, $productos_detalles);

if (!$run_productos_detalles)
{
	die($die_start."Error al crear tabla productos_detalles: ".mysqli_error($conexion)."$die_end");
}


//Tabla img_productos
$img_productos = "CREATE TABLE `img_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo` varchar(255) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `galeria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_img_productos = mysqli_query($conexion, $img_productos);

if (!$run_img_productos)
{
	die($die_start."Error al crear tabla img_productos: ".mysqli_error($conexion)."$die_end");
}



//Tabla pedidos
$pedidos = "CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oc` varchar(100) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `medio_de_pago` varchar(100) DEFAULT NULL,
  `giro` varchar(200) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `rut` varchar(50) DEFAULT NULL,
  `regalo` int(1) DEFAULT NULL,
  `retiro_en_tienda` int(1) DEFAULT '0',
  `direccion` varchar(100) DEFAULT '',
  `comuna` varchar(255) DEFAULT '',
  `region` varchar(255) DEFAULT '',
  `ciudad` varchar(255) DEFAULT '',
  `localidad` varchar(255) DEFAULT NULL,
  `comentarios_envio` text,
  `email` varchar(100) DEFAULT '',
  `telefono` varchar(100) DEFAULT '',
  `fecha` date DEFAULT NULL,
  `codigo_descuento` varchar(100) DEFAULT '',
  `descuento` int(100) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `valor_despacho` int(11) DEFAULT NULL,
  `total_pagado` int(11) DEFAULT NULL,
  `cant_productos` int(11) DEFAULT NULL,
  `razon_social` varchar(255) DEFAULT NULL,
  `direccion_factura` varchar(255) DEFAULT NULL,
  `rut_factura` varchar(30) DEFAULT NULL,
  `email_factura` varchar(255) DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `factura` int(1) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `retiro_en_tienda_id` int(11) DEFAULT NULL,
  `accounting_date` varchar(30) DEFAULT NULL,
  `card_number` varchar(20) DEFAULT NULL,
  `card_expiration_date` varchar(20) DEFAULT NULL,
  `authorization_code` varchar(20) DEFAULT NULL,
  `payment_type_code` varchar(10) DEFAULT NULL,
  `response_code` varchar(100) DEFAULT NULL,
  `shares_number` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `transaction_date` varchar(100) DEFAULT NULL,
  `vci` varchar(20) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `commerce_code` varchar(30) DEFAULT NULL,
  `nombre_retiro` varchar(255) DEFAULT NULL,
  `apellidos_retiro` varchar(255) DEFAULT NULL,
  `rut_retiro` varchar(255) DEFAULT NULL,
  `codigo_pago` varchar(255) DEFAULT NULL,
  `mp_payment_type` varchar(255) DEFAULT NULL,
  `mp_payment_method` varchar(255) DEFAULT NULL,
  `mp_auth_code` varchar(255) DEFAULT NULL,
  `mp_paid_amount` int(11) DEFAULT NULL,
  `mp_card_number` varchar(255) DEFAULT NULL,
  `mp_id_paid` varchar(255) DEFAULT NULL,
  `mp_cuotas` int(11) DEFAULT NULL,
  `mp_valor_cuotas` int(11) DEFAULT NULL,
  `mp_transaction_date` varchar(255) DEFAULT NULL,
  `id_notificacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_pedidos = mysqli_query($conexion, $pedidos);

if (!$run_pedidos)
{
	die($die_start."Error al crear tabla pedidos: ".mysqli_error($conexion)."$die_end");
}





//Tabla productos_pedidos
$productos_pedidos = "CREATE TABLE `productos_pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `productos_detalle_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unitario` int(11) DEFAULT NULL,
  `descuento` int(11) DEFAULT NULL,
  `precio_total` int(11) DEFAULT NULL,
  `codigo` varchar(255) DEFAULT '',
  `codigo_pack` varchar(100) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_productos_pedidos = mysqli_query($conexion, $productos_pedidos);

if (!$run_productos_pedidos)
{
	die($die_start."Error al crear tabla productos_pedidos: ".mysqli_error($conexion)."$die_end");
}


//Tabla codigo_descuento
$codigo_descuento = "CREATE TABLE `codigo_descuento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `codigo` varchar(255) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `porcentaje` int(2) DEFAULT '0',
  `activo` int(1) DEFAULT '1',
  `oc` text,
  `fecha_uso` datetime DEFAULT NULL,
  `fecha_desde` date DEFAULT NULL,
  `fecha_hasta` date DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `cantidad` int(11) DEFAULT '0',
  `usados` int(11) DEFAULT '0',
  `descuento_opcion_id` int(11) DEFAULT '0',
  `donde_el_descuento` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_codigo_descuento = mysqli_query($conexion, $codigo_descuento);

if (!$run_codigo_descuento)
{
	die($die_start."Error al crear tabla codigo_descuento: ".mysqli_error($conexion)."$die_end");
}


//Tabla codigo_descuento
$descuento_opciones = "CREATE TABLE `descuento_opciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_descuento_opciones = mysqli_query($conexion, $descuento_opciones);

if (!$run_descuento_opciones)
{
	die($die_start."Error al crear tabla descuento_opciones: ".mysqli_error($conexion)."$die_end");
}

$insert_descuento_opciones = "INSERT INTO `descuento_opciones` (`id`, `nombre`, `fecha_creacion`, `fecha_modificacion`)
VALUES
	(1,'marcas',NOW(),NOW()),
	(2,'categorias',NOW(),NOW()),
	(3,'subcategorias',NOW(),NOW()),
	(4,'Aplicar a todo',NOW(),NOW());";
$run_insert_descuento_opciones = mysqli_query($conexion, $insert_descuento_opciones);

if (!$run_insert_descuento_opciones)
{
	die($die_start."Error al ingresar datos en tabla insert_descuento_opciones: ".mysqli_error($conexion)."$die_end");
}






//Tabla estados
$estados = "CREATE TABLE `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_estados = mysqli_query($conexion, $estados);

if (!$run_estados)
{
	die($die_start."Error al ingresar datos en tabla estados: ".mysqli_error($conexion)."$die_end");
}

$insert_estados = "INSERT INTO `estados` (`id`, `nombre`, `fecha_creacion`, `fecha_modificacion`)
VALUES
	(1,'Pendiente',NULL,NULL),
	(2,'Pagado',NULL,NULL),
	(3,'Rechazado',NULL,NULL);";
$run_insert_estados = mysqli_query($conexion, $insert_estados);

if (!$run_insert_estados)
{
	die($die_start."Error al ingresar datos en tabla estados: ".mysqli_error($conexion)."$die_end");
}








//Tabla regiones
$regiones = "CREATE TABLE `regiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_regiones = mysqli_query($conexion, $regiones);

if (!$run_regiones)
{
	die($die_start."Error al crear tabla estados: ".mysqli_error($conexion)."$die_end");
}

//Tabla regiones
$insert_regiones = "INSERT INTO `regiones` (`id`, `nombre`, `posicion`, `fecha_creacion`, `fecha_modificacion`)
VALUES
	
	(1,'Región de Tarapacá',2,NOW(),NOW()),
	(2,'Antofagasta',3,NOW(),NOW()),
	(3,'Atacama',4,NOW(),NOW()),
	(4,'Coquimbo',5,NOW(),NOW()),
	(5,'Valparaíso',6,NOW(),NOW()),
	(6,'Región del Libertador Gral. Bernardo O`Higgins',8,NOW(),NOW()),
	(7,'Región del Maule',9,NOW(),NOW()),
	(8,'Región del Biobío',10,NOW(),NOW()),
	(9,'Región de la Araucanía',11,NOW(),NOW()),
	(10,'Región de Los Lagos',13,NOW(),NOW()),
	(11,'Región Aisén del Gral. Carlos Ibáñez del Campo',14,NOW(),NOW()),
	(12,'Región de Magallanes y de la Antártica Chilena',15,NOW(),NOW()),
	(13,'Región Metropolitana de Santiago',7,NOW(),NOW()),
	(14,'Región de Los Ríos',13,NOW(),NOW()),
	(15,'Arica y Parinacota',1,NOW(),NOW()),
	(0,'sin región',NULL,NOW(),NOW());";
$run_insert_regiones = mysqli_query($conexion, $insert_regiones);

if (!$run_insert_regiones)
{
	die($die_start."Error al insertar en tabla regiones: ".mysqli_error($conexion)."$die_end");
}









//Tabla ciudades
$ciudades = "CREATE TABLE `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT '',
  `region_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_ciudades = mysqli_query($conexion, $ciudades);

if (!$run_ciudades)
{
	die($die_start."Error al crear tabla ciudades: ".mysqli_error($conexion)."$die_end");
}

$insert_ciudades = "INSERT INTO `ciudades` (`id`, `nombre`, `region_id`, `fecha_creacion`, `fecha_modificacion`)
VALUES
	(1,'ARICA',15,NULL,NULL),
	(2,'PARINACOTA',15,NULL,NULL),
	(3,'IQUIQUE',1,NULL,NULL),
	(4,'TOCOPILLA',2,NULL,NULL),
	(5,'EL LOA',2,NULL,NULL),
	(6,'ANTOFAGASTA',2,NULL,NULL),
	(7,'CHAÑARAL',3,NULL,NULL),
	(8,'COPIAPÓ',3,NULL,NULL),
	(9,'HUASCO',3,NULL,NULL),
	(10,'ELQUI',4,NULL,NULL),
	(11,'LIMARÃ',4,NULL,NULL),
	(12,'CHOAPA',4,NULL,NULL),
	(13,'VALPARAÍSO',5,NULL,NULL),
	(14,'PETORCA',5,NULL,NULL),
	(15,'LOS ANDES',5,NULL,NULL),
	(16,'SAN FELIPE DE ACONCAGUA',5,NULL,NULL),
	(17,'QUILLOTA',5,NULL,NULL),
	(18,'SAN ANTONIO',5,NULL,NULL),
	(19,'ISLA DE PASCUA',5,NULL,NULL),
	(20,'CACHAPOAL',6,NULL,NULL),
	(21,'COLCHAHUA',6,NULL,NULL),
	(22,'CARDENAL CARO',6,NULL,NULL),
	(23,'CURICÓ',7,NULL,NULL),
	(24,'TALCA',7,NULL,NULL),
	(25,'LINARES',7,NULL,NULL),
	(26,'CAUQUENES',7,NULL,NULL),
	(27,'ÑUBLE',8,NULL,NULL),
	(28,'BIO BIO',8,NULL,NULL),
	(29,'CONCEPCIÓN',8,NULL,NULL),
	(30,'ARAUCO',8,NULL,NULL),
	(31,'MALLECO',9,NULL,NULL),
	(32,'CAUTÍN',9,NULL,NULL),
	(33,'OSORNO',10,NULL,NULL),
	(34,'LLANQUIHUE',10,NULL,NULL),
	(35,'CHILOÉ',10,NULL,NULL),
	(36,'PALENA',10,NULL,NULL),
	(37,'CAPITÁN PRATT',11,NULL,NULL),
	(38,'AYSÉN',11,NULL,NULL),
	(39,'COIHAIQUE',11,NULL,NULL),
	(40,'GENERAL CARRERA',11,NULL,NULL),
	(41,'ÚLTIMA ESPERANZA',12,NULL,NULL),
	(42,'MAGALLANES',12,NULL,NULL),
	(43,'TIERRA DEL FUEGO',12,NULL,NULL),
	(45,'SANTIAGO',13,NULL,NULL),
	(46,'CORDILLERA',13,NULL,NULL),
	(47,'MELIPILLA',13,NULL,NULL),
	(48,'TALAGANTE',13,NULL,NULL),
	(49,'MAIPO',13,NULL,NULL),
	(50,'CHACABUCO',13,NULL,NULL),
	(51,'VALDIVIA',14,NULL,NULL),
	(52,'RANCO',14,NULL,NULL),
	(0,'sin ciudad',0,NULL,NULL);";
$run_insert_ciudades = mysqli_query($conexion, $insert_ciudades);

if (!$run_insert_ciudades)
{
	die($die_start."Error al insertar en tabla ciudades: ".mysqli_error($conexion)."$die_end");
}

	





//Tabla comunas
$comunas = "CREATE TABLE `comunas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT '',
  `ciudade_id` int(11) DEFAULT NULL,
  `precio_envio` int(11) DEFAULT NULL,
  `despacho_gratis` int(1) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `ciudad_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_comunas = mysqli_query($conexion, $comunas);

if (!$run_comunas)
{
	die($die_start."Error al crear tabla comunas: ".mysqli_error($conexion)."$die_end");
}

//Tabla comunas
$insert_comunas = "INSERT INTO `comunas` (`id`, `nombre`, `ciudade_id`, `precio_envio`, `despacho_gratis`, `fecha_creacion`, `fecha_modificacion`, `ciudad_id`)
VALUES
	(1, 'ARICA', 1, 0, 0, NULL, NULL, 1),
	(2, 'CAMARONES', 1, 0, 0, NULL, NULL, 1),
	(3, 'PUTRE', 2, 0, 0, NULL, NULL, 2),
	(4, 'GENERAL LAGOS', 2, 0, 0, NULL, NULL, 2),
	(5, 'IQUIQUE', 3, 0, 0, NULL, NULL, 3),
	(6, 'ALTO HOSPICIO', 3, 0, 0, NULL, NULL, 3),
	(7, 'HUARA', 3, 0, 0, NULL, NULL, 3),
	(8, 'CAMIÑA', 3, 0, 0, NULL, NULL, 3),
	(9, 'COLCHANE', 3, 0, 0, NULL, NULL, 3),
	(10, 'PICA', 3, 0, 0, NULL, NULL, 3),
	(11, 'POZO ALMONTE', 3, 0, 0, NULL, NULL, 3),
	(12, 'TOCOPILLA', 4, 0, 0, NULL, NULL, 4),
	(13, 'MARÍA ELENA', 4, 0, 0, NULL, NULL, 4),
	(14, 'CALAMA', 5, 0, 0, NULL, NULL, 5),
	(15, 'OLLAGUE', 5, 0, 0, NULL, NULL, 5),
	(16, 'SAN PEDRO DE ATACAMA', 5, 0, 0, NULL, NULL, 5),
	(17, 'ANTOFAGASTA', 6, 0, 0, NULL, NULL, 6),
	(18, 'MEJILLONES', 6, 0, 0, NULL, NULL, 6),
	(19, 'SIERRA GORDA', 6, 0, 0, NULL, NULL, 6),
	(20, 'TALTAL', 6, 0, 0, NULL, NULL, 6),
	(21, 'CHAÑARAL', 7, 0, 0, NULL, NULL, 7),
	(22, 'DIEGO DE ALMAGRO', 7, 0, 0, NULL, NULL, 7),
	(23, 'COPIAPÓ', 8, 0, 0, NULL, NULL, 8),
	(24, 'CALDERA', 8, 0, 0, NULL, NULL, 8),
	(25, 'TIERRA AMARILLA', 8, 0, 0, NULL, NULL, 8),
	(26, 'VALLENAR', 9, 0, 0, NULL, NULL, 9),
	(27, 'FREIRINA', 9, 0, 0, NULL, NULL, 9),
	(28, 'HUASCO', 9, 0, 0, NULL, NULL, 9),
	(29, 'ALTO DEL CARMEN', 9, 0, 0, NULL, NULL, 9),
	(30, 'LA SERENA', 10, 0, 0, NULL, NULL, 10),
	(31, 'LA HIGUERA', 10, 0, 0, NULL, NULL, 10),
	(32, 'COQUIMBO', 10, 0, 0, NULL, NULL, 10),
	(33, 'ANDACOLLO', 10, 0, 0, NULL, NULL, 10),
	(34, 'VICUÑA', 10, 0, 0, NULL, NULL, 10),
	(35, 'PAIHUANO', 10, 0, 0, NULL, NULL, 10),
	(36, 'OVALLE', 11, 0, 0, NULL, NULL, 11),
	(37, 'RÍO HURTADO', 11, 0, 0, NULL, NULL, 11),
	(38, 'MONTE PATRIA', 11, 0, 0, NULL, NULL, 11),
	(39, 'COMBARBALÁ', 11, 0, 0, NULL, NULL, 11),
	(40, 'PUNITAQUI', 11, 0, 0, NULL, NULL, 11),
	(41, 'ILLAPEL', 12, 0, 0, NULL, NULL, 12),
	(42, 'SALAMANCA', 12, 0, 0, NULL, NULL, 12),
	(43, 'LOS VILOS', 12, 0, 0, NULL, NULL, 12),
	(44, 'CANELA', 12, 0, 0, NULL, NULL, 12),
	(45, 'VALPARAÍSO', 13, 0, 0, NULL, NULL, 13),
	(46, 'CASABLANCA', 13, 0, 0, NULL, NULL, 13),
	(47, 'CONCON', 13, 0, 0, NULL, NULL, 13),
	(48, 'JUAN FERNÁNDEZ', 13, 0, 0, NULL, NULL, 13),
	(49, 'PUCHUNCAVÍ', 13, 0, 0, NULL, NULL, 13),
	(50, 'QUILPUÉ', 13, 0, 0, NULL, NULL, 13),
	(51, 'QUINTERO', 13, 0, 0, NULL, NULL, 13),
	(52, 'VILLA ALEMANA', 13, 0, 0, NULL, NULL, 13),
	(53, 'VIÑA DEL MAR', 13, 0, 0, NULL, NULL, 13),
	(54, 'PETORCA', 14, 0, 0, NULL, NULL, 14),
	(55, 'LA LIGUA', 14, 0, 0, NULL, NULL, 14),
	(56, 'CABILDO', 14, 0, 0, NULL, NULL, 14),
	(57, 'PAPUDO', 14, 0, 0, NULL, NULL, 14),
	(58, 'ZAPALLAR', 14, 0, 0, NULL, NULL, 14),
	(59, 'LOS ANDES', 15, 0, 0, NULL, NULL, 15),
	(60, 'SAN ESTEBAN', 15, 0, 0, NULL, NULL, 15),
	(61, 'CALLE LARGA', 15, 0, 0, NULL, NULL, 15),
	(62, 'RINCONADA', 15, 0, 0, NULL, NULL, 15),
	(63, 'SAN FELIPE', 16, 0, 0, NULL, NULL, 16),
	(64, 'CATEMU', 16, 0, 0, NULL, NULL, 16),
	(65, 'LLAY LLAY', 16, 0, 0, NULL, NULL, 16),
	(66, 'PANQUEHUE', 16, 0, 0, NULL, NULL, 16),
	(67, 'PUTAENDO', 16, 0, 0, NULL, NULL, 16),
	(68, 'SANTA MARÍA', 16, 0, 0, NULL, NULL, 16),
	(69, 'QUILLOTA', 17, 0, 0, NULL, NULL, 17),
	(70, 'CALERA', 17, 0, 0, NULL, NULL, 17),
	(71, 'HIJUELAS', 17, 0, 0, NULL, NULL, 17),
	(72, 'LIMACHE', 17, 0, 0, NULL, NULL, 17),
	(73, 'LA CRUZ', 17, 0, 0, NULL, NULL, 17),
	(74, 'NOGALES', 17, 0, 0, NULL, NULL, 17),
	(75, 'OLMUÉ', 17, 0, 0, NULL, NULL, 17),
	(76, 'SAN ANTONIO', 18, 0, 0, NULL, NULL, 18),
	(77, 'ALGARROBO', 18, 0, 0, NULL, NULL, 18),
	(78, 'CARTAGENA', 18, 0, 0, NULL, NULL, 18),
	(79, 'EL QUISCO', 18, 0, 0, NULL, NULL, 18),
	(80, 'EL TABO', 18, 0, 0, NULL, NULL, 18),
	(81, 'SANTO DOMINGO', 18, 0, 0, NULL, NULL, 18),
	(82, 'ISLA DE PASCUA', 19, 0, 0, NULL, NULL, 19),
	(83, 'RANCAGUA', 20, 0, 0, NULL, NULL, 20),
	(84, 'CODEGUA', 20, 0, 0, NULL, NULL, 20),
	(85, 'COINCO', 20, 0, 0, NULL, NULL, 20),
	(86, 'COLTAUCO', 20, 0, 0, NULL, NULL, 20),
	(87, 'DOÑIHUE', 20, 0, 0, NULL, NULL, 20),
	(88, 'GRANEROS', 20, 0, 0, NULL, NULL, 20),
	(89, 'LAS CABRAS', 20, 0, 0, NULL, NULL, 20),
	(90, 'MOSTAZAL', 20, 0, 0, NULL, NULL, 20),
	(91, 'MACHALÍ', 20, 0, 0, NULL, NULL, 20),
	(92, 'MALLOA', 20, 0, 0, NULL, NULL, 20),
	(93, 'OLIVAR', 20, 0, 0, NULL, NULL, 20),
	(94, 'PEUMO', 20, 0, 0, NULL, NULL, 20),
	(95, 'PICHIDEGUA', 20, 0, 0, NULL, NULL, 20),
	(96, 'QUINTA DE TILCOCO', 20, 0, 0, NULL, NULL, 20),
	(97, 'RENGO', 20, 0, 0, NULL, NULL, 20),
	(98, 'REQUINOA', 20, 0, 0, NULL, NULL, 20),
	(99, 'SAN VICENTE', 20, 0, 0, NULL, NULL, 20),
	(100, 'SAN FERNANDO', 21, 0, 0, NULL, NULL, 21),
	(101, 'CHÉPICA', 21, 0, 0, NULL, NULL, 21),
	(102, 'CHIMBARONGO', 21, 0, 0, NULL, NULL, 21),
	(103, 'LOLOL', 21, 0, 0, NULL, NULL, 21),
	(104, 'NANCAGUA', 21, 0, 0, NULL, NULL, 21),
	(105, 'PALMILLA', 21, 0, 0, NULL, NULL, 21),
	(106, 'PERALILLO', 21, 0, 0, NULL, NULL, 21),
	(107, 'PLACILLA', 21, 0, 0, NULL, NULL, 21),
	(108, 'PUMANQUE', 21, 0, 0, NULL, NULL, 21),
	(109, 'SANTA CRUZ', 21, 0, 0, NULL, NULL, 21),
	(110, 'PICHILEMU', 22, 0, 0, NULL, NULL, 22),
	(111, 'LA ESTRELLA', 22, 0, 0, NULL, NULL, 22),
	(112, 'LITUECHE', 22, 0, 0, NULL, NULL, 22),
	(113, 'MARCHIGUE', 22, 0, 0, NULL, NULL, 22),
	(114, 'NAVIDAD', 22, 0, 0, NULL, NULL, 22),
	(115, 'PAREDONES', 22, 0, 0, NULL, NULL, 22),
	(116, 'CURICÓ', 23, 0, 0, NULL, NULL, 23),
	(117, 'TENO', 23, 0, 0, NULL, NULL, 23),
	(118, 'ROMERAL', 23, 0, 0, NULL, NULL, 23),
	(119, 'MOLINA', 23, 0, 0, NULL, NULL, 23),
	(120, 'SAGRADA FAMILIA', 23, 0, 0, NULL, NULL, 23),
	(121, 'HUALAIHUE', 23, 0, 0, NULL, NULL, 23),
	(122, 'LICANTÉN', 23, 0, 0, NULL, NULL, 23),
	(123, 'VICHUQUÉN', 23, 0, 0, NULL, NULL, 23),
	(124, 'RAUCO', 23, 0, 0, NULL, NULL, 23),
	(125, 'TALCA', 24, 0, 0, NULL, NULL, 24),
	(126, 'PELARCO', 24, 0, 0, NULL, NULL, 24),
	(127, 'RÍO CLARO', 24, 0, 0, NULL, NULL, 24),
	(128, 'SAN CLEMENTE', 24, 0, 0, NULL, NULL, 24),
	(129, 'MAULE', 24, 0, 0, NULL, NULL, 24),
	(130, 'SAN RAFAEL', 24, 0, 0, NULL, NULL, 24),
	(131, 'EMPEDRADO', 24, 0, 0, NULL, NULL, 24),
	(132, 'PENCAHUE', 24, 0, 0, NULL, NULL, 24),
	(133, 'CONSTITUCIÓN', 24, 0, 0, NULL, NULL, 24),
	(134, 'CUREPTO', 24, 0, 0, NULL, NULL, 24),
	(135, 'LINARES', 25, 0, 0, NULL, NULL, 25),
	(136, 'YERBAS BUENAS', 25, 0, 0, NULL, NULL, 25),
	(137, 'COLBÚN', 25, 0, 0, NULL, NULL, 25),
	(138, 'LONGAVÍ', 25, 0, 0, NULL, NULL, 25),
	(139, 'PARRAL', 25, 0, 0, NULL, NULL, 25),
	(140, 'RETIRO', 25, 0, 0, NULL, NULL, 25),
	(141, 'VILLA ALEGRE', 25, 0, 0, NULL, NULL, 25),
	(142, 'SAN JAVIER', 25, 0, 0, NULL, NULL, 25),
	(143, 'CAUQUENES', 26, 0, 0, NULL, NULL, 26),
	(144, 'PELLUHUE', 26, 0, 0, NULL, NULL, 26),
	(145, 'CHANCO', 26, 0, 0, NULL, NULL, 26),
	(146, 'CHILLÁN', 27, 0, 0, NULL, NULL, 27),
	(147, 'BULNES', 27, 0, 0, NULL, NULL, 27),
	(148, 'CHILLAN VIEJO', 27, 0, 0, NULL, NULL, 27),
	(149, 'COBQUECURA', 27, 0, 0, NULL, NULL, 27),
	(150, 'COELEMU', 27, 0, 0, NULL, NULL, 27),
	(151, 'COIHUECO', 27, 0, 0, NULL, NULL, 27),
	(152, 'EL CARMEN', 27, 0, 0, NULL, NULL, 27),
	(153, 'NINHUE', 27, 0, 0, NULL, NULL, 27),
	(154, 'Ã?IQUÃ?N', 27, 0, 0, NULL, NULL, 27),
	(155, 'PEMUCO', 27, 0, 0, NULL, NULL, 27),
	(156, 'PINTO', 27, 0, 0, NULL, NULL, 27),
	(157, 'PORTEZUELO', 27, 0, 0, NULL, NULL, 27),
	(158, 'QUILLÁN', 27, 0, 0, NULL, NULL, 27),
	(159, 'QUIRIHUE', 27, 0, 0, NULL, NULL, 27),
	(160, 'RANQUIL', 27, 0, 0, NULL, NULL, 27),
	(161, 'SAN CARLOS', 27, 0, 0, NULL, NULL, 27),
	(162, 'SAN FABIÁN', 27, 0, 0, NULL, NULL, 27),
	(163, 'SAN IGNACIO', 27, 0, 0, NULL, NULL, 27),
	(164, 'SAN NICOLÁS', 27, 0, 0, NULL, NULL, 27),
	(165, 'TREHUACO', 27, 0, 0, NULL, NULL, 27),
	(166, 'YUNGAY', 27, 0, 0, NULL, NULL, 27),
	(167, 'LOS ANGELES', 28, 0, 0, NULL, NULL, 28),
	(168, 'ALTO BIO BIO', 28, 0, 0, NULL, NULL, 28),
	(169, 'ANTUCO', 28, 0, 0, NULL, NULL, 28),
	(170, 'CABRERO', 28, 0, 0, NULL, NULL, 28),
	(171, 'LAJA', 28, 0, 0, NULL, NULL, 28),
	(172, 'MULCHÉN', 28, 0, 0, NULL, NULL, 28),
	(173, 'NACIMIENTO', 28, 0, 0, NULL, NULL, 28),
	(174, 'NEGRETE', 28, 0, 0, NULL, NULL, 28),
	(175, 'QUILACO', 28, 0, 0, NULL, NULL, 28),
	(176, 'QUILLECO', 28, 0, 0, NULL, NULL, 28),
	(177, 'SANTA BÁRBARA', 28, 0, 0, NULL, NULL, 28),
	(178, 'SAN ROSENDO', 28, 0, 0, NULL, NULL, 28),
	(179, 'TUCAPEL', 28, 0, 0, NULL, NULL, 28),
	(180, 'YUMBEL', 28, 0, 0, NULL, NULL, 28),
	(181, 'CONCEPCIÓN', 29, 0, 0, NULL, NULL, 29),
	(182, 'CHIGUAYANTE', 29, 0, 0, NULL, NULL, 29),
	(183, 'CORONEL', 29, 0, 0, NULL, NULL, 29),
	(184, 'FLORIDA', 29, 0, 0, NULL, NULL, 29),
	(185, 'HUALPÉN', 29, 0, 0, NULL, NULL, 29),
	(186, 'HUALQUI', 29, 0, 0, NULL, NULL, 29),
	(187, 'LOTA', 29, 0, 0, NULL, NULL, 29),
	(188, 'PENCO', 29, 0, 0, NULL, NULL, 29),
	(189, 'SAN PEDRO DE LA PAZ', 29, 0, 0, NULL, NULL, 29),
	(190, 'SANTA JUANA', 29, 0, 0, NULL, NULL, 29),
	(191, 'TALCAHUANO', 29, 0, 0, NULL, NULL, 29),
	(192, 'TOMÉ', 29, 0, 0, NULL, NULL, 29),
	(193, 'ARAUCO', 30, 0, 0, NULL, NULL, 30),
	(194, 'CAÑETE', 30, 0, 0, NULL, NULL, 30),
	(195, 'CONTULMO', 30, 0, 0, NULL, NULL, 30),
	(196, 'CURANILAHUE', 30, 0, 0, NULL, NULL, 30),
	(197, 'LEBU', 30, 0, 0, NULL, NULL, 30),
	(198, 'LOS ALAMOS', 30, 0, 0, NULL, NULL, 30),
	(199, 'TIRUA', 30, 0, 0, NULL, NULL, 30),
	(200, 'ANGOL', 31, 0, 0, NULL, NULL, 31),
	(201, 'COLLIPULLI', 31, 0, 0, NULL, NULL, 31),
	(202, 'CURACAUTÍN', 31, 0, 0, NULL, NULL, 31),
	(203, 'ERCILLA', 31, 0, 0, NULL, NULL, 31),
	(204, 'LONQUIMAY', 31, 0, 0, NULL, NULL, 31),
	(205, 'LOS SAUCES', 31, 0, 0, NULL, NULL, 31),
	(206, 'LUMACO', 31, 0, 0, NULL, NULL, 31),
	(207, 'PURÉN', 31, 0, 0, NULL, NULL, 31),
	(208, 'RENAICO', 31, 0, 0, NULL, NULL, 31),
	(209, 'TRAIGUÉN', 31, 0, 0, NULL, NULL, 31),
	(210, 'VICTORIA', 31, 0, 0, NULL, NULL, 31),
	(211, 'TEMUCO', 32, 0, 0, NULL, NULL, 32),
	(212, 'CARAHUE', 32, 0, 0, NULL, NULL, 32),
	(213, 'CHOLCHOL', 32, 0, 0, NULL, NULL, 32),
	(214, 'CUNCO', 32, 0, 0, NULL, NULL, 32),
	(215, 'CURARREHUE', 32, 0, 0, NULL, NULL, 32),
	(216, 'FREIRE', 32, 0, 0, NULL, NULL, 32),
	(217, 'GALVARINO', 32, 0, 0, NULL, NULL, 32),
	(218, 'GORBEA', 32, 0, 0, NULL, NULL, 32),
	(219, 'LAUTARO', 32, 0, 0, NULL, NULL, 32),
	(220, 'LONCOCHE', 32, 0, 0, NULL, NULL, 32),
	(221, 'MELIPEUCO', 32, 0, 0, NULL, NULL, 32),
	(222, 'NUEVA IMPERIAL', 32, 0, 0, NULL, NULL, 32),
	(223, 'PADRE LAS CASAS', 32, 0, 0, NULL, NULL, 32),
	(224, 'PERQUENCO', 32, 0, 0, NULL, NULL, 32),
	(225, 'PITRUFQUÉN', 32, 0, 0, NULL, NULL, 32),
	(226, 'PUCÓN', 32, 0, 0, NULL, NULL, 32),
	(227, 'SAAVEDRA', 32, 0, 0, NULL, NULL, 32),
	(228, 'TEODORO SCHMIDT', 32, 0, 0, NULL, NULL, 32),
	(229, 'TOLTÉN', 32, 0, 0, NULL, NULL, 32),
	(230, 'VILCÚN', 32, 0, 0, NULL, NULL, 32),
	(231, 'VILLARICA', 32, 0, 0, NULL, NULL, 32),
	(232, 'OSORNO', 33, 0, 0, NULL, NULL, 33),
	(233, 'PUERTO OCTAY', 33, 0, 0, NULL, NULL, 33),
	(234, 'PURRANQUE', 33, 0, 0, NULL, NULL, 33),
	(235, 'PUYEHUE', 33, 0, 0, NULL, NULL, 33),
	(236, 'RÍO NEGRO', 33, 0, 0, NULL, NULL, 33),
	(237, 'SAN JUAN DE LA COSTA', 33, 0, 0, NULL, NULL, 33),
	(238, 'SAN PABLO', 33, 0, 0, NULL, NULL, 33),
	(239, 'CALBUCO', 34, 0, 0, NULL, NULL, 34),
	(240, 'COCHAMÓ', 34, 0, 0, NULL, NULL, 34),
	(241, 'FRESIA', 34, 0, 0, NULL, NULL, 34),
	(242, 'FRUTILLAR', 34, 0, 0, NULL, NULL, 34),
	(243, 'LOS MUERMOS', 34, 0, 0, NULL, NULL, 34),
	(244, 'LLANQUIHUE', 34, 0, 0, NULL, NULL, 34),
	(245, 'MAULLÍN', 34, 0, 0, NULL, NULL, 34),
	(246, 'PUERTO MONTT', 34, 0, 0, NULL, NULL, 34),
	(247, 'PUERTO VARAS', 34, 0, 0, NULL, NULL, 34),
	(248, 'CHILOÉ', 35, 0, 0, NULL, NULL, 35),
	(249, 'ANCUD', 35, 0, 0, NULL, NULL, 35),
	(250, 'CASTRO', 35, 0, 0, NULL, NULL, 35),
	(251, 'CURACO DE VÉLEZ', 35, 0, 0, NULL, NULL, 35),
	(252, 'CHONCHI', 35, 0, 0, NULL, NULL, 35),
	(253, 'DALCAHUE', 35, 0, 0, NULL, NULL, 35),
	(254, 'PUQUELDÓN', 35, 0, 0, NULL, NULL, 35),
	(255, 'QUEILÉN', 35, 0, 0, NULL, NULL, 35),
	(256, 'QUELLÓN', 35, 0, 0, NULL, NULL, 35),
	(257, 'QUEMCHI', 35, 0, 0, NULL, NULL, 35),
	(258, 'QUINCHAO', 35, 0, 0, NULL, NULL, 35),
	(259, 'CHAITÉN', 36, 0, 0, NULL, NULL, 36),
	(260, 'FUTALEUFÚ', 36, 0, 0, NULL, NULL, 36),
	(261, 'HUALAIHUÉ', 36, 0, 0, NULL, NULL, 36),
	(262, 'PALENA', 36, 0, 0, NULL, NULL, 36),
	(263, 'COCHRANE', 37, 0, 0, NULL, NULL, 37),
	(264, 'O\'HIGGINS', 37, 0, 0, NULL, NULL, 37),
	(265, 'TORTEL', 37, 0, 0, NULL, NULL, 37),
	(266, 'AYSÉN', 38, 0, 0, NULL, NULL, 38),
	(267, 'CISNES', 38, 0, 0, NULL, NULL, 38),
	(268, 'GUAITECAS', 38, 0, 0, NULL, NULL, 38),
	(269, 'COIHAIQUE', 39, 0, 0, NULL, NULL, 39),
	(270, 'LAGO VERDE', 39, 0, 0, NULL, NULL, 39),
	(271, 'CHILE CHICO', 40, 0, 0, NULL, NULL, 40),
	(272, 'RÍO IBAÑEZ', 40, 0, 0, NULL, NULL, 40),
	(273, 'PUERTO NATALES', 41, 0, 0, NULL, NULL, 41),
	(274, 'TORRES DEL PAINE', 41, 0, 0, NULL, NULL, 41),
	(275, 'PUNTA ARENAS', 42, 0, 0, NULL, NULL, 42),
	(276, 'RÍO VERDE', 42, 0, 0, NULL, NULL, 42),
	(277, 'LAGUNA BLANCA', 42, 0, 0, NULL, NULL, 42),
	(278, 'SAN GREGORIO', 42, 0, 0, NULL, NULL, 42),
	(279, 'PORVENIR', 43, 0, 0, NULL, NULL, 43),
	(280, 'PRIMAVERA', 43, 0, 0, NULL, NULL, 43),
	(281, 'TIMAUKEL', 43, 0, 0, NULL, NULL, 43),
	(282, 'NAVARINO', 44, 0, 0, NULL, NULL, 44),
	(283, 'ANTÁRTICA', 44, 0, 0, NULL, NULL, 44),
	(284, 'CERRILLOS', 45, 0, 1, NULL, NULL, 45),
	(285, 'CERRO NAVIA', 45, 0, 1, NULL, NULL, 45),
	(286, 'CONCHALÍ', 45, 0, 1, NULL, NULL, 45),
	(287, 'EL BOSQUE', 45, 0, 1, NULL, NULL, 45),
	(288, 'ESTACIÓN CENTRAL', 45, 0, 1, NULL, NULL, 45),
	(289, 'HUECHURABA', 45, 0, 1, NULL, NULL, 45),
	(290, 'INDEPENDENCIA', 45, 0, 1, NULL, NULL, 45),
	(291, 'LA CISTERNA', 45, 0, 1, NULL, NULL, 45),
	(292, 'LA FLORIDA', 45, 0, 1, NULL, NULL, 45),
	(293, 'LA GRANJA', 45, 0, 1, NULL, NULL, 45),
	(294, 'LA PINTANA', 45, 0, 1, NULL, NULL, 45),
	(295, 'LA REINA', 45, 0, 1, NULL, NULL, 45),
	(296, 'LAS CONDES', 45, 0, 1, NULL, NULL, 45),
	(297, 'LO BARNECHEA', 45, 0, 1, NULL, NULL, 45),
	(298, 'LO ESPEJO', 45, 0, 1, NULL, NULL, 45),
	(299, 'LO PRADO', 45, 0, 1, NULL, NULL, 45),
	(300, 'MACUL', 45, 0, 1, NULL, NULL, 45),
	(301, 'MAIPÚ', 45, 0, 1, NULL, NULL, 45),
	(302, 'ÑUÑOA', 45, 0, 1, NULL, NULL, 45),
	(303, 'PEDRO AGUIRRE CERDA', 45, 0, 1, NULL, NULL, 45),
	(304, 'PEÑALOLÉN', 45, 0, 1, NULL, NULL, 45),
	(305, 'PROVIDENCIA', 45, 0, 1, NULL, NULL, 45),
	(306, 'PUDAHUEL', 45, 0, 1, NULL, NULL, 45),
	(307, 'QUILICURA', 45, 0, 1, NULL, NULL, 45),
	(308, 'QUINTA NORMAL', 45, 0, 1, NULL, NULL, 45),
	(309, 'RECOLETA', 45, 0, 1, NULL, NULL, 45),
	(310, 'RENCA', 45, 0, 1, NULL, NULL, 45),
	(311, 'SAN JOAQUÍN', 45, 0, 1, NULL, NULL, 45),
	(312, 'SAN MIGUEL', 45, 0, 1, NULL, NULL, 45),
	(313, 'SAN RAMÓN', 45, 0, 1, NULL, NULL, 45),
	(314, 'SANTIAGO', 45, 0, 1, NULL, NULL, 45),
	(315, 'VITACURA', 45, 0, 1, NULL, NULL, 45),
	(316, 'PUENTE ALTO', 46, 0, 1, NULL, NULL, 46),
	(317, 'PIRQUE', 46, 0, 0, NULL, NULL, 46),
	(318, 'SAN JOSÉ DE MAIPO', 46, 0, 0, NULL, NULL, 46),
	(319, 'MELIPILLA', 47, 0, 0, NULL, NULL, 47),
	(320, 'MARÍA PINTO', 47, 0, 0, NULL, NULL, 47),
	(321, 'CURACAVÍ', 47, 0, 0, NULL, NULL, 47),
	(322, 'ALHUÉ', 47, 0, 0, NULL, NULL, 47),
	(323, 'SAN PEDRO', 47, 0, 0, NULL, NULL, 47),
	(324, 'TALAGANTE', 48, 0, 0, NULL, NULL, 48),
	(325, 'EL MONTE', 48, 0, 0, NULL, NULL, 48),
	(326, 'ISLA DE MAIPO', 48, 0, 0, NULL, NULL, 48),
	(327, 'PADRE HURTADO', 48, 0, 0, NULL, NULL, 48),
	(328, 'PEÑAFLOR', 48, 0, 0, NULL, NULL, 48),
	(329, 'BUIN', 49, 0, 0, NULL, NULL, 49),
	(330, 'CALERA DE TANGO', 49, 0, 0, NULL, NULL, 49),
	(331, 'PAINE', 49, 0, 0, NULL, NULL, 49),
	(332, 'SAN BERNARDO', 49, 0, 1, NULL, NULL, 49),
	(333, 'COLINA', 50, 0, 0, NULL, NULL, 50),
	(334, 'LAMPA', 50, 0, 0, NULL, NULL, 50),
	(335, 'TIL TIL', 50, 0, 0, NULL, NULL, 50),
	(336, 'VALDIVIA', 51, 0, 0, NULL, NULL, 51),
	(337, 'CORRAL', 51, 0, 0, NULL, NULL, 51),
	(338, 'LANCO', 51, 0, 0, NULL, NULL, 51),
	(339, 'LOS LAGOS', 51, 0, 0, NULL, NULL, 51),
	(340, 'MAFIL', 51, 0, 0, NULL, NULL, 51),
	(341, 'MARIQUINA', 51, 0, 0, NULL, NULL, 51),
	(342, 'PAILLACO', 51, 0, 0, NULL, NULL, 51),
	(343, 'PANGUIPULLI', 51, 0, 0, NULL, NULL, 51),
	(344, 'LA UNION', 52, 0, 0, NULL, NULL, 52),
	(345, 'FUTRONO', 52, 0, 0, NULL, NULL, 52),
	(346, 'LAGO RANCO', 52, 0, 0, NULL, NULL, 52),
	(348, 'tienda', 0, 0, 0, NULL, NULL, 0),
	(349, 'RIO BUENO', 52, 0, 0, NULL, NULL,52),
	(0, 'sin comuna', 0, 0, 0, NULL, NULL, 0);";
$run_insert_comunas = mysqli_query($conexion, $insert_comunas);

if (!$run_insert_comunas)
{
	die($die_start."Error al insertar en tabla comunas: ".mysqli_error($conexion)."$die_end");
}







//Tabla LOCALIDADES ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$localidades = "CREATE TABLE `localidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comuna_id` int(11) DEFAULT NULL,
  `base` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_zona` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dias_despacho` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;";
$run_localidades = mysqli_query($conexion, $localidades);

if (!$run_localidades)
{
	die($die_start."Error al crear tabla localidades: ".mysqli_error($conexion)."$die_end");
}


$insert_localidades = "INSERT INTO `localidades` (`id`, `nombre`, `comuna_id`, `base`, `tipo_zona`, `dias_despacho`)
VALUES
	(1, 'ARICA', 1, 'ARI', 'ZB', 8),
	(2, 'RANCHO ARICA', 1, 'ARI', 'ZB', 8),
	(3, 'ALTO RAMIREZ', 1, 'ARI', 'ZI', 8),
	(4, 'AUSIPAR', 1, 'ARI', 'ZI', 8),
	(5, 'AZAPA', 1, 'ARI', 'ZI', 8),
	(6, 'POCONCHILE', 1, 'ARI', 'ZI', 8),
	(7, 'SOBRAYA', 1, 'ARI', 'ZI', 8),
	(8, 'VILLA FRONTERA', 1, 'ARI', 'ZI', 8),
	(9, 'CAMARONES', 2, 'ARI', 'ZE', 8),
	(10, 'CUYA', 2, 'ARI', 'ZE', 8),
	(11, 'CUZ CUZ', 1, 'ARI', 'ZE', 8),
	(12, 'MOLINOS', 1, 'ARI', 'ZE', 8),
	(13, 'AGUAS CALIENTES', 4, 'ARI', 'ZME', 8),
	(14, 'AZUFRERA TACORA', 4, 'ARI', 'ZME', 8),
	(15, 'BELEN', 3, 'ARI', 'ZME', 8),
	(16, 'CALETA CAMARONES', 2, 'ARI', 'ZME', 8),
	(17, 'CAQUENA', 3, 'ARI', 'ZME', 8),
	(18, 'CHACUYO', 3, 'ARI', 'ZME', 8),
	(19, 'CHAPIQUIA', 3, 'ARI', 'ZME', 8),
	(20, 'CHICAYA', 3, 'ARI', 'ZME', 8),
	(21, 'CHOQUELIMPIE', 3, 'ARI', 'ZME', 8),
	(22, 'CODPA', 1, 'ARI', 'ZME', 8),
	(23, 'COPAQUILLA', 3, 'ARI', 'ZME', 8),
	(24, 'CORONEL ALCERRECA', 3, 'ARI', 'ZME', 8),
	(25, 'COSAPILLA', 4, 'ARI', 'ZME', 8),
	(26, 'ESQUINA', 1, 'ARI', 'ZME', 8),
	(27, 'GENERAL LAGOS', 4, 'ARI', 'ZME', 8),
	(28, 'GUACOLLO', 4, 'ARI', 'ZME', 8),
	(29, 'GUALLATIRI', 3, 'ARI', 'ZME', 8),
	(30, 'GUANACAHUA', 1, 'ARI', 'ZME', 8),
	(31, 'ITISA', 3, 'ARI', 'ZME', 8),
	(32, 'JURASI', 3, 'ARI', 'ZME', 8),
	(33, 'LAGO CHUNGARA', 3, 'ARI', 'ZME', 8),
	(34, 'MAILLKU', 3, 'ARI', 'ZME', 8),
	(35, 'MINIMINE', 2, 'ARI', 'ZME', 8),
	(36, 'MINITA', 2, 'ARI', 'ZME', 8),
	(37, 'NASAHUENTO', 4, 'ARI', 'ZME', 8),
	(38, 'PACHAMA', 3, 'ARI', 'ZME', 8),
	(39, 'PARINACOTA', 3, 'ARI', 'ZME', 8),
	(40, 'PUQUISA', 3, 'ARI', 'ZME', 8),
	(41, 'PUTRE', 3, 'ARI', 'ZME', 8),
	(42, 'QUIPINTA', 2, 'ARI', 'ZME', 8),
	(43, 'SOCOROMA', 3, 'ARI', 'ZME', 8),
	(44, 'SORA', 1, 'ARI', 'ZME', 8),
	(45, 'SUCA', 2, 'ARI', 'ZME', 8),
	(46, 'TARUGUIRE', 3, 'ARI', 'ZME', 8),
	(47, 'TERMAS DE CHITUNE', 3, 'ARI', 'ZME', 8),
	(48, 'TIGNAMAR', 3, 'ARI', 'ZME', 8),
	(49, 'TIMALCHACA', 3, 'ARI', 'ZME', 8),
	(50, 'TIMAR', 1, 'ARI', 'ZME', 8),
	(51, 'VILLA INDUSTRIAL', 4, 'ARI', 'ZME', 8),
	(52, 'VISVIRI', 4, 'ARI', 'ZME', 8),
	(53, 'IQUIQUE', 5, 'IQQ', 'ZB', 8),
	(54, 'ALTO HOSPICIO', 6, 'IQQ', 'ZI', 8),
	(55, 'CALETA BUENA', 5, 'IQQ', 'ZI', 8),
	(56, 'PLAYA BLANCA', 5, 'IQQ', 'ZI', 8),
	(57, 'POZO ALMONTE', 11, 'IQQ', 'ZI', 8),
	(58, 'CAMINA', 8, 'IQQ', 'ZE', 8),
	(59, 'FUERTE BAQUEDANO', 11, 'IQQ', 'ZE', 8),
	(60, 'HUARA', 7, 'IQQ', 'ZE', 8),
	(61, 'LA HUAICA', 10, 'IQQ', 'ZE', 8),
	(62, 'LA TIRANA', 11, 'IQQ', 'ZE', 8),
	(63, 'MAMIA', 11, 'IQQ', 'ZE', 8),
	(64, 'MATILLA', 10, 'IQQ', 'ZE', 8),
	(65, 'PICA', 10, 'IQQ', 'ZE', 8),
	(66, 'PISAGUA', 8, 'IQQ', 'ZE', 8),
	(67, 'PUERTO PATILLOS', 5, 'IQQ', 'ZE', 8),
	(68, 'PUNTA LOBOS', 5, 'IQQ', 'ZE', 8),
	(69, 'ALPAJERES', 8, 'IQQ', 'ZME', 8),
	(70, 'ALTUZA', 8, 'IQQ', 'ZME', 8),
	(71, 'ANAGUANI', 9, 'IQQ', 'ZME', 8),
	(72, 'ANCOVINTO', 9, 'IQQ', 'ZME', 8),
	(73, 'ANCUAQUE', 9, 'IQQ', 'ZME', 8),
	(74, 'CALATAMBO', 8, 'IQQ', 'ZME', 8),
	(75, 'CALETA CHICA', 2, 'IQQ', 'ZME', 8),
	(76, 'CANCOSA', 7, 'IQQ', 'ZME', 8),
	(77, 'CARIQUIMA', 9, 'IQQ', 'ZME', 8),
	(78, 'CARITAYA', 8, 'IQQ', 'ZME', 8),
	(79, 'CHAPIQUITA', 8, 'IQQ', 'ZME', 8),
	(80, 'CHIAPA', 9, 'IQQ', 'ZME', 8),
	(81, 'CHIPANA', 5, 'IQQ', 'ZME', 8),
	(82, 'CHUSMISA', 7, 'IQQ', 'ZME', 8),
	(83, 'COLCHANE', 9, 'IQQ', 'ZME', 8),
	(84, 'COLLACAGUA', 7, 'IQQ', 'ZME', 8),
	(85, 'COLLAHUASI', 10, 'IQQ', 'ZME', 8),
	(86, 'COLONIA PINTADOS', 10, 'IQQ', 'ZME', 8),
	(87, 'CORSA', 8, 'IQQ', 'ZME', 8),
	(88, 'ENQUELGA', 9, 'IQQ', 'ZME', 8),
	(89, 'GUATACONDO', 10, 'IQQ', 'ZME', 8),
	(90, 'ISLUGA', 9, 'IQQ', 'ZME', 8),
	(91, 'LIRIMA', 7, 'IQQ', 'ZME', 8),
	(92, 'MACAYA', 11, 'IQQ', 'ZME', 8),
	(93, 'MINA FAKIR', 5, 'IQQ', 'ZME', 8),
	(94, 'MINA LOBOS', 5, 'IQQ', 'ZME', 8),
	(95, 'MINERA QUEBRADA BLANCA', 10, 'IQQ', 'ZME', 8),
	(96, 'MOCHA', 7, 'IQQ', 'ZME', 8),
	(97, 'MOCOMUCANE', 9, 'IQQ', 'ZME', 8),
	(98, 'NAMA', 8, 'IQQ', 'ZME', 8),
	(99, 'OFICINA VICTORIA', 10, 'IQQ', 'ZME', 8),
	(100, 'PACHICA', 7, 'IQQ', 'ZME', 8),
	(101, 'PALCA', 8, 'IQQ', 'ZME', 8),
	(102, 'POROMA', 7, 'IQQ', 'ZME', 8),
	(103, 'PUQUIOS', 10, 'IQQ', 'ZME', 8),
	(104, 'RIO SECO', 5, 'IQQ', 'ZME', 8),
	(105, 'SAN MARCOS', 5, 'IQQ', 'ZME', 8),
	(106, 'SOTOCA', 9, 'IQQ', 'ZME', 8),
	(107, 'TAMBILLO', 11, 'IQQ', 'ZME', 8),
	(108, 'TARAPACA', 7, 'IQQ', 'ZME', 8),
	(109, 'VILAVILA', 8, 'IQQ', 'ZME', 8),
	(110, 'CALAMA', 14, 'CJC', 'ZB', 8),
	(111, 'CHUQUICAMATA', 14, 'CJC', 'ZI', 8),
	(112, 'MINA RADOMIRO TOMIC', 14, 'CJC', 'ZI', 8),
	(113, 'MINERA SPENCER', 14, 'CJC', 'ZI', 8),
	(114, 'CONCHI   ', 14, 'CJC', 'ZE', 8),
	(115, 'CUPO', 14, 'CJC', 'ZE', 8),
	(116, 'MINA CERRO DOMINADOR', 14, 'CJC', 'ZE', 8),
	(117, 'MINA FARIDE', 14, 'CJC', 'ZE', 8),
	(118, 'MINA GABY', 14, 'CJC', 'ZE', 8),
	(119, 'SAN JOSE  ', 14, 'CJC', 'ZE', 8),
	(120, 'SAN PEDRO DE ATACAMA', 16, 'CJC', 'ZE', 8),
	(121, 'TOCONAO', 16, 'CJC', 'ZE', 8),
	(122, 'TUINA', 14, 'CJC', 'ZE', 8),
	(123, 'AGUAS BLANCAS', 16, 'CJC', 'ZME', 8),
	(124, 'ALITAR', 16, 'CJC', 'ZME', 8),
	(125, 'AMINCHA', 15, 'CJC', 'ZME', 8),
	(126, 'ASCOTAN', 15, 'CJC', 'ZME', 8),
	(127, 'AUCANQUILCHA', 15, 'CJC', 'ZME', 8),
	(128, 'AYQUINA', 14, 'CJC', 'ZME', 8),
	(129, 'BANOS DE TURI', 14, 'CJC', 'ZME', 8),
	(130, 'CALACHUZ', 15, 'CJC', 'ZME', 8),
	(131, 'CAMAR', 16, 'CJC', 'ZME', 8),
	(132, 'CARACOLES', 19, 'CJC', 'ZME', 8),
	(133, 'CASPANA', 14, 'CJC', 'ZME', 8),
	(134, 'CENTINELA', 19, 'CJC', 'ZME', 8),
	(135, 'CHIUCHIU', 14, 'CJC', 'ZME', 8),
	(136, 'CONCHI VIEJO', 14, 'CJC', 'ZME', 8),
	(137, 'ESTACION CARCOTE', 15, 'CJC', 'ZME', 8),
	(138, 'ESTACION CEBOLLAR', 15, 'CJC', 'ZME', 8),
	(139, 'ESTACION CERRITOS BAYOS', 14, 'CJC', 'ZME', 8),
	(140, 'FLOR DEL DESIERTO', 19, 'CJC', 'ZME', 8),
	(141, 'INCACALIRI', 14, 'CJC', 'ZME', 8),
	(142, 'LASANA', 14, 'CJC', 'ZME', 8),
	(143, 'LEQUENA', 15, 'CJC', 'ZME', 8),
	(144, 'LINZOR', 14, 'CJC', 'ZME', 8),
	(145, 'MELLIZOS', 19, 'CJC', 'ZME', 8),
	(146, 'MINA EL ABRA', 14, 'CJC', 'ZME', 8),
	(147, 'MINA EL LITIO', 14, 'CJC', 'ZME', 8),
	(148, 'OLLAGUE', 15, 'CJC', 'ZME', 8),
	(149, 'PEINE', 16, 'CJC', 'ZME', 8),
	(150, 'POLAN', 15, 'CJC', 'ZME', 8),
	(151, 'PURITAMA', 16, 'CJC', 'ZME', 8),
	(152, 'RIO GRANDE', 276, 'CJC', 'ZME', 15),
	(153, 'SAN BARTOLO', 16, 'CJC', 'ZME', 8),
	(154, 'SAN PEDRO', 15, 'CJC', 'ZME', 8),
	(155, 'SANTA ROSA', 14, 'CJC', 'ZME', 8),
	(156, 'SOCAIRE', 16, 'CJC', 'ZME', 8),
	(157, 'TALABRE', 16, 'CJC', 'ZME', 8),
	(158, 'TILO POZO', 16, 'CJC', 'ZME', 8),
	(159, 'TILOMONTE', 16, 'CJC', 'ZME', 8),
	(160, 'TOCONCE', 14, 'CJC', 'ZME', 8),
	(161, 'TURI', 14, 'CJC', 'ZME', 8),
	(162, 'ANTOFAGASTA', 17, 'ANF', 'ZB', 8),
	(163, 'RANCHO ANTOFAGASTA', 17, 'ANF', 'ZB', 8),
	(164, 'BAQUEDANO', 17, 'ANF', 'ZI', 8),
	(165, 'CERRO MORENO', 17, 'ANF', 'ZI', 8),
	(166, 'JUAN LOPEZ', 17, 'ANF', 'ZI', 8),
	(167, 'LA NEGRA', 17, 'ANF', 'ZI', 8),
	(168, 'MEJILLONES', 18, 'ANF', 'ZI', 8),
	(169, 'PUERTO COLOSO', 17, 'ANF', 'ZI', 8),
	(170, 'ESTACION LA RIOJA', 17, 'ANF', 'ZE', 8),
	(171, 'HORNITOS', 18, 'ANF', 'ZE', 8),
	(172, 'MANTOS BLANCOS', 17, 'ANF', 'ZE', 8),
	(173, 'MARIA ELENA', 13, 'ANF', 'ZE', 8),
	(174, 'MARIA ELENA SOQUIMICH', 13, 'ANF', 'ZE', 8),
	(175, 'MINA LOMAS BAYAS', 17, 'ANF', 'ZE', 8),
	(176, 'MINA MICHILLA', 12, 'ANF', 'ZE', 8),
	(177, 'SIERRA GORDA', 19, 'ANF', 'ZE', 8),
	(178, 'TOCOPILLA', 12, 'ANF', 'ZE', 8),
	(179, 'AGUA VERDE', 17, 'ANF', 'ZME', 8),
	(180, 'AZUFRERA', 17, 'ANF', 'ZME', 8),
	(181, 'AZUFRERA PLATO DE SOPA', 17, 'ANF', 'ZME', 8),
	(182, 'BLANCO ENCALADA', 17, 'ANF', 'ZME', 8),
	(183, 'BRITANIA', 3, 'ANF', 'ZME', 8),
	(184, 'CALETA BOTIJA', 17, 'ANF', 'ZME', 8),
	(185, 'CALETA BOY', 12, 'ANF', 'ZME', 8),
	(186, 'CALETA BUENA ', 12, 'ANF', 'ZME', 8),
	(187, 'CALETA EL COBRE', 17, 'ANF', 'ZME', 8),
	(188, 'CARMEN ALTO', 17, 'ANF', 'ZME', 8),
	(189, 'CERRO PARANAL', 17, 'ANF', 'ZME', 8),
	(190, 'COBIJA', 12, 'ANF', 'ZME', 8),
	(191, 'EL GUANACO', 17, 'ANF', 'ZME', 8),
	(192, 'EL MEDANO', 17, 'ANF', 'ZME', 8),
	(193, 'EL WAY', 17, 'ANF', 'ZME', 8),
	(194, 'ENSUENO', 17, 'ANF', 'ZME', 8),
	(195, 'ESTACION AGUA BUENA', 17, 'ANF', 'ZME', 8),
	(196, 'ESTACION AGUAS BLANCAS', 17, 'ANF', 'ZME', 8),
	(197, 'ESTACION AUGUSTA VICTORIA', 17, 'ANF', 'ZME', 8),
	(198, 'ESTACION CATALINA', 17, 'ANF', 'ZME', 8),
	(199, 'ESTACION LOS VIENTOS', 17, 'ANF', 'ZME', 8),
	(200, 'ESTACION MIRAJE', 13, 'ANF', 'ZME', 8),
	(201, 'ESTACION MONTURAQUI', 17, 'ANF', 'ZME', 8),
	(202, 'ESTACION O`HIGGINS', 17, 'ANF', 'ZME', 8),
	(203, 'ESTACION PALESTINA', 17, 'ANF', 'ZME', 8),
	(204, 'ESTACION PAN DE AZUCAR', 17, 'ANF', 'ZME', 8),
	(205, 'ESTACION SOCOMPA', 17, 'ANF', 'ZME', 8),
	(206, 'ESTACION VARILLA', 17, 'ANF', 'ZME', 8),
	(207, 'EX OFICINA ALEMANIA', 17, 'ANF', 'ZME', 8),
	(208, 'EX OFICINA CHILE', 17, 'ANF', 'ZME', 8),
	(209, 'EX OFICINA FLOR DE CHILE', 17, 'ANF', 'ZME', 8),
	(210, 'ISLA SANTA MARIA', 17, 'ANF', 'ZME', 8),
	(211, 'MICHILLA', 12, 'ANF', 'ZME', 8),
	(212, 'MINERA ZALDIVAR', 19, 'ANF', 'ZME', 8),
	(213, 'OFICINA PEDRO DE VALDIVIA', 13, 'ANF', 'ZME', 8),
	(214, 'OFICINA VERGARA', 13, 'ANF', 'ZME', 8),
	(215, 'PAPOSO', 17, 'ANF', 'ZME', 8),
	(216, 'PARANAL', 17, 'ANF', 'ZME', 8),
	(217, 'QUILLAGUA', 13, 'ANF', 'ZME', 8),
	(218, 'SAN CRISTOBAL', 17, 'ANF', 'ZME', 8),
	(219, 'COPIAPO', 23, 'CPO', 'ZB', 8),
	(220, 'CALDERA', 24, 'CPO', 'ZI', 8),
	(221, 'EL BORATILLO', 26, 'CPO', 'ZI', 8),
	(222, 'ELISA DE BORDO', 25, 'CPO', 'ZI', 8),
	(223, 'MINA CANDELARIA', 25, 'CPO', 'ZI', 8),
	(224, 'NANTOCO', 25, 'CPO', 'ZI', 8),
	(225, 'PAIPOTE', 25, 'CPO', 'ZI', 8),
	(226, 'PUERTO VIEJO', 24, 'CPO', 'ZI', 8),
	(227, 'RANCHO CALDERA', 24, 'CPO', 'ZI', 8),
	(228, 'TIERRA AMARILLA', 25, 'CPO', 'ZI', 8),
	(229, 'TOTORALILLO', 32, 'CPO', 'ZI', 8),
	(230, 'ALGARROBAL', 26, 'CPO', 'ZE', 8),
	(231, 'BAHIA INGLESA', 24, 'CPO', 'ZE', 8),
	(232, 'CHANARAL', 21, 'CPO', 'ZE', 8),
	(233, 'COLINA', 333, 'CPO', 'ZE', 4),
	(234, 'DIEGO DE ALMAGRO', 22, 'CPO', 'ZE', 8),
	(235, 'EL DONKEY', 26, 'CPO', 'ZE', 8),
	(236, 'EL SALADO', 22, 'CPO', 'ZE', 8),
	(237, 'EL SALVADOR', 22, 'CPO', 'ZE', 8),
	(238, 'HORNITOS', 18, 'CPO', 'ZE', 8),
	(239, 'INCA DE ORO', 22, 'CPO', 'ZE', 8),
	(240, 'LA HOYADA', 26, 'CPO', 'ZE', 8),
	(241, 'LOS COLORADOS', 26, 'CPO', 'ZE', 8),
	(242, 'PUQUIOS', 10, 'CPO', 'ZE', 8),
	(243, 'TALTAL', 20, 'CPO', 'ZE', 8),
	(244, 'ALTAMIRA', 20, 'CPO', 'ZME', 8),
	(245, 'AMOLANAS', 25, 'CPO', 'ZME', 8),
	(246, 'BARRANQUILLAS', 23, 'CPO', 'ZME', 8),
	(247, 'BOCAMINA', 22, 'CPO', 'ZME', 8),
	(248, 'CALETA PAJONAL', 23, 'CPO', 'ZME', 8),
	(249, 'CIFUNCHO', 20, 'CPO', 'ZME', 8),
	(250, 'EL PINO', 22, 'CPO', 'ZME', 8),
	(251, 'EL VOLCAN', 318, 'CPO', 'ZME', 4),
	(252, 'ESMERALDA', 333, 'CPO', 'ZME', 4),
	(253, 'FINCA DE CHANARAL', 22, 'CPO', 'ZME', 8),
	(254, 'FLAMENCO', 21, 'CPO', 'ZME', 8),
	(255, 'HACIENDA CASTILLA', 23, 'CPO', 'ZME', 8),
	(256, 'LA ARENA ', 29, 'CPO', 'ZME', 8),
	(257, 'LA GUARDIA', 25, 'CPO', 'ZME', 8),
	(258, 'LA OLA', 22, 'CPO', 'ZME', 8),
	(259, 'LA POLVORA', 20, 'CPO', 'ZME', 8),
	(260, 'LA PUERTA', 25, 'CPO', 'ZME', 8),
	(261, 'LAS JUNTAS', 38, 'CPO', 'ZME', 8),
	(262, 'LLANTA', 22, 'CPO', 'ZME', 8),
	(263, 'LOS AZULES', 25, 'CPO', 'ZME', 8),
	(264, 'LOS LOROS', 25, 'CPO', 'ZME', 8),
	(265, 'MINA CHIVATO', 22, 'CPO', 'ZME', 8),
	(266, 'MINA DICHOSA', 21, 'CPO', 'ZME', 8),
	(267, 'MINA LA COIPA', 25, 'CPO', 'ZME', 8),
	(268, 'MINA LA ESTRELLA', 21, 'CPO', 'ZME', 8),
	(269, 'MINA MARTE', 25, 'CPO', 'ZME', 8),
	(270, 'MINA ROSARIO', 21, 'CPO', 'ZME', 8),
	(271, 'MONTANDON', 22, 'CPO', 'ZME', 8),
	(272, 'OBISPITO', 21, 'CPO', 'ZME', 8),
	(273, 'PAN DE AZUCAR', 21, 'CPO', 'ZME', 8),
	(274, 'PASTOS LARGOS', 25, 'CPO', 'ZME', 8),
	(275, 'PLAYA REFUGIO', 21, 'CPO', 'ZME', 8),
	(276, 'POTRERILLOS', 22, 'CPO', 'ZME', 8),
	(277, 'PUERTO FINO', 21, 'CPO', 'ZME', 8),
	(278, 'SAN ANTONIO', 76, 'CPO', 'ZME', 8),
	(279, 'TERMAS DE RIO NEGRO', 22, 'CPO', 'ZME', 8),
	(280, 'TOTORAL', 23, 'CPO', 'ZME', 8),
	(281, 'VALLE HERMOSO', 25, 'CPO', 'ZME', 8),
	(282, 'VEGAS DE CHANARAL ALTO', 22, 'CPO', 'ZME', 8),
	(283, 'COQUIMBO', 32, 'LSC', 'ZB', 8),
	(284, 'LA HERRADURA', 32, 'LSC', 'ZB', 8),
	(285, 'LA SERENA', 30, 'LSC', 'ZB', 8),
	(286, 'TIERRAS BLANCAS', 32, 'LSC', 'ZB', 8),
	(287, 'ALGARROBITO', 36, 'LSC', 'ZI', 12),
	(288, 'ALTO DEL CARMEN', 29, 'LSC', 'ZI', 8),
	(289, 'ALTOVALSOL', 30, 'LSC', 'ZI', 8),
	(290, 'BARRANCAS', 33, 'LSC', 'ZI', 8),
	(291, 'BARRAZA', 36, 'LSC', 'ZI', 8),
	(292, 'CAMARICO VIEJO', 36, 'LSC', 'ZI', 8),
	(293, 'CERRILLOS DE TAMAYA', 36, 'LSC', 'ZI', 8),
	(294, 'CHALINGA', 36, 'LSC', 'ZI', 8),
	(295, 'CHILECITO', 38, 'LSC', 'ZI', 8),
	(296, 'CHUNGUNGO', 31, 'LSC', 'ZI', 8),
	(297, 'COQUIMBITO', 30, 'LSC', 'ZI', 8),
	(298, 'CORRAL QUEMADO', 37, 'LSC', 'ZI', 8),
	(299, 'DOMEYKO', 26, 'LSC', 'ZI', 8),
	(300, 'EL ALTAR', 36, 'LSC', 'ZI', 8),
	(301, 'EL CARRIZAL', 38, 'LSC', 'ZI', 8),
	(302, 'EL ISLON', 30, 'LSC', 'ZI', 8),
	(303, 'EL MAITEN', 38, 'LSC', 'ZI', 8),
	(304, 'EL MAQUI ', 38, 'LSC', 'ZI', 8),
	(305, 'EL PENON', 32, 'LSC', 'ZI', 8),
	(306, 'EL RETAMO', 29, 'LSC', 'ZI', 8),
	(307, 'EL ROMERAL', 30, 'LSC', 'ZI', 8),
	(308, 'EL TOFO', 31, 'LSC', 'ZI', 8),
	(309, 'FREIRINA', 27, 'LSC', 'ZI', 8),
	(310, 'GUACHALALUME', 32, 'LSC', 'ZI', 8),
	(311, 'GUAMALATA', 36, 'LSC', 'ZI', 8),
	(312, 'GUAMPULLA', 36, 'LSC', 'ZI', 8),
	(313, 'GUANAQUEROS', 33, 'LSC', 'ZI', 8),
	(314, 'HIGUERITAS', 36, 'LSC', 'ZI', 8),
	(315, 'HUASCO', 28, 'LSC', 'ZI', 8),
	(316, 'HUASCO BAJO', 28, 'LSC', 'ZI', 8),
	(317, 'INCAGUASI', 31, 'LSC', 'ZI', 8),
	(318, 'LA AGUADA', 40, 'LSC', 'ZI', 8),
	(319, 'LA CHIMBA', 36, 'LSC', 'ZI', 8),
	(320, 'LA COMPAêA', 30, 'LSC', 'ZI', 12),
	(321, 'LA FUNDINA', 37, 'LSC', 'ZI', 8),
	(322, 'LA HIGUERA', 29, 'LSC', 'ZI', 8),
	(323, 'LA PLACILLA', 40, 'LSC', 'ZI', 8),
	(324, 'LA TORRE', 36, 'LSC', 'ZI', 8),
	(325, 'LAMBERT', 30, 'LSC', 'ZI', 8),
	(326, 'LAS CARDAS', 36, 'LSC', 'ZI', 8),
	(327, 'LAS JUNTAS', 38, 'LSC', 'ZI', 8),
	(328, 'LAS ROJAS', 30, 'LSC', 'ZI', 8),
	(329, 'LAS TACAS', 32, 'LSC', 'ZI', 8),
	(330, 'LIMARI', 36, 'LSC', 'ZI', 8),
	(331, 'LOS HORNOS', 31, 'LSC', 'ZI', 8),
	(332, 'OTAROLA', 36, 'LSC', 'ZI', 8),
	(333, 'OVALLE', 36, 'LSC', 'ZI', 8),
	(334, 'PACHINGO', 36, 'LSC', 'ZI', 8),
	(335, 'PAN DE AZUCAR ', 32, 'LSC', 'ZI', 8),
	(336, 'PEJERREYES', 36, 'LSC', 'ZI', 8),
	(337, 'PENA BLANCA', 40, 'LSC', 'ZI', 8),
	(338, 'PENUELAS', 32, 'LSC', 'ZI', 8),
	(339, 'PICHASCA', 37, 'LSC', 'ZI', 8),
	(340, 'PUERTO VELERO', 33, 'LSC', 'ZI', 8),
	(341, 'PUNITAQUI', 40, 'LSC', 'ZI', 8),
	(342, 'QUEBRADA SECA', 36, 'LSC', 'ZI', 8),
	(343, 'RAPEL', 38, 'LSC', 'ZI', 8),
	(344, 'RECOLETA', 36, 'LSC', 'ZI', 8),
	(345, 'RETIRO', 32, 'LSC', 'ZI', 8),
	(346, 'SAMO ALTO', 36, 'LSC', 'ZI', 8),
	(347, 'SAMO BAJO', 36, 'LSC', 'ZI', 8),
	(348, 'SAN PABLO', 30, 'LSC', 'ZI', 8),
	(349, 'SOTAQUI', 36, 'LSC', 'ZI', 8),
	(350, 'TEATINOS', 30, 'LSC', 'ZI', 8),
	(351, 'TERMAS DE SOCO', 36, 'LSC', 'ZI', 8),
	(352, 'TONGOY', 33, 'LSC', 'ZI', 8),
	(353, 'TOTORALILLO', 32, 'LSC', 'ZI', 8),
	(354, 'TRES CRUCES', 31, 'LSC', 'ZI', 8),
	(355, 'VALLENAR', 26, 'LSC', 'ZI', 8),
	(356, 'VICUNA', 34, 'LSC', 'ZI', 8),
	(357, 'AGUA GRANDE', 30, 'LSC', 'ZE', 8),
	(358, 'ALMIRANTE LATORRE', 30, 'LSC', 'ZE', 8),
	(359, 'ANDACOLLO', 33, 'LSC', 'ZE', 8),
	(360, 'ANDACOLLO HOLDING', 34, 'LSC', 'ZE', 12),
	(361, 'BALALA', 35, 'LSC', 'ZE', 8),
	(362, 'CACHIYUYO', 26, 'LSC', 'ZE', 8),
	(363, 'CAMARICO', 36, 'LSC', 'ZE', 8),
	(364, 'CANTO DE AGUA', 28, 'LSC', 'ZE', 8),
	(365, 'CAREN', 41, 'LSC', 'ZE', 8),
	(366, 'CASERONES', 34, 'LSC', 'ZE', 8),
	(367, 'CENTRAL LOS MOLLES', 38, 'LSC', 'ZE', 8),
	(368, 'CHAPILCA', 35, 'LSC', 'ZE', 8),
	(369, 'CHOROS BAJOS', 31, 'LSC', 'ZE', 8),
	(370, 'CONDORIACO', 34, 'LSC', 'ZE', 8),
	(371, 'CORTADERA', 34, 'LSC', 'ZE', 8),
	(372, 'DIAGUITAS', 34, 'LSC', 'ZE', 8),
	(373, 'EL CHACAY', 30, 'LSC', 'ZE', 8),
	(374, 'EL OLIVO', 31, 'LSC', 'ZE', 8),
	(375, 'EL ROMERAL', 30, 'LSC', 'ZE', 8),
	(376, 'EL TAMBO', 42, 'LSC', 'ZE', 8),
	(377, 'EL TRANSITO', 29, 'LSC', 'ZE', 8),
	(378, 'HURTADO', 37, 'LSC', 'ZE', 8),
	(379, 'LA FRAGUITA', 27, 'LSC', 'ZE', 8),
	(380, 'LA HIGUERITA', 29, 'LSC', 'ZE', 8),
	(381, 'LA LAJA', 34, 'LSC', 'ZE', 8),
	(382, 'LA LIGUA BAJO', 39, 'LSC', 'ZE', 8),
	(383, 'LABRAR', 27, 'LSC', 'ZE', 8),
	(384, 'LAS MOLLACAS', 38, 'LSC', 'ZE', 8),
	(385, 'LITIPAMPA', 40, 'LSC', 'ZE', 8),
	(386, 'LLANOS DE GUANTA', 35, 'LSC', 'ZE', 8),
	(387, 'LOS TOYOS', 28, 'LSC', 'ZE', 8),
	(388, 'MAITENCILLO', 27, 'LSC', 'ZE', 8),
	(389, 'MANTOS DE HORNILLOS', 44, 'LSC', 'ZE', 8),
	(390, 'MINA ALGARROBO', 27, 'LSC', 'ZE', 8),
	(391, 'MIRAFLORES', 28, 'LSC', 'ZE', 8),
	(392, 'MONTE PATRIA', 38, 'LSC', 'ZE', 8),
	(393, 'OBSERVATORIO EL TOLOLO', 34, 'LSC', 'ZE', 8),
	(394, 'OBSERVATORIO LA SILLA', 26, 'LSC', 'ZE', 8),
	(395, 'OBSERVATORIO LAS CAMPANAS', 26, 'LSC', 'ZE', 8),
	(396, 'PEDREGAL', 38, 'LSC', 'ZE', 8),
	(397, 'PERALILLO', 106, 'LSC', 'ZE', 8),
	(398, 'PISCO ELQUI', 35, 'LSC', 'ZE', 8),
	(399, 'PLACILLA   ', 32, 'LSC', 'ZE', 8),
	(400, 'PUNTA CHOROS', 31, 'LSC', 'ZE', 8),
	(401, 'QUILITAPIA', 39, 'LSC', 'ZE', 8),
	(402, 'RIO HURTADO', 37, 'LSC', 'ZE', 8),
	(403, 'RIVADAVIA', 35, 'LSC', 'ZE', 8),
	(404, 'SAN FELIX', 29, 'LSC', 'ZE', 8),
	(405, 'SAN MARCOS', 5, 'LSC', 'ZE', 8),
	(406, 'TRAPICHE', 31, 'LSC', 'ZE', 8),
	(407, 'TULAHUEN', 38, 'LSC', 'ZE', 8),
	(408, 'VARILLAR', 35, 'LSC', 'ZE', 8),
	(409, 'VINITA BAJA', 34, 'LSC', 'ZE', 8),
	(410, 'ALCOHUAS', 35, 'LSC', 'ZME', 8),
	(411, 'ALTO BUEY', 37, 'LSC', 'ZME', 8),
	(412, 'BANOS DEL GORDITO', 38, 'LSC', 'ZME', 8),
	(413, 'BANOS DEL TORO', 35, 'LSC', 'ZME', 8),
	(414, 'BOCATOMA', 38, 'LSC', 'ZME', 8),
	(415, 'CALETA CHANARAL', 27, 'LSC', 'ZME', 8),
	(416, 'CALETA DEL MEDIO', 23, 'LSC', 'ZME', 8),
	(417, 'CALETA MORRITOS', 44, 'LSC', 'ZME', 8),
	(418, 'CALETA SARCO', 27, 'LSC', 'ZME', 8),
	(419, 'CARRIZAL BAJO', 28, 'LSC', 'ZME', 8),
	(420, 'CARRIZALILLO', 27, 'LSC', 'ZME', 8),
	(421, 'CHANAR ', 37, 'LSC', 'ZME', 8),
	(422, 'CHANARAL ALTO', 38, 'LSC', 'ZME', 8),
	(423, 'CHANARCILLO', 25, 'LSC', 'ZME', 8),
	(424, 'CHINEO', 39, 'LSC', 'ZME', 8),
	(425, 'CHINGAY', 39, 'LSC', 'ZME', 8),
	(426, 'COCHIGUAS', 35, 'LSC', 'ZME', 8),
	(427, 'COGOTI', 39, 'LSC', 'ZME', 8),
	(428, 'COMBARBALA', 39, 'LSC', 'ZME', 8),
	(429, 'CONAY', 29, 'LSC', 'ZME', 8),
	(430, 'EL NEVADO', 29, 'LSC', 'ZME', 8),
	(431, 'EL PALQUI', 38, 'LSC', 'ZME', 8),
	(432, 'GUANTA', 35, 'LSC', 'ZME', 8),
	(433, 'GUATULAME', 38, 'LSC', 'ZME', 8),
	(434, 'HACIENDA LOS ANDES', 37, 'LSC', 'ZME', 8),
	(435, 'HILARICOS', 39, 'LSC', 'ZME', 8),
	(436, 'JUNTA VALERIANO', 29, 'LSC', 'ZME', 8),
	(437, 'JUNTAS DEL TORO', 35, 'LSC', 'ZME', 8),
	(438, 'LAGUNA GRANDE', 29, 'LSC', 'ZME', 8),
	(439, 'LAS BREAS', 37, 'LSC', 'ZME', 8),
	(440, 'LAS COLORADAS', 39, 'LSC', 'ZME', 8),
	(441, 'LAS HEDIONDAS', 35, 'LSC', 'ZME', 8),
	(442, 'LAS RAMADAS', 38, 'LSC', 'ZME', 8),
	(443, 'LLAHUIN', 39, 'LSC', 'ZME', 8),
	(444, 'LOS PERALES', 143, 'LSC', 'ZME', 8),
	(445, 'MINA DEL INDIO', 35, 'LSC', 'ZME', 8),
	(446, 'MONTE GRANDE', 35, 'LSC', 'ZME', 8),
	(447, 'NUEVA ELQUI', 35, 'LSC', 'ZME', 8),
	(448, 'PABELLON', 37, 'LSC', 'ZME', 8),
	(449, 'PAIHUANO', 35, 'LSC', 'ZME', 8),
	(450, 'PARMA', 39, 'LSC', 'ZME', 8),
	(451, 'SAN LORENZO', 56, 'LSC', 'ZME', 8),
	(452, 'SANTA CECILIA', 39, 'LSC', 'ZME', 8),
	(453, 'SOL NACIENTE', 35, 'LSC', 'ZME', 8),
	(454, 'LA CALERA', 70, 'ZLC', 'ZB', 8),
	(455, 'LA CRUZ', 322, 'ZLC', 'ZB', 4),
	(456, 'QUILLOTA', 69, 'ZLC', 'ZB', 8),
	(457, 'ALICAHUE', 56, 'ZLC', 'ZI', 8),
	(458, 'ARTIFICIO', 56, 'ZLC', 'ZI', 8),
	(459, 'CABILDO', 56, 'ZLC', 'ZI', 8),
	(460, 'CACHAGUA', 58, 'ZLC', 'ZI', 8),
	(461, 'CALETA HORCON', 49, 'ZLC', 'ZI', 8),
	(462, 'CALETA POLCURA', 55, 'ZLC', 'ZI', 8),
	(463, 'CANELA ALTA', 49, 'ZLC', 'ZI', 8),
	(464, 'CASUTO', 43, 'ZLC', 'ZI', 8),
	(465, 'CATAPILCO', 58, 'ZLC', 'ZI', 8),
	(466, 'CHIGUALOCO', 43, 'ZLC', 'ZI', 8),
	(467, 'EL COBRE ', 74, 'ZLC', 'ZI', 8),
	(468, 'EL GUAYACAN', 56, 'ZLC', 'ZI', 8),
	(469, 'EL INGENIO', 55, 'ZLC', 'ZI', 8),
	(470, 'EL MELON', 74, 'ZLC', 'ZI', 8),
	(471, 'EL RINCON', 49, 'ZLC', 'ZI', 8),
	(472, 'GRANIZO', 75, 'ZLC', 'ZI', 8),
	(473, 'GUANGALI', 43, 'ZLC', 'ZI', 8),
	(474, 'HIERRO VIEJO', 54, 'ZLC', 'ZI', 8),
	(475, 'HIJUELAS', 71, 'ZLC', 'ZI', 8),
	(476, 'HUENTELAUQUEN', 41, 'ZLC', 'ZI', 8),
	(477, 'ILLAPEL', 41, 'ZLC', 'ZI', 8),
	(478, 'LA LAGUNA', 49, 'ZLC', 'ZI', 8),
	(479, 'LA LIGUA', 55, 'ZLC', 'ZI', 8),
	(480, 'LA MORA', 56, 'ZLC', 'ZI', 8),
	(481, 'LA PATAGUA', 55, 'ZLC', 'ZI', 8),
	(482, 'LA QUEBRADA', 113, 'ZLC', 'ZI', 8),
	(483, 'LA VINA ', 56, 'ZLC', 'ZI', 8),
	(484, 'LAS PALMAS', 56, 'ZLC', 'ZI', 8),
	(485, 'LLAILLAY', 65, 'ZLC', 'ZI', 8),
	(486, 'LONGOTOMA', 55, 'ZLC', 'ZI', 8),
	(487, 'LOS ERMITANOS', 43, 'ZLC', 'ZI', 8),
	(488, 'LOS MOLLES', 43, 'ZLC', 'ZI', 8),
	(489, 'LOS VILOS', 43, 'ZLC', 'ZI', 8),
	(490, 'MAITENCILLO', 27, 'ZLC', 'ZI', 8),
	(491, 'MANUEL MONTT', 54, 'ZLC', 'ZI', 8),
	(492, 'MINCHA', 41, 'ZLC', 'ZI', 8),
	(493, 'MINCHA SUR', 41, 'ZLC', 'ZI', 8),
	(494, 'NOGALES', 74, 'ZLC', 'ZI', 8),
	(495, 'OLMUE', 75, 'ZLC', 'ZI', 8),
	(496, 'PAPUDO', 57, 'ZLC', 'ZI', 8),
	(497, 'PEDEGUA', 56, 'ZLC', 'ZI', 8),
	(498, 'PICHICUY', 55, 'ZLC', 'ZI', 8),
	(499, 'PICHIDANGUI', 43, 'ZLC', 'ZI', 8),
	(500, 'PUCHUNCAVI', 49, 'ZLC', 'ZI', 8),
	(501, 'PUPIO', 43, 'ZLC', 'ZI', 8),
	(502, 'QUEBRADA ALVARADO', 72, 'ZLC', 'ZI', 8),
	(503, 'QUILIMARI', 43, 'ZLC', 'ZI', 8),
	(504, 'QUINQUIMO', 55, 'ZLC', 'ZI', 8),
	(505, 'RABANALES', 41, 'ZLC', 'ZI', 8),
	(506, 'SAN LORENZO', 56, 'ZLC', 'ZI', 8),
	(507, 'SAN PEDRO', 15, 'ZLC', 'ZI', 8),
	(508, 'SANTA MARTA', 56, 'ZLC', 'ZI', 8),
	(509, 'TRAPICHE', 31, 'ZLC', 'ZI', 8),
	(510, 'VALLE ALEGRE', 51, 'ZLC', 'ZI', 8),
	(511, 'VENTANAS', 49, 'ZLC', 'ZI', 8),
	(512, 'ZAPALLAR', 42, 'ZLC', 'ZI', 8),
	(513, 'ARBOLEDA GRANDE', 42, 'ZLC', 'ZE', 8),
	(514, 'CAIMANES', 43, 'ZLC', 'ZE', 8),
	(515, 'CANELA', 44, 'ZLC', 'ZE', 8),
	(516, 'CANELA BAJA', 49, 'ZLC', 'ZE', 8),
	(517, 'CHINCOLCO', 54, 'ZLC', 'ZE', 8),
	(518, 'CHOAPA', 41, 'ZLC', 'ZE', 8),
	(519, 'COCUA', 41, 'ZLC', 'ZE', 8),
	(520, 'EL TAMBO', 42, 'ZLC', 'ZE', 8),
	(521, 'HUINTIL', 41, 'ZLC', 'ZE', 8),
	(522, 'JORQUERA', 42, 'ZLC', 'ZE', 8),
	(523, 'LAS CANAS', 41, 'ZLC', 'ZE', 8),
	(524, 'LAS PIRCAS', 41, 'ZLC', 'ZE', 8),
	(525, 'LIMAHUIDA', 42, 'ZLC', 'ZE', 8),
	(526, 'MATANCILLA', 41, 'ZLC', 'ZE', 8),
	(527, 'MINA EL ROSARIO', 54, 'ZLC', 'ZE', 8),
	(528, 'PEDERNAL', 54, 'ZLC', 'ZE', 8),
	(529, 'PETORCA', 54, 'ZLC', 'ZE', 8),
	(530, 'SALAMANCA', 42, 'ZLC', 'ZE', 8),
	(531, 'TILAMA', 43, 'ZLC', 'ZE', 8),
	(532, 'TOTORALILLO', 32, 'ZLC', 'ZE', 8),
	(533, 'TUNGA NORTE', 41, 'ZLC', 'ZE', 8),
	(534, 'TUNGA SUR', 41, 'ZLC', 'ZE', 8),
	(535, 'ALMENDRALILLO', 42, 'ZLC', 'ZME', 8),
	(536, 'BATUCO', 334, 'ZLC', 'ZME', 4),
	(537, 'CAREN', 41, 'ZLC', 'ZME', 8),
	(538, 'CHALACO', 54, 'ZLC', 'ZME', 8),
	(539, 'CHELLEPIN', 42, 'ZLC', 'ZME', 8),
	(540, 'COIRON', 42, 'ZLC', 'ZME', 8),
	(541, 'CUZ CUZ', 1, 'ZLC', 'ZME', 8),
	(542, 'FARELLON SANCHEZ', 41, 'ZLC', 'ZME', 8),
	(543, 'LLIMPO', 42, 'ZLC', 'ZME', 8),
	(544, 'LOS PELADEROS', 42, 'ZLC', 'ZME', 8),
	(545, 'MAURO', 43, 'ZLC', 'ZME', 8),
	(546, 'PALQUIAL', 42, 'ZLC', 'ZME', 8),
	(547, 'POZA HONDA', 44, 'ZLC', 'ZME', 8),
	(548, 'PUERTO OSCURO', 44, 'ZLC', 'ZME', 8),
	(549, 'QUELON', 43, 'ZLC', 'ZME', 8),
	(550, 'SAN AGUSTIN', 42, 'ZLC', 'ZME', 8),
	(551, 'CONCON', 47, 'KNA', 'ZB', 8),
	(552, 'RENACA', 53, 'KNA', 'ZB', 8),
	(553, 'VALPARAISO', 45, 'KNA', 'ZB', 8),
	(554, 'VINA DEL MAR', 53, 'KNA', 'ZB', 8),
	(555, 'ALGARROBO', 77, 'KNA', 'ZI', 8),
	(556, 'CALETAS LAS DOCAS', 45, 'KNA', 'ZI', 8),
	(557, 'CARTAGENA', 78, 'KNA', 'ZI', 8),
	(558, 'CASABLANCA', 46, 'KNA', 'ZI', 8),
	(559, 'COSTA AZUL', 79, 'KNA', 'ZI', 8),
	(560, 'EL BELLOTO', 50, 'KNA', 'ZI', 8),
	(561, 'EL QUISCO', 79, 'KNA', 'ZI', 8),
	(562, 'EL TABO', 80, 'KNA', 'ZI', 8),
	(563, 'ISLA NEGRA', 79, 'KNA', 'ZI', 8),
	(564, 'LAGUNA VERDE', 45, 'KNA', 'ZI', 8),
	(565, 'LAGUNILLAS', 46, 'KNA', 'ZI', 8),
	(566, 'LAS CRUCES', 78, 'KNA', 'ZI', 8),
	(567, 'LAS DICHAS', 46, 'KNA', 'ZI', 8),
	(568, 'LAS MERCEDES', 46, 'KNA', 'ZI', 8),
	(569, 'LAS TABLAS', 45, 'KNA', 'ZI', 8),
	(570, 'LIMACHE', 72, 'KNA', 'ZI', 8),
	(571, 'LLO-LLEO', 76, 'KNA', 'ZI', 8),
	(572, 'LO ABARCA', 78, 'KNA', 'ZI', 8),
	(573, 'LO GALLARDO', 81, 'KNA', 'ZI', 8),
	(574, 'LO OROZCO', 46, 'KNA', 'ZI', 8),
	(575, 'LO VASQUEZ', 46, 'KNA', 'ZI', 8),
	(576, 'MIRASOL', 77, 'KNA', 'ZI', 8),
	(577, 'PENUELAS', 32, 'KNA', 'ZI', 8),
	(578, 'PEABLANCA', 52, 'KNA', 'ZI', 12),
	(579, 'PLACILLA', 107, 'KNA', 'ZI', 8),
	(580, 'PUNTA BARRANCA', 114, 'KNA', 'ZI', 8),
	(581, 'PUNTA CURAUMILLA', 45, 'KNA', 'ZI', 8),
	(582, 'PUNTA DE TRALCA', 79, 'KNA', 'ZI', 8),
	(583, 'QUILPUE', 50, 'KNA', 'ZI', 8),
	(584, 'QUINTAY', 77, 'KNA', 'ZI', 8),
	(585, 'QUINTERO', 51, 'KNA', 'ZI', 8),
	(586, 'RITOQUE', 51, 'KNA', 'ZI', 8),
	(587, 'SAN ANTONIO', 76, 'KNA', 'ZI', 8),
	(588, 'SAN SEBASTIAN', 78, 'KNA', 'ZI', 8),
	(589, 'SANTO DOMINGO', 81, 'KNA', 'ZI', 8),
	(590, 'VILLA ALEMANA', 52, 'KNA', 'ZI', 8),
	(591, 'EL CARPINTERO', 46, 'KNA', 'ZE', 8),
	(592, 'EL TURCO', 78, 'KNA', 'ZE', 8),
	(593, 'LA RETUCA', 50, 'KNA', 'ZE', 8),
	(594, 'LEYDA', 81, 'KNA', 'ZE', 8),
	(595, 'RAPEL', 38, 'KNA', 'ZE', 8),
	(596, 'SAN GERONIMO', 46, 'KNA', 'ZE', 8),
	(597, 'TAPIHUE', 46, 'KNA', 'ZE', 8),
	(598, 'BUCALEMU', 115, 'KNA', 'ZME', 8),
	(599, 'CORNECHE', 114, 'KNA', 'ZME', 8),
	(600, 'LA BOCA', 185, 'KNA', 'ZME', 8),
	(601, 'LA ESTRELLA', 111, 'KNA', 'ZME', 8),
	(602, 'LAS DAMAS', 111, 'KNA', 'ZME', 8),
	(603, 'LITUECHE', 112, 'KNA', 'ZME', 8),
	(604, 'MATANZAS', 114, 'KNA', 'ZME', 8),
	(605, 'NAVIDAD', 114, 'KNA', 'ZME', 8),
	(606, 'PUERTECILLO', 114, 'KNA', 'ZME', 8),
	(607, 'PUNTA PERRO', 114, 'KNA', 'ZME', 8),
	(608, 'PUNTA TORO', 323, 'KNA', 'ZME', 4),
	(609, 'SAN ENRIQUE', 114, 'KNA', 'ZME', 8),
	(610, 'SAN VICENTE DE PUCALAN', 114, 'KNA', 'ZME', 8),
	(611, 'TOPOCALMA', 112, 'KNA', 'ZME', 8),
	(612, 'AEROPUERTO ARTURO MERINO BENITEZ', 306, 'SCL', 'ZB', 4),
	(613, 'CERRILLOS', 284, 'SCL', 'ZB', 4),
	(614, 'CERRO NAVIA', 285, 'SCL', 'ZB', 4),
	(615, 'CONCHALI', 286, 'SCL', 'ZB', 4),
	(616, 'EL BOSQUE', 287, 'SCL', 'ZB', 4),
	(617, 'ESTACION CENTRAL', 288, 'SCL', 'ZB', 4),
	(618, 'HUECHURABA', 289, 'SCL', 'ZB', 4),
	(619, 'INDEPENDENCIA', 290, 'SCL', 'ZB', 4),
	(620, 'LA CISTERNA', 291, 'SCL', 'ZB', 4),
	(621, 'LA FLORIDA', 292, 'SCL', 'ZB', 4),
	(622, 'LA GRANJA', 293, 'SCL', 'ZB', 4),
	(623, 'LA PINTANA', 294, 'SCL', 'ZB', 4),
	(624, 'LA REINA', 295, 'SCL', 'ZB', 4),
	(625, 'LAS CONDES', 296, 'SCL', 'ZB', 4),
	(626, 'LO  ERRAZURIZ', 66, 'SCL', 'ZB', 8),
	(627, 'LO BARNECHEA', 297, 'SCL', 'ZB', 4),
	(628, 'LO ESPEJO', 298, 'SCL', 'ZB', 4),
	(629, 'LO PRADO', 299, 'SCL', 'ZB', 4),
	(630, 'MACUL', 300, 'SCL', 'ZB', 4),
	(631, 'MAIPU', 301, 'SCL', 'ZB', 4),
	(632, 'NUNOA', 302, 'SCL', 'ZB', 4),
	(633, 'PEDRO AGUIRRE CERDA', 303, 'SCL', 'ZB', 4),
	(634, 'PENALOLEN', 304, 'SCL', 'ZB', 4),
	(635, 'PROVIDENCIA', 305, 'SCL', 'ZB', 4),
	(636, 'PUDAHUEL', 306, 'SCL', 'ZB', 4),
	(637, 'PUENTE ALTO', 316, 'SCL', 'ZB', 4),
	(638, 'QUILICURA', 307, 'SCL', 'ZB', 4),
	(639, 'QUINTA NORMAL', 308, 'SCL', 'ZB', 4),
	(640, 'RECOLETA', 36, 'SCL', 'ZB', 8),
	(641, 'RENCA', 310, 'SCL', 'ZB', 4),
	(642, 'SAN JOAQUIN', 311, 'SCL', 'ZB', 4),
	(643, 'SAN MIGUEL', 172, 'SCL', 'ZB', 8),
	(644, 'SAN RAMON', 313, 'SCL', 'ZB', 4),
	(645, 'SANTIAGO', 314, 'SCL', 'ZB', 4),
	(646, 'VITACURA', 315, 'SCL', 'ZB', 4),
	(647, 'ALTO JAHUEL', 330, 'SCL', 'ZI', 4),
	(648, 'BATUCO', 334, 'SCL', 'ZI', 4),
	(649, 'BOLLENAR', 320, 'SCL', 'ZI', 4),
	(650, 'BUIN', 329, 'SCL', 'ZI', 4),
	(651, 'CALERA DE TANGO', 330, 'SCL', 'ZI', 4),
	(652, 'CALLE LARGA', 61, 'SCL', 'ZI', 8),
	(653, 'CATEMU', 64, 'SCL', 'ZI', 8),
	(654, 'CERRILLOS DE SAN FELIPE', 63, 'SCL', 'ZI', 8),
	(655, 'CERRO BLANCO', 335, 'SCL', 'ZI', 4),
	(656, 'CHAGRES', 64, 'SCL', 'ZI', 8),
	(657, 'CHAMPA', 331, 'SCL', 'ZI', 4),
	(658, 'CHICUREO', 333, 'SCL', 'ZI', 4),
	(659, 'CHOCALAN ', 319, 'SCL', 'ZI', 4),
	(660, 'CHOROMBO', 320, 'SCL', 'ZI', 4),
	(661, 'CODIGUA', 319, 'SCL', 'ZI', 4),
	(662, 'COLINA', 333, 'SCL', 'ZI', 4),
	(663, 'COLLIGUAY', 321, 'SCL', 'ZI', 4),
	(664, 'CRUCE LAS ARANAS', 323, 'SCL', 'ZI', 4),
	(665, 'CULIPRAN', 319, 'SCL', 'ZI', 4),
	(666, 'CURACAVI', 321, 'SCL', 'ZI', 4),
	(667, 'CURIMON', 63, 'SCL', 'ZI', 8),
	(668, 'EL CANELO', 316, 'SCL', 'ZI', 4),
	(669, 'EL CERRADO', 64, 'SCL', 'ZI', 8),
	(670, 'EL COBRE', 64, 'SCL', 'ZI', 8),
	(671, 'EL MELOCOTON', 318, 'SCL', 'ZI', 4),
	(672, 'EL MONTE', 325, 'SCL', 'ZI', 4),
	(673, 'EL PAICO', 325, 'SCL', 'ZI', 4),
	(674, 'EL TARTARO', 67, 'SCL', 'ZI', 8),
	(675, 'EL TRANSITO', 29, 'SCL', 'ZI', 8),
	(676, 'EL TREBOL', 321, 'SCL', 'ZI', 4),
	(677, 'ESMERALDA', 333, 'SCL', 'ZI', 4),
	(678, 'GRANALLA', 67, 'SCL', 'ZI', 8),
	(679, 'HOSPITAL', 331, 'SCL', 'ZI', 4),
	(680, 'HUELQUEN', 331, 'SCL', 'ZI', 4),
	(681, 'ISLA DE MAIPO', 326, 'SCL', 'ZI', 4),
	(682, 'ISLA DE PIRQUE', 317, 'SCL', 'ZI', 4),
	(683, 'JAHUEL', 68, 'SCL', 'ZI', 8),
	(684, 'LA OBRA', 316, 'SCL', 'ZI', 4),
	(685, 'LAMPA', 334, 'SCL', 'ZI', 4),
	(686, 'LAS CANTERAS', 333, 'SCL', 'ZI', 4),
	(687, 'LAS MARIPOSAS', 319, 'SCL', 'ZI', 4),
	(688, 'LAS VERTIENTES', 316, 'SCL', 'ZI', 4),
	(689, 'LINDEROS', 331, 'SCL', 'ZI', 12),
	(690, 'LO CHACON', 322, 'SCL', 'ZI', 4),
	(691, 'LOLENCO', 201, 'SCL', 'ZI', 8),
	(692, 'LONQUEN', 327, 'SCL', 'ZI', 4),
	(693, 'LOS ANDES', 59, 'SCL', 'ZI', 8),
	(694, 'LOS ARRAYANES', 321, 'SCL', 'ZI', 4),
	(695, 'LOS RULOS', 320, 'SCL', 'ZI', 4),
	(696, 'LOYCA', 323, 'SCL', 'ZI', 4),
	(697, 'MAIPO', 329, 'SCL', 'ZI', 4),
	(698, 'MALLOCO', 328, 'SCL', 'ZI', 4),
	(699, 'MANDINGA', 319, 'SCL', 'ZI', 4),
	(700, 'MARIA PINTO', 320, 'SCL', 'ZI', 4),
	(701, 'MELIPILLA', 319, 'SCL', 'ZI', 4),
	(702, 'MONTENEGRO', 65, 'SCL', 'ZI', 8),
	(703, 'NILHUE', 64, 'SCL', 'ZI', 8),
	(704, 'OCOA', 73, 'SCL', 'ZI', 8),
	(705, 'PABELLON', 37, 'SCL', 'ZI', 8),
	(706, 'PADRE HURTADO', 327, 'SCL', 'ZI', 4),
	(707, 'PAINE', 331, 'SCL', 'ZI', 4),
	(708, 'PANQUEHUE', 66, 'SCL', 'ZI', 8),
	(709, 'PENAFLOR', 328, 'SCL', 'ZI', 4),
	(710, 'PINTUE', 331, 'SCL', 'ZI', 4),
	(711, 'PIRQUE', 317, 'SCL', 'ZI', 4),
	(712, 'POLPAICO', 335, 'SCL', 'ZI', 4),
	(713, 'POMAIRE', 319, 'SCL', 'ZI', 4),
	(714, 'PUEBLO HUNDIDO', 323, 'SCL', 'ZI', 4),
	(715, 'PUTAENDO', 67, 'SCL', 'ZI', 8),
	(716, 'QUIMCAHUE', 323, 'SCL', 'ZI', 4),
	(717, 'RANGUE', 331, 'SCL', 'ZI', 4),
	(718, 'RESGUARDO LOS PATOS', 67, 'SCL', 'ZI', 8),
	(719, 'RINCONADA', 195, 'SCL', 'ZI', 8),
	(720, 'RINCONADA DE GUZMAN', 67, 'SCL', 'ZI', 8),
	(721, 'RINCONADA DE SILVA', 67, 'SCL', 'ZI', 8),
	(722, 'RIO BLANCO', 59, 'SCL', 'ZI', 8),
	(723, 'SALTO DEL SOLDADO', 59, 'SCL', 'ZI', 8),
	(724, 'SAN BERNARDO', 332, 'SCL', 'ZI', 4),
	(725, 'SAN ESTEBAN', 60, 'SCL', 'ZI', 8),
	(726, 'SAN FELIPE', 63, 'SCL', 'ZI', 8),
	(727, 'SAN MANUEL', 319, 'SCL', 'ZI', 4),
	(728, 'SANTA INES', 320, 'SCL', 'ZI', 4),
	(729, 'SANTA MARIA', 68, 'SCL', 'ZI', 8),
	(730, 'SANTA MARTA', 56, 'SCL', 'ZI', 8),
	(731, 'TALAGANTE', 324, 'SCL', 'ZI', 4),
	(732, 'TERMAS DE JAHUEL', 68, 'SCL', 'ZI', 8),
	(733, 'TIL TIL', 335, 'SCL', 'ZI', 4),
	(734, 'VILLA ALHUE', 322, 'SCL', 'ZI', 4),
	(735, 'ALHUE', 322, 'SCL', 'ZE', 4),
	(736, 'CHACAY', 318, 'SCL', 'ZE', 4),
	(737, 'EL MANZANILLO', 318, 'SCL', 'ZE', 4),
	(738, 'EL VOLCAN', 318, 'SCL', 'ZE', 4),
	(739, 'FARELLONES', 297, 'SCL', 'ZE', 4),
	(740, 'GUAYACAN', 318, 'SCL', 'ZE', 4),
	(741, 'JUNCAL', 59, 'SCL', 'ZE', 8),
	(742, 'LAS MELOSAS', 318, 'SCL', 'ZE', 4),
	(743, 'LO VALDES', 318, 'SCL', 'ZE', 4),
	(744, 'LONCHA', 322, 'SCL', 'ZE', 4),
	(745, 'LOS MAITENES', 318, 'SCL', 'ZE', 4),
	(746, 'LOS QUELTEHUES', 318, 'SCL', 'ZE', 4),
	(747, 'PORTILLO', 59, 'SCL', 'ZE', 8),
	(748, 'RUNGUE', 65, 'SCL', 'ZE', 8),
	(749, 'SAN ALFONSO', 194, 'SCL', 'ZE', 8),
	(750, 'SAN GABRIEL', 318, 'SCL', 'ZE', 4),
	(751, 'SAN JOSE DE MAIPO', 318, 'SCL', 'ZE', 4),
	(752, 'SAN PEDRO', 15, 'SCL', 'ZE', 8),
	(753, 'TRES FUERTES', 67, 'SCL', 'ZE', 8),
	(754, 'VILLA DEL VALLE', 318, 'SCL', 'ZE', 4),
	(755, 'EL MEMBRILLO', 322, 'SCL', 'ZME', 4),
	(756, 'GUARDIA VIEJA', 59, 'SCL', 'ZME', 8),
	(757, 'ISLA DE PASCUA', 82, 'SCL', 'ZME', 8),
	(758, 'ISLA JUAN FERNANDEZ', 48, 'SCL', 'ZME', 8),
	(759, 'LA CRUZ', 322, 'SCL', 'ZME', 4),
	(760, 'SALADILLO', 59, 'SCL', 'ZME', 8),
	(761, 'RANCAGUA', 83, 'RCG', 'ZB', 8),
	(762, 'CERRILLOS', 284, 'RCG', 'ZI', 4),
	(763, 'LA COMPANêA', 84, 'RCG', 'ZI', 12),
	(764, 'LA LEONERA', 84, 'RCG', 'ZI', 8),
	(765, 'LA PUNTA', 84, 'RCG', 'ZI', 8),
	(766, 'LAS NIEVES', 97, 'RCG', 'ZI', 8),
	(767, 'LOS MAQUIS', 97, 'RCG', 'ZI', 8),
	(768, 'OLIVAR ALTO', 93, 'RCG', 'ZI', 8),
	(769, 'OLIVAR BAJO', 93, 'RCG', 'ZI', 8),
	(770, 'PANIAHUE', 109, 'RCG', 'ZI', 8),
	(771, 'PIMPINELA', 98, 'RCG', 'ZI', 8),
	(772, 'POPETA', 97, 'RCG', 'ZI', 8),
	(773, 'PUREN VI', 87, 'RCG', 'ZI', 12),
	(774, 'AGUA BUENA', 100, 'RCG', 'ZE', 8),
	(775, 'CHAPA VERDE', 91, 'RCG', 'ZE', 8),
	(776, 'CHIMBARONGO', 102, 'RCG', 'ZE', 8),
	(777, 'COCALAN', 89, 'RCG', 'ZE', 8),
	(778, 'CODEGUA', 84, 'RCG', 'ZE', 8),
	(779, 'COINCO', 85, 'RCG', 'ZE', 8),
	(780, 'COLON', 91, 'RCG', 'ZE', 8),
	(781, 'COLTAUCO', 86, 'RCG', 'ZE', 8),
	(782, 'COYA', 91, 'RCG', 'ZE', 8),
	(783, 'CUNACO', 109, 'RCG', 'ZE', 8),
	(784, 'DOIHUE', 87, 'RCG', 'ZE', 12),
	(785, 'EL MANZANO', 89, 'RCG', 'ZE', 8),
	(786, 'EL TOCO', 95, 'RCG', 'ZE', 8),
	(787, 'GRANEROS', 88, 'RCG', 'ZE', 8),
	(788, 'HACIENDA LOS LINGUES', 97, 'RCG', 'ZE', 8),
	(789, 'LA CEBADA', 89, 'RCG', 'ZE', 8),
	(790, 'LARMAHUE', 95, 'RCG', 'ZE', 8),
	(791, 'LAS CABRAS', 89, 'RCG', 'ZE', 8),
	(792, 'LIHUEIMO', 106, 'RCG', 'ZE', 8),
	(793, 'LO MIRANDA', 93, 'RCG', 'ZE', 8),
	(794, 'MACHALI', 91, 'RCG', 'ZE', 8),
	(795, 'MALLOA', 92, 'RCG', 'ZE', 8),
	(796, 'MILLAHUE', 99, 'RCG', 'ZE', 8),
	(797, 'MINA LA JUANITA', 91, 'RCG', 'ZE', 8),
	(798, 'NANCAGUA', 104, 'RCG', 'ZE', 8),
	(799, 'PALMILLA', 135, 'RCG', 'ZE', 8),
	(800, 'PELEQUEN', 97, 'RCG', 'ZE', 8),
	(801, 'PERALILLO', 106, 'RCG', 'ZE', 8),
	(802, 'PEUMO', 94, 'RCG', 'ZE', 8),
	(803, 'PICHIDEGUA', 95, 'RCG', 'ZE', 8),
	(804, 'PICHILEMU', 110, 'RCG', 'ZE', 8),
	(805, 'PLACILLA', 107, 'RCG', 'ZE', 8),
	(806, 'PUENTE NEGRO', 100, 'RCG', 'ZE', 8),
	(807, 'PUNTA VERDE', 89, 'RCG', 'ZE', 8),
	(808, 'QUINTA DE TILCOCO', 96, 'RCG', 'ZE', 12),
	(809, 'RENGO', 97, 'RCG', 'ZE', 8),
	(810, 'REQUINOA', 98, 'RCG', 'ZE', 8),
	(811, 'RINCONADA DE YAQUIL', 109, 'RCG', 'ZE', 8),
	(812, 'ROMA', 100, 'RCG', 'ZE', 8),
	(813, 'ROSARIO', 98, 'RCG', 'ZE', 8),
	(814, 'SAN FCO DE MOSTAZAL', 90, 'RCG', 'ZE', 12),
	(815, 'SAN FERNANDO', 100, 'RCG', 'ZE', 8),
	(816, 'SAN JOSE DE MARCHIHUE', 113, 'RCG', 'ZE', 8),
	(817, 'SAN VICENTE DE T.T', 99, 'RCG', 'ZE', 12),
	(818, 'SANTA CRUZ', 109, 'RCG', 'ZE', 8),
	(819, 'TERMAS DE CAUQUENES', 100, 'RCG', 'ZE', 8),
	(820, 'TROMPETILLA', 100, 'RCG', 'ZE', 8),
	(821, 'TUNCA ARRIBA', 94, 'RCG', 'ZE', 8),
	(822, 'ZUNIGA', 86, 'RCG', 'ZE', 8),
	(823, 'ALCONES', 113, 'RCG', 'ZME', 8),
	(824, 'ALTO COLORADO', 110, 'RCG', 'ZME', 8),
	(825, 'BOYERUCA', 115, 'RCG', 'ZME', 8),
	(826, 'BUCALEMU', 115, 'RCG', 'ZME', 8),
	(827, 'CAHUIL', 110, 'RCG', 'ZME', 8),
	(828, 'CALETONES', 91, 'RCG', 'ZME', 8),
	(829, 'CIRUELOS', 341, 'RCG', 'ZME', 8),
	(830, 'EL CARMEN', 152, 'RCG', 'ZME', 8),
	(831, 'EL GUAICO', 103, 'RCG', 'ZME', 8),
	(832, 'EL PUESTO', 110, 'RCG', 'ZME', 8),
	(833, 'ESPERANZA', 113, 'RCG', 'ZME', 8),
	(834, 'LA LAJUELA', 109, 'RCG', 'ZME', 8),
	(835, 'LA QUEBRADA', 113, 'RCG', 'ZME', 8),
	(836, 'LA RUFINA', 100, 'RCG', 'ZME', 8),
	(837, 'LAS PATAGUAS', 113, 'RCG', 'ZME', 8),
	(838, 'LO VALDIVIA', 115, 'RCG', 'ZME', 8),
	(839, 'LOLOL', 103, 'RCG', 'ZME', 8),
	(840, 'MARCHANT', 113, 'RCG', 'ZME', 8),
	(841, 'MARCHIHUE', 113, 'RCG', 'ZME', 8),
	(842, 'NERQUIHUE', 109, 'RCG', 'ZME', 8),
	(843, 'NILAHUE', 108, 'RCG', 'ZME', 8),
	(844, 'PAREDONES', 115, 'RCG', 'ZME', 8),
	(845, 'POBLACION', 106, 'RCG', 'ZME', 8),
	(846, 'PUMANQUE', 108, 'RCG', 'ZME', 8),
	(847, 'PUNTA DE CORTES', 83, 'RCG', 'ZME', 8),
	(848, 'SANTA GRACIELA DE ALCONES', 110, 'RCG', 'ZME', 8),
	(849, 'SEWELL', 91, 'RCG', 'ZME', 8),
	(850, 'SIERRA BELLAVISTA', 100, 'RCG', 'ZME', 8),
	(851, 'TERMAS DEL FLACO', 100, 'RCG', 'ZME', 8),
	(852, 'TINGUIRIRICA', 100, 'RCG', 'ZME', 8),
	(853, 'TALCA', 125, 'ZCA', 'ZB', 8),
	(854, 'ASTILLERO', 126, 'ZCA', 'ZI', 8),
	(855, 'AURORA ', 128, 'ZCA', 'ZI', 8),
	(856, 'BATUCO', 334, 'ZCA', 'ZI', 4),
	(857, 'BOBADILLA', 142, 'ZCA', 'ZI', 12),
	(858, 'BOTALCURA', 125, 'ZCA', 'ZI', 8),
	(859, 'CAUQUENES', 143, 'ZCA', 'ZI', 8),
	(860, 'COIPUE', 132, 'ZCA', 'ZI', 8),
	(861, 'COLBUN', 137, 'ZCA', 'ZI', 8),
	(862, 'COLIN', 125, 'ZCA', 'ZI', 8),
	(863, 'CONSTITUCION', 133, 'ZCA', 'ZI', 8),
	(864, 'CORINTO', 125, 'ZCA', 'ZI', 8),
	(865, 'CORRALONES', 128, 'ZCA', 'ZI', 8),
	(866, 'CUMPEO', 127, 'ZCA', 'ZI', 8),
	(867, 'CURICO', 116, 'ZCA', 'ZI', 8),
	(868, 'CURTIDURIA', 125, 'ZCA', 'ZI', 8),
	(869, 'EL COLORADO', 128, 'ZCA', 'ZI', 8),
	(870, 'GUALLECO', 132, 'ZCA', 'ZI', 8),
	(871, 'ITAHUE', 127, 'ZCA', 'ZI', 8),
	(872, 'LINARES', 135, 'ZCA', 'ZI', 8),
	(873, 'LITU', 130, 'ZCA', 'ZI', 8),
	(874, 'LONGAVI', 138, 'ZCA', 'ZI', 8),
	(875, 'LONTUE', 119, 'ZCA', 'ZI', 8),
	(876, 'LOS NICHES', 116, 'ZCA', 'ZI', 12),
	(877, 'MAULE', 129, 'ZCA', 'ZI', 8),
	(878, 'MOLINA', 119, 'ZCA', 'ZI', 8),
	(879, 'PANIMAVIDA', 136, 'ZCA', 'ZI', 8),
	(880, 'PARRAL', 139, 'ZCA', 'ZI', 8),
	(881, 'PELARCO', 126, 'ZCA', 'ZI', 8),
	(882, 'PENCAHUE', 132, 'ZCA', 'ZI', 8),
	(883, 'RAUCO', 124, 'ZCA', 'ZI', 8),
	(884, 'RIO CLARO', 127, 'ZCA', 'ZI', 8),
	(885, 'ROMERAL', 118, 'ZCA', 'ZI', 8),
	(886, 'SAGRADA FAMILIA', 120, 'ZCA', 'ZI', 8),
	(887, 'SAN CLEMENTE', 128, 'ZCA', 'ZI', 8),
	(888, 'SAN JAVIER', 142, 'ZCA', 'ZI', 8),
	(889, 'SAN RAFAEL', 130, 'ZCA', 'ZI', 8),
	(890, 'SANTA ANA', 137, 'ZCA', 'ZI', 8),
	(891, 'TENO', 117, 'ZCA', 'ZI', 8),
	(892, 'VILLA ALEGRE', 193, 'ZCA', 'ZI', 8),
	(893, 'YERBAS BUENAS', 136, 'ZCA', 'ZI', 8),
	(894, 'AJIAL', 139, 'ZCA', 'ZE', 8),
	(895, 'ARBOLILLO', 141, 'ZCA', 'ZE', 8),
	(896, 'CHEPICA', 101, 'ZCA', 'ZE', 8),
	(897, 'CONVENTO VIEJO', 102, 'ZCA', 'ZE', 8),
	(898, 'EL PENON', 32, 'ZCA', 'ZE', 8),
	(899, 'LA MONTANA', 117, 'ZCA', 'ZE', 8),
	(900, 'LAS CAMPANAS', 141, 'ZCA', 'ZE', 8),
	(901, 'MELOZAL', 141, 'ZCA', 'ZE', 8),
	(902, 'NIRIVILO', 131, 'ZCA', 'ZE', 8),
	(903, 'PALMILLA', 135, 'ZCA', 'ZE', 8),
	(904, 'PASO NEVADO', 128, 'ZCA', 'ZE', 8),
	(905, 'PERQUILAUQUEN', 139, 'ZCA', 'ZE', 8),
	(906, 'QUINAHUE', 101, 'ZCA', 'ZE', 8),
	(907, 'QUINTA', 102, 'ZCA', 'ZE', 8),
	(908, 'RETIRO', 32, 'ZCA', 'ZE', 8),
	(909, 'ADUANA  ', 119, 'ZCA', 'ZME', 8),
	(910, 'ADUANA PEJERREY', 135, 'ZCA', 'ZME', 8),
	(911, 'ARMERILLO', 128, 'ZCA', 'ZME', 8),
	(912, 'AUQUINCO', 101, 'ZCA', 'ZME', 8),
	(913, 'BARRERA', 137, 'ZCA', 'ZME', 8),
	(914, 'BULLILLEO', 139, 'ZCA', 'ZME', 8),
	(915, 'CAMPAMENTO ANCOA', 135, 'ZCA', 'ZME', 8),
	(916, 'CHANCO', 145, 'ZCA', 'ZME', 8),
	(917, 'CULENAR', 117, 'ZCA', 'ZME', 8),
	(918, 'CURANIPE', 145, 'ZCA', 'ZME', 8),
	(919, 'CUREPTO', 134, 'ZCA', 'ZME', 8),
	(920, 'DUAO', 129, 'ZCA', 'ZME', 8),
	(921, 'EL MANZANO', 89, 'ZCA', 'ZME', 8),
	(922, 'EL PLANCHON', 118, 'ZCA', 'ZME', 8),
	(923, 'EL SALTO', 135, 'ZCA', 'ZME', 8),
	(924, 'EL TRANSITO', 29, 'ZCA', 'ZME', 8),
	(925, 'EMPEDRADO', 131, 'ZCA', 'ZME', 8),
	(926, 'ENDESA ', 128, 'ZCA', 'ZME', 8),
	(927, 'HUALANE', 121, 'ZCA', 'ZME', 8),
	(928, 'HUALVE', 143, 'ZCA', 'ZME', 8),
	(929, 'HUEMUL', 102, 'ZCA', 'ZME', 8),
	(930, 'ILOCA', 123, 'ZCA', 'ZME', 8),
	(931, 'JUNQUILLAR', 133, 'ZCA', 'ZME', 8),
	(932, 'LA LORA', 134, 'ZCA', 'ZME', 8),
	(933, 'LA MINA', 128, 'ZCA', 'ZME', 8),
	(934, 'LA TRINCHERA', 123, 'ZCA', 'ZME', 8),
	(935, 'LAS GARZAS', 128, 'ZCA', 'ZME', 8),
	(936, 'LICANTEN', 122, 'ZCA', 'ZME', 8),
	(937, 'LIPIMAVIDA', 123, 'ZCA', 'ZME', 8),
	(938, 'LLEPO', 135, 'ZCA', 'ZME', 8),
	(939, 'LLICO', 193, 'ZCA', 'ZME', 8),
	(940, 'LOS CRISTALES', 138, 'ZCA', 'ZME', 8),
	(941, 'LOS NABOS', 143, 'ZCA', 'ZME', 8),
	(942, 'LOS PERALES', 143, 'ZCA', 'ZME', 8),
	(943, 'LOS QUENES', 118, 'ZCA', 'ZME', 8),
	(944, 'LOS RABONES', 135, 'ZCA', 'ZME', 8),
	(945, 'MELADO', 135, 'ZCA', 'ZME', 8),
	(946, 'MELAO', 138, 'ZCA', 'ZME', 8),
	(947, 'MESAMAVIDA', 138, 'ZCA', 'ZME', 8),
	(948, 'MIRAFLORES', 28, 'ZCA', 'ZME', 8),
	(949, 'MONTE OSCURO', 116, 'ZCA', 'ZME', 8),
	(950, 'MORZA', 102, 'ZCA', 'ZME', 8),
	(951, 'PALQUIBUDA', 124, 'ZCA', 'ZME', 12),
	(952, 'PASO HONDO', 170, 'ZCA', 'ZME', 8),
	(953, 'PELLUHUE', 144, 'ZCA', 'ZME', 8),
	(954, 'PICHAMAN', 132, 'ZCA', 'ZME', 8),
	(955, 'PICHIBUDI', 123, 'ZCA', 'ZME', 8),
	(956, 'POTRERO GRANDE', 116, 'ZCA', 'ZME', 8),
	(957, 'POTRERO GRANDE CHICO', 118, 'ZCA', 'ZME', 8),
	(958, 'PUTU', 133, 'ZCA', 'ZME', 8),
	(959, 'QUEBRADA HONDA', 116, 'ZCA', 'ZME', 8),
	(960, 'QUELLA', 143, 'ZCA', 'ZME', 8),
	(961, 'QUINCHIMAVIDA', 139, 'ZCA', 'ZME', 8),
	(962, 'QUINMAVIDA', 136, 'ZCA', 'ZME', 8),
	(963, 'QUIVOLGO', 133, 'ZCA', 'ZME', 8),
	(964, 'RADAL', 151, 'ZCA', 'ZME', 8),
	(965, 'RANGUIL', 103, 'ZCA', 'ZME', 8),
	(966, 'REBECA', 117, 'ZCA', 'ZME', 8),
	(967, 'ROBLERIA', 135, 'ZCA', 'ZME', 8),
	(968, 'SAN PABLO', 30, 'ZCA', 'ZME', 8),
	(969, 'SAN PEDRO DE ALCANTARA', 103, 'ZCA', 'ZME', 8),
	(970, 'TERMAS DE CATILLO', 139, 'ZCA', 'ZME', 8),
	(971, 'TRINCAO', 256, 'ZCA', 'ZME', 8),
	(972, 'UNICAVEN', 143, 'ZCA', 'ZME', 8),
	(973, 'UPEO', 116, 'ZCA', 'ZME', 8),
	(974, 'VICHUQUEN', 123, 'ZCA', 'ZME', 8),
	(975, 'VILLA PRAT', 120, 'ZCA', 'ZME', 8),
	(976, 'VILLA ROSAS', 139, 'ZCA', 'ZME', 8),
	(977, 'VILLA SECA', 138, 'ZCA', 'ZME', 8),
	(978, 'YACEL', 119, 'ZCA', 'ZME', 8),
	(979, 'LOS ANGELES', 167, 'LSQ', 'ZB', 8),
	(980, 'ANGOL', 200, 'LSQ', 'ZI', 8),
	(981, 'ANTUCO', 169, 'LSQ', 'ZI', 8),
	(982, 'CABRERO', 170, 'LSQ', 'ZI', 8),
	(983, 'CANICURA', 176, 'LSQ', 'ZI', 8),
	(984, 'CANTERAS', 176, 'LSQ', 'ZI', 8),
	(985, 'CHOROICO', 173, 'LSQ', 'ZI', 8),
	(986, 'COIHUE', 173, 'LSQ', 'ZI', 8),
	(987, 'COLLIPULLI', 201, 'LSQ', 'ZI', 8),
	(988, 'COLONIA', 171, 'LSQ', 'ZI', 8),
	(989, 'DIUQUIN', 173, 'LSQ', 'ZI', 8),
	(990, 'EL ALAMO', 167, 'LSQ', 'ZI', 8),
	(991, 'EL AVELLANO', 172, 'LSQ', 'ZI', 8),
	(992, 'EL GUACHI', 177, 'LSQ', 'ZI', 8),
	(993, 'EL MORRO', 172, 'LSQ', 'ZI', 8),
	(994, 'ESTACION YUMBEL', 180, 'LSQ', 'ZI', 8),
	(995, 'HUEPIL', 179, 'LSQ', 'ZI', 8),
	(996, 'LAJA', 171, 'LSQ', 'ZI', 8),
	(997, 'LAS NIEVES', 97, 'LSQ', 'ZI', 8),
	(998, 'LONCOPANGUE', 175, 'LSQ', 'ZI', 8),
	(999, 'LOS BRUJOS', 177, 'LSQ', 'ZI', 8),
	(1000, 'LOS MAICAS', 172, 'LSQ', 'ZI', 8),
	(1001, 'MAITENREHUE', 200, 'LSQ', 'ZI', 8),
	(1002, 'MELICA', 172, 'LSQ', 'ZI', 8),
	(1003, 'MININCO', 208, 'LSQ', 'ZI', 8),
	(1004, 'MONTE AGUILA', 180, 'LSQ', 'ZI', 8),
	(1005, 'MULCHEN', 172, 'LSQ', 'ZI', 8),
	(1006, 'NACIMIENTO', 173, 'LSQ', 'ZI', 8),
	(1007, 'NEGRETE', 174, 'LSQ', 'ZI', 8),
	(1008, 'PANGAL', 245, 'LSQ', 'ZI', 8),
	(1009, 'PASO HONDO', 170, 'LSQ', 'ZI', 8),
	(1010, 'PROGRESO', 173, 'LSQ', 'ZI', 8),
	(1011, 'QUILACO', 175, 'LSQ', 'ZI', 8),
	(1012, 'QUILLECO', 176, 'LSQ', 'ZI', 8),
	(1013, 'RENAICO', 208, 'LSQ', 'ZI', 8),
	(1014, 'RIHUE', 174, 'LSQ', 'ZI', 8),
	(1015, 'RIO CLARO', 127, 'LSQ', 'ZI', 8),
	(1016, 'RUCALHUE', 175, 'LSQ', 'ZI', 8),
	(1017, 'SAN CARLOS PUREN', 167, 'LSQ', 'ZI', 12),
	(1018, 'SAN MIGUEL', 172, 'LSQ', 'ZI', 8),
	(1019, 'SAN ROSENDO', 178, 'LSQ', 'ZI', 8),
	(1020, 'SANTA BARBARA', 177, 'LSQ', 'ZI', 8),
	(1021, 'SANTA FE', 173, 'LSQ', 'ZI', 8),
	(1022, 'TIJERAL', 208, 'LSQ', 'ZI', 8),
	(1023, 'TRUPAN', 179, 'LSQ', 'ZI', 8),
	(1024, 'TUCAPEL', 179, 'LSQ', 'ZI', 8),
	(1025, 'VILLA MERCEDES', 176, 'LSQ', 'ZI', 8),
	(1026, 'VILLUCURA', 177, 'LSQ', 'ZI', 8),
	(1027, 'YUMBEL', 180, 'LSQ', 'ZI', 8),
	(1028, 'ALTO CALEDONIA', 175, 'LSQ', 'ZE', 8),
	(1029, 'BUENURAQUI', 178, 'LSQ', 'ZE', 8),
	(1030, 'CANADA', 201, 'LSQ', 'ZE', 8),
	(1031, 'CERRO DEL PADRE', 175, 'LSQ', 'ZE', 8),
	(1032, 'CURACO ', 201, 'LSQ', 'ZE', 8),
	(1033, 'EL ABANICO', 169, 'LSQ', 'ZE', 8),
	(1034, 'EL TORO', 169, 'LSQ', 'ZE', 8),
	(1035, 'LOLCO', 177, 'LSQ', 'ZE', 8),
	(1036, 'LOLENCO', 201, 'LSQ', 'ZE', 8),
	(1037, 'LOS PLACERES', 177, 'LSQ', 'ZE', 8),
	(1038, 'NANCO', 201, 'LSQ', 'ZE', 8),
	(1039, 'PIEDRA DEL AGUILA', 200, 'LSQ', 'ZE', 8),
	(1040, 'RALCO', 168, 'LSQ', 'ZE', 8),
	(1041, 'VEGAS BLANCAS', 200, 'LSQ', 'ZE', 8),
	(1042, 'ALTO BIO BIO', 168, 'LSQ', 'ZME', 12),
	(1043, 'CASA LOLCO', 168, 'LSQ', 'ZME', 8),
	(1044, 'COMUNIDAD CANICU', 168, 'LSQ', 'ZME', 8),
	(1045, 'EL AMARGO', 201, 'LSQ', 'ZME', 8),
	(1046, 'LOS BARROS', 169, 'LSQ', 'ZME', 8),
	(1047, 'MAITENES', 172, 'LSQ', 'ZME', 8),
	(1048, 'PANGUE', 168, 'LSQ', 'ZME', 8),
	(1049, 'POLCURA', 179, 'LSQ', 'ZME', 8),
	(1050, 'RALCO LEPOY', 168, 'LSQ', 'ZME', 8),
	(1051, 'SANTA CLARA', 170, 'LSQ', 'ZME', 8),
	(1052, 'TERMAS DE PEMEHUE', 201, 'LSQ', 'ZME', 8),
	(1053, 'TERMAS DEL AVELLANO', 168, 'LSQ', 'ZME', 8),
	(1054, 'TRINTRE', 201, 'LSQ', 'ZME', 8),
	(1055, 'CHILLAN', 146, 'YAI', 'ZB', 8),
	(1056, 'CHILLAN VIEJO', 148, 'YAI', 'ZB', 8),
	(1057, 'CACHAPOAL', 161, 'YAI', 'ZI', 8),
	(1058, 'COIPIN', 153, 'YAI', 'ZI', 8),
	(1059, 'EL CARMEN', 152, 'YAI', 'ZI', 8),
	(1060, 'EL SAUCE', 161, 'YAI', 'ZI', 8),
	(1061, 'GENERAL CRUZ', 155, 'YAI', 'ZI', 8),
	(1062, 'LA CAPILLA', 151, 'YAI', 'ZI', 8),
	(1063, 'LAS TRANCAS', 163, 'YAI', 'ZI', 8),
	(1064, 'LIUCURA', 170, 'YAI', 'ZI', 8),
	(1065, 'MINAS DEL PRADO', 151, 'YAI', 'ZI', 8),
	(1066, 'NAHUELTORO', 161, 'YAI', 'ZI', 8),
	(1067, 'NIPAS', 150, 'YAI', 'ZI', 8),
	(1068, 'NIQUEN', 154, 'YAI', 'ZI', 8),
	(1069, 'NUEVA ALDEA', 150, 'YAI', 'ZI', 8),
	(1070, 'POCILLAS', 153, 'YAI', 'ZI', 8),
	(1071, 'PUEBLO SECO', 152, 'YAI', 'ZI', 8),
	(1072, 'QUINCHAMALI', 147, 'YAI', 'ZI', 8),
	(1073, 'QUIRIQUINA', 152, 'YAI', 'ZI', 8),
	(1074, 'RANQUIL', 160, 'YAI', 'ZI', 8),
	(1075, 'RUCAPEQUEN', 148, 'YAI', 'ZI', 8),
	(1076, 'SAN GREGORIO NIQUEN', 161, 'YAI', 'ZI', 12),
	(1077, 'SAN MIGUEL', 172, 'YAI', 'ZI', 8),
	(1078, 'SAN PEDRO', 15, 'YAI', 'ZI', 8),
	(1079, 'SANTA CLARA', 170, 'YAI', 'ZI', 8),
	(1080, 'TANILVORO', 151, 'YAI', 'ZI', 8),
	(1081, 'TORRECILLA', 153, 'YAI', 'ZI', 8),
	(1082, 'ZEMITA', 161, 'YAI', 'ZI', 8),
	(1083, 'CAMPANARIO', 166, 'YAI', 'ZE', 8),
	(1084, 'CANCHA ALEGRE', 153, 'YAI', 'ZE', 8),
	(1085, 'CHOLGUAN', 166, 'YAI', 'ZE', 8),
	(1086, 'COBQUECURA', 149, 'YAI', 'ZE', 8),
	(1087, 'CONFLUENCIA', 157, 'YAI', 'ZE', 8),
	(1088, 'EL PARRON', 153, 'YAI', 'ZE', 8),
	(1089, 'EL SALTILLO', 166, 'YAI', 'ZE', 8),
	(1090, 'FUNDO LOS ROBLES', 151, 'YAI', 'ZE', 8),
	(1091, 'LA PUNTILLA', 162, 'YAI', 'ZE', 8),
	(1092, 'LOS CASTANOS', 152, 'YAI', 'ZE', 8),
	(1093, 'LOS PUQUIOS', 162, 'YAI', 'ZE', 8),
	(1094, 'PASO HONDO', 170, 'YAI', 'ZE', 8),
	(1095, 'RECINTO', 163, 'YAI', 'ZE', 8),
	(1096, 'SAN FABIAN DE ALICO', 162, 'YAI', 'ZE', 8),
	(1097, 'TERMAS DE CHILLAN', 163, 'YAI', 'ZE', 8),
	(1098, 'TOMECO', 170, 'YAI', 'ZE', 8),
	(1099, 'TREGUALEMU', 152, 'YAI', 'ZE', 8),
	(1100, 'YUNGAY', 166, 'YAI', 'ZE', 8),
	(1101, 'BUCHUPUREO', 149, 'YAI', 'ZME', 8),
	(1102, 'BULNES', 147, 'YAI', 'ZME', 8),
	(1103, 'COIHUECO', 151, 'YAI', 'ZME', 8),
	(1104, 'COLMUYAO', 165, 'YAI', 'ZME', 8),
	(1105, 'MELA', 165, 'YAI', 'ZME', 8),
	(1106, 'NINHUE', 153, 'YAI', 'ZME', 8),
	(1107, 'PEMUCO', 155, 'YAI', 'ZME', 8),
	(1108, 'PINTO', 156, 'YAI', 'ZME', 8),
	(1109, 'PORTEZUELO', 157, 'YAI', 'ZME', 8),
	(1110, 'PULLAY', 149, 'YAI', 'ZME', 8),
	(1111, 'QUILLON', 158, 'YAI', 'ZME', 8),
	(1112, 'QUIRIHUE', 159, 'YAI', 'ZME', 8),
	(1113, 'SAN CARLOS', 161, 'YAI', 'ZME', 8),
	(1114, 'SAN IGNACIO', 150, 'YAI', 'ZME', 8),
	(1115, 'SAN NICOLAS', 164, 'YAI', 'ZME', 8),
	(1116, 'ZAPALLAR', 42, 'YAI', 'ZME', 8),
	(1117, 'CONCEPCION', 181, 'CCP', 'ZB', 8),
	(1118, 'RANCHO TALCAHUANO', 191, 'CCP', 'ZB', 8),
	(1119, 'SAN PEDRO DE LA PAZ', 189, 'CCP', 'ZB', 8),
	(1120, 'TALCAHUANO', 191, 'CCP', 'ZB', 8),
	(1121, 'AGUAS DE LA GLORIA', 184, 'CCP', 'ZI', 8),
	(1122, 'ARAUCO', 193, 'CCP', 'ZI', 8),
	(1123, 'CARAMPANGUE', 193, 'CCP', 'ZI', 8),
	(1124, 'CHIGUAYANTE', 182, 'CCP', 'ZI', 8),
	(1125, 'COELEMU', 150, 'CCP', 'ZI', 8),
	(1126, 'COPIULEMU', 184, 'CCP', 'ZI', 8),
	(1127, 'CORONEL', 183, 'CCP', 'ZI', 8),
	(1128, 'DICHATO', 192, 'CCP', 'ZI', 8),
	(1129, 'EL BOLDO', 193, 'CCP', 'ZI', 8),
	(1130, 'HUALPEN', 185, 'CCP', 'ZI', 8),
	(1131, 'HUALQUI', 186, 'CCP', 'ZI', 8),
	(1132, 'LA BOCA', 185, 'CCP', 'ZI', 8),
	(1133, 'LARAQUETE', 193, 'CCP', 'ZI', 8),
	(1134, 'LIRQUEN', 188, 'CCP', 'ZI', 8),
	(1135, 'LOTA', 187, 'CCP', 'ZI', 8),
	(1136, 'MENQUE', 192, 'CCP', 'ZI', 8),
	(1137, 'PENCO', 188, 'CCP', 'ZI', 8),
	(1138, 'QUILACOYA', 186, 'CCP', 'ZI', 8),
	(1139, 'RAFAEL', 192, 'CCP', 'ZI', 8),
	(1140, 'RAMADILLA', 193, 'CCP', 'ZI', 8),
	(1141, 'RANGUELMO', 181, 'CCP', 'ZI', 8),
	(1142, 'RAQUI', 193, 'CCP', 'ZI', 8),
	(1143, 'RAQUIL', 150, 'CCP', 'ZI', 8),
	(1144, 'ROA', 188, 'CCP', 'ZI', 8),
	(1145, 'SAN VICENTE', 191, 'CCP', 'ZI', 8),
	(1146, 'TALCAMAVIDA', 186, 'CCP', 'ZI', 8),
	(1147, 'TOME', 192, 'CCP', 'ZI', 8),
	(1148, 'VILLA ALEGRE', 193, 'CCP', 'ZI', 8),
	(1149, 'ANTIGUALA', 198, 'CCP', 'ZE', 8),
	(1150, 'CANETE', 194, 'CCP', 'ZE', 8),
	(1151, 'CONAIR', 150, 'CCP', 'ZE', 8),
	(1152, 'CONTULMO', 195, 'CCP', 'ZE', 8),
	(1153, 'CURANILAHUE', 196, 'CCP', 'ZE', 8),
	(1154, 'FLORIDA', 184, 'CCP', 'ZE', 8),
	(1155, 'ISLA QUIRIQUINA', 191, 'CCP', 'ZE', 8),
	(1156, 'LEBU', 197, 'CCP', 'ZE', 8),
	(1157, 'LLICO', 193, 'CCP', 'ZE', 8),
	(1158, 'LOS ALAMOS', 198, 'CCP', 'ZE', 8),
	(1159, 'PILPILCO', 198, 'CCP', 'ZE', 8),
	(1160, 'SAN IGNACIO', 150, 'CCP', 'ZE', 8),
	(1161, 'SAN JOSE DE COLICO', 196, 'CCP', 'ZE', 8),
	(1162, 'SANTA JUANA', 190, 'CCP', 'ZE', 8),
	(1163, 'VEGAS DE ITATA', 192, 'CCP', 'ZE', 8),
	(1164, 'CAYUCUPIL', 194, 'CCP', 'ZME', 8),
	(1165, 'CURACO', 197, 'CCP', 'ZME', 8),
	(1166, 'MILLONHUE', 197, 'CCP', 'ZME', 8),
	(1167, 'PEHUEN', 197, 'CCP', 'ZME', 8),
	(1168, 'QUIDICO', 193, 'CCP', 'ZME', 8),
	(1169, 'QUINAHUE', 101, 'CCP', 'ZME', 8),
	(1170, 'RANQUILCO', 197, 'CCP', 'ZME', 8),
	(1171, 'RUCARAQUIL', 197, 'CCP', 'ZME', 8),
	(1172, 'SAN ALFONSO', 194, 'CCP', 'ZME', 8),
	(1173, 'SARA DE LEBU', 197, 'CCP', 'ZME', 8),
	(1174, 'TREHUACO', 165, 'CCP', 'ZME', 8),
	(1175, 'TRES PINOS', 198, 'CCP', 'ZME', 8),
	(1176, 'YENECO', 197, 'CCP', 'ZME', 8),
	(1177, 'TEMUCO', 211, 'ZCO', 'ZB', 8),
	(1178, 'AGUA SANTA', 219, 'ZCO', 'ZI', 8),
	(1179, 'ALMAGRO', 222, 'ZCO', 'ZI', 8),
	(1180, 'BOROA', 222, 'ZCO', 'ZI', 8),
	(1181, 'CAJON', 211, 'ZCO', 'ZI', 8),
	(1182, 'CARAHUE', 212, 'ZCO', 'ZI', 8),
	(1183, 'CHERQUENCO', 230, 'ZCO', 'ZI', 8),
	(1184, 'COLONIA LAUTARO', 219, 'ZCO', 'ZI', 8),
	(1185, 'CUNCO', 214, 'ZCO', 'ZI', 8),
	(1186, 'EL ALAMBRADO', 223, 'ZCO', 'ZI', 8),
	(1187, 'ERCILLA', 203, 'ZCO', 'ZI', 8),
	(1188, 'FREIRE', 216, 'ZCO', 'ZI', 8),
	(1189, 'GALVARINO', 217, 'ZCO', 'ZI', 8),
	(1190, 'GENERAL LOPEZ', 211, 'ZCO', 'ZI', 8),
	(1191, 'GORBEA', 218, 'ZCO', 'ZI', 8),
	(1192, 'LABRANZA', 211, 'ZCO', 'ZI', 8),
	(1193, 'LANCO', 338, 'ZCO', 'ZI', 8),
	(1194, 'LAS HORTENCIAS', 214, 'ZCO', 'ZI', 8),
	(1195, 'LAS MARIPOSAS', 319, 'ZCO', 'ZI', 4),
	(1196, 'LAUTARO', 219, 'ZCO', 'ZI', 8),
	(1197, 'LONCOCHE', 220, 'ZCO', 'ZI', 8),
	(1198, 'LOS PRADOS', 219, 'ZCO', 'ZI', 8),
	(1199, 'METRENCO', 211, 'ZCO', 'ZI', 8),
	(1200, 'MISION BOROA', 216, 'ZCO', 'ZI', 8),
	(1201, 'NUEVA IMPERIAL', 222, 'ZCO', 'ZI', 8),
	(1202, 'PADRE LAS CASAS', 223, 'ZCO', 'ZI', 8),
	(1203, 'PAILACO', 199, 'ZCO', 'ZI', 8),
	(1204, 'PEDREGOSO', 231, 'ZCO', 'ZI', 8),
	(1205, 'PERQUENCO', 224, 'ZCO', 'ZI', 8),
	(1206, 'PILLANLELBUN', 211, 'ZCO', 'ZI', 8),
	(1207, 'PITRUFQUEN', 225, 'ZCO', 'ZI', 8),
	(1208, 'PUA', 210, 'ZCO', 'ZI', 8),
	(1209, 'PUCON', 226, 'ZCO', 'ZI', 8),
	(1210, 'PUERTO SAAVEDRA', 227, 'ZCO', 'ZI', 8),
	(1211, 'QUEPE', 216, 'ZCO', 'ZI', 8),
	(1212, 'QUINQUEN', 204, 'ZCO', 'ZI', 8),
	(1213, 'QUITRATUE', 218, 'ZCO', 'ZI', 8),
	(1214, 'RADAL', 151, 'ZCO', 'ZI', 8),
	(1215, 'RETEN DOLLINCO', 219, 'ZCO', 'ZI', 8),
	(1216, 'SAN PATRICIO', 230, 'ZCO', 'ZI', 8),
	(1217, 'SAN PEDRO', 15, 'ZCO', 'ZI', 8),
	(1218, 'TEODORO SCHMIDT', 228, 'ZCO', 'ZI', 8),
	(1219, 'TOLTEN', 229, 'ZCO', 'ZI', 8),
	(1220, 'TRAIGUEN', 209, 'ZCO', 'ZI', 8),
	(1221, 'VICTORIA', 210, 'ZCO', 'ZI', 8),
	(1222, 'VILCUN', 230, 'ZCO', 'ZI', 8),
	(1223, 'VILLARRICA', 231, 'ZCO', 'ZI', 8),
	(1224, 'BARROS ARANA', 228, 'ZCO', 'ZE', 8),
	(1225, 'BOCA BUDI', 227, 'ZCO', 'ZE', 8),
	(1226, 'CAMARONES', 2, 'ZCO', 'ZE', 8),
	(1227, 'CAPITAN PASTENE', 206, 'ZCO', 'ZE', 8),
	(1228, 'CHOLCHOL', 213, 'ZCO', 'ZE', 8),
	(1229, 'CHOSHUENCO', 343, 'ZCO', 'ZE', 8),
	(1230, 'COMUY', 229, 'ZCO', 'ZE', 8),
	(1231, 'CURACAUTIN', 202, 'ZCO', 'ZE', 8),
	(1232, 'HELO SUR', 214, 'ZCO', 'ZE', 8),
	(1233, 'HUALPIN', 229, 'ZCO', 'ZE', 8),
	(1234, 'HUISCAPI', 231, 'ZCO', 'ZE', 8),
	(1235, 'LA SOMBRA', 202, 'ZCO', 'ZE', 8),
	(1236, 'LASTARRIA', 218, 'ZCO', 'ZE', 8),
	(1237, 'LICAN RAY', 343, 'ZCO', 'ZE', 12),
	(1238, 'LOLEN', 204, 'ZCO', 'ZE', 8),
	(1239, 'LOMACURA', 221, 'ZCO', 'ZE', 8),
	(1240, 'LOS LAURELES', 214, 'ZCO', 'ZE', 8),
	(1241, 'LOS SAUCES', 205, 'ZCO', 'ZE', 8),
	(1242, 'LOS TALLOS', 343, 'ZCO', 'ZE', 8),
	(1243, 'LUMACO', 206, 'ZCO', 'ZE', 8),
	(1244, 'MALALCAHUELO ', 202, 'ZCO', 'ZE', 8),
	(1245, 'MANZANAR', 202, 'ZCO', 'ZE', 8),
	(1246, 'MAQUEHUE', 211, 'ZCO', 'ZE', 8),
	(1247, 'MELIPEUCO', 221, 'ZCO', 'ZE', 8),
	(1248, 'NANCUL', 343, 'ZCO', 'ZE', 8),
	(1249, 'NEHUENTUE', 212, 'ZCO', 'ZE', 8),
	(1250, 'PAILAHUENQUE', 203, 'ZCO', 'ZE', 8),
	(1251, 'PANGUIPULLI', 343, 'ZCO', 'ZE', 8),
	(1252, 'PELECO', 228, 'ZCO', 'ZE', 8),
	(1253, 'PICHIPELLAHUEN', 206, 'ZCO', 'ZE', 8),
	(1254, 'PUERTO DOMINGUEZ', 228, 'ZCO', 'ZE', 8),
	(1255, 'PUERTO PUMA', 214, 'ZCO', 'ZE', 8),
	(1256, 'PUREN', 87, 'ZCO', 'ZE', 8),
	(1257, 'QUIDICO', 193, 'ZCO', 'ZE', 8),
	(1258, 'QUILQUE', 229, 'ZCO', 'ZE', 8),
	(1259, 'RELUN', 206, 'ZCO', 'ZE', 8),
	(1260, 'RUCATRARO', 217, 'ZCO', 'ZE', 8),
	(1261, 'SELVA OSCURA', 224, 'ZCO', 'ZE', 8),
	(1262, 'SIERRA NEVADA', 204, 'ZCO', 'ZE', 8),
	(1263, 'TIRUA', 199, 'ZCO', 'ZE', 8),
	(1264, 'TRES ESQUINAS', 210, 'ZCO', 'ZE', 8),
	(1265, 'TROVOLHUE', 212, 'ZCO', 'ZE', 8),
	(1266, 'VILLA ARAUCANIA', 212, 'ZCO', 'ZE', 8),
	(1267, 'ANTIQUINA', 195, 'ZCO', 'ZME', 8),
	(1268, 'CABURGUA', 226, 'ZCO', 'ZME', 8),
	(1269, 'CALAFQUEN', 343, 'ZCO', 'ZME', 8),
	(1270, 'CARIRRINGUE', 343, 'ZCO', 'ZME', 8),
	(1271, 'CENTENARIO', 205, 'ZCO', 'ZME', 8),
	(1272, 'COARIPE', 343, 'ZCO', 'ZME', 12),
	(1273, 'CURARREHUE', 215, 'ZCO', 'ZME', 8),
	(1274, 'ENCO', 343, 'ZCO', 'ZME', 8),
	(1275, 'HUIFE', 226, 'ZCO', 'ZME', 8),
	(1276, 'ICALMA', 221, 'ZCO', 'ZME', 8),
	(1277, 'LIQUINE', 343, 'ZCO', 'ZME', 8),
	(1278, 'LIUCURA', 170, 'ZCO', 'ZME', 8),
	(1279, 'LOBERIA', 212, 'ZCO', 'ZME', 8),
	(1280, 'LONQUIMAY', 204, 'ZCO', 'ZME', 8),
	(1281, 'NAHUELVE', 205, 'ZCO', 'ZME', 8),
	(1282, 'NELTUME', 343, 'ZCO', 'ZME', 8),
	(1283, 'PLAYA NEGRA', 214, 'ZCO', 'ZME', 8),
	(1284, 'PUERTO FUY', 343, 'ZCO', 'ZME', 8),
	(1285, 'PUERTO PIRIHUEICO', 343, 'ZCO', 'ZME', 8),
	(1286, 'PUESCO', 215, 'ZCO', 'ZME', 8),
	(1287, 'PULLINGUE', 343, 'ZCO', 'ZME', 8),
	(1288, 'QUECHEREGUAS', 209, 'ZCO', 'ZME', 8),
	(1289, 'REFUGIO', 226, 'ZCO', 'ZME', 8),
	(1290, 'REFUGIO LLAIMA', 230, 'ZCO', 'ZME', 8),
	(1291, 'REIGOLIL', 215, 'ZCO', 'ZME', 8),
	(1292, 'RINCONADA', 195, 'ZCO', 'ZME', 8),
	(1293, 'SANTA ROSA', 14, 'ZCO', 'ZME', 8),
	(1294, 'TERMAS DE CONARIPE', 343, 'ZCO', 'ZME', 8),
	(1295, 'TERMAS DE HUIFE', 226, 'ZCO', 'ZME', 8),
	(1296, 'TERMAS DE MENETUE', 226, 'ZCO', 'ZME', 8),
	(1297, 'TERMAS DE MOLULCO', 221, 'ZCO', 'ZME', 8),
	(1298, 'TERMAS DE PALGUIN', 226, 'ZCO', 'ZME', 8),
	(1299, 'TERMAS DE PANGUI', 215, 'ZCO', 'ZME', 8),
	(1300, 'TERMAS DE RIO BLANCO', 202, 'ZCO', 'ZME', 8),
	(1301, 'TERMAS DE SAN LUIS', 215, 'ZCO', 'ZME', 8),
	(1302, 'TERMAS DE SAN SEBASTIAN', 214, 'ZCO', 'ZME', 8),
	(1303, 'TERMAS DE TOLHUACA', 210, 'ZCO', 'ZME', 8),
	(1304, 'TROYO', 204, 'ZCO', 'ZME', 8),
	(1305, 'LOS MOLINOS', 336, 'ZAL', 'ZB', 8),
	(1306, 'NIEBLA', 336, 'ZAL', 'ZB', 8),
	(1307, 'VALDIVIA', 38, 'ZAL', 'ZB', 8),
	(1308, 'ANTILHUE', 339, 'ZAL', 'ZI', 8),
	(1309, 'CHANCOYAN', 340, 'ZAL', 'ZI', 8),
	(1310, 'CIRUELOS', 341, 'ZAL', 'ZI', 8),
	(1311, 'CURINANCO', 336, 'ZAL', 'ZI', 8),
	(1312, 'HUEYELHUE', 336, 'ZAL', 'ZI', 8),
	(1313, 'LA PENA', 342, 'ZAL', 'ZI', 8),
	(1314, 'LAS VENTANAS', 344, 'ZAL', 'ZI', 8),
	(1315, 'LOS LAGOS', 339, 'ZAL', 'ZI', 8),
	(1316, 'LOS PELLINES', 244, 'ZAL', 'ZI', 8),
	(1317, 'LOS ULMOS', 342, 'ZAL', 'ZI', 8),
	(1318, 'MAFIL', 340, 'ZAL', 'ZI', 8),
	(1319, 'PAILLACO', 342, 'ZAL', 'ZI', 8),
	(1320, 'PICHIRROPULLI', 342, 'ZAL', 'ZI', 8),
	(1321, 'PICHOY', 341, 'ZAL', 'ZI', 8),
	(1322, 'PUERTO PAICO', 340, 'ZAL', 'ZI', 8),
	(1323, 'REUMEN', 342, 'ZAL', 'ZI', 8),
	(1324, 'RUNCA', 339, 'ZAL', 'ZI', 8),
	(1325, 'S.J. DE LA MARIQUINA', 341, 'ZAL', 'ZI', 12),
	(1326, 'SANTA ELISA', 344, 'ZAL', 'ZI', 8),
	(1327, 'TRALCAO', 336, 'ZAL', 'ZI', 8),
	(1328, 'COIQUE', 345, 'ZAL', 'ZE', 8),
	(1329, 'COLEGUAL', 244, 'ZAL', 'ZE', 8),
	(1330, 'CORRAL', 337, 'ZAL', 'ZE', 8),
	(1331, 'CRUCES', 220, 'ZAL', 'ZE', 8),
	(1332, 'DOLLINCO', 345, 'ZAL', 'ZE', 8),
	(1333, 'FOLILCO', 339, 'ZAL', 'ZE', 8),
	(1334, 'FUERTE SAN LUIS', 341, 'ZAL', 'ZE', 8),
	(1335, 'FUNDO ALTUE', 340, 'ZAL', 'ZE', 8),
	(1336, 'FUTRONO', 345, 'ZAL', 'ZE', 8),
	(1337, 'HUICHACO', 339, 'ZAL', 'ZE', 8),
	(1338, 'HUITE', 339, 'ZAL', 'ZE', 8),
	(1339, 'LA CAPILLA', 151, 'ZAL', 'ZE', 8),
	(1340, 'LA LEONERA', 84, 'ZAL', 'ZE', 8),
	(1341, 'LLIFEN', 345, 'ZAL', 'ZE', 8),
	(1342, 'LOS CONALES', 344, 'ZAL', 'ZE', 8),
	(1343, 'MALALHUE', 338, 'ZAL', 'ZE', 8),
	(1344, 'MANAO', 342, 'ZAL', 'ZE', 8),
	(1345, 'MEHUIN', 341, 'ZAL', 'ZE', 8),
	(1346, 'MONTUELA', 345, 'ZAL', 'ZE', 8),
	(1347, 'PUCARA', 339, 'ZAL', 'ZE', 8),
	(1348, 'PUCONO', 339, 'ZAL', 'ZE', 8),
	(1349, 'PUNUCAPA', 336, 'ZAL', 'ZE', 8),
	(1350, 'PURINGUE', 341, 'ZAL', 'ZE', 8),
	(1351, 'PURULON', 338, 'ZAL', 'ZE', 8),
	(1352, 'QUEULE', 341, 'ZAL', 'ZE', 8),
	(1353, 'SALTO DEL AGUA', 338, 'ZAL', 'ZE', 8),
	(1354, 'BANOS DE CHIHUIO', 345, 'ZAL', 'ZME', 8),
	(1355, 'CHABRANCO', 345, 'ZAL', 'ZME', 8),
	(1356, 'EL MUNDIAL', 345, 'ZAL', 'ZME', 8),
	(1357, 'HUEQUECURA', 345, 'ZAL', 'ZME', 8),
	(1358, 'LIPINGUE', 339, 'ZAL', 'ZME', 8),
	(1359, 'LOS LLOLLES', 345, 'ZAL', 'ZME', 8),
	(1360, 'MAIHUE', 345, 'ZAL', 'ZME', 8),
	(1361, 'PUNTA CHAIHUIN', 337, 'ZAL', 'ZME', 8),
	(1362, 'RINIHUE', 339, 'ZAL', 'ZME', 8),
	(1363, 'TRAITRACO', 342, 'ZAL', 'ZME', 8),
	(1364, 'OSORNO', 232, 'ZOS', 'ZB', 8),
	(1365, 'BAHIA MANSA', 237, 'ZOS', 'ZI', 8),
	(1366, 'CANCURA', 232, 'ZOS', 'ZI', 8),
	(1367, 'CARACOL', 238, 'ZOS', 'ZI', 8),
	(1368, 'CASMA', 234, 'ZOS', 'ZI', 8),
	(1369, 'CAYURRUCA', 347, 'ZOS', 'ZI', 8),
	(1370, 'CENTRAL RUPANCO', 233, 'ZOS', 'ZI', 8),
	(1371, 'CHAHUILCO', 236, 'ZOS', 'ZI', 8),
	(1372, 'CHANCHAN', 347, 'ZOS', 'ZI', 8),
	(1373, 'CHIRRE', 238, 'ZOS', 'ZI', 8),
	(1374, 'CONTACO', 237, 'ZOS', 'ZI', 8),
	(1375, 'CORTE ALTO', 234, 'ZOS', 'ZI', 8),
	(1376, 'CRUCERO', 234, 'ZOS', 'ZI', 8),
	(1377, 'EL ENCANTO ', 235, 'ZOS', 'ZI', 8),
	(1378, 'EL MIRADOR', 344, 'ZOS', 'ZI', 8),
	(1379, 'ENTRE LAGOS', 235, 'ZOS', 'ZI', 8),
	(1380, 'FILUCO', 238, 'ZOS', 'ZI', 8),
	(1381, 'FRUTILLAR', 242, 'ZOS', 'ZI', 8),
	(1382, 'HUILMA', 236, 'ZOS', 'ZI', 8),
	(1383, 'IGNAO', 346, 'ZOS', 'ZI', 8),
	(1384, 'LA UNION', 344, 'ZOS', 'ZI', 8),
	(1385, 'LAS LUMAS', 232, 'ZOS', 'ZI', 8),
	(1386, 'LOS CONALES', 344, 'ZOS', 'ZI', 8),
	(1387, 'LOS CORRALES', 234, 'ZOS', 'ZI', 8),
	(1388, 'MAICOLPUE', 237, 'ZOS', 'ZI', 8),
	(1389, 'MONTE VERDE', 232, 'ZOS', 'ZI', 8),
	(1390, 'PARAGUAY', 242, 'ZOS', 'ZI', 8),
	(1391, 'PICHI DAMAS', 232, 'ZOS', 'ZI', 8),
	(1392, 'PUAUCHO', 237, 'ZOS', 'ZI', 8),
	(1393, 'PUCATRIHUE', 237, 'ZOS', 'ZI', 8),
	(1394, 'PUERTO NUEVO', 347, 'ZOS', 'ZI', 8),
	(1395, 'PUERTO OCTAY', 233, 'ZOS', 'ZI', 8),
	(1396, 'PURRANQUE', 234, 'ZOS', 'ZI', 8),
	(1397, 'PUYEHUE', 235, 'ZOS', 'ZI', 8),
	(1398, 'RAPACO', 344, 'ZOS', 'ZI', 8),
	(1399, 'REMEHUE', 232, 'ZOS', 'ZI', 8),
	(1400, 'RIO BUENO', 347, 'ZOS', 'ZI', 8),
	(1401, 'RIO NEGRO', 236, 'ZOS', 'ZI', 8),
	(1402, 'SAN JUAN DE LA COSTA', 237, 'ZOS', 'ZI', 8),
	(1403, 'SAN PABLO', 30, 'ZOS', 'ZI', 8),
	(1404, 'SANTA ROSA', 14, 'ZOS', 'ZI', 8),
	(1405, 'TRAPI', 346, 'ZOS', 'ZI', 8),
	(1406, 'TRUMAO', 344, 'ZOS', 'ZI', 8),
	(1407, 'VIVANCO', 346, 'ZOS', 'ZI', 8),
	(1408, 'AGUAS CALIENTES', 4, 'ZOS', 'ZE', 8),
	(1409, 'ANTICURA', 235, 'ZOS', 'ZE', 8),
	(1410, 'CONCORDIA', 234, 'ZOS', 'ZE', 8),
	(1411, 'EL BOLSON', 236, 'ZOS', 'ZE', 8),
	(1412, 'EL ISLOTE', 235, 'ZOS', 'ZE', 8),
	(1413, 'HUEYUSCA', 234, 'ZOS', 'ZE', 8),
	(1414, 'ILIHUE', 346, 'ZOS', 'ZE', 8),
	(1415, 'LA BARRA', 344, 'ZOS', 'ZE', 8),
	(1416, 'LAGO RANCO', 346, 'ZOS', 'ZE', 8),
	(1417, 'LAS CASCADAS', 233, 'ZOS', 'ZE', 8),
	(1418, 'LLIHUE', 346, 'ZOS', 'ZE', 8),
	(1419, 'LOS BAJOS', 233, 'ZOS', 'ZE', 8),
	(1420, 'LOS CHILCOS', 347, 'ZOS', 'ZE', 8),
	(1421, 'MILLANTUE', 236, 'ZOS', 'ZE', 8),
	(1422, 'NILQUE', 235, 'ZOS', 'ZE', 8),
	(1423, 'PIEDRAS NEGRAS', 233, 'ZOS', 'ZE', 8),
	(1424, 'PUERTO CLOCKER', 233, 'ZOS', 'ZE', 8),
	(1425, 'PUERTO FONCK', 233, 'ZOS', 'ZE', 8),
	(1426, 'PUERTO RICO', 235, 'ZOS', 'ZE', 8),
	(1427, 'PURRAPEL', 238, 'ZOS', 'ZE', 8),
	(1428, 'REFUGIO LA PICADA', 233, 'ZOS', 'ZE', 8),
	(1429, 'RININAHUE', 346, 'ZOS', 'ZE', 8),
	(1430, 'TEGUALDA', 242, 'ZOS', 'ZE', 8),
	(1431, 'TRINIDAD', 344, 'ZOS', 'ZE', 8),
	(1432, 'HUEICOLLA', 344, 'ZOS', 'ZME', 8),
	(1433, 'PAJARITOS', 235, 'ZOS', 'ZME', 8),
	(1434, 'REFUGIO ANTILLANCA', 235, 'ZOS', 'ZME', 8),
	(1435, 'TERMAS DE PUYEHUE', 235, 'ZOS', 'ZME', 8),
	(1436, 'PUERTO MONTT', 246, 'PMC', 'ZB', 8),
	(1437, 'RANCHO PUERTO MONTT', 246, 'PMC', 'ZB', 8),
	(1438, 'ALERCE', 246, 'PMC', 'ZI', 8),
	(1439, 'CALBUCO', 239, 'PMC', 'ZI', 8),
	(1440, 'CALETA LA ARENA', 246, 'PMC', 'ZI', 8),
	(1441, 'CHAMIZA', 246, 'PMC', 'ZI', 8),
	(1442, 'COLEGUAL', 244, 'PMC', 'ZI', 8),
	(1443, 'CORRENTOSO', 246, 'PMC', 'ZI', 8),
	(1444, 'EL TEPUAL', 246, 'PMC', 'ZI', 8),
	(1445, 'FRESIA', 241, 'PMC', 'ZI', 8),
	(1446, 'HUELMO', 239, 'PMC', 'ZI', 8),
	(1447, 'LA POSA', 247, 'PMC', 'ZI', 8),
	(1448, 'LAS QUEMAS', 243, 'PMC', 'ZI', 8),
	(1449, 'LENCA', 246, 'PMC', 'ZI', 8),
	(1450, 'LLANQUIHUE', 244, 'PMC', 'ZI', 8),
	(1451, 'LOLCURA', 243, 'PMC', 'ZI', 8),
	(1452, 'LONCOTORO', 244, 'PMC', 'ZI', 8),
	(1453, 'LOS MUERMOS', 243, 'PMC', 'ZI', 8),
	(1454, 'LOS PELLINES', 244, 'PMC', 'ZI', 8),
	(1455, 'MAULLIN', 245, 'PMC', 'ZI', 8),
	(1456, 'MISQUIHUE', 245, 'PMC', 'ZI', 8),
	(1457, 'NUEVA BRAUNAU', 247, 'PMC', 'ZI', 8),
	(1458, 'PARGA', 241, 'PMC', 'ZI', 8),
	(1459, 'PARGUA', 245, 'PMC', 'ZI', 8),
	(1460, 'PUELPUN', 245, 'PMC', 'ZI', 8),
	(1461, 'PUERTO VARAS', 247, 'PMC', 'ZI', 8),
	(1462, 'QUILLAIPE', 246, 'PMC', 'ZI', 8),
	(1463, 'RIO SUR', 247, 'PMC', 'ZI', 8),
	(1464, 'ABTAO', 250, 'PMC', 'ZE', 8),
	(1465, 'ANCUD', 249, 'PMC', 'ZE', 8),
	(1466, 'CARELMAPU', 245, 'PMC', 'ZE', 8),
	(1467, 'CASTRO', 250, 'PMC', 'ZE', 8),
	(1468, 'CAULIN', 249, 'PMC', 'ZE', 8),
	(1469, 'CHACAO', 249, 'PMC', 'ZE', 8),
	(1470, 'ENSENADA', 247, 'PMC', 'ZE', 8),
	(1471, 'LINAO', 257, 'PMC', 'ZE', 8),
	(1472, 'LLIUCO', 257, 'PMC', 'ZE', 8),
	(1473, 'LOS RISCOS', 247, 'PMC', 'ZE', 8),
	(1474, 'MAICHIHUE', 241, 'PMC', 'ZE', 8),
	(1475, 'MANAO', 342, 'PMC', 'ZE', 8),
	(1476, 'PANGAL', 245, 'PMC', 'ZE', 8),
	(1477, 'PETROHUE', 247, 'PMC', 'ZE', 8),
	(1478, 'QUELLON', 256, 'PMC', 'ZE', 8),
	(1479, 'QUEMCHI', 257, 'PMC', 'ZE', 8),
	(1480, 'RANCHO QUELLON', 256, 'PMC', 'ZE', 8),
	(1481, 'RIO FRIO', 243, 'PMC', 'ZE', 8),
	(1482, 'ACHAO', 258, 'PMC', 'ZME', 8),
	(1483, 'AGUAS  BUENAS', 257, 'PMC', 'ZME', 8),
	(1484, 'AHONI', 255, 'PMC', 'ZME', 8),
	(1485, 'AITUI', 255, 'PMC', 'ZME', 8),
	(1486, 'ALDACHILDO', 254, 'PMC', 'ZME', 8),
	(1487, 'ALTO BUTALCURA', 253, 'PMC', 'ZME', 8),
	(1488, 'APIAO', 258, 'PMC', 'ZME', 8),
	(1489, 'AUCAR', 257, 'PMC', 'ZME', 8),
	(1490, 'AULEN', 261, 'PMC', 'ZME', 8),
	(1491, 'AYACARA', 261, 'PMC', 'ZME', 8),
	(1492, 'BANOS DE SOTOMO', 240, 'PMC', 'ZME', 8),
	(1493, 'BELBEN', 257, 'PMC', 'ZME', 8),
	(1494, 'BUILL', 259, 'PMC', 'ZME', 8),
	(1495, 'CALETA GONZALO', 259, 'PMC', 'ZME', 8),
	(1496, 'CALETA HUALAIHUE', 261, 'PMC', 'ZME', 8),
	(1497, 'CALETA SANTA BARBARA', 259, 'PMC', 'ZME', 8),
	(1498, 'CANUTILLAR', 240, 'PMC', 'ZME', 8),
	(1499, 'CHADMO CENTRAL', 252, 'PMC', 'ZME', 8),
	(1500, 'CHAITEN', 259, 'PMC', 'ZME', 8),
	(1501, 'CHAPARANO', 261, 'PMC', 'ZME', 8),
	(1502, 'CHAULINEC', 258, 'PMC', 'ZME', 8),
	(1503, 'CHOLGO', 261, 'PMC', 'ZME', 8),
	(1504, 'CHONCHI', 252, 'PMC', 'ZME', 8),
	(1505, 'COCHAMO', 240, 'PMC', 'ZME', 8),
	(1506, 'COINCO', 85, 'PMC', 'ZME', 8),
	(1507, 'CONTAO', 261, 'PMC', 'ZME', 8),
	(1508, 'CONTAY', 255, 'PMC', 'ZME', 8),
	(1509, 'CUCAO', 252, 'PMC', 'ZME', 8),
	(1510, 'CURACO DE VELEZ', 251, 'PMC', 'ZME', 8),
	(1511, 'CURAHUE', 250, 'PMC', 'ZME', 8),
	(1512, 'DALCAHUE', 253, 'PMC', 'ZME', 8),
	(1513, 'DEGAN', 257, 'PMC', 'ZME', 8),
	(1514, 'EL BARRACO', 240, 'PMC', 'ZME', 8),
	(1515, 'FUTALEUFU', 260, 'PMC', 'ZME', 8),
	(1516, 'GUALBUN', 249, 'PMC', 'ZME', 8),
	(1517, 'HORNOPIREN', 261, 'PMC', 'ZME', 8),
	(1518, 'HUALAIHUE', 261, 'PMC', 'ZME', 8),
	(1519, 'HUILDAD', 256, 'PMC', 'ZME', 8),
	(1520, 'HUILLINCO', 257, 'PMC', 'ZME', 8),
	(1521, 'HUINAY', 261, 'PMC', 'ZME', 8),
	(1522, 'LAGO YELCHO', 260, 'PMC', 'ZME', 8),
	(1523, 'LLANADA GRANDE', 261, 'PMC', 'ZME', 8),
	(1524, 'MECHUQUE', 253, 'PMC', 'ZME', 8),
	(1525, 'PAILDAD', 255, 'PMC', 'ZME', 8),
	(1526, 'PALENA', 262, 'PMC', 'ZME', 8),
	(1527, 'PICHANCO', 261, 'PMC', 'ZME', 8),
	(1528, 'POYO', 261, 'PMC', 'ZME', 8),
	(1529, 'PRIMER CORRAL', 261, 'PMC', 'ZME', 8),
	(1530, 'PUELO', 240, 'PMC', 'ZME', 8),
	(1531, 'PUERTO CARDENAS', 259, 'PMC', 'ZME', 8),
	(1532, 'PUERTO PIEDRA', 260, 'PMC', 'ZME', 8),
	(1533, 'PUERTO RAMIREZ', 260, 'PMC', 'ZME', 8),
	(1534, 'PUQUELDON', 254, 'PMC', 'ZME', 8),
	(1535, 'QUEILEN', 255, 'PMC', 'ZME', 8),
	(1536, 'QUELLON VIEJO', 256, 'PMC', 'ZME', 8),
	(1537, 'QUETELMAHUE', 249, 'PMC', 'ZME', 8),
	(1538, 'QUICAVI', 253, 'PMC', 'ZME', 8),
	(1539, 'QUINCHAO', 251, 'PMC', 'ZME', 8),
	(1540, 'RALUN', 240, 'PMC', 'ZME', 8),
	(1541, 'ROLLIZO', 240, 'PMC', 'ZME', 8),
	(1542, 'SAN JUAN', 253, 'PMC', 'ZME', 8),
	(1543, 'SEGUNDO CORRAL', 261, 'PMC', 'ZME', 8),
	(1544, 'TERMAS DE LLANCATUE', 261, 'PMC', 'ZME', 8),
	(1545, 'TERMAS EL AMARILLO', 259, 'PMC', 'ZME', 8),
	(1546, 'TEUPA', 252, 'PMC', 'ZME', 8),
	(1547, 'TRINCAO', 256, 'PMC', 'ZME', 8),
	(1548, 'VILLA SANTA LUCIA', 260, 'PMC', 'ZME', 8),
	(1549, 'VILLA VANGUARDIA', 262, 'PMC', 'ZME', 8),
	(1550, 'YALDAD', 256, 'PMC', 'ZME', 8),
	(1551, 'COYHAIQUE', 269, 'GXQ', 'ZB', 15),
	(1552, 'BALMACEDA', 269, 'GXQ', 'ZI', 15),
	(1553, 'PUERTO AYSEN', 266, 'GXQ', 'ZI', 15),
	(1554, 'COYHAIQUE ALTO', 269, 'GXQ', 'ZE', 15),
	(1555, 'MANIHUALES', 266, 'GXQ', 'ZE', 15),
	(1556, 'PUERTO CHACABUCO', 266, 'GXQ', 'ZE', 15),
	(1557, 'RIO IBAEZ', 272, 'GXQ', 'ZE', 12),
	(1558, 'VILLA MANIHUALES', 266, 'GXQ', 'ZE', 15),
	(1559, 'BANO NUEVO', 269, 'GXQ', 'ZME', 15),
	(1560, 'CALETA TORTEL', 265, 'GXQ', 'ZME', 15),
	(1561, 'CHILE CHICO', 271, 'GXQ', 'ZME', 15),
	(1562, 'CISNES', 267, 'GXQ', 'ZME', 15),
	(1563, 'COCHRANE', 263, 'GXQ', 'ZME', 15),
	(1564, 'EL GATO', 266, 'GXQ', 'ZME', 15),
	(1565, 'GUAITECAS', 268, 'GXQ', 'ZME', 15),
	(1566, 'ISLA ANGAMOS', 268, 'GXQ', 'ZME', 15),
	(1567, 'ISLA BENJAMIN', 268, 'GXQ', 'ZME', 15),
	(1568, 'ISLA CAMPANA', 265, 'GXQ', 'ZME', 15),
	(1569, 'ISLA CUPTANA', 268, 'GXQ', 'ZME', 15),
	(1570, 'ISLA IPUN', 268, 'GXQ', 'ZME', 15),
	(1571, 'ISLA IZAZO', 268, 'GXQ', 'ZME', 15),
	(1572, 'ISLA LEVEL', 268, 'GXQ', 'ZME', 15),
	(1573, 'ISLA MELCHOR', 268, 'GXQ', 'ZME', 15),
	(1574, 'ISLA MERINO JARPA', 265, 'GXQ', 'ZME', 15),
	(1575, 'ISLA NALCAYEC', 268, 'GXQ', 'ZME', 15),
	(1576, 'ISLA PATRICIO LYNCH', 265, 'GXQ', 'ZME', 15),
	(1577, 'ISLA VICTORIA', 268, 'GXQ', 'ZME', 15),
	(1578, 'LA JUNTA', 270, 'GXQ', 'ZME', 15),
	(1579, 'LA TAPERA', 267, 'GXQ', 'ZME', 15),
	(1580, 'LAGO COCHRANE', 263, 'GXQ', 'ZME', 15),
	(1581, 'LAGO VERDE', 270, 'GXQ', 'ZME', 15),
	(1582, 'LAGUNA SAN RAFAEL', 269, 'GXQ', 'ZME', 15),
	(1583, 'LEVICAN', 272, 'GXQ', 'ZME', 15),
	(1584, 'MELINKA', 268, 'GXQ', 'ZME', 15),
	(1585, 'MINA EL TOQUI', 266, 'GXQ', 'ZME', 15),
	(1586, 'NIREGUAO', 269, 'GXQ', 'ZME', 15),
	(1587, 'O HIGGINS', 264, 'GXQ', 'ZME', 15),
	(1588, 'PUERTO AGUIRRE', 266, 'GXQ', 'ZME', 15),
	(1589, 'PUERTO BERTRAND', 271, 'GXQ', 'ZME', 15),
	(1590, 'PUERTO CISNES', 267, 'GXQ', 'ZME', 15),
	(1591, 'PUERTO FACHINAL', 271, 'GXQ', 'ZME', 15),
	(1592, 'PUERTO GAVIOTA', 266, 'GXQ', 'ZME', 15),
	(1593, 'PUERTO GUADAL', 271, 'GXQ', 'ZME', 15),
	(1594, 'PUERTO HERRADURA', 263, 'GXQ', 'ZME', 15),
	(1595, 'PUERTO ING.IBANEZ', 272, 'GXQ', 'ZME', 15),
	(1596, 'PUERTO MURTA', 271, 'GXQ', 'ZME', 15),
	(1597, 'PUERTO SANCHEZ', 271, 'GXQ', 'ZME', 15),
	(1598, 'PUERTO TRANQUILO', 271, 'GXQ', 'ZME', 15),
	(1599, 'PUERTO YUNGAY', 265, 'GXQ', 'ZME', 15),
	(1600, 'PUYUHUAPI', 267, 'GXQ', 'ZME', 15),
	(1601, 'RIO CISNES', 267, 'GXQ', 'ZME', 15),
	(1602, 'SANTA MARIA DEL MAR', 266, 'GXQ', 'ZME', 15),
	(1603, 'TERMAS DE PUYUHUAPI', 267, 'GXQ', 'ZME', 15),
	(1604, 'TORTEL', 265, 'GXQ', 'ZME', 15),
	(1605, 'VILLA AMENGUAL', 267, 'GXQ', 'ZME', 15),
	(1606, 'VILLA CERRO CASTILLO', 272, 'GXQ', 'ZME', 15),
	(1607, 'VILLA CHACABUCO', 263, 'GXQ', 'ZME', 15),
	(1608, 'VILLA OHIGGINS', 264, 'GXQ', 'ZME', 15),
	(1609, 'VILLA ORTEGA', 269, 'GXQ', 'ZME', 15),
	(1610, 'PUNTA ARENAS', 275, 'PUQ', 'ZB', 15),
	(1611, 'RANCHO PUNTA ARENAS', 275, 'PUQ', 'ZB', 15),
	(1612, 'MINA PECKET', 275, 'PUQ', 'ZI', 15),
	(1613, 'SAN SEBASTIAN', 78, 'PUQ', 'ZI', 8),
	(1614, 'CABEZA DE MAR', 275, 'PUQ', 'ZE', 15),
	(1615, 'ENTRE VIENTOS', 276, 'PUQ', 'ZE', 15),
	(1616, 'ESTANCIA SAN JUAN', 275, 'PUQ', 'ZE', 15),
	(1617, 'FUERTE BULNES', 275, 'PUQ', 'ZE', 15),
	(1618, 'ANTARTICA', 283, 'PUQ', 'ZME', 8),
	(1619, 'ARMONIA', 279, 'PUQ', 'ZME', 15),
	(1620, 'CABO DE HORNOS', 282, 'PUQ', 'ZME', 8),
	(1621, 'CALETA EUGENIA', 282, 'PUQ', 'ZME', 8),
	(1622, 'CAMERON', 281, 'PUQ', 'ZME', 15),
	(1623, 'CERRO CASTILLO ', 273, 'PUQ', 'ZME', 15),
	(1624, 'CERRO SOMBRERO', 273, 'PUQ', 'ZME', 12),
	(1625, 'CULLEN', 280, 'PUQ', 'ZME', 15),
	(1626, 'ESTANCIA CHINA CR.', 280, 'PUQ', 'ZME', 15),
	(1627, 'ESTANCIA LOS OLIVOS', 280, 'PUQ', 'ZME', 15),
	(1628, 'ESTANCIA SN GREGORIO', 278, 'PUQ', 'ZME', 15),
	(1629, 'ESTANCIA VICTORINA', 274, 'PUQ', 'ZME', 15),
	(1630, 'GAIKE', 278, 'PUQ', 'ZME', 15),
	(1631, 'GALLEGOS CHICOS', 278, 'PUQ', 'ZME', 15),
	(1632, 'ISLA DAWSON', 279, 'PUQ', 'ZME', 15),
	(1633, 'ISLA NAVARINO', 282, 'PUQ', 'ZME', 8),
	(1634, 'LA JUNTA  ', 273, 'PUQ', 'ZME', 15),
	(1635, 'LAGUNA BLANCA', 277, 'PUQ', 'ZME', 15),
	(1636, 'LAPATAIA', 282, 'PUQ', 'ZME', 8),
	(1637, 'MANANTIALES', 280, 'PUQ', 'ZME', 15),
	(1638, 'MONTE AYMOND', 278, 'PUQ', 'ZME', 15),
	(1639, 'MORRO CHICO', 276, 'PUQ', 'ZME', 15),
	(1640, 'ONAISIN', 280, 'PUQ', 'ZME', 15),
	(1641, 'PAMPA GUANACOS', 279, 'PUQ', 'ZME', 15),
	(1642, 'PEHOE', 274, 'PUQ', 'ZME', 15),
	(1643, 'PORVENIR', 201, 'PUQ', 'ZME', 8),
	(1644, 'PRIMAVERA', 280, 'PUQ', 'ZME', 15),
	(1645, 'PUERTO ALTAMIRANO', 276, 'PUQ', 'ZME', 15),
	(1646, 'PUERTO ARTURO', 281, 'PUQ', 'ZME', 15),
	(1647, 'PUERTO BORIES', 273, 'PUQ', 'ZME', 15),
	(1648, 'PUERTO CONDOR', 281, 'PUQ', 'ZME', 15),
	(1649, 'PUERTO NATALES', 273, 'PUQ', 'ZME', 15),
	(1650, 'PUERTO PERCY', 279, 'PUQ', 'ZME', 15),
	(1651, 'PUERTO TORO', 282, 'PUQ', 'ZME', 8),
	(1652, 'PUERTO WILLIAMS', 282, 'PUQ', 'ZME', 8),
	(1653, 'PUERTO YARTAU', 281, 'PUQ', 'ZME', 15),
	(1654, 'PUNTA DELGADA', 278, 'PUQ', 'ZME', 15),
	(1655, 'RIO GRANDE', 276, 'PUQ', 'ZME', 15),
	(1656, 'RIO TURBIO', 273, 'PUQ', 'ZME', 15),
	(1657, 'RIO VERDE', 276, 'PUQ', 'ZME', 15),
	(1658, 'RUBENS', 273, 'PUQ', 'ZME', 15),
	(1659, 'SAN GREGORIO', 278, 'PUQ', 'ZME', 15),
	(1660, 'SECCION RUSSFIN', 279, 'PUQ', 'ZME', 15),
	(1661, 'TERMINAL CABO NEGRO', 275, 'PUQ', 'ZME', 15),
	(1662, 'TERMINAL SAN GREGORIO', 278, 'PUQ', 'ZME', 15),
	(1663, 'TIMAUKEL', 281, 'PUQ', 'ZME', 15),
	(1664, 'TORRES DEL PAINE', 274, 'PUQ', 'ZME', 15),
	(1665, 'VILLA TEHUELCHES', 276, 'PUQ', 'ZME', 15),
	(1666, 'YENDEGAIA', 282, 'PUQ', 'ZME', 8),
	(0, 'sin localidad', 0, NULL, NULL, NULL);";

$run_insert_localidades = mysqli_query($conexion, $insert_localidades);

if (!$run_insert_localidades)
{
	die($die_start."Error al insertar en tabla localidades: ".mysqli_error($conexion)."$die_end");
}


//Tabla clientes
$clientes = "CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activo` int(1) DEFAULT NULL,
  `id_cliente_hbt` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `rut` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `password_salt` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `descuento_segunda_compra` int(1) DEFAULT '0',
  `vendedor` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_clientes = mysqli_query($conexion, $clientes);

if (!$run_clientes)
{
	die($die_start."Error al crear tabla clientes: ".mysqli_error($conexion)."$die_end");
}





//Tabla clientes_direcciones
$clientes_direcciones = "CREATE TABLE `clientes_direcciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `ciudad_id` int(11) DEFAULT NULL,
  `comuna_id` int(11) DEFAULT NULL,
  `localidad_id` int(11) DEFAULT NULL,
  `calle` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `numero` varchar(100) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
$run_clientes_direcciones = mysqli_query($conexion, $clientes_direcciones);

if (!$run_clientes_direcciones)
{
	die($die_start."Error al crear tabla clientes_direcciones: ".mysqli_error($conexion)."$die_end");
}

//Tabla clientes_direcciones
$posicion_productos = "CREATE TABLE `posicion_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) DEFAULT NULL,
  `linea_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `subcategoria_id` int(11) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$run_posicion_productos = mysqli_query($conexion, $posicion_productos);

if (!$run_posicion_productos)
{
	die($die_start."Error al crear tabla posicion_productos: ".mysqli_error($conexion)."$die_end");
}


//Tabla carros abandonados
$carros_abandonados = "CREATE TABLE `carros_abandonados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(100) DEFAULT NULL,
  `productos` varchar(255) DEFAULT NULL,
  `oc` varchar(50) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_notificacion` datetime DEFAULT NULL,
  `fecha_respuesta` datetime DEFAULT NULL,
  `fecha_notificacion2` datetime DEFAULT NULL,
  `fecha_respuesta2` datetime DEFAULT NULL,
  `fecha_notificacion3` datetime DEFAULT NULL,
  `fecha_respuesta3` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";
$run_carros_abandonados = mysqli_query($conexion, $carros_abandonados);

if (!$run_carros_abandonados)
{
	die($die_start."Error al crear tabla carros_abandonados: ".mysqli_error($conexion)."$die_end");
}


//Tabla cotizaciones
$cotizaciones = "CREATE TABLE `cotizaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oc` varchar(100) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `rut` varchar(50) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `comentarios` text,
  `fecha` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `cant_productos` int(11) DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;";
$run_cotizaciones = mysqli_query($conexion, $cotizaciones);

if (!$run_cotizaciones)
{
	die($die_start."Error al crear tabla cotizaciones: ".mysqli_error($conexion)."$die_end");
}


//Tabla productos cotizaciones
$productos_cotizaciones = "CREATE TABLE `productos_cotizaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cotizacion_id` int(11) DEFAULT NULL,
  `productos_detalle_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unitario` int(11) DEFAULT NULL,
  `descuento` int(11) DEFAULT NULL,
  `precio_total` int(11) DEFAULT NULL,
  `codigo` varchar(255) DEFAULT '',
  `codigo_pack` varchar(100) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;";
$run_productos_cotizaciones = mysqli_query($conexion, $productos_cotizaciones);

if (!$run_productos_cotizaciones){
	die($die_start."Error al crear tabla cotizaciones: ".mysqli_error($conexion)."$die_end");
}


//Tabla productos cotizaciones
$sucursales = "CREATE TABLE `sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publicado` int(1) DEFAULT NULL,
  `retiro_en_tienda` int(1) DEFAULT NULL,
  `posicion` int(11) DEFAULT '1',
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `comuna_id` int(11) DEFAULT NULL,
  `link_mapa` text,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `regiones` int(11) DEFAULT NULL,
  `ciudades` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";
$run_sucursales = mysqli_query($conexion, $sucursales);

if (!$run_sucursales){
	die($die_start."Error al crear tabla sucursales: ".mysqli_error($conexion)."$die_end");
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$insert_opciones = "INSERT INTO `opciones` (`nombre`, `valor`)
VALUES
	('informe_tienda', '1'),
	('servicio', 'https://mercadojardin.cl/ws_rvm.php'),
	('seo_titulo', NULL),
	('seo_descripcion', NULL),
	('seo_keywords', NULL),
	('seo_url_imagen', NULL),
	('cotizador', 0),
	('dominio', 'tiendadeprueba.cl'),
	('logo_mail', 'https://moldeable.com/img/logo-moldeable.png'),
	('color_logo', '#ee5613'),
	('setFrom_mail', 'no-responder@tiendaprueba.cl'),
	('nombre_setFrom_mail', 'Tienda de prueba'),
	('Host', 'host182.hostmonster.com'),
	('Port', '587'),
	('SMTPSecure', 'tls'),
	('Username', 'pruebas@clientes-moldeable.com'),
	('Password', 'molde3938'),
	('correo_venta', 'info@tiendadeprueba.cl'),
	('correo_admin1', 'htorres@moldeable.com'),
	('nombre_correo_admin1', 'Hernan Torres'),
	('correo_admin2', ''),
	('nombre_correo_admin2', ''),
	('correo_admin3', ''),
	('nombre_correo_admin3', ''),
	('configuracion_cliente', '1'),
	('chat', NULL),
  ('tienda_facebook', '1'),
  ('google_merchancenter', '1'),
  ('categoria_google', NULL),
  ('ultimas_unidades', '0'),
  ('cant_ultimas_unidades', '5'),
  ('key_mailchimp', NULL),
  ('descuento_premium', '0'),
  ('popup', '0');";

$run_insert_opciones = mysqli_query($conexion, $insert_opciones);

if (!$run_insert_opciones)
{
	die($die_start."Error al ingresar datos en tabla opciones: ".mysqli_error($conexion)."$die_end");
}



///////////////
$insert_tablas = "INSERT INTO `tablas` (`id`, `nombre`, `display`, `parent`, `is_sub_menu`, `op`, `posicion`, `filtro`, `show_in_menu`, `submenu_table`, `save_in`, `icono`)
VALUES
	(218, 'prod', 'Productos', 0, 1, NULL, 2, NULL, 1, NULL, NULL, '<i class=\"fas fa-archive\"></i>'),
	(219, 'productos', 'Productos', 218, 0, NULL, 4, NULL, 1, NULL, NULL, NULL),
	(220, 'categorias', 'Categorias', 218, 0, NULL, 2, NULL, 1, NULL, NULL, NULL),
	(223, 'productos_detalles', 'Listado productos', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL),
	(224, 'subcategorias', 'Subcategorías', 218, 0, NULL, 3, NULL, 1, NULL, NULL, NULL),
	(225, 'new', 'Newsletter', 0, 1, NULL, 4, NULL, 1, NULL, NULL, '<i class=\"fas fa-at\"></i>'),
	(226, 'newsletter', 'Listar Newsletter', 225, 0, NULL, 1, NULL, 1, NULL, NULL, NULL),
	(227, 'newsletr', 'XLS Newsletter', 225, 0, 'bd_226', 2, NULL, 1, NULL, NULL, NULL),
	(231, 'pedidos', 'Compras exitosas', 233, 0, NULL, 1, 'estado_id=2', 1, NULL, NULL, NULL),
	(232, 'productos_pedidos', 'Productos pedidos', 0, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL),
	(233, 'transacciones', 'Transacciones', 0, 1, NULL, NULL, NULL, 1, NULL, NULL, '<i class=\"fas fa-shopping-cart\"></i>'),
	(234, 'pedidos', 'Compras Fracasadas', 233, 0, NULL, 2, 'estado_id=3', 1, NULL, NULL, NULL),
	(235, 'pedidos', 'Compras Abandonadas', 233, 0, NULL, 3, 'estado_id=1', 1, NULL, NULL, NULL),
	(236, 'pedidos', 'Todas', 233, 0, NULL, 4, NULL, 1, NULL, NULL, NULL),
	(237, 'lineas', 'Lineas', 218, 0, NULL, 1, NULL, 1, NULL, NULL, NULL),
	(238, 'marcas', 'Marcas', 218, 0, NULL, 6, NULL, 1, NULL, NULL, NULL),
	(241, 'prod', 'XLS Productos', 218, 0, 'bd_223', 7, NULL, 1, NULL, NULL, '<i class=\"fas fa-file-excel\"></i>'),
	(242, 'codigo_descuento', 'Códigos de descuento', 0, 0, NULL, 7, NULL, 1, NULL, NULL, '<i class=\"fas fa-hand-holding-usd\"></i>'),
	(243, 'comunas', 'Comunas', 246, 0, NULL, 3, NULL, 1, NULL, NULL, NULL),
	(244, 'ciudades', 'Ciudades', 246, 0, NULL, 2, NULL, 1, NULL, NULL, NULL),
	(245, 'regiones', 'Regiones', 246, 0, NULL, 1, NULL, 1, NULL, NULL, NULL),
	(246, 'dir', 'Direcciones', 0, 1, NULL, NULL, NULL, 1, NULL, NULL, '<i class=\"fas fa-globe-americas\"></i>'),
	(247, 'localidades', 'Localidades', 246, 0, NULL, 4, NULL, 1, NULL, NULL, NULL),
	(248, 'order', 'Posiciones', 218, 0, 'order', 7, NULL, 1, NULL, NULL, NULL),
	(249, 'productos_relacionados', 'Productos relacionados', 0, 0, 'order', 0, NULL, 1, NULL, NULL, NULL),
	(250, 'cotizaciones', 'Cotizaciones', 0, 0, NULL, 7, NULL, 1, NULL, NULL, '<i class=\"fas fa-file-invoice-dollar\"></i>'),
	(251, 'productos_cotizaciones', 'productos cotizados', 0, 0, NULL, 7, NULL, 1, NULL, NULL, NULL),
	(253, 'sucursales', 'Sucursales', 0, 0, NULL, 8, NULL, 1, NULL, NULL, '<i class=\"fas fa-store\"></i>'),
  (254, 'tienda_fb', 'Tienda Facebook', 0, 0, 'tienda_fb', 7, NULL, 1, NULL, NULL, '<i class=\"fab fa-facebook\"></i>'),
  (255, 'tienda_gm', 'Tienda Google Merchantcenter', 0, 0, 'tienda_gm', 7, NULL, 1, NULL, NULL, '<i class=\"fab fa-google\"></i>'),
  (256, 'precios_cantidades', 'Precios x cantidad', 0, 0, NULL, 7, NULL, 0, NULL, NULL, '<i class=\"fab fa-google\"></i>'),
  (257, 'creador_newsletteres', 'Generar Newsletter', 225, 0, NULL, 3, NULL, 1, NULL, NULL, NULL),
  (258, 'productos_grilla', 'Productos Grilla', 0, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL),
  (259, 'excels', 'Carga Excel', 218, 0, 'carga', 8, NULL, 1, NULL, NULL, NULL),
  (260, 'mailchimp', 'Mailchimp', 0, 0, 'mail', 6, NULL, 1, NULL, NULL, '<i class=\"fab fa-mailchimp\"></i>'),
  (261, 'slider', 'Slider', 0, 0, NULL, 1, NULL, 1, NULL, NULL, '<i class=\"fas fa-images\"></i>');";

$run_insert_tablas = mysqli_query($conexion, $insert_tablas);

if (!$run_insert_tablas){
	die($die_start."Error al ingresar datos en tabla tablas: ".mysqli_error($conexion)."$die_end");
}

$insert_tablas_perfiles = "INSERT INTO `tablas_perfiles` (`tabla_id`, `perfil_id`, `permiso_id`)
VALUES
	(218, 1, 3),
	(219, 1, 3),
	(220, 1, 3),
	(224, 1, 3),
	(225, 1, 3),
	(226, 1, 3),
	(227, 1, 3),
	(231, 1, 3),
	(233, 1, 3),
	(234, 1, 3),
	(235, 1, 3),
	(236, 1, 3),
	(237, 1, 3),
	(238, 1, 3),
	(241, 1, 1),
	(242, 1, 3),
	(243, 1, 3),
	(244, 1, 3),
	(245, 1, 3),
	(246, 1, 3),
	(247, 1, 3),
	(248, 1, 3),
	(250, 1, 3),
	(253, 1, 3),
  (254, 1, 3),
  (255, 1, 3),
  (223, 1, 3),
  (256, 1, 3),
  (257, 1, 3),
  (259, 1, 3),
  (260, 1, 3),
  (261, 1, 3);";
$run_insert_tablas_perfiles = mysqli_query($conexion, $insert_tablas_perfiles);

if (!$run_insert_tablas_perfiles){
	die($die_start."Error al ingresar datos en tabla tablas_perfiles: ".mysqli_error($conexion)."$die_end");
}


$insert_opciones_tablas = "INSERT INTO `opciones_tablas` (`id`, `tabla_id`, `nombre`, `valor`)
VALUES
	(34, 219, 'obligatorios', 'nombre'),
	(38, 219, 'tabs', '{\"tabs\": [{        \n  \"nombre\": \"Packs\",\n  \"campos\": \"pack, codigos\"\n},{        \n  \"nombre\": \"SEO\",\n  \"campos\": \"descripcion_seo, keywords\"\n}]}'),
	(39, 219, 'has_many', 'productos_detalles'),
	(49, 219, 'exclusiones_list', 'vacio,descripcion,envios,especificaciones, especificaciones_tecnicas, marca, descripcion_seo, metatags, fecha_creacion, fecha_modificacion,thumbs, video, codigos, precio_pack,ficha_tecnica,keywords'),
	(47, 219, 'many_to_many', 'lineas'),
	(50, 219, 'exclusiones', 'vacio'),
	(97, 219, 'archivos', 'ficha_tecnica, thumbs'),
	(98, 219, 'thumbs', '525,390'),
	(106, 219, 'galerias', '1'),
	(107, 219, 'width_gal', '1000'),
	(108, 219, 'height_gal', '1000'),
	(111, 219, 'tips', 'La imagen thumbs debe medir 525 X 390 px.'),
	(91, 220, 'archivos', 'banner, banner_movil'),
	(93, 220, 'banner', '1470,172'),
	(95, 220, 'tips', 'La imagen debe medir 1470 x 172 pixeles.<br/>\nLa imagen movil debe medir 640 x 218 pixeles.'),
	(96, 220, 'banner_movil', '640,218'),
	(40, 223, 'exclusiones', 'producto_id,id,fecha_creacion,fecha_modificacion,creado_por,modificado_por'),
	(41, 223, 'exclusiones_list', 'fecha_creacion,fecha_modificacion,creado_por,modificado_por,imagen1,imagen2,imagen3,imagen4,imagen5, ancho,alto,largo,dias_de_fabricacion,medidas_unidas,oferta_tiempo_activa,oferta_tiempo_hasta,oferta_tiempo_descuento'),
	(48, 224, 'belongs_to', 'categorias'),
	(62, 231, 'exclusiones_list', 'cliente_id, accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code, comuna, region, direccion, fecha_modificacion, ciudad, telefono,cant_productos, retiro_en_tienda_id, id_hbt, codigo_descuento, descuento, regalo, factura, direccion_factura, rut_factura, email_factura, fecha_creacion, giro, razon_social, retiro_en_tienda_id,retiro_en_tienda, comentarios_envio, id_transaccion_servipag, como_llego, id_cliente_servipag, estado_del_pago, mensaje_transaccion, xml_confirmacion, mensaje_transaccion, rut, nombre_retiro, apellidos_retiro, rut_retiro, id_notificacion, mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_cuotas, mp_valor_cuotas, mp_transaction_date, codigo_pago, mp_id_paid'),
	(63, 231, 'tabs', '{\"tabs\": [{        \n  \"nombre\": \"Regalo\",\n  \"campos\": \"regalo\"\n},{\n  \"nombre\": \"Despacho\",\n  \"campos\": \"direccion, region, ciudad, comuna\"\n},{\n  \"nombre\": \"Retiro\",\n  \"campos\": \"retiro_en_tienda,nombre_retiro, apellidos_retiro, rut_retiro\"\n},{\n  \"nombre\": \"Valores\",\n  \"campos\": \"total, total_pagado, cant_productos, valor_despacho,descuento,codigo_descuento\"\n},{\n  \"nombre\": \"Webpay\",\n  \"campos\": \"accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code\"\n},{\n  \"nombre\": \"Mercadopago\",\n  \"campos\": \"mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_cuotas, mp_valor_cuotas, mp_transaction_date, codigo_pago, mp_id_paid\"\n},{\n  \"nombre\": \"Datos Facturación\",\n  \"campos\": \"factura,razon_social,direccion_factura, rut_factura, email_factura, giro\"\n}]}'),
	(64, 231, 'has_many', 'productos_pedidos'),
	(65, 231, 'exclusiones', 'id_hbt, retiro_en_tienda_id'),
	(67, 232, 'exclusiones_list', 'fecha_modificacion'),
	(68, 234, 'exclusiones_list', 'cliente_id, accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code, comuna, region, direccion, fecha_modificacion, ciudad, telefono,cant_productos, retiro_en_tienda_id, id_hbt, codigo_descuento, descuento, regalo, factura, direccion_factura, rut_factura, email_factura, fecha_creacion, giro, razon_social, retiro_en_tienda_id,retiro_en_tienda, comentarios_envio, id_transaccion_servipag, como_llego, id_cliente_servipag, estado_del_pago, mensaje_transaccion, xml_confirmacion, mensaje_transaccion, rut, nombre_retiro, apellidos_retiro, rut_retiro, id_notificacion, mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_cuotas, mp_valor_cuotas, mp_transaction_date, codigo_pago, mp_id_paid'),
	(69, 234, 'tabs', '{\"tabs\": [{        \n  \"nombre\": \"Regalo\",\n  \"campos\": \"regalo\"\n},{\n  \"nombre\": \"Despacho\",\n  \"campos\": \"direccion, region, ciudad, comuna\"\n},{\n\"nombre\": \"Retiro\",\n  \"campos\": \"retiro_en_tienda,nombre_retiro, apellidos_retiro, rut_retiro\"\n},{\n  \"nombre\": \"Valores\",\n  \"campos\": \"total, total_pagado, cant_productos, valor_despacho,descuento,codigo_descuento\"\n},{\n  \"nombre\": \"Webpay\",\n  \"campos\": \"accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code\"\n},{\n  \"nombre\": \"Mercadopago\",\n  \"campos\": \"mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_cuotas, mp_valor_cuotas, mp_transaction_date, codigo_pago, mp_id_paid\"\n},{\n  \"nombre\": \"Datos Facturación\",\n  \"campos\": \"factura,razon_social,direccion_factura, rut_factura, email_factura, giro\"\n}]}\n'),
	(70, 234, 'has_many', 'productos_pedidos'),
	(71, 234, 'exclusiones', 'id_hbt, retiro_en_tienda_id'),
	(72, 235, 'exclusiones_list', 'cliente_id, accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code, comuna, region, direccion, fecha_modificacion, ciudad, telefono,cant_productos, retiro_en_tienda_id, id_hbt, codigo_descuento, descuento, regalo, factura, direccion_factura, rut_factura, email_factura, fecha_creacion, giro, razon_social, retiro_en_tienda_id,retiro_en_tienda, comentarios_envio, id_transaccion_servipag, como_llego, id_cliente_servipag, estado_del_pago, mensaje_transaccion, xml_confirmacion, mensaje_transaccion, rut, nombre_retiro, apellidos_retiro, rut_retiro, id_notificacion, mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_cuotas, mp_valor_cuotas, mp_transaction_date, codigo_pago, mp_id_paid'),
	(73, 235, 'tabs', '{\"tabs\": [{        \n  \"nombre\": \"Regalo\",\n  \"campos\": \"regalo\"\n},{\n  \"nombre\": \"Despacho\",\n  \"campos\": \"direccion, region, ciudad, comuna\"\n},{\n  \"nombre\": \"Retiro\",\n  \"campos\": \"retiro_en_tienda,nombre_retiro, apellidos_retiro, rut_retiro\"\n},{\n  \"nombre\": \"Valores\",\n  \"campos\": \"total, total_pagado, cant_productos, valor_despacho,descuento,codigo_descuento\"\n},{\n  \"nombre\": \"Webpay\",\n  \"campos\": \"accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code\"\n},{\n  \"nombre\": \"Mercadopago\",\n  \"campos\": \"mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_cuotas, mp_valor_cuotas, mp_transaction_date, codigo_pago, mp_id_paid\"\n},{\n  \"nombre\": \"Datos Facturación\",\n  \"campos\": \"factura,razon_social,direccion_factura, rut_factura, email_factura, giro\"\n}]}\n'),
	(74, 235, 'has_many', 'productos_pedidos'),
	(75, 235, 'exclusiones', 'id_hbt, retiro_en_tienda_id'),
	(76, 236, 'exclusiones_list', 'cliente_id, accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code, comuna, region, direccion, fecha_modificacion, ciudad, telefono,cant_productos, retiro_en_tienda_id, id_hbt, codigo_descuento, descuento, regalo, factura, direccion_factura, rut_factura, email_factura, fecha_creacion, giro, razon_social, retiro_en_tienda_id,retiro_en_tienda, comentarios_envio, id_transaccion_servipag, como_llego, id_cliente_servipag, estado_del_pago, mensaje_transaccion, xml_confirmacion, mensaje_transaccion, rut, nombre_retiro, apellidos_retiro, rut_retiro, id_notificacion, mp_payment_type, mp_payment_method, mp_auth_code, mp_paid_amount, mp_card_number, mp_id_paid, mp_cuotas, mp_valor_cuotas, mp_transaction_date,formas_de_pago_id'),
	(77, 236, 'tabs', '{\"tabs\": [{        \n  \"nombre\": \"Regalo\",\n  \"campos\": \"regalo\"\n},{\n  \"nombre\": \"Despacho\",\n  \"campos\": \"direccion, region, ciudad, comuna\"\n},{\n  \"nombre\": \"Retiro\",\n  \"campos\": \"retiro_en_tienda,nombre_retiro, apellidos_retiro, rut_retiro\"\n},{\n  \"nombre\": \"Valores\",\n  \"campos\": \"total, total_pagado, cant_productos, valor_despacho,descuento,codigo_descuento\"\n},{\n  \"nombre\": \"Webpay\",\n  \"campos\": \"accounting_date, card_number, card_expiration_date, authorization_code, payment_type_code, response_code, shares_number, amount, transaction_date, vci, token, commerce_code\"\n},{\n  \"nombre\": \"MercadoPago\",\n  \"campos\": \"mp_payment_type,mp_payment_method,mp_auth_code,mp_paid_amount,mp_card_number,mp_id_paid,mp_cuotas,mp_valor_cuotas,mp_transaction_date\"\n},{\n  \"nombre\": \"Datos Facturación\",\n  \"campos\": \"factura,razon_social,direccion_factura, rut_factura, email_factura, giro\"\n}]}\n'),
	(78, 236, 'has_many', 'productos_pedidos'),
	(79, 236, 'exclusiones', 'id_hbt, retiro_en_tienda_id'),
	(81, 237, 'archivos', 'icono,banner, banner_movil'),
	(82, 237, 'icono', '70,35'),
	(83, 237, 'banner', '1470,172'),
	(84, 237, 'create', '0'),
	(85, 237, 'tips', 'La imagen debe medir 1470 x 172 pixeles.<br/>\nLa imagen movil debe medir 640 x 218 pixeles.<br/>\nEl icono debe medir 70 X 35 pixeles.'),
	(86, 237, 'banner_movil', '640,218'),
	(88, 238, 'archivos', 'imagen,imagen2'),
	(89, 238, 'imagen', '400,400'),
	(90, 238, 'imagen2', '400,400'),
	(116, 242, 'exclusiones_list', 'fecha_creacion,fecha_modificacion,cantidad,usados,descuento_opcion_id,nombre_descuento'),
	(113, 243, 'belongs_to', 'ciudades'),
	(114, 244, 'belongs_to', 'regiones'),
	(118, 249, 'autocompletar', 'productos,producto_relacionado'),
	(119, 249, 'exclusiones', 'fecha_creacion, fecha_modificacion, creado_por, producto_id'),
	(120, 249, 'exclusiones_list', 'producto_id, fecha_modificacion'),
	(121, 250, 'has_many', 'productos_cotizaciones'),
	(122, 250, 'tabs', '{\"tabs\": [{        \n  \"nombre\": \"Datos cliente\",\n  \"campos\": \"nombre, apellido, rut, telefono, email\"\n}]}\n'),
	(123, 253, 'exclusiones_list', 'latitud, longitud, regiones, ciudades, fecha_creacion, fecha_modificacion, link_mapa'),
	(124, 253, 'exclusiones', 'regiones, ciudades'),
  (125, 220, 'belongs_to', 'lineas'),
  (126, 223, 'tabs', '{\"tabs\":[{\"nombre\":\"Oferta Imperdible\",\"campos\":\"oferta_tiempo_activa,oferta_tiempo_hasta,oferta_tiempo_descuento\"}]}'),
  (127, 256, 'exclusiones', 'fecha_creacion,fecha_modificacion,creado_por,id,productos_detall_id'),
  (128, 256, 'exclusiones_list', 'productos_detall_id,fecha_modificacion,creado_por'),
  (129, 223, 'has_many', 'precios_cantidades'),
  (130, 257, 'many_to_many', 'productos'),
  (131, 257, 'obligatorios', 'nombre'),
  (132, 257, 'has_many', 'productos_grilla'),
  (133, 258, 'autocompletar', 'productos,producto_relacionado'),
  (134, 258, 'exclusiones', 'creador_newsletter_id,id,fecha_creacion,fecha_modificacion,creado_por'),
  (135, 257, 'archivos', 'banner_principal,banner_secundario'),
  (136, 257, 'banner_principal', '600,239'),
  (137, 257, 'banner_secundario', '173,368'),
  (138, 261, 'exclusiones_list', 'imagen, imagen_tablet,imagen_movil,link'),
  (139, 261, 'obligatorios', 'nombre'),
  (140, 261, 'tips', 'Tamaño Imagen 1450 x 330 px. <br/>\nTamaño Imagen Tablet 768 x 350 px. <br/>\nTamaño Imagen Movil 640 x 510 px.'),
  (141, 261, 'archivos', 'imagen,imagen_tablet,imagen_movil'),
  (142, 261, 'imagen', '1450,330'),
  (143, 261, 'imagen_tablet', '768,350'),
  (144, 261, 'imagen_movil', '640,510');
";

$run_insert_opciones_tablas = mysqli_query($conexion, $insert_opciones_tablas);

if (!$run_insert_opciones_tablas){
	die($die_start."Error al ingresar datos en tabla opciones_tablas: ".mysqli_error($conexion)."$die_end");
}

$checkout_token = "CREATE TABLE `checkout_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `oc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";

$run_checkout_token = mysqli_query($conexion, $checkout_token);

if (!$run_checkout_token) {
  die($die_start."Error al crear tabla sucursales: ".mysqli_error($conexion)."$die_start");
}


//Tabla lineas
$stock_temporal = "CREATE TABLE `stock_temporal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oc` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;";
$run_stock_temporal = mysqli_query($conexion, $stock_temporal);

if (!$run_stock_temporal)
{
  die($die_start."Error al crear tabla stock_temporal: ".mysqli_error($conexion)."$die_end");
}

// Tablas popup
$popup = "CREATE TABLE `popup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
$run_popup = mysqli_query($conexion, $popup);

if (!$run_popup) {
  die($die_start."Error al crear tabla popup: ".mysqli_error($conexion)."$die_end");
}

$active_popup = "CREATE TABLE `active_popup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activo` int(1) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$run_active_popup = mysqli_query($conexion, $active_popup);
if (!$run_active_popup) {
  die($die_start."Error al crear tabla active_popup: ".mysqli_error($conexion)."$die_end");
}

$insert_active_popup = "INSERT INTO `active_popup` (`id`, `activo`, `fecha_creacion`, `fecha_modificacion`)
VALUES
  (1,0,NULL,NULL)";

$run_insert_active_popup = mysqli_query($conexion, $insert_active_popup);

$creador_newsletteres = "CREATE TABLE `creador_newsletteres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_principal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_secundario` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$run_creador_newsletteres = mysqli_query($conexion, $creador_newsletteres);
if (!$run_creador_newsletteres) {
  die($die_start."Error al crear tabla creador newsletter: ".mysqli_error($conexion)."$die_end");
}

$productos_creador_newsletteres = "CREATE TABLE `productos_creador_newsletteres` (
  `producto_id` int(11) NOT NULL,
  `creador_newsletter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$run_productos_creador_newsletteres = mysqli_query($conexion, $productos_creador_newsletteres);
if (!$run_productos_creador_newsletteres) {
  die($die_start."Error al crear tabla productos creador newsletter: ".mysqli_error($conexion)."$die_end");
}

$productos_grilla = "CREATE TABLE `productos_grilla` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_relacionado` int(11) DEFAULT NULL,
  `creador_newsletter_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$run_productos_grilla = mysqli_query($conexion, $productos_grilla);
if (!$run_productos_grilla) {
  die($die_start."Error al crear tabla productos grilla newsletter: ".mysqli_error($conexion)."$die_end");
}


$precios_cantidades = "CREATE TABLE `precios_cantidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productos_detall_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `rango` int(11) DEFAULT NULL,
  `descuento` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";

$run_precios_cantidades = mysqli_query($conexion, $precios_cantidades);
if (!$run_precios_cantidades) {
  die($die_start."Error al crear tabla precios_cantidades: ".mysqli_error($conexion)."$die_end");
}

$historial_upload_excel = "CREATE TABLE `historial_upload_excel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_subida` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$run_historial_upload_excel = mysqli_query($conexion, $historial_upload_excel);
if (!$run_historial_upload_excel) {
  die($die_start."Error al crear tabla historial_upload_excel: ".mysqli_error($conexion)."$die_end");
}

$productos_relacionados = "CREATE TABLE `productos_relacionados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_relacionado` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacoin` datetime DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";

$run_productos_relacionados = mysqli_query($conexion, $productos_relacionados);
if (!$run_productos_relacionados) {
  die($die_start."Error al crear tabla productos_relacionados: ".mysqli_error($conexion)."$die_end");
}

$mailchimp = "CREATE TABLE `mailchimp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_lista` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$run_mailchimp = mysqli_query($conexion, $mailchimp);
if (!$run_mailchimp) {
  die($die_start."Error al crear tabla mailchimp: ".mysqli_error($conexion)."$die_end");
}

$insert_mailchimp = "INSERT INTO `mailchimp` (`id`, `nombre`, `id_lista`, `short_name`)
VALUES
  (1, 'Newsletter', '1c6350ff88', 'newsletter'),
  (2, 'Compras Exitosas', '6563f77cbe', 'exito'),
  (3, 'Compras Pendientes', '374fd24cb3', 'pendiente'),
  (4, 'Compras Fracasadas', '923fd16b44', 'fracaso'),
  (5, 'Todas las compras', 'a115b2d94b', 'todas_compras'),
  (6, 'Todos los mail', 'fe2b0d1b1e', 'todos_mail');
";

$run_insert_mailchimp = mysqli_query($conexion, $insert_mailchimp);



$tamaños = "CREATE TABLE `tamaños` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$run_tamaños = mysqli_query($conexion, $tamaños);
if (!$run_tamaños) {
  die($die_start."Error al crear tabla tamaños: ".mysqli_error($conexion)."$die_end");
}


$slider = "CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publicado` int(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `imagen_tablet` varchar(255) DEFAULT NULL,
  `imagen_movil` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;";

$run_slider = mysqli_query($conexion, $slider);
if (!$run_slider) {
  die($die_start."Error al crear tabla tamaños: ".mysqli_error($conexion)."$die_end");
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (!$run_stock_temporal OR !$run_lineas OR !$run_categorias OR !$run_subcategorias OR !$run_lineas_productos OR !$run_marcas OR !$run_productos OR !$run_productos_detalles OR !$run_img_productos OR !$run_pedidos OR !$run_productos_pedidos OR !$run_estados OR !$run_descuento_opciones OR !$run_codigo_descuento OR !$run_newsletter OR !$run_regiones OR !$run_ciudades OR !$run_clientes_direcciones OR !$run_clientes OR !$run_insert_opciones OR !$run_insert_tablas OR !$run_insert_opciones_tablas OR !$run_insert_tablas_perfiles OR !$run_carros_abandonados OR !$run_productos_cotizaciones OR !$run_cotizaciones OR !$run_sucursales OR !$run_checkout_token OR !$run_tamaños) 
{
	die($die_start."Por favor revise que la base de datos est&eacute; bien configurada".$die_end);
}
else
{
	echo "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Success!!</span> <br /><br />Instalaci&oacute;n correcta, por favor elimine este archivo y cambie los permisos de escritura en el directorio ra&iacute;z.<br /><p style='font-family: verdana;'>Haga clic <a href='adlogin.php'>aqu&iacute;</a> para ir al panel de administraci&oacute;n.</p>";
}

?>