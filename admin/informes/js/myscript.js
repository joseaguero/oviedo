$(document).ready(function(){
	showSubMenu();
});

function showSubMenu(){
		$('.nav-totales:has(ul)').click(function(){
			if ($(this).hasClass('activado')) {
				$(this).removeClass('activado');
				$(this).children('ul').slideUp();
			}else{
				$('.nav-totales ul').slideUp();
				$('.nav-totales').removeClass('activado');

				$(this).children('ul').slideDown();
				$(this).addClass('activado');
			}
		});
	}