<div class="contenedor">
        
        <div class="selectores">
        	<form method="get" action="../../results.php">
                <div class="grafico">
                    <select name="tienda" id="tienda">
                        <option value="0">Seleccione una tienda</option>
                        <?php switch ($tienda) {
                            case 'Amoble':
                                echo '<option value="Amoble" selected="selected">Amoble</option>';
                                echo '<option value="Forastero">Forastero</option>';
                                echo '<option value="MercadoJardin">Mercado Jardín</option>';
                                echo '<option value="Hbt">Hbt</option>';
                                echo '<option value="todas">Todas</option>';
                                break;
                            case 'Forastero':
                                echo '<option value="Amoble">Amoble</option>';
                                echo '<option value="Forastero" selected="selected">Forastero</option>';
                                echo '<option value="MercadoJardin">Mercado Jardín</option>';
                                echo '<option value="Hbt">Hbt</option>';
                                echo '<option value="todas">Todas</option>';
                            break;
                            case 'MercadoJardin':
                                echo '<option value="Amoble">Amoble</option>';
                                echo '<option value="Forastero">Forastero</option>';
                                echo '<option value="MercadoJardin" selected="selected">Mercado Jardín</option>';
                                echo '<option value="Hbt">Hbt</option>';
                                echo '<option value="todas">Todas</option>';
                            break;
                            case 'Hbt':
                                echo '<option value="Amoble">Amoble</option>';
                                echo '<option value="Forastero">Forastero</option>';
                                echo '<option value="MercadoJardin">Mercado Jardín</option>';
                                echo '<option value="Hbt" selected="selected">Hbt</option>';
                                echo '<option value="todas">Todas</option>';
                            break;
                            case 'todas':
                                echo '<option value="Amoble">Amoble</option>';
                                echo '<option value="Forastero">Forastero</option>';
                                echo '<option value="MercadoJardin">Mercado Jardín</option>';
                                echo '<option value="Hbt">Hbt</option>';
                                echo '<option value="todas" selected="selected">Todas</option>';
                            break;
                        }?>
                    </select>
                </div>
                <div class="representacion">
                    <select name="representacion" id="representacion">
                        <option selected="selected" value="0">Seleccione Representación</option>
                        <option value="1">Pedidos por mes/año</option>
                        <option value="2">Ventas por mes/año</option>
                        <option value="3">Cantidad Productos por mes/año</option>
                        <option value="4">Retiro en Tienda</option>
                        <option value="5">Tipo de Pago</option>
                    </select>
                </div>
            	<input type="submit" name="Graficar" value="Graficar" id="graficar" />
            </form>
        </div>
        <div id="error"></div>
</div>
<script type="text/javascript">
$(function(){ 
    $("select").uniform(); 
});

$('#graficar').click(function(){
    if ($('#representacion').val() == 0 || $('#tienda').val() == 0) {
        $('#error').fadeIn();
        $('#error').css('display', 'block');

        $('#error').html('<p>Debe seleccionar tienda + representación</p>');
        return false;
    }else{
        $('#error').css('display', 'none');
        return true;
    }
});
</script>