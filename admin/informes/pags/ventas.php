<?php
	include('../../conf.php');
	require_once "../functions.php";
	$tienda = "Ejemkplo moldeable";//$_GET['tienda'];
	$service = "";
	$serv1 = ""; $serv2 = ""; $serv3 = ""; $serv4 = "";

	//$select = "SELECT nombre, servicio FROM tienda WHERE url='$tienda'";
	//$query = mysqli_query($conexion, $select);

	//$result = mysqli_fetch_assoc($query);

	

	
	$totalVentas = totalVentas("https://mercadojardin.cl/ws/ws.php");
	$tiendaName = "Ejemkplo moldeable";//$result["nombre"];
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda <?php echo $tiendaName; ?> - Gráfico Ventas</title>
	<script src="../../js/jquery-1.8.0.js"></script>
	<script src="../../js/highcharts.js"></script>
	<script src="../../js/exporting.js"></script>

	<script type="text/javascript" src="../../js/jquery.uniform.js"></script>

	<link rel="stylesheet" href="../../css/themes/aristo/css/uniform.aristo.css" type="text/css" media="screen" charset="utf-8" />

	<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
	<link href="../../css/font-awesome.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript">
	$(function () {
	    var ventas = new Highcharts.Chart({
	        chart: {
	            type: 'column',
	            backgroundColor: '#FAFAFA',
	            renderTo: 'grafico_ventas'
	        },
	        title: {
	            text: 'Valor ventas por mes y año'
	        },
	        xAxis: {
	            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ]

	        },
	        yAxis: {
	            title: {
	                text: 'Total por mes'
	            }
	        },
	        series: [{
		        name: '2016',
		        data: [

		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
					echo $ventas2016[$i] . ',';
		        		endfor; 
		        	else:
		        		for($i = 0; $i < 12; $i++):
		        			echo $totalVentas['2016'][$i] . ',';
		        		endfor;
		        	endif;
		        	?>
		        ],
		        tooltip: {
		            valuePrefix: '$ '
		        }
		    	}, {
		    	name: '2017',
			    data: [
			    	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
					echo $ventas2017[$i] . ',';
		        		endfor; 
		        	else:
		        		for($i = 0; $i < 12; $i++):
		        			echo $totalVentas['2017'][$i] . ',';
		        		endfor;
		        	endif;
		        	?>
		        ],
		        tooltip: {
		            valuePrefix: '$ '
		        }
		    	},{

			   	name: '2018',
			    data: [
			    	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
					echo $ventas2018[$i] . ',';
		        		endfor; 
		        	else:
		        		for($i = 0; $i < 12; $i++):
		        			echo $totalVentas['2018'][$i] . ',';
		        		endfor;
		        	endif;?>
		        ],
		        tooltip: {
		            valuePrefix: '$ '
		        }
		    }]
	    });

	
	});
</script>
</head>
<body>
        <div class="contenedor">
            <h1>Tiendas Moldeable</h1>
            <?php 
                if(!isset($tiendaName)):
                    echo "<h2>Elije una opcion a mostrar</h2>";
                else:
                    echo "<h2>Tienda " . $tiendaName . "</h2>";
                endif;
            ?>
            
            
            <div class="selectores">
                <form method="get" action="results.php">
                    <div class="grafico">
                        
                    </div>
                    <div class="representacion">
                        <select name="representacion" id="representacion">
                            <option value="0" >Seleccione Representación</option>
                            <option value="1" >Pedidos por mes/año</option>
                            <option value="2">Ventas por mes/año</option>
                            <option value="3">Cantidad Productos por mes/año</option>
                            <option value="4">Retiro en Tienda</option>
                            <option value="5">Tipo de Pago</option>
                        </select>
                    </div>
                    <input type="submit" name="Graficar" value="Graficar" id="graficar" />
                </form>
            </div>
            <div id="error"></div>
        </div>
    
    
    <div class="flecha"><a href="home"><i class="fa fa-reply fa-3x" aria-hidden="true"></i></a></div>
	<div class="contenedor" style="margin-top:20px;">
        <?php //include('../index.php'); ?>

        <div class="total-por-anio">
        	<h1>Total Ventas</h1>
        	<h2>POR AÑO</h2>
        	<div class="box">
        		<p>2016</p>
        		<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
        		<?php echo number_format($totalVentas['t2016'][0]); ?>
        		</p>
        	</div>

        	<div class="box">
        		<p>2017</p>
        		<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
        		<?php echo number_format($totalVentas['t2017'][0]); ?>
        		</p>
        	</div>

        	<div class="box">
        		<p>2018</p>
        		<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
        		<?php echo number_format($totalVentas['t2018'][0]); ?>
        		</p>
        	</div>
        </div>
        <div class="div-g">
        	<div id="grafico_ventas" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
            <!--<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>-->
        </div><!--fin selectores-->
    </div>
</body>
</html>