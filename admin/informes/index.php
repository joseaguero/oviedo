<?php 
    //include('conex.php');

    //$select = "SELECT url, nombre FROM tienda ORDER BY nombre";
    //$query = mysqli_query($conexion, $select);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tiendas Moldeable</title>
<base href="http://localhost/desarrollo/admin/informes">
<!--<base href="http://localhost/m/">-->
<script type="text/javascript" src="js/jquery-1.8.0.js"></script>
<script type="text/javascript" src="js/jquery.uniform.js"></script>

<link rel="stylesheet" href="css/themes/aristo/css/uniform.aristo.css" type="text/css" media="screen" charset="utf-8" />

<link href="css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
        <div class="contenedor">
        <h1>Tiendas Moldeable</h1>
        <?php 
            if(!isset($tiendaName)):
                echo "<h2>Elije una opcion a mostrar</h2>";
            else:
                echo "<h2>Tienda " . $tiendaName . "</h2>";
            endif;
        ?>
        
        
        <div class="selectores">
        	<form method="get" action="results.php">
                <div class="grafico">
                    
                </div>
                <div class="representacion">
                    <select name="representacion" id="representacion">
                        <option value="0" >Seleccione Representación</option>
                        <option value="1" >Pedidos por mes/año</option>
                        <option value="2">Ventas por mes/año</option>
                        <option value="3">Cantidad Productos por mes/año</option>
                        <option value="4">Retiro en Tienda</option>
                        <option value="5">Tipo de Pago</option>
                    </select>
                </div>
            	<input type="submit" name="Graficar" value="Graficar" id="graficar" />
            </form>
        </div>
        <div id="error"></div>
    </div>
</body>
<script type="text/javascript">
$(function(){ 
	$("select").uniform(); 
});

$('#graficar').click(function(){
    if ($('#representacion').val() == 0 || $('#tienda').val() == 0) {
        $('#error').fadeIn();
        $('#error').css('display', 'block');

        $('#error').html('<p>Debe seleccionar tienda + representación</p>');
        return false;
    }else{
        $('#error').css('display', 'none');
        return true;
    }
});
</script>
</html>