<?php 

include('../conf.php');
include('../includes/tienda/cart/inc/functions.inc.php');
include('../../PHPMailer/PHPMailerAutoload.php');
date_default_timezone_set('America/Santiago');

$oc = mysqli_real_escape_string($conexion, $_POST['oc']);
$accion = mysqli_real_escape_string($conexion, $_POST['accion']);

$asunto = ($accion == 1) ? 'Tu Pedido ha sido Confirmado': 'Tu pedido ha sido anulado';
$url_sitio = opciones('url_sitio');
$logo = $url_sitio.'/images/logo.png';

$pedido = consulta_bd('nombre, total_pagado, email, id, estado_id', 'pedidos', "oc = '$oc'", '');

$id_pedido = $pedido[0][3];
$id_estado = $pedido[0][4];

if ($id_estado == 2) {
	echo json_encode(array('status' => 'error'));
}else{
	if ($accion == 1) {
		$body = '<body style="background: #f4f4f4;">
		<div style="border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; max-width: 600px; margin: auto; background: #fff; padding: 15px;">
			<img src="'.$logo.'">

			<h4 style="color: #db4437;">Estimado(a) '.$pedido[0][0].'</h4>

			<p style="color: #4e4e4e;">Gracias por comprar en HerramientasChile. La disponibilidad de los productos ofrecidos por HerramientasChile a través de su sitio web puede variar en función de la demanda de los clientes o disponibilidad de los proveedores. Pese a que HerramientasChile actualiza el stock periódicamente, el producto solicitado por el cliente podría estar agotado en el momento de la compra. En tal caso, HerramientasChile remitirá un e-mail al cliente informándole de la imposibilidad de dar cumplimiento al pedido</p>

			<p style="color: #4e4e4e;"><strong>A continuación le enviamos los datos de su pedido:</strong> <br>
			OC: '.$oc.' <br>
			Monto: $'.number_format($pedido[0][1], 0, ",", ".").'</p>

			<p>El plazo de entrega estimado de tu pedido es de 48  a 72 Horas Hábiles </p>
			<p>Tu pedido está en preparación, estaremos validando tus datos y la disponibilidad de Stock, en caso de algún inconveniente te estaremos contactado vía telefónica o por correo electrónico para poder solucionarlo.</p>

			<p>Recibirás un correo de confirmación de envió cuando tu paquete este en tránsito. Para que puedas hacer un seguimiento de entrega de tu pedido.</p>

			<p>Agradecemos tu confianza y te invitamos a seguir viendo nuestras ofertas y promociones en nuestra página web: <a href="https://herramientaschile.cl/">herramientaschile.cl</a></p>
		</div>
		</body>';

		$estado_pago = 'Pago confirmado';
	}else{
		$body = '<body style="background: #f4f4f4;">
		<div style="border-top: 2px solid #db4437; border-bottom: 2px solid #db4437; max-width: 600px; margin: auto; background: #fff; padding: 15px;">
			<img src="'.$logo.'">

			<h4 style="color: #db4437;">Estimado(a) '.$pedido[0][0].'</h4>

			<p style="color: #4e4e4e;">Damos aviso que su pedido con número de orden '.$oc.' ha sido anulado, ya que, no se realizó la transferencia correspondiente.</p>

			<p>Cualquier duda comunicarse con servicioalcliente@ferretek.cl</p>

			<p>Atte, HerramientasChile.cl</p>
		</div>
		</body>';

		$estado_pago = 'Pago anulado';
	}


	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");

	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	//Set an alternative reply-to address

	$mail->addAddress($pedido[0][2], $pedido[0][0]);
	$mail->Subject = $asunto;
	$mail->msgHTML($body);
	$mail->AltBody = $body;
	$mail->CharSet = 'UTF-8';
	//send the message, check for errors
	if (!$mail->send()) {
	    echo json_encode(array('status' => 'error'));
	} else {
		$fecha = date("Y-m-d H:i:s", time());
		if ($accion == 1) {
			update_bd('pedidos', "transferencia_pagada = 1, estado_envio = '$estado_pago', estado_id = 2, fecha_modificacion = '$fecha'", "oc = '$oc'");
			// GenerarEnvio($id_pedido);
			exec( "php -f ../../ajax/postEnviame.php $id_pedido", $output, $return_var);
		}else{
			update_bd('pedidos', "transferencia_pagada = 0, estado_envio = '$estado_pago', estado_id = 3, fecha_modificacion = '$fecha'", "oc = '$oc'");
		}
		
		echo json_encode(array('status' => 'success'));
	}
}



?>