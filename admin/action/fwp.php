<?php
include ("../conf.php");

if ($_POST[fwp])
{
	if ($_POST[mail])
	{
		
		$url_q = consulta_bd("valor","opciones","nombre = 'url_sitio'","");
		$url_sitio = $url_q[0][0];
		
		$email = $_POST[mail];
		$ok = comprobar_mail($email);
		if ($ok)
		{
			$hash = md5(date("Ymds"));
			$update = update_bd("administradores","password_hash = '$hash'","email = '$email'");
			$cant = mysqli_affected_rows($conexion);
			if ($cant == 1)
			{
				$para = $email;
				$asunto = "Olvido de contraseña";
	
				$header = "From: Moldeable CMS <no-reply@moldeable.com>\nReply-To:no-reply@moldeable.com\n";
				$header .= "X-Mailer:PHP/".phpversion()."\n";
				$header .= "Mime-Version: 1.0\n";
				$header .= "Content-Type: text/plain";
				
				$msg .= "Estimado Cliente:";
				$msg .= "\n\n";
				$msg .= "Se ha solicitado un cambio de contraseña desde el administrador de contenidos de su sitio web, si Ud. no ha solicitado este cambio no debe realizar ninguna acción.";
				$msg .= "\n\n";
				$msg .= "Para poder realizar el cambio de contraseña haga clic en el siguiente link o cópielo en su navegador:";
				$msg .= "\n\n";
				$msg .= "$url_sitio/admin/adlogin.php?op=fgp&hash=$hash&e=$email";
				$msg .= "\n\n";
				$msg .= "Muchas gracias.";
				$msg .= "\n\n";
				$msg .= "Equipo de Moldeable S.A.";
				$aviso = mail($para, $asunto, utf8_decode($msg) ,$header);
				if ($aviso)
				{
					$error = "Se le ha enviado un correo con las instrucciones para el cambio de contraseña.&tipo=ventana";
				}
				else
				{
					$error = "Error al generar el correo, por favor inténtelo nuevamente.&tipo=error";
				}
				
			    /*Log*/
			    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date","'administradores','Solicitud de cambio de contraseña enviada', '','$row', NOW()");
			    /*Fin Log*/
				
			}
			else
			{
				$error = "No se ha encontrado el correo ingresado en nuestros registros, por favor regístrese nuevamente.&tipo=ventana";
			}
		}
		else
		{
			$error = "Correo inválido&tipo=error";
		}
	}
	else
	{
		$error = "Debe ingresar su correo.&tipo=notificacion";
	}
}

header("location:../adlogin.php?error=$error");


?>