<?php
include_once('../conf.php');
backup_tables('*',0);

/* backup the db OR just a table */
function backup_tables($tables = '*', $download)
{
	$conexion = $GLOBALS['conexion'];
	//$link = mysql_connect($host,$user,$pass);
	//mysql_select_db($name,$link);
	
	//get all of the tables
	if($tables == '*')
	{
		$tables = array();
		$result = mysqli_query($conexion, 'SHOW TABLES');
		while($row = mysqli_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}
	
	//cycle through
	foreach($tables as $table)
	{
		$result = mysqli_query($conexion, 'SELECT * FROM '.$table);
		$num_fields = mysqli_num_fields($result);
		
		$return.= 'DROP TABLE '.$table.';';
		$row2 = mysqli_fetch_row(mysqli_query($conexion, 'SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";
		
		for ($i = 0; $i < $num_fields; $i++) 
		{
			while($row = mysqli_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j<$num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = ereg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j<($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
	}
	
	//save file
	$file_name = "db-backup-".date('Y_m_d').'.sql';
	$handle = fopen($file_name,'w+');
	fwrite($handle,$return);
	fclose($handle);
	
	if (!file_exists('backup/')) {
	    mkdir('backup/', 0777, true);
	}
	
	$new_file = 'backup/'.$file_name;
	
	//Move file
	$copy = copy($file_name, $new_file);
	if ($copy)
	{
		//Delete local File
		unlink($file_name);	
	}
	
	//Download file
	if (file_exists($new_file) and $download == 1) {
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($new_file));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($new_file));
	    ob_clean();
	    flush();
	    readfile($new_file);
	    exit;
	}
	


	
}
?>