<?php
	date_default_timezone_set('America/Santiago');
	include('../conf.php');
	mysqli_query($conexion, "SET NAMES 'latin1'");
	
	header("Content-type: application/octet-stream");
	header("Content: charset=UTF-8");
	header("Content-Disposition: attachment; filename=clientes_BD.xls"); 
	header("Pragma: no-cache" );
	header("Expires: 0"); 
	
	$hoy = date("d/m/Y");
?>

<h3>Base de datos de clientes, fecha de reporte <?php echo "$hoy";?></h3>
<table width="700" border="1">
  <tr >
  	<td>N°</td>
  	<td>Id</td>
    <td>Nombre</td>
    <td>Email</td>
    <td>Telefono</td>
    <td>Empresa</td>
    <td>Newsletter?</td>
    <td>Fecha de registro</td>
    <td>Cantidad de compras</td>
  </tr>

<?php    
	$sql = "
		SELECT c.id, c.nombre, c.email, c.telefono,c.empresa, c.newsletter, c.fecha_registro, COUNT(p.id) FROM clientes c LEFT JOIN  pedidos p ON c.id = p.cliente_id GROUP BY c.id;";
	$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)." <br />$sql");;
	$i=1;
	while ($fila = mysqli_fetch_array($run))
	{
	
			$news = ($fila[5] == 1) ? "Si" : "No";
			
			echo '<tr>
					<td>'.$i.'</td>
					<td>'.$fila[0].'</td>
					<td>'.$fila[1].'</td>
					<td>'.$fila[2].'</td>
					<td>'.$fila[3].'</td>
					<td>'.$fila[4].'</td>
					<td>'.$news.'</td>
					<td>'.$fila[6].'</td>
					<td>'.$fila[7].'</td>
				</tr>';
			$i++;
		
	}
  ?>

</table>