<!--<li>
    	<a href="javascript:void(0)" class="menu" id="contraerMenu">
        	<span class="iconoMenuAccion"><i class="fas fa-angle-double-left"></i></span>
            <span class="iconoMenuAccion2"><i class="fas fa-angle-double-right"></i></span>
        </a>
    </li>-->
<aside>
	<div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
			<ul class="sidebar-menu" id="nav-accordion">
	
	<?php
		$tablas = consulta_bd("t.id, t.is_sub_menu, t.op, t.parent, tp.permiso_id, t.submenu_table, t.icono", "tablas t, tablas_perfiles tp", "t.parent = 0 AND tp.tabla_id = t.id AND tp.perfil_id = $perfil_admin AND t.show_in_menu = 1", "posicion");
		$i = 0;
		while($i <= (sizeof($tablas)-1))
		{	
			$access_menu = ($tablas[$i][4] != 1) ? true:false;

			if ($access_menu)
			{
				$is_sub_menu = $tablas[$i][1];
				if (!$is_sub_menu)
				{
					if ($tablas[$i][3] == 0)
					{
						$op_link = ($tablas[$i][2] != '') ? $tablas[$i][2]: $tablas[$i][0].'a';
						$nombre = get_table_name($tablas[$i][0]);
						if($op_link == $op){ $activo = 'activo';} else {$activo = '';}
						echo '<li class="current1 '.$activo.'">
								<a href="index.php?op='.$op_link.'" class="menu">
									<span class="iconoMenu">'.$tablas[$i][6].'</span>
									<span class="nomMenu">'.$nombre.'</span>
								</a>
							  </li>';
					}
				}
				elseif ($tablas[$i][5] != '')
				{
					$submenu_table = $tablas[$i][5];
                                        if(substr_count($submenu_table, ",")!=0){
                                            $tablas_menu = explode(",", $submenu_table);
                                            $submenu_table = $tablas_menu[0];
                                        }
                                        $menu_items = consulta_bd("nombre, id",$submenu_table,"","");
					$nombre = get_table_name($tablas[$i][0]);
					
					echo '<li class="submenu current2"><a class="menu">'.$nombre.'</a>';
					echo '<ul class="sub" >';
					$sm = 0;
					while($sm <= (sizeof($menu_items)-1))
					{	
						$op_link_sm = $tablas[$i][0].'a&id='.$menu_items[$sm][1].'&r='.$submenu_table;
						echo '<li class="current3"><a href="index.php?op='.$op_link_sm.'" class="menu">'.ucwords($menu_items[$sm][0]).'</a></li>';
						$sm++;
					}
					echo '</ul>';
				}
				else
				{
					$parent_id = $tablas[$i][0];
					$nombre = get_table_name($tablas[$i][0]);
					
					echo '<li class="submenu current4 '.$parent_id.'">
							<a class="menu">
								<span class="iconoMenu">'.$tablas[$i][6].'</span>
								<span class="nomMenu">'.$nombre.'</span>
							</a>';
					  echo '<ul class="sub">';
					$childs = consulta_bd("t.id, t.op, tp.permiso_id, t.is_sub_menu","tablas t, tablas_perfiles tp","t.parent = $parent_id AND tp.tabla_id = t.id AND tp.perfil_id = $perfil_admin ","posicion");
					$j = 0;
					while($j <= (sizeof($childs)-1))
					{	
						$access = $childs[$j][2];
						if ($access != 1)
						{
							$nombre_child = get_table_name($childs[$j][0]);
							$is_sub_sub_menu = $childs[$j][3];
							if ($is_sub_sub_menu)
							{
								$child_id = $childs[$j][0];
								echo '<li class="current6"><a class="menu">'.$nombre_child.'</a><ul>';
								$grandsons = consulta_bd("t.id, t.op, tp.permiso_id, t.is_sub_menu","tablas t, tablas_perfiles tp","t.parent = $child_id AND tp.tabla_id = t.id AND tp.perfil_id = $perfil_admin ","posicion");
								$z = 0;
								while($z <= (sizeof($grandsons)-1))
								{	
									$op_link = ($grandsons[$z][1] != '') ? $grandsons[$z][1]: $grandsons[$z][0].'a';
									$nombre_grandsons = get_table_name($grandsons[$z][0]);
									echo '<li class="current7"><a href="index.php?op='.$op_link.'" class="menu">'.$nombre_grandsons.'</a></li>';
									$z++;
								}
								echo '</ul></li>';
							}
							else
							{
								$op_link = ($childs[$j][1] != '') ? $childs[$j][1]: $childs[$j][0].'a';
								if($op_link == $op){$activoSub = 'currentSub';} else {{$activoSub = '';}}
								echo '<li class="'.$activoSub.'"><a href="index.php?op='.$op_link.'" class="menu">'.$nombre_child.'</a></li>';
							}
						}
						$j++;
					}
					echo '</ul></li>';
				} 
			}
			$i++;
		}
	?>
    <li class="submenu"><a href="javascript:void(0)" class="menu ">
        <span class="iconoMenu"><i class="fas fa-file-excel"></i></span>
        <span class="nomMenu">Descargas Xls</span></a>
        <ul class="sub">
        	<li class="">
            	<a href="exportarProductosXLS.php" class="menu">Descargar productos</a>
            </li>
            <li class="">
            	<a href="exportarPedidos.php" class="menu">Descargar compras</a>
            </li>
        </ul>
    </li>
    
    <?php  if (opciones("informe_tienda") == 1){ ?>
    <li class="submenu <?php if($op == 'informes'){echo 'activo';}?>">
    	<a href="javascript:void(0)" class="menu ">
            <span class="iconoMenu"><i class="fas fa-chart-line"></i></span>
            <span class="nomMenu">Informes</span>
        </a>
        <ul class="sub">
        	<li class="<?php if($op == 'ventas'){echo 'currentSub';}; ?>">
            	<a href="index.php?op=ventas" class="menu">Ventas por mes/año</a>
            </li>
            <li class="<?php if($op == 'pedidos'){echo 'currentSub';}; ?>">
            	<a href="index.php?op=pedidos" class="menu">Pedidos por mes/año</a>
            </li>
            
            <li class="<?php if($op == 'productos'){echo 'currentSub';}; ?>">
            	<a href="index.php?op=productos" class="menu">Cantidad Productos por mes/año</a>
            </li>
            <li class="<?php if($op == 'retiro-tienda'){echo 'currentSub';}; ?>">
            	<a href="index.php?op=retiro-tienda" class="menu">Productos Retiro en tienda</a>
            </li>
            <li class="<?php if($op == 'tipo-pago'){echo 'currentSub';}; ?>">
            	<a href="index.php?op=tipo-pago" class="menu">Formas de pago</a>
            </li>
            
        </ul>
    </li>

    <?php } ?>
    
	<!--<li><a href="action/logout.php" class="menu salir"><span class="iconoMenu"><i class="fas fa-user-times"></i></span><span class="nomMenu">Salir</span></a></li>-->
    
</ul>
		</div>
        <!-- sidebar menu end-->
    </div>
</aside>



<?php /*?>

<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="index.html">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-laptop"></i>
                        <span>Layouts</span>
                    </a>
                    <ul class="sub">
                        <li><a href="boxed_page.html">Boxed Page</a></li>
                        <li><a href="horizontal_menu.html">Horizontal Menu</a></li>
                        <li><a href="language_switch.html">Language Switch Bar</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span>UI Elements</span>
                    </a>
                    <ul class="sub">
                        <li><a href="general.html">General</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
<li><a href="typography.html">Typography</a></li>
                        <li><a href="widget.html">Widget</a></li>
                        <li><a href="slider.html">Slider</a></li>
                        <li><a href="tree_view.html">Tree View</a></li>
                        <li><a href="nestable.html">Nestable</a></li>
                        <li><a href="grids.html">Grids</a></li>
                        <li><a href="calendar.html">Calender</a></li>
                        <li><a href="draggable_portlet.html">Draggable Portlet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="fontawesome.html">
                        <i class="fa fa-bullhorn"></i>
                        <span>Fontawesome </span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Data Tables</span>
                    </a>
                    <ul class="sub">
                        <li><a href="basic_table.html">Basic Table</a></li>
                        <li><a href="responsive_table.html">Responsive Table</a></li>
                        <li><a href="dynamic_table.html">Dynamic Table</a></li>
                        <li><a href="editable_table.html">Editable Table</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span>Form Components</span>
                    </a>
                    <ul class="sub">
                        <li><a href="form_component.html">Form Elements</a></li>
                        <li><a href="advanced_form.html">Advanced Components</a></li>
                        <li><a href="form_wizard.html">Form Wizard</a></li>
                        <li><a href="form_validation.html">Form Validation</a></li>
                        <li><a href="file_upload.html">Muliple File Upload</a></li>

                        <li><a href="dropzone.html">Dropzone</a></li>
                        <li><a href="inline_editor.html">Inline Editor</a></li>

                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-envelope"></i>
                        <span>Mail </span>
                    </a>
                    <ul class="sub">
                        <li><a href="mail.html">Inbox</a></li>
                        <li><a href="mail_compose.html">Compose Mail</a></li>
                        <li><a href="mail_view.html">View Mail</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Charts</span>
                    </a>
                    <ul class="sub">
                        <li><a href="morris.html">Morris</a></li>
                        <li><a href="chartjs.html">Chartjs</a></li>
                        <li><a href="flot_chart.html">Flot Charts</a></li>
                        <li><a href="c3_chart.html">C3 Chart</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Maps</span>
                    </a>
                    <ul class="sub">
                        <li><a href="google_map.html">Google Map</a></li>
                        <li><a href="vector_map.html">Vector Map</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-glass"></i>
                        <span>Extra</span>
                    </a>
                    <ul class="sub">
                        <li><a href="blank.html">Blank Page</a></li>
                        <li><a href="lock_screen.html">Lock Screen</a></li>
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="invoice.html">Invoice</a></li>
                        <li><a href="pricing_table.html">Pricing Table</a></li>
                        <li><a href="timeline.html">Timeline</a></li>                    
<li><a href="gallery.html">Media Gallery</a></li><li><a href="404.html">404 Error</a></li>
                        <li><a href="500.html">500 Error</a></li>
                        <li><a href="registration.html">Registration</a></li>
                    </ul>
                </li>
                <li>
                    <a href="login.html">
                        <i class="fa fa-user"></i>
                        <span>Login Page</span>
                    </a>
                </li>
            </ul>            </div>
        <!-- sidebar menu end-->
    </div>
</aside><?php */?>