<?php 
/**
 * Clase para automatizar procesos con MercadoLibre
 *
 * Para ocupar esta clase se necesita una tabla en la base de datos llamada mercadolibre con las siguientes columnas:
 * ambiente => Se utiliza para saber que credenciales ocupar (Siempre tener una de desarrollo y otra producción)
 * appid
 * secret_key
 * userid => Para obtener el user Id visitar (https://developers.mercadolibre.com/es_ar/producto-autenticacion-autorizacion) y bajar hasta donde dice ¡Obtén tu access_token!
 * redirect_url => Debe ser la misma configurada en la aplicación.
 * token
 * refresh_token
 * 
 * @version v1
*/

require_once('meli/meli.php');

class MercadoLibre
{
	public $appId;
	public $secretKey;
	public $idUser;
	public $redirectURI;
	public $siteId;
	public $token;
	public $ambiente = 'env';
	public $refresh_token;

	public $meli_obj;
	
	function __construct()
	{
		$data = consulta_bd('appid, secret_key, userid, token, refresh_token, redirect_url', 'mercadolibre', "ambiente = '$this->ambiente'", '');
		$this->appId = $data[0][0];
		$this->secretKey = $data[0][1];
		$this->idUser = $data[0][2];
		$this->redirectURI = $data[0][5]; // Probando propio SimpleOAuth
		$this->siteId = 'MLC';
		$this->token = $data[0][3];
		$this->refresh_token = $data[0][4];
		$this->meli_obj = new Meli($this->appId, $this->secretKey);
	}

	public function get_token(){	

		if (!isset($_GET['code'])) {
			header('Location: '.$this->get_code());
		}else{
			return $this->authorize($_GET['code']);
		}
	}

	public function notifications(){
		// Recepcionamos los datos enviados por post y transformamos el json en un objeto de php.
		$json = file_get_contents("php://input");
		$data = json_decode($json, true);

		// Refrescamos el token;
		$access_token = json_decode($this->refresh_token(), true);
		// Vemos el tipo de notificación enviada (orders, items, payments, etc) y su ID
		$type = $data['topic'];
		$id = explode('/', $data['resource'])[2];

		if ($type == 'orders') {
			$json_orden = file_get_contents("https://api.mercadolibre.com/orders/{$id}?access_token=".$access_token['access_token']);

			$orden = json_decode($json_orden, true);
			$status = $orden['payments'][0]['status'];
			$status_detail = $orden['payments'][0]['status_detail'];
			$fecha_creacion = $orden['date_created'];
			$id_meli = $orden['order_items'][0]['item']['id'];
			$qty = $orden['order_items'][0]['quantity'];
			$sku = $orden['order_items'][0]['item']['seller_custom_field'];
			$monto = $orden['total_amount'];
			$monto_con_despacho = $orden['total_amount_with_shipping'];
			$total_pagado = $orden['paid_amount'];
			$metodo_pago = $orden['payments'][0]['payment_method_id'];
			$usuario = $orden['buyer']['nickname'];
			$nombre = $orden['buyer']['first_name'] . ' ' . $orden['buyer']['last_name'];
			$email = $orden['buyer']['email'];
			// var_dump($orden['payments'][0]['status_detail']);
			insert_bd('mercadolibre_logs', 'message', "'Orden: {$id} está: {$status} y el status_detail es: {$status_detail}, Se compraron {$qty} productos | SKU: {$sku}'");
			if ($status_detail == 'accredited') {
				$existe_orden = consulta_bd('id_orden', 'pedidos_mercadolibre', "id_orden = '$id'");
				if (!is_array($existe_orden)) {
					update_bd('productos_detalles', "stock = stock - {$qty}", "sku = '{$sku}'");
					insert_bd('pedidos_mercadolibre', 'id_orden, fecha_creacion, item_comprado, sku, cantidad, total, total_con_despacho, total_pagado, metodo_de_pago, estado, comprador, nombre, email', "'$id', '$fecha_creacion', '$id_meli', '$sku', $qty, $monto, $monto_con_despacho, $total_pagado, '$metodo_pago', '$status', '$usuario', '$nombre', '$email'");
				}
			}
		}
	}

	/*
	* Retorna la url para autorizar al usuario.
	*/
	public function get_code(){
		$url = "https://auth.mercadolibre.cl/authorization?response_type=code&client_id=".$this->appId;
		return $url;
	}

	/*
	* Proceso para autorizar el uso de la aplicación al usuario.
	* También guarda el access_token y el refresh_token para poder utilizarlo más adelante.
	*/
	public function authorize($code){
		$data = array(
			'grant_type' => 'authorization_code',
			'client_id' => $this->appId,
			'client_secret' => $this->secretKey,
			'code' => $code,
			'redirect_uri' => $this->redirectURI
		);

		$url = 'https://api.mercadolibre.com/oauth/token';

		$request = $this->execute($url, "POST", $data);

		if ($request['body']->status != NULL) {
			$out = array(
				'status' => 'error',
				'messagge' => $request['body']
			);
		}else{
			$this->token = $request['body']->access_token;
			$this->refresh_token = $request['body']->refresh_token;
			if (update_bd('mercadolibre', "token = '$this->token', refresh_token = '$this->refresh_token'", "ambiente = '$this->ambiente'")) {
				$out = array(
					'status' => 'success',
					'messagge' => 'Acceso autorizado y datos guardados en la base de datos'
				);
			}
		}

		return json_encode($out);
	}

	/*
	* Refresca el access_token para así facilitar uno válido siempre.
	*/
	public function refresh_token(){
		$data = array(
			'grant_type' => 'refresh_token',
			'client_id' => $this->appId,
			'client_secret' => $this->secretKey,
			'refresh_token' => $this->refresh_token
		);

		$url = 'https://api.mercadolibre.com/oauth/token';
		$request = $this->execute($url, "POST", $data);

		if ($request['body']->status == NULL) {
			$out['access_token'] = $request['body']->access_token;
		}else{
			$out = array(
				'status' => 'error',
				'messagge' => $request['body']
			);
		} 

		return json_encode($out);
	}

	/*
	* Agregar todos los productos a mercadolibre
	* La función necesita un objeto json llamado products.
	* Sirve para agregar 1 o más productos.
	*/
	public function add_products(){
		if (isset($_POST)) {
			$products = file_get_contents("php://input");

			$products = json_decode($products, true);

			// Refrescamos el token;
			$access_token = json_decode($this->refresh_token(), true);
			foreach ($products['products'] as $aux) {
				// Variables enviadas por POST, todas son obligatorias.
				$title = $aux['title'];
				$price = (int)$aux['price'];
				$stock = (int)$aux['stock'];
				$description = $aux['description'];
				$sku = $aux['sku'];
				$warranty_text = $aux['warranty_text'];
				$status = $aux['status'];
				// id tienda oficial
				// $id_tienda = $aux['official_store_id']; 

				$images = array();

				$image_1 = ($aux['image_1'] != '' OR $aux['image_1'] != NULL) ? $images[0] =$aux['image_1'] : "";
				$image_2 = ($aux['image_2'] != '' OR $aux['image_2'] != NULL) ? $images[1] =$aux['image_2'] : "";
				$image_3 = ($aux['image_3'] != '' OR $aux['image_3'] != NULL) ? $images[2] =$aux['image_3'] : "";
				$image_4 = ($aux['image_4'] != '' OR $aux['image_4'] != NULL) ? $images[3] =$aux['image_4'] : "";
				$image_5 = ($aux['image_5'] != '' OR $aux['image_5'] != NULL) ? $images[4] =$aux['image_5'] : "";
				$image_6 = ($aux['image_6'] != '' OR $aux['image_6'] != NULL) ? $images[5] =$aux['image_6'] : "";

				$index = 0;
				foreach ($images as $value) {
					$images_ok[$index]['source'] = $value;
					$index++;
				}

				$item = array(
			        "title"                 => $title,
			        "category_id"           => "MLC3530",
			        "price"                 => $price,
			        "currency_id"           => "CLP",
			        "available_quantity"    => $stock,
			        "buying_mode"           => "buy_it_now",
			        "listing_type_id"       => "bronze", // tipo de publicacion
			        "condition"             => "new",
			        "description"           => array( "plain_text" => $description ),
			        "seller_custom_field"   => $sku,
			        "status"				=> $status,
			        "tags" => array(
				        "immediate_payment"
				    ),
			        "warranty"              => $warranty_text,
			        "shipping"              => array(
			            "mode"          => "custom",
			            "local_pick_up" => false,
			            "free_shipping" => false,
			            "free_methods"  => []
			        ),
			        "pictures" => $images_ok
			    );

			    $out = $this->meli_obj->post('/items', $item, array('access_token' => $access_token['access_token']));
			    $out = json_decode(json_encode($out), true);
			    print_r($out);
			    $id_meli = $out['body']['id'];
				update_bd('productos_detalles', "id_meli = '$id_meli'", "sku = '$sku'");
			    // echo "<-- " . $out['body']['id'] . " -->";
			}
		    
		}else{
			return 'false';
		}
	}

	/*
	* Edita 1 o más productos.
	* La función necesita un objeto json llamado products.
	* A diferencia que la función add_products esta necesita el parametro id_meli.
	*/
	public function edit_products(){
		if (isset($_POST)) {
			$products = file_get_contents("php://input");
			$products = json_decode($products, true);
			// Refrescamos el token;
			$access_token = json_decode($this->refresh_token(), true);
			foreach ($products['products'] as $aux) {
				// Variables enviadas por POST, todas son obligatorias.
				$title = $aux['title'];
				$price = (int)$aux['price'];
				$stock = (int)$aux['stock'];
				$description = $aux['description'];
				$sku = $aux['sku'];
				$id_meli = $aux['id_meli'];
				$status = $aux['status'];

				$images = array();

				$image_1 = ($aux['image_1'] != '' OR $aux['image_1'] != NULL) ? $images[0] =$aux['image_1'] : "";
				$image_2 = ($aux['image_2'] != '' OR $aux['image_2'] != NULL) ? $images[1] =$aux['image_2'] : "";
				$image_3 = ($aux['image_3'] != '' OR $aux['image_3'] != NULL) ? $images[2] =$aux['image_3'] : "";
				$image_4 = ($aux['image_4'] != '' OR $aux['image_4'] != NULL) ? $images[3] =$aux['image_4'] : "";
				$image_5 = ($aux['image_5'] != '' OR $aux['image_5'] != NULL) ? $images[4] =$aux['image_5'] : "";
				$image_6 = ($aux['image_6'] != '' OR $aux['image_6'] != NULL) ? $images[5] =$aux['image_6'] : "";

				$index = 0;
				foreach ($images as $value) {
					$images_ok[$index]['source'] = $value;
					$index++;
				}

				$item = array(
					"title" 				=> $title,
					"price" 				=> $price,
					"available_quantity"	=> $stock,
					"pictures" 				=> $images_ok,
					"status"				=> $status
				);
				
				$out = $this->meli_obj->put("/items/{$id_meli}", $item,  array('access_token' => $access_token['access_token']));
				$body = array("plain_text" => $description);
				if($description){
					$this->meli_obj->put("/items/{$id_meli}/description", $body, array('access_token' => $access_token['access_token']));
				}
				$out = json_decode(json_encode($out), true);
			    print_r($out);
			}  
		}else{
			return 'false';
		}
	}

	public function paused_products(){
		if (isset($_POST)) {
			$products = file_get_contents("php://input");
			$products = json_decode($products, true);
			// Refrescamos el token;
			$access_token = json_decode($this->refresh_token(), true);
			foreach ($products['products'] as $aux) {
				$id_meli = $aux['id_meli'];
				$status = $aux['status'];

				$item = array(
					"status" => $status 
				);

				$out = $this->meli_obj->put("/items/{$id_meli}", $item,  array('access_token' => $access_token['access_token']));
				$out = json_decode(json_encode($out), true);
			    print_r($out);
			}  
		}else{
			return 'false';
		}
	}

	public function active_products(){
		if (isset($_POST)) {
			$products = file_get_contents("php://input");
			$products = json_decode($products, true);
			// Refrescamos el token;
			$access_token = json_decode($this->refresh_token(), true);
			foreach ($products['products'] as $aux) {
				// Variables enviadas por POST, todas son obligatorias.
				$id_meli = $aux['id_meli'];
				$status = $aux['status'];

				$item = array(
					"status" => $status 
				);

				$out = $this->meli_obj->put("/items/{$id_meli}", $item,  array('access_token' => $access_token['access_token']));
				$out = json_decode(json_encode($out), true);
			    print_r($out);
			}  
		}else{
			return 'false';
		}
	}

	public function edit_images(){
		$products = file_get_contents("php://input");
		$products = json_decode($products, true);
		// Refrescamos el token;
		$access_token = json_decode($this->refresh_token(), true);
		foreach ($products['products'] as $aux) {
			$id_meli = $aux['id_meli'];

			$images = array();

			$image_1 = ($aux['image_1'] != '' OR $aux['image_1'] != NULL) ? $images[0] =$aux['image_1'] : "";
			$image_2 = ($aux['image_2'] != '' OR $aux['image_2'] != NULL) ? $images[1] =$aux['image_2'] : "";
			$image_3 = ($aux['image_3'] != '' OR $aux['image_3'] != NULL) ? $images[2] =$aux['image_3'] : "";
			$image_4 = ($aux['image_4'] != '' OR $aux['image_4'] != NULL) ? $images[3] =$aux['image_4'] : "";
			$image_5 = ($aux['image_5'] != '' OR $aux['image_5'] != NULL) ? $images[4] =$aux['image_5'] : "";
			$image_6 = ($aux['image_6'] != '' OR $aux['image_6'] != NULL) ? $images[5] =$aux['image_6'] : "";

			$index = 0;
			foreach ($images as $value) {
				$images_ok[$index]['source'] = $value;
				$index++;
			}

			$item = array(
				"pictures" => $images_ok 
			);

			$this->meli_obj->put("/items/{$id_meli}", $item,  array('access_token' => $access_token['access_token']));
		}  
	}

	public function edit_stock(){
		$products = file_get_contents("php://input");
		$products = json_decode($products, true);
		// Refrescamos el token;
		$access_token = json_decode($this->refresh_token(), true);
		foreach ($products['products'] as $aux) {
			$id_meli = $aux['id_meli'];
			$stoc = (int)$aux['stock'];

			$item = array(
				"available_quantity"	=> $stock
			);

			$this->meli_obj->put("/items/{$id_meli}", $item,  array('access_token' => $access_token['access_token']));
		}  
	}

	public function compra_web($oc){
		$productos = consulta_bd('id_meli, stock', 'productos_detalles pd JOIN productos_pedidos pp ON pd.id = pp.productos_detalle_id JOIN pedidos p ON p.id = pp.pedido_id', "p.oc = '$oc'");

		$access_token = json_decode($this->refresh_token(), true);
		foreach ($productos as $producto) {
			if ($producto[0] != '' OR $producto[0] != NULL) {
				$item = array(
					"available_quantity"	=> $producto[1]
				);

				$out = $this->meli_obj->put("/items/{$producto[0]}", $item,  array('access_token' => $access_token['access_token']));
			}
		}
	}

	/*
		- Ejecuta una acción CURL.
	*/
	public function execute($url, $type, $data){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$return["body"] = json_decode(curl_exec($ch), false);
		curl_close($ch);

		return $return;
	}


	public function exist_token(){
		$query = consulta_bd('token, refresh_token', 'mercadolibre', "ambiente = '$this->ambiente'", '');
		if ($query[0][0] != NULL OR $query[0][0] != '' AND $query[0][1] != NULL OR $query[0][1] != '') {
			$out['access_token'] = $query[0][0];
			$out['refresh_token'] = $query[0][1];
			return $out;
		}else{
			return false;
		}
	}

}


?>