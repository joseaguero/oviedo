<?php 
// header('Content-Type: application/json');
include('../../conf.php');
require_once('mercadolibre.class.php');

$action = $_GET['op'];
$params = $_GET;
$out = NULL;
foreach ($params as $param => $value) {
	if ($param != 'op') {
		$out[$param] = $value;
	}
}

$obj = new MercadoLibre();

if ($out != null) {
	echo $obj->$action($out);
}else{
	echo $obj->$action();
}

?>