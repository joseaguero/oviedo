<?php
require 'meli.php';

$meli = new Meli('8588887725855473', 'pbWndk96mankz3GMEY3kUTvWHTlrJZJL';
$redirectURI = 'https://clientes-moldeable.com/proyectos/apimeli/admin/includes/apimeli/meli/oauth.php';

if($_GET['code']){ 
    // Autorizamos la acción.
    $user = $meli->authorize($_GET['code'], $redirectURI);

    // Creamos las sesiones.
    $access_token = $user['body']->access_token;
    $expires_in = $user['body']->expires_in;
    $refresh_token = $user['body']->refresh_token;

    // Si el token expira, generamos uno nuevo.
    if($expires_in + time() + 1 < time()) {
        try {
            print_r($meli->refreshAccessToken());
        } catch (Exception $e) {
            echo "Exception: ",  $e->getMessage(), "\n";
        }
    }

    $obj = array('access_token' => $access_token);

    echo json_encode($obj);
}else{
    $url = $meli->getAuthUrl($redirectURI, Meli::$AUTH_URL['MLC']);
    header('Location: '.$url);
}
