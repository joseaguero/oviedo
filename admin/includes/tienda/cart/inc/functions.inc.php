<?php
//total de items agregados al carro de compra
function totalCart(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto_madre = consulta_bd('p.id', "productos p join productos_detalles pd on pd.producto_id = p.id", "pd.id = $prd_id", '');
			if ($is_cyber AND is_cyber_product($producto_madre[0][0])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber']*$qty;
			}else{
				$total += getPrecio($prd_id)*$qty;
			}
		}
	}
	return round($total);
}

//cantidad de productos en el carro
function qty_pro(){
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		$items = explode(',',$cart);
		return count($items);
	}
	else
	{
		return 0;
	}
}

function ultimasUnidades($pid){
	$ultimas = get_option('ultimas_unidades');
	if($ultimas){
		$cant = get_option('cant_ultimas_unidades');
		$prd = consulta_bd("stock","productos_detalles","id = $pid","");
		if((int)$prd[0][0] <= (int)$cant){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function ofertaTiempo($id){
	$oferta = consulta_bd("oferta_tiempo_activa, oferta_tiempo_hasta","productos_detalles","id=$id","");
	if($oferta){

		$fecha_actual = strtotime(date("Y-m-d H:i:s",time()));
		$fecha_entrada = strtotime($oferta[0][1]);

		if($oferta[0][0] and ($fecha_entrada > $fecha_actual)){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function ofertaTiempoHasta($id){
	$oferta = consulta_bd("oferta_tiempo_hasta","productos_detalles","id=$id","");
	return $oferta[0][0];
}
function ofertaTiempoDescuento($id){
	$oferta = consulta_bd("oferta_tiempo_descuento","productos_detalles","id=$id","");
	return $oferta[0][0];
}

function getPrecio($pd){
	$is_cyber 	= (opciones('cyber') == 1) ? true : false;
	$detalles 	= consulta_bd("pd.precio, pd.descuento, p.id, pd.precio_cyber","productos_detalles pd join productos p on p.id = pd.producto_id","pd.id = $pd","");
	$precio 	= $detalles[0][0];

	if ($is_cyber AND is_cyber_product($detalles[0][2])) {
		$descuento 	= $detalles[0][3];
	}else{
		$descuento 	= $detalles[0][1];
	}

	if(ofertaTiempo($pd)){
		if(ofertaTiempoDescuento($pd) > 0){
			$descuento = ofertaTiempoDescuento($pd);
		}
	}

	if($descuento AND $precio > $descuento){
		$precio_final = $descuento;
	}else{
		$precio_final = $precio;
	}

	return $precio_final;
}
function getPrecioNormal($pd){
	$detalles 	= consulta_bd("precio","productos_detalles","id = $pd","");
	return $detalles[0][0];
}
function tieneDescuento($pd){
	$detalles 	= consulta_bd("precio, descuento","productos_detalles","id = $pd","");
	$precio 	= $detalles[0][0];
	$descuento 	= $detalles[0][1];
	if($descuento AND $precio > $descuento) return true;
	else return false;
}
function get_cyber_price($pd){
	$sql = consulta_bd("precio, precio_cyber", "productos_detalles", "id = $pd", "");
	$out['precio'] = $sql[0][0];
	$out['precio_cyber'] = $sql[0][1];
	return $out;
}




function ShowCart(){
	global $db;

	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima, pd.stock","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}

			$stock_prd = $producto[0][7];
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}

			$no_disponible = ($_GET['stock'] == $prd_id) ? 1 : 0;

			
			   
	$output[] .='<div class="filaProductos" id="fila_carro_'.$prd_id.'">
                	<div class="imgFila">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="imagenes/productos/'.$thumbs.'" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCart">
						<span class="title_row">Producto</span>
						<div class="nombreFila">
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</div>
						<div class="skuShowCart">SKU: '.$producto[0][5].'</div>
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					<div class="precioFila">';
						if (tieneDescuento($prd_id)) {
							$output[] .= '<span class="title_row mbottom-5">Precio unitario</span>';
						}else{
							$output[] .= '<span class="title_row">Precio unitario</span>';
						}
						
						if (tieneDescuento($prd_id)) {
							$output[] .= '<span class="descuento_row">$'.number_format($producto[0][2],0,",",".").'</span>';
						}	
						$output[] .= '<span>$'.number_format($valorUnitario,0,",",".").'</span> <span class="unidadMovil">c/u
						</span>
					</div>
					
					<div class="totalFila">
                    	<span class="title_row">Total ítem</span>
                    	<span>$'.number_format($valor,0,",",".").'</span>
                    </div>

                    <div class="cantFila">';

                    if($no_disponible){
                    	$output[] .='<div class="sin-stock">Sin stock</div>';
                    }

           $output[] .='
			<span class="title_row mbottom-5">Cantidad</span>
           				<div class="box_main_qty_cart select_carro" data-id="'.$prd_id.'">
                        	<input type="text" name="cant" class="campoCantCarroResumen cantidadProducto" value="'.$qty.'" onclick="openSelect(this)" readonly />

                        	<div class="box_select_stock">';

			                    if ($stock_prd > 10):
			                        
			                        for ($i=0; $i < 10; $i++) {
			                        	$contadorr = $i+1;
			                            $output[] .='<div class="row_stock" onclick="changeSelect(this)" data-val="'. $contadorr .'">'. $contadorr .'</div>';
			                        } 

			                        $output[] .='<div class="row_stock" onclick="changeSelect(this)" data-val="add">+ más</div>';
			                    else:
			                        for ($i=0; $i < $stock_prd; $i++) {
			                        	$contadorr = $i+1;
			                            $output[] .='<div class="row_stock" onclick="changeSelect(this)" data-val="'.$contadorr.'">'. $contadorr .'</div>';
			                        }
			                    endif;
			                    
			                $output[] .='</div>
                        </div>
                    </div>
                    
					<div class="botoneraShowCart botoneraMovil">
						<a href="javascript:void(0)" onclick="eliminaItemCarro('.$prd_id.')">Eliminar</a>';
						if (isset($_COOKIE['usuario_id'])) {
							$output[] .= '<span> | </span>
						<a href="javascript:void(0)" onclick="guardarParaDespues('.$prd_id.')">Guardar para despues</a>';
						}
						
					$output[] .= '</div>
					<div class="botoneraShowCart">
						<a href="javascript:void(0)" onclick="eliminaItemCarro('.$prd_id.')">Eliminar</a>';
						if (isset($_COOKIE['usuario_id'])) {
							$output[] .= '<span> | </span>
							<a href="javascript:void(0)" onclick="guardarParaDespues('.$prd_id.')">Guardar para despues</a>';
						}
					$output[] .= '</div>
                </div><!-- fin filaProductos-->';
		}
			
	} else {
		$output[] = '<div class="carroVacio">
						<p class="carroVacio">Su carro está vacío.</p><br />
					</div>';
	}
	return join('',$output);
	
}


function saveForLater(){
	global $db;
	$cliente_id = $_COOKIE['usuario_id'];
	//$listaDeseos = $_SESSION['listaDeseos'];
	$query = consulta_bd('producto_id', 'productos_guardados', "cliente_id = $cliente_id", '');

	if (is_array($query)) {
		$itemDiferente = count($query);

		$output[] = '<div class="tituloGuardados">Articulos guardados para despues <span>('.$itemDiferente.')</span></div>';
		
		
		for ($i=0; $i<sizeof($query); $i++){
         	$prd_id = $query[$i][0];
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, p.descripcion, pd.sku","productos p, productos_detalles pd","p.id=pd.producto_id and pd.producto_id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);
			
			
			   
	$output[] .='<div class="filaProductos" id="fila_carro_guardado'.$prd_id.'">
                	<div class="imgFila ancho20">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="imagenes/productos/'.$thumbs.'" width="100%" />
						</a>
					</div>
					<div class="contCarroGuardado">
						<div class="nombreFila">
							<span class="title_row">Producto</span>
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>

							<span class="skuGuardados">SKU: '.$producto[0][6].'</span>
						</div>
					</div><!-- fin contCarroGuardado -->
                    <div class="precioFilaGuardado">
                    	<span class="title_row">Total ítem</span>
						<span>$'.number_format($valorUnitario,0,",",".").'</span>


						<div class="botonesFilaGuardado">
							<a class="" href="javascript:void(0)" onclick="eliminaFavorito('.$prd_id.')" rel="'.$prd_id.'">Eliminar</a>
							<span> | </span>
							<a class="" href="javascript:void(0)" onclick="moverAlCarro('.$prd_id.')" rel="'.$prd_id.'">Mover al carro</a>
						</div>
					</div>
                </div><!-- fin filaProductos-->';
		}//fin for
		
		
		
		
		
		
			
			
	} 
	return join('',$output);
	}






function guardadoParaDespues($id){
	if(!isset($_COOKIE[listaDeseos])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$listaDeseos = $_COOKIE[listaDeseos];

	if ($listaDeseos) {
		$items = explode(',',$listaDeseos);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$estado = 0;
		foreach ($contents as $prd_id => $qty) {
			if($id == $prd_id){
				$estado = 1;
				}
		}
	} 
	return $estado;
}



function resumenCompra(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		$i = 1;
		$total = 0;
		$cantArticulos = 0;
		foreach ($contents as $prd_id=>$qty) {
			$sql = consulta_bd("pd.id, pd.precio, pd.descuento, p.id","productos_detalles pd join productos p on p.id = pd.producto_id","p.id=$prd_id","");

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$total 			+= $valorUnitario * $qty;
					}else{
						$total += getPrecio($prd_id) * $qty;
					}
				}else{
					$total += getPrecio($prd_id) * $qty;
				}
			}else{
				$total += getPrecio($prd_id) * $qty;	
			}
			
			$cantArticulos = $cantArticulos + $qty;
		}
		
	}
	$neto = $total/1.19;
	$iva = $total - $neto;
	$resumen = '<div class="ancho100">
				<div class="title_resumen">Resumen</div>
				<div class="cont_gray">
					<h3>Subtotal ('.$cantArticulos.' articulos): <span>$'.number_format(round($total),0,",",".").'</span></h3>
					<div class="ancho100 filatoolTip">*El valor del despacho se definira en el tercer paso del carro</div>
					<a href="envio-y-pago" class="btnCompletarCompra btn" id="btnCompletarCompra">ir a pagar</a>
				</div>
				</div>';
	$resumen .= '<div class="cont100carro1 fixedCarro1">
					<div class="cont100Centro centroFixedCarro1">
						<a href="envio-y-pago" class="btnCompletarCompra">ir a pagar</a>
						<div class="valoresCarro1Fixed"><span>Subtotal('.$cantArticulos.' articulos):</span> <span class="montoAPagarFixed"><strong>$'.number_format(round($total),0,",",".").'</strong></span></div>
					</div>
				</div>';
	if(qty_pro() > 0){
		return $resumen;
		} else {
		//return false;
		}
	
}







function vistosRecientemente($vista, $cantidad) {
	global $db;
	$recientes = $_COOKIE['productosRecientes'];
	if ($recientes) {
		$items = explode(',',$recientes);
		$items = array_reverse($items);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '<div class="title_resumen">Vistos recientemente</div>';
		$i = 0;
		foreach ($contents as $id=>$qty) {
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.id","productos p, productos_detalles pd","p.id=$id and p.id = pd.producto_id","");
			$cant = count($producto);
			
			if($cant > 0){
				$j = $j + 1;
				if($producto[0][0] != ''){
					$thumbs = $producto[0][0];
				} else {
					$thumbs = "img/sinImagenGrilla.jpg";
				}
				
				$valorUnitario 	= getPrecio($producto[0][6]);
	
				
				//solo muestro los ultimos 3
				if($cantidad > 0){
					$cantidad = $cantidad;
					} else {
						$cantidad = 99999999999;
						}
				
				if($vista == "grilla"){
					$vista = "grilla";
					} else {
						$vista = "filaProductosVistos";
						}		
		
				if($i < $cantidad){	
						   
				$output[] .='<div class="cont_gray"><div class="'.$vista.'" >
								<div class="imgFila ">
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
										<img src="imagenes/productos/'.$thumbs.'" width="100%" />
									</a>
								</div>
								<div class="contInfoShowCart">
									<div class="nombreFila">
										<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
											<span>'.$producto[0][1].'</span>
										</a>
									</div>
									<!-- <div class="precioFila"><span>$'.number_format($valorUnitario,0,",",".").'</span></div> -->
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">Ver ficha</a>
								</div><!-- fin contInfoShowCart-->
								
							</div></div><!-- fin filaProductos-->';
							$i = $i + 1;
							
						}
						
				} //fin condicion de cantidad
		}
			
	} 
	
	return join('',$output);
	
}


function resumenCompraShort($comuna_id, $retiro) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/



			$precio_final = getPrecio($prd_id) * $qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}

			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			$despacho = valorDespacho($comuna_id);//3000;//$address[0][5];
		$output[] = '
					<div class="valores">
                        <div class="filaValor">
							<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						
    					<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
                        </div>
						<div class="filaValor">';
						if (is_numeric($despacho)) {
							$output[] = '<div class="montoValor">$'.number_format($despacho,0,",",".").'</div>';
						}else{
							$output[] = '<div class="montoValor">Por pagar</div>';
						}
							$output[] = '<div class="nombreValor">Envío:</div>
                        </div>
						';
    				$tiene_descuento = (isset($_SESSION['descuento'])) ? descuentoBy($_SESSION['descuento']) : 0;
					if($tiene_descuento == 1){
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($_SESSION['val_descuento'],0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>
										 ';
					}
					
		$output[] = '	
					<div class="filaValor">
						<div class="montoValor total_carro">$'.number_format(($total_neto+$iva+$despacho)-$_SESSION['val_descuento'],0,",",".").'</div>
						<div class="nombreValor">Total:</div>
                    </div>
						
               </div>';
        /*if ($retiro == 0) {
        	$output[] = '<div class="mensajesDespacho">Su tiempo estimado de envio es de 5 días hábiles.</div>';
        }*/
					
					
		
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('',$output);
	
}

function get_precio_total($comuna_id, $retiro){

	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	} 
	$valorTotal = 0;
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");



			$precio_final = getPrecio($prd_id) * $qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}

			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			$despacho = ($retiro == 0) ? valorDespacho($comuna_id) : 0;//3000;//$address[0][5];
	}

	$valorTotal = $total_neto+$iva+$despacho;

	return $valorTotal;

}

//resumen de productos en paso identificacion y envio
function resumenCompraShort2($comuna_id) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
	
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/
			
			$precio_final = getPrecio($prd_id) *$qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}
			
			
		$output[] .= '<div class="filaResumen">
                       <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="imgThumbsCartHeader">
                              <img src="imagenes/productos/'.$productos[0][4].'" width="100%" />
                       </a>
					   <div class="datosProductoCart3">
						   <div class="tituloProductoResumen">
							   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'">'.$productos[0][1].'</a>
						   </div>
						   <div class="cantResumen">'.$qty.'</div>
						   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="valorFilaHeader text-right">$'.number_format($precio_final,0,",",".").'</a>
					   </div>
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			$despacho = valorDespacho(0);
		
		$output[] = '
					<div class="valores">
						<div class="filaValor">
							<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
						</div>
						<div class="filaValor">
							<div class="montoValor">$'.number_format($despacho,0,",",".").'</div>
							<div class="nombreValor">Envío:</div>
						</div>
					';
    				
					if(isset($_SESSION["descuento"])){
							$codigo = $_SESSION["descuento"];
							$descuento = consulta_bd("id, codigo, valor, porcentaje","codigo_descuento","codigo = '$codigo' and activo = 1","");
							if($descuento[0][3] > 0){
								$nPorcentaje = $descuento[0][3]/100;
								$descuentoProducto = round($nPorcentaje*$total);
							} else if($descuento[0][2] > 0){
								$descuentoProducto = $descuento[0][2];
							} else {
								$descuentoProducto = 0;
							}
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($descuentoProducto,0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>';
					}
					
		$output[] = '<div class="filaValor">
						<div class="montoValor">$'.number_format(($total_neto+$iva+$despacho)-$descuentoProducto,0,",",".").'</div>
						<div class="nombreValor">Total:</div>
                        
                    </div>
				</div>';
					
					
		
	} else {
		$output[] = '<p>Su carro está vacío.</p><br />';
	}
	return join('',$output);
	
}



//valor despacho, REGLAS POR DEFINIR SEGUN CADA CASO
function valorDespacho($comuna_id) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];

	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}

		
		$envio = 0;
		$i = 1;
		$peso = 0;
		$volumen = 0;
		
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("peso, ancho, alto, largo","productos_detalles","id = $prd_id","");
			
			$ancho = $productos[0][1];
			$alto = $productos[0][2];
			$largo = $productos[0][3];

			$volumen += round(((($ancho * $largo) * $alto) / 4000)) * $qty;

			$peso += $productos[0][0] * $qty;
			$i++;
		}
			
	} else {
		//$envio = 0;
	}

	if ($volumen < 100 AND $peso < 100) {
		$peso = ($peso > $volumen) ? $peso : $volumen;
	}elseif($volumen > 100 AND $peso < 100){
		$peso = $peso;
	}elseif($volumen < 100 AND $peso > 100){
		$peso = $peso;
	}elseif($volumen > 100 AND $peso > 100){
		$peso = $volumen;
	}

	if ($comuna_id > 0) {

		if ($peso < 30) {

			$valores_despachos = consulta_bd('a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t', 'despachos', "comuna_id = $comuna_id", '');

			if ($peso <= 1) {
				$envio = $valores_despachos[0][0];
			}elseif($peso > 1 AND $peso <= 1.5){
				$envio = $valores_despachos[0][1];
			}elseif($peso > 1.5 AND $peso <= 2){
				$envio = $valores_despachos[0][2];
			}elseif($peso > 2 AND $peso <= 3){
				$envio = $valores_despachos[0][3];
			}elseif($peso > 3 AND $peso <= 4){
				$envio = $valores_despachos[0][4];
			}elseif($peso > 4 AND $peso <= 5){
				$envio = $valores_despachos[0][5];
			}elseif($peso > 5 AND $peso <= 6){
				$envio = $valores_despachos[0][6];
			}elseif($peso > 6 AND $peso <= 7){
				$envio = $valores_despachos[0][7];
			}elseif($peso > 7 AND $peso <= 8){
				$envio = $valores_despachos[0][8];
			}elseif($peso > 8 AND $peso <= 9){
				$envio = $valores_despachos[0][9];
			}elseif($peso > 9 AND $peso <= 10){
				$envio = $valores_despachos[0][10];
			}elseif($peso > 10 AND $peso <= 20){
				$envio = $valores_despachos[0][11];
			}elseif($peso > 20 AND $peso <= 30){
				$envio = $valores_despachos[0][12];
			}elseif($peso > 30 AND $peso <= 40){
				$envio = $valores_despachos[0][13];
			}elseif($peso > 40 AND $peso <= 50){
				$envio = $valores_despachos[0][14];
			}elseif($peso > 50 AND $peso <= 60){
				$envio = $valores_despachos[0][15];
			}elseif($peso > 60 AND $peso <= 70){
				$envio = $valores_despachos[0][16];
			}elseif($peso > 70 AND $peso <= 80){
				$envio = $valores_despachos[0][17];
			}elseif($peso > 80 AND $peso <= 90){
				$envio = $valores_despachos[0][18];
			}elseif ($peso > 90 AND $peso <= 100){
				$envio = $valores_despachos[0][19];
			}elseif ($peso > 100){
				$envio = "x";
			}
		}else{
			$valores_despachos = consulta_bd('a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t', 'despachos_blx', "comuna_id = $comuna_id", '');

			if ($peso <= 1) {
				$envio = $valores_despachos[0][0];
			}elseif ($peso > 1 AND $peso <= 2){
				$envio = $valores_despachos[0][2];
			}elseif ($peso > 2 AND $peso <= 3){
				$envio = $valores_despachos[0][3];
			}elseif ($peso > 3 AND $peso <= 4){
				$envio = $valores_despachos[0][4];
			}elseif($peso > 4 AND $peso <= 5){
				$envio = $valores_despachos[0][5];
			}elseif ($peso > 5 AND $peso <= 6){
				$envio = $valores_despachos[0][6];
			}elseif ($peso > 6 AND $peso <= 7){
				$envio = $valores_despachos[0][7];
			}elseif ($peso > 7 AND $peso <= 8){
				$envio = $valores_despachos[0][8];
			}elseif ($peso > 8 AND $peso <= 9){
				$envio = $valores_despachos[0][9];
			}elseif ($peso > 9 AND $peso <= 10){
				$envio = $valores_despachos[0][10];
			}elseif ($peso > 10 AND $peso <= 20){
				$envio = $valores_despachos[0][11];
			}elseif ($peso > 20 AND $peso <= 30){
				$envio = $valores_despachos[0][12];
			}elseif ($peso > 30 AND $peso <= 40){
				$envio = $valores_despachos[0][13];
			}elseif ($peso > 40 AND $peso <= 50){
				$envio = $valores_despachos[0][14];
			}elseif ($peso > 50 AND $peso <= 60){
				$envio = $valores_despachos[0][15];
			}elseif ($peso > 60 AND $peso <= 70){
				$envio = $valores_despachos[0][16];
			}elseif ($peso > 70 AND $peso <= 80){
				$envio = $valores_despachos[0][17];
			}elseif ($peso > 80 AND $peso <= 90){
				$envio = $valores_despachos[0][18];
			}elseif ($peso > 90 AND $peso <= 100){
				$envio = $valores_despachos[0][19];
			}elseif ($peso > 100){
				$envio = "x";
			}
		}

	}else{
		$envio = 0;
	}
	
	

	if ($envio == "x") {
		return $envio;
	}

	return round($envio * 1.19);
}

function check_carrier(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		$envio = 0;
		$i = 1;
		$peso = 0;
		
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("peso, ancho, alto, largo","productos_detalles","id = $prd_id","");

			$ancho = $productos[0][1];
			$alto = $productos[0][2];
			$largo = $productos[0][3];

			$volumen += round(((($ancho * $largo) * $alto) / 4000)) * $qty;

			$peso += $productos[0][0] * $qty;
			$i++;
		}
			
	} else {
		//$envio = 0;
	}

	if ($volumen < 100 AND $peso < 100) {
		$peso = ($peso > $volumen) ? $peso : $volumen;
	}elseif($volumen > 100 AND $peso < 100){
		$peso = $peso;
	}elseif($volumen < 100 AND $peso > 100){
		$peso = $peso;
	}elseif($volumen > 100 AND $peso > 100){
		$peso = $volumen;
	}

	if ($peso < 30) {
		return 'CHX';
	}else{
		return 'BLX';
	}
}

function showCartExito($oc){
	$pedido = consulta_bd("id, oc, total, valor_despacho, total_pagado, descuento","pedidos","oc='$oc'","");
	$productos_pedidos = consulta_bd("pp.cantidad, pp.precio_unitario, pp.precio_total, p.nombre, p.thumbs, p.id, pd.sku","productos_pedidos pp, productos p, productos_detalles pd","pp.productos_detalle_id = pd.id and pd.producto_id=p.id and pp.pedido_id=".$pedido[0][0],"pp.id");
	
	$carro_exito = '<div class="contCarro">
						<h2>Productos Asociados</h2>
						<div class="cont100 filaTitulos">
							<div class="ancho50"><span style="float:left; margin-left:10px;">Producto</span></div>
							<div class="ancho20">Precio unitario</div>
							<div class="ancho10">Cantidad</div>
							<div class="ancho20">Total Item</div>
						</div>';
		for($i=0; $i<sizeof($productos_pedidos); $i++) {
			$carro_exito .= '<div class="filaProductos">
								<div class="imgFila ancho10">
									<a href="ficha/'.$productos_pedidos[$i][5].'/'.url_amigables($productos_pedidos[$i][3]).'">
										<img src="imagenes/productos/'.$productos_pedidos[$i][4].'" width="100%">
									</a>
								</div>
								<div class="contInfoShowCart">
									<div class="nombreFila">
										<a href="ficha/'.$productos_pedidos[$i][5].'/'.url_amigables($productos_pedidos[$i][3]).'">
											<span>'.$productos_pedidos[$i][3].'</span>
										</a>
									</div>
									<div class="skuShowCart">SKU: '.$productos_pedidos[$i][6].'</div>
								</div>
								
								<div class="precioFila ancho20">
									<span>$'.number_format($productos_pedidos[$i][1],0,",",".").'</span>
									<span class="unidadMovil">c/u</span>
								</div>
								<div class="cantFila ancho10">
									<span>'.$productos_pedidos[$i][0].'</span>
									<span class="unidadMovil">Unidades</span>
								</div>
								<div class="totalFila ancho20">
									<span>$'.number_format($productos_pedidos[$i][2],0,",",".").'</span>
								</div>
							</div>';
		}
		$carro_exito .= '</div>';
	//Fin ciclo
	$carro_exito .= '<div class="contTotalesExito">
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][2],0,",",".").'</span>
							<span class="nomValor">Subtotal</span>
						</div>
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][3],0,",",".").'</span>
							<span class="nomValor">Envío</span>
						</div>
                        <div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][5],0,",",".").'</span>
							<span class="nomValor">Descuento</span>
						</div>
						<div class="cont100 filaValoresExito filaTotal">
							<span class="valorValor">$'.number_format($pedido[0][4],0,",",".").'</span>
							<span class="nomValor">Total</span>
						</div>
					</div>';
	return $carro_exito;
}


//FUNCION PARA SABER TIPO DE PAGO DE WEB PAY Y NUMERO DE CUOTAS
function tipo_pago($tipo_pago,$num_cuotas,$method){
	if ($method == 'tbk') {
		switch ($tipo_pago){
	        case 'VN':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin cuotas";
	            $cuota = "00";
	            break;

	        case 'VC':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Cuotas Normales";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }                
	            break;

	        case 'SI':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin interés";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }
	            break;

	        case 'CI':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Cuotas Comercio";
	            $cuota_valores = strlen($num_cuotas);
	            $cuota_valores = $num_cuotas ." cuotas";
	            
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas ." cuotas";
	            }else{
	                $cuota = $num_cuotas ." cuotas";
	            }
	            break;

	        case 'VD':
	            $tipo_pago = "Débito";
	            $tipo_cuota = "Venta Débito";
	            $cuota = "00";
	            break;
	    }
	}else{
		switch ($tipo_pago) {
			case 'credit_card':
				$tipo_pago = "Crédito";
				$tipo_cuota = ($num_cuotas > 6) ? 'Cuotas normales' : 'Sin interés';
				$cuota_valores = strlen($num_cuotas);
				if ($cuota_valores == 1) {
					$cuota = "0".$num_cuotas;
				}else{
					$cuota = $num_cuotas;
				}
				break;
			
			case 'debit_card':
				$tipo_pago = "Débito";
				$tipo_cuota = 'Venta Débito';
				$cuota = "00";
			break;

			case 'bank_transfer':
				$tipo_pago = "Débito";
				$tipo_cuota = 'Venta Débito';
				$cuota = "00";
			break;
		}
	}
    

    return array("tipo_pago" => $tipo_pago, "tipo_cuota" => $tipo_cuota, "cuota" => $cuota);
}

function payment_type($medio_pago, $type){
	$out = '';
	if ($medio_pago == 'webpay') {

		if ($type == 'VN' OR $type == 'VC' OR $type == 'SI' OR $type == 'CI') {
			$out = 'Credito';
		}elseif($type == 'VD'){
			$out = 'Debito';
		}
		
	}elseif($medio_pago == 'mercadopago'){

		if ($type == 'credit_card') {
			$out = 'Credito';
		}elseif($type == 'debit_card' OR $type == 'bank_transfer'){
			$out = 'Debito';
		}

	}

	return $out;
}

function cambioDePrecio(){
	$cambioPrecios = json_decode($_COOKIE[listaDeseos], true); 
	$cantidad = 0;
	$cadena = "";
	/*
return var_dump($cambioPrecios);
	die();
*/
	//if(!$cambioPrecios){
		for ($i=0; $i<sizeof($cambioPrecios); $i++){
			$id = $cambioPrecios[$i]['id'];
			$valor = $cambioPrecios[$i]['valor'];
				
			$precios = consulta_bd("pd.precio, pd.descuento, p.nombre","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $id","");
			if($precios[0][1] > 0 and $precios[0][1] < $precios[0][0]){
				$precioAplicable = $precios[0][1];
				} else {
					$precioAplicable = $precios[0][0];
				}
			//imprimo valores que cambian
			if($valor > $precioAplicable){
				//bajo de precio
				$cantidad = $cantidad + 1;
				$cadena .='<div class="filaProductoPrecio">'.$precios[0][2].' ha bajado su precio de: <strong class="rojo">$'.number_format($valor,0,",",".").' a $'.number_format($precioAplicable,0,",",".").'</strong></div>';
				$cambioPrecios[$i]['valor'] = "$precioAplicable";
				} else if($valor < $precioAplicable){
					//subio de precio
					$cantidad = $cantidad + 1;
					$cadena .='<div class="filaProductoPrecio">'.$precios[0][2].' ha subido su precio de: <strong class="rojo">$'.number_format($precioAplicable,0,",",".").' a $'.number_format($valor,0,",",".").'</strong></div>';
					$cambioPrecios[$i]['valor'] = "$valor";
					} else {
						//se mantiene el precio
						}
				
				//
			}//Fin for
	
	
	
	//}
	
	 $resultado = '<div class="cont100">
						<div class="cont100Centro">
							
							<div class="contCambioPrecios">
								<h2><strong>'.$cantidad.'</strong> productos han cambiado de precio</h2>
								'.$cadena.'
							</div>
						</div>
					</div>';

		setcookie("listaDeseos", json_encode($cambioPrecios), time() + (365 * 24 * 60 * 60), "/");
	
	

		if($cantidad > 0){
		//solo si un producto cambio su precio respondo
		return $resultado;
		} else {
		
		}

	
}//fin function



/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/   

  
function enviarComprobanteCliente($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = $url_sitio."/img/logo.png";
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
    $msje_despacho = 'Su pedido fue recibido y será procesado para despacho.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion, retiro_en_tienda, direccion, retiro_en_tienda, medio_de_pago, region, despacho_por_pagar","pedidos","oc='$oc'","");
    
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
      
    $id_pedido = $datos_cliente[0][2];
    
    $id_pedidoAdminitrador = $datos_cliente[0][4];  

    $es_retiro = $datos_cliente[0][6];
    $medioDePago = $datos_cliente[0][7];

    $despacho_por_pagar = $datos_cliente[0][9];
      
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_pedidos","pedido_id=$id_pedido and codigo_pack is NULL","");
	
	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos","productos_pedidos","pedido_id=$id_pedido and codigo_pack <> '' group by codigo_pack","");
    
	$despacho = $datos_cliente[0][3];
    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][3].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($detalle_pedido[$i][2],0,",",".").'</p>
											</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
					
					
					for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
						$skuPack = $detalle_pack[$i][4];
						
						$cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id= pd. producto_id and pd.sku='$skuPack'","");
						$productosPorPack = explode(",", $pDP[0][0]);
						$cantArreglo = count($productosPorPack);
					
						
						$pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
						if($pDP[0][2] > 0){
							$precio_unitario = $pDP[0][2];
						} else {
							$precio_unitario = $pDP[0][1];
						}
						//die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

						
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td  align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$skuPack.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($subtotalFila,0,",",".").'</p>
											</td>'; //nombre producto
						$tabla_compra .= '</tr>';

                    }
					

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado","pedidos","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			if($totales[0][1] != '' || $totales[0][1] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}
			
			

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
            if ($despacho_por_pagar == 0) {
            	$tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            }else{
            	$tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">Por pagar</span></td>';
            }
            
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'; text-align:right;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="center" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="150"/>
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="center" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado '.$nombre_cliente.'</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por su compra</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de compra es: <br />
<strong style="color:'.$color_logo.';">'.$oc.'</strong></p>';
                                    if ($medioDePago != 'transferencia') {
                                    	$msg2 .= '<p style="float:right;width:100%;margin: 15px 0px;">
                                        <a style="display:inline-block; color:#fff;padding:10px 20px; margin: 5px 10px; text-decoration:none; background-color:'.$color_logo.';" href="'.$url_sitio.'/tienda/boucher/boucherCompra.php?oc='.$oc.'">
                                            VER VOUCHER
                                        </a>';
                                    }
									
									if ($es_retiro == 0 AND $medioDePago != 'transferencia') {	
										$msg2 .= '<a style="margin:5px 10px; display:inline-block; color:#fff;padding:10px 20px;text-decoration:none; background-color:'.$color_logo.';" href="'.$url_sitio.'/tienda/boucher/boucherCompra.php?oc='.$oc.'">
                                            SEGUIMIENTO
                                        </a>';
                                    }
										
                                    $msg2 .= '</p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #333;">PRODUCTOS COMPRADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">'.$msje_despacho.'</p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;

    
save_in_mailchimp($oc, 'exito');
    save_in_mailchimp($oc, 'todas_compras');
    
	$mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	
    $mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
    //Set an alternative reply-to address

    $mail->addAddress($email_cliente, $nombre_cliente);
    $mail->Subject = $asunto;
    $mail->msgHTML($msg2);
    $mail->AltBody = $msg2;
    $mail->CharSet = 'UTF-8';
    //send the message, check for errors
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return 'envio exitoso';
    }




}


function enviarComprobanteAdmin($oc){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = $url_sitio."/img/logo.png";
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
    
    /*
$email_admin = 'ventas@moldeable.com';
	$email_admin = 'htorres@moldeable.com';
*/
	
    
    $msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_pedido = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre,
    email,
    id,
    direccion, 
    region,
    ciudad,
    comuna,
    direccion,
    telefono,
    rut,
    factura,
    direccion_factura,
    giro,
    telefono_factura,
    rut_factura,
    fecha_creacion,
    telefono,
    regalo,
	razon_social,
	payment_type_code,
	shares_number, 
	transaction_date, 
    comentarios_envio, 
    nombre_retiro,
    apellidos_retiro, 
    rut_retiro, 
    nombre, 
    retiro_en_tienda,
    medio_de_pago, mp_cuotas, mp_payment_type, mp_transaction_date, despacho_por_pagar, nombre_factura","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];

    $payment_type = ($datos_cliente[0][28] == 'webpay') ? $datos_cliente[0][19] : $datos_cliente[0][30];
    $method = ($datos_cliente[0][28] == 'webpay') ? 'tbk' : 'mpago';
    $cuotas = ($datos_cliente[0][28] == 'webpay') ? $datos_cliente[0][20] : $datos_cliente[0][29];
    $fecha = ($datos_cliente[0][28] == 'webpay') ? $datos_cliente[21] : $datos_cliente[0][31];

    $despacho_por_pagar = $datos_cliente[0][32];

    $medioDePago = $datos_cliente[0][28];

    $fecha = date('d/m/Y H:i:s', strtotime($fecha));
    
    $tipo_pago = tipo_pago($datos_cliente[0][19],$datos_cliente[0][20], $method);
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, descuento","productos_pedidos","pedido_id=$id_pedido","");
    $despacho = $datos_cliente[0][3];
    
    


    $tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;font-family: Trebuchet MS, sans-serif; color:#666;">
                    <tr>
                        <th align="center" width="16%">Imagen</th>
                        <th align="center" width="16%">Producto</th>
                        <th align="center" width="16%">SKU</th>
						<th align="center" width="16%">CÓDIGO PACK</th>
                        <th align="center" width="16%">Cantidad</th>
                        <th align="center" width="16%">Precio Unitario</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;
						
						if($detalle_pedido[$i][4] != ""){
							$tabla_compra .= '<tr bgcolor="#efefef">';
							} else {
							$tabla_compra .= '<tr>';
							}
                        
						
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;">
												<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100"/>
											</td>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][3].'</td>'; //codigo producto
						$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][4].'</td>'; //codigo pack
						
                        $tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][1].'</td>'; //cantidad
						
						if($detalle_pedido[$i][5] > 0){
							$tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:'.$color_logo.';">$'.number_format($precio_unitario,0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
							} else {
							$tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:'.$color_logo.';">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
							}
                        

                    }

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado","pedidos","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4]/1.19,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';
			
			$tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">IVA:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format(($totales[0][4]/1.19)*0.19,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			if($totales[0][1] != '' || $totales[0][1] != 0){
				$tabla_compra .='<tr class="cart_total_price">';
            	$tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
            	$tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
				$tabla_compra .='</tr>';
			}
			
			

            $tabla_compra .='     <tr class="cart_total_price">';
            if ($despacho_por_pagar == 1) {
            	$tabla_compra .='       <td align="right" width="15%"><span style="color:#999">Por pagar</span></td>';
            }else{
            	$tabla_compra .='       <td align="right" width="15%"><span style="color:#999">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            }
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="150"/>
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;line-height:20px;">'.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">ha generado una compra web.</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">Su número de compra es: <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                    <p style="float:right;width:100%;margin: 10px 0px 0 0;">
                                        <a style="text-decoration: none; float:right; background-color:'.$color_logo.'; padding:10px 20px; color:#fff;" href="'.$url_sitio.'/tienda/boucher/boucherCompra.php?oc='.$oc.'">
                                            Ver voucher
                                        </a>
                                    </p>
                                </th>
                            </tr>
                        </table>
                        <br/><br/>';
                        
                        if ($medioDePago == 'transferencia') {
							$msg2 .= '<p style="text-align:center;color:#4f4d51;font-size:18px;background:#f8f8f8;padding:20px 0px;border-top:1px solid '.$color_logo.';border-bottom:1px solid '.$color_logo.';">
								Esta es una venta con transferencia bancaria.
							</p>';
						}
                        
                        $msg2 .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>
                                <td valign="top" width="50%">';
                                    if($datos_cliente[0][27] == 0){
                                        
                                $msg2.='<h3>Dirección de entrega</h3>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'</li>
                                            <li><strong style="color:'.$color_logo.';">Region: </strong>'.$datos_cliente[0][4].'</li>
                                            <li><strong style="color:'.$color_logo.';">Ciudad: </strong>'.$datos_cliente[0][5].'</li>
                                            <li><strong style="color:'.$color_logo.';">Comuna: </strong>'.$datos_cliente[0][6].'</li>
                                        </ul>';
                                    } else {
                                $msg2.='<h3>Datos retiro</h3>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][23].' '.$datos_cliente[0][24].'</li>
                                            <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][25].'</li>
                                            <li><strong style="color:'.$color_logo.';">Patente: </strong>'.$datos_cliente[0][26].'</li>
                                        </ul>';
                                    }
                                    
                                    
                                    
                        $msg2.='</td>
                                <td valign="top" width="50%">
                                    <h3>Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][0].'</li>
                                            <li><strong style="color:'.$color_logo.';">Correo: </strong>'.$datos_cliente[0][1].'</li>
                                            <li><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$datos_cliente[0][8].'</li>
                                            <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][9].'</li>
                                            <li><strong style="color:'.$color_logo.';">Comenatrios Cliente: </strong>'.$datos_cliente[0][22].'</li>
                                            
                                        </ul>
                                    </p>
                                    
    
                                </td>
                            </tr>
                        </table>
                        <br/>
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">';
                        if ($medioDePago == 'webpay') {
                        	$msg2.='
								<td valign="top" width="50%">
									<h3>Datos Transbank</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                            <li><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$tipo_pago[tipo_pago].'</li>
                                            <li><strong style="color:'.$color_logo.';">Tipo de cuota: </strong>'.$tipo_pago[tipo_cuota].'</li>
											<li><strong style="color:'.$color_logo.';">Nº Cuotas: </strong>'.$tipo_pago[cuota].'</li>
											<li><strong style="color:'.$color_logo.';">Fecha: </strong>'.$fecha.'</li>
                                        </ul>
                                    </p>
								</td>';
                        }elseif($medioDePago == 'mercadopago'){
                        	$msg2.='
								<td valign="top" width="50%">
									<h3>Datos Mercadopago</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                            <li><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$tipo_pago[tipo_pago].'</li>
                                            <li><strong style="color:'.$color_logo.';">Tipo de cuota: </strong>'.$tipo_pago[tipo_cuota].'</li>
											<li><strong style="color:'.$color_logo.';">Nº Cuotas: </strong>'.$tipo_pago[cuota].'</li>
											<li><strong style="color:'.$color_logo.';">Fecha: </strong>'.$fecha.'</li>
                                        </ul>
                                    </p>
								</td>';
                        }
						
                            $msg2.='<tr>
                                <td valign="top" width="50%">';
                                if($datos_cliente[0][10] == 1){
                            $msg2.='<h3>Datos empresa</h3>
                                    <ul>
                                        <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][33]. '</li>
                                        <li><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][11].'</li>
                                        <li><strong style="color:'.$color_logo.';">Giro: </strong>'.$datos_cliente[0][12].'</li>
                                        <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][14].'</li>
                                        <li><strong style="color:'.$color_logo.';">Telefono: </strong>'.$datos_cliente[0][13].'</li>
                                        <li><strong style="color:'.$color_logo.';">Razón social: </strong>'.$datos_cliente[0][18].'</li>
                                    </ul>';
                                }
                            $msg2.='</td>';
    
	
                            
							
                                   
                        $msg2.='
                            </tr>
                        </table>
                        <br/><br/>
                        
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #797979;">PRODUCTOS COMPRADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">Para ver el detalle de la compra y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'admin/index.php?op=35c&id='.$id_pedido.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;
    
	
		
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = opciones("Host");
		$mail->Port = opciones("Port");
		$mail->SMTPSecure = opciones("SMTPSecure");
		$mail->SMTPAuth = true;
		$mail->Username = opciones("Username");
		$mail->Password = opciones("Password");
		$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	
		if(opciones("correo_admin1") != ""){
			$mail->addAddress(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
			}
		
		if(opciones("correo_admin2") != ""){
			$mail->addAddress(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
			}
			
		if(opciones("correo_admin3") != ""){
			$mail->addAddress(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
			}
		
		$mail->Subject = $asunto;
		$mail->msgHTML($msg2);
		$mail->AltBody = $msg2;
		$mail->CharSet = 'UTF-8';
		if (!$mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return 'envio exitoso';
		}	
}



/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// 
FUNCTIONES COTIZACION*/


function totalCartCotizacion($cotizacion){
	global $db;
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "$cotizacion", time() + (365 * 24 * 60 * 60), "/");
		}
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',',$cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto = consulta_bd("pd.id, pd.precio, pd.descuento, p.id","productos_detalles pd join productos p on p.id = pd.producto_id","pd.id=$prd_id","");
			if ($is_cyber AND is_cyber_product($producto[0][3])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber']*$qty;
			}else{
				if($producto[0][2] > 0){
					$total += $producto[0][2]*$qty;
				} else {
					$total += $producto[0][1]*$qty;
				}
			}
		}
	}
	return round($total);
}

//cantidad de productos en el carro
function qty_proCotizacion(){
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion){
		$items = explode(',',$cotizacion);
		return count($items);
	}
	else
	{
		return 0;
	}
}





function ShowCartCotizacion() {
	global $db;
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',',$cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "img/sinImagenGrilla.jpg";
			}

			$valor = getPrecio($prd_id)*$qty;
			$valorUnitario = getPrecio($prd_id);
			
			
			   
	$output[] .='<div class="filaProductos" id="fila_carro_'.$prd_id.'">
                	<div class="imgFila ancho10">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="imagenes/productos/'.$thumbs.'" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCart">
						<div class="nombreFila">
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</div>
						<div class="skuShowCart">SKU: '.$producto[0][5].'</div>
						<div class="botoneraShowCart">
							<a href="javascript:void(0)" onclick="eliminaItemCarroCotizacion('.$prd_id.')">Eliminar</a>
						</div>
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					<div class="precioFila ancho20"><span>$'.number_format($valorUnitario,0,",",".").'</span></div>
                    <div class="cantFila ancho10">
                    	<div class="pull-left spinnerCarro">
                        	<input type="text" name="cant" class="campoCantCarroResumen" value="'.$qty.'" />
                            <div class="contFlechas">
                                <span class="mas" onclick="agregarElementoCarroCotizacion('.$prd_id.')"  rel="'.$prd_id.'">▲</span>
                                <span class="menos" onclick="quitarElementoCarroCotizacion('.$prd_id.')" rel="'.$prd_id.'">▼</span>
                            </div>
                    	</div>
                    </div>
                    <div class="totalFila ancho20"><span>$'.number_format($valor,0,",",".").'</span></div>
                    
                </div><!-- fin filaProductos-->';
		}
			
	} else {
		$output[] = '<div class="carroVacio">
						<p class="carroVacio">Su carro está vacío.</p><br />
					</div>';
	}
	return join('',$output);
	
}


/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/   

  
  function enviarComprobanteClienteCotizacion($oc){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	$datos_cliente = consulta_bd("nombre,email,id, apellido","cotizaciones","oc='$oc'","");
    
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][3];
    $email_cliente = $datos_cliente[0][1];
      
    $id_cotizacion = $datos_cliente[0][2];
    
    $id_pedidoAdminitrador = $datos_cliente[0][4];  
      
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_cotizaciones","cotizacion_id=$id_cotizacion and codigo_pack is NULL","");
	
	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos","productos_cotizaciones","cotizacion_id=$id_cotizacion and codigo_pack <> '' group by codigo_pack","");
    
	$despacho = $datos_cliente[0][3];
    
    $asunto = "Cotización N°".$oc."";
    $tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][3].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($detalle_pedido[$i][2],0,",",".").'</p>
											</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
					
					
					for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
						$skuPack = $detalle_pack[$i][4];
						
						$cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id= pd. producto_id and pd.sku='$skuPack'","");
						$productosPorPack = explode(",", $pDP[0][0]);
						$cantArreglo = count($productosPorPack);
					
						
						$pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
						if($pDP[0][2] > 0){
							$precio_unitario = $pDP[0][2];
						} else {
							$precio_unitario = $pDP[0][1];
						}
						//die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

						
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="center" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td  align="center" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$skuPack.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($subtotalFila,0,",",".").'</p>
											</td>'; //nombre producto
						$tabla_compra .= '</tr>';

                    }
					

                 

            $tabla_compra .= '</table>';
            
			$totales = consulta_bd("id, fecha_creacion, total","cotizaciones","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][2],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

			
			$tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">por acordar</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'; text-align:right;">$'.number_format($totales[0][2],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="center" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="center" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Estimado '.$nombre_cliente.'</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por cotizar</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de cotización es: <br />
<strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #333;">PRODUCTOS COTIZADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">'.$msje_despacho.'</p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	//return $msg2;
    
  
  
  	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	$mail->addAddress($email_cliente, $nombre_cliente);
    $mail->Subject = $asunto;
    $mail->msgHTML($msg2);
    $mail->AltBody = $msg2;
    $mail->CharSet = 'UTF-8';
    //send the message, check for errors
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return 'envio exitoso';
    }

}


function enviarComprobanteAdminCotizacion($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	$email_admin = 'ventas@moldeable.com';
	
    
    $datos_cliente = consulta_bd("nombre,email,id,apellido","cotizaciones","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][3];
    $email_cliente = $datos_cliente[0][1];
    $id_cotizacion = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre, apellido,
    email,
    id,
    telefono,
    rut,
    fecha_creacion,
    comentarios","cotizaciones","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][1];
    $nombre = $nombre_cliente;
	$email = $datos_cliente[0][2];
	$telefono = $datos_cliente[0][4];
	$rut = $datos_cliente[0][5];
    $comentarios = $datos_cliente[0][7];
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_cotizaciones","cotizacion_id=$id_cotizacion","");
    
    
    


    $tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;font-family: Trebuchet MS, sans-serif; color:#666;">
                    <tr>
                        <th align="center" width="16%">Imagen</th>
                        <th align="center" width="16%">Producto</th>
                        <th align="center" width="16%">SKU</th>
						<th align="center" width="16%">CÓDIGO PACK</th>
                        <th align="center" width="16%">Cantidad</th>
                        <th align="center" width="16%">Precio Unitario</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id=".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;">
												<img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100"/>
											</td>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][3].'</td>'; //codigo producto
						$tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][4].'</td>'; //codigo pack
						
                        $tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        $tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:'.$color_logo.';">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';

                    }

                 

            $tabla_compra .= '</table>';
            
			$totales = consulta_bd("id, fecha_creacion,total","cotizaciones","oc='$oc'","");
			
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][2],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';
			
			
            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">Según acuerdo</span></td>';
            $tabla_compra .='     </tr>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:'.$color_logo.'">$'.number_format($totales[0][2],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';

    
    $asunto = "Cotización N°".$oc."";
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;line-height:20px;">'.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">ha solicitado una cotización.</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">Su número de cotización es: <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                    
                                </th>
                            </tr>
                        </table>
                        <br/><br/>
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>';
                        $msg2.='
                                <td valign="top" width="50%">
                                    <h3>Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$nombre.'</li>
                                            <li><strong style="color:'.$color_logo.';">Correo: </strong>'.$email.'</li>
                                            <li><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$telefono.'</li>
                                            <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$rut.'</li>
                                            <li><strong style="color:'.$color_logo.';">Comenatrios Cliente: </strong>'.$comentarios.'</li>
                                            
                                        </ul>
                                    </p>
                                    
    
                                </td>
                            </tr>
                        </table>
                        <br/>';
                        
                        
                        
                        $msg2.='
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #797979;">PRODUCTOS COTIZADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">Para ver el detalle de la cotización y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'admin/index.php?op=250c&id='.$id_cotizacion.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	//return $msg2;
    
	$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = opciones("Host");
		$mail->Port = opciones("Port");
		$mail->SMTPSecure = opciones("SMTPSecure");
		$mail->SMTPAuth = true;
		$mail->Username = opciones("Username");
		$mail->Password = opciones("Password");
		$mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
	
		if(opciones("correo_admin1") != ""){
			$mail->addAddress(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
			}
		
		if(opciones("correo_admin2") != ""){
			$mail->addAddress(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
			}
			
		if(opciones("correo_admin3") != ""){
			$mail->addAddress(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
			}
			
		$mail->Subject = $asunto;
		$mail->msgHTML($msg2);
		$mail->AltBody = $msg2;
		$mail->CharSet = 'UTF-8';
		if (!$mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return 'envio exitoso';
		}

}

/* FIN FUNCIONES COTIZACION  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */



function get_precio($id, $tipo) {
	$filas = consulta_bd("precio","productos", "id = $id", "");
	$precio = $filas[0][0];
	$descuento = $filas[0][1];
	if ($tipo == 'normal')
	{
		$valor = $precio;
	}
	else if ($tipo == 'oferta')
	{
		if ($descuento != 0 and $descuento > 0)
		{
			$valor = round($precio*(1-$descuento/100));
		}
		else
		{
			$valor = $precio;
		}	
	}
	return $valor;
}

function writeMiniCart() {
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return count($items).' producto'.$s;
	}
	else
	{
		return ' Mis compras';
	}
}
function writeShoppingCart(){
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return '<p>Ud tiene <strong>'.count($items).' producto'.$s.'</strong> en su carro de compras:</p>';
	}
}



function get_total_price(){
	$total = 0;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	
	$items = explode(',',$cart);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}
	
	$i = 1;
	foreach ($contents as $prd_id=>$qty) {

		$precio_final = getPrecio($prd_id) * $qty;

		if(!tieneDescuento($prd_id)){
			$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
			if($pd[0][0]){
				$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
				if($precios_cantidad){
					$pc = $precios_cantidad[0];
					$rango 			= $pc[1];
					$descuento 		= $pc[2];
					$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
					$precio_final 	= $valorUnitario * $qty;
				}
			}
		}



		$total += round($precio_final);
		$i++;
	}
	
	return $total;
}

function generateToken($oc, $monto) {    
	// generar token de forma aleatoria (sin ninguna relación al monto, la relación solo está en la bd)
	$token = md5(uniqid(microtime(), true));
	$insert = insert_bd('checkout_token', 'token, monto, oc', "'$token', $monto, '$oc'");
	
	if ($insert == true) {
		return $token;
	}else{
		return false;
	}
}

function save_in_mailchimp($oc, $donde){
	$mailchimp_module = opciones('mailchimp');
	if ($mailchimp_module == '1') {

		$list = consulta_bd('id_lista', 'mailchimp', "short_name = '$donde'", '');
		$cliente = consulta_bd('nombre, email', 'pedidos', "oc = '$oc'");

		$apikey = opciones('key_mailchimp');

		$n_cliente = explode(' ', $cliente[0][0]);
		$nombre = $n_cliente[0];
		$apellido = $n_cliente[1];

		$data['email'] = $cliente[0][1];
	  	$data['listId'] = $list[0][0];
	  	$data['nombre'] = $nombre;
	  	$data['apellido'] = $apellido;

	  	$mcc = new MailChimpClient($apikey);

	  	$mcc->subscribe($data);

	  	save_all_mailchimp($cliente[0][1], $nombre, $apellido);

	}
}

function save_all_mailchimp($email, $nombre, $apellido){
	$list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');
	$apikey = opciones('key_mailchimp');

	$data['email'] = $email;
  	$data['listId'] = $exito_list[0][0];
  	$data['nombre'] = $nombre;
  	$data['apellido'] = $apellido;

  	$mcc = new MailChimpClient($apikey);

  	$mcc->subscribe($data);
}

function descuentoBy($cod){
	global $db;

	$session_correo = $_SESSION['correo'];

	/* Consultamos si el codigo de descuento es válido */
	$fecha = date('Ymd');
	$consulta = consulta_bd('id, valor, porcentaje, codigo, descuento_opcion_id,donde_el_descuento, cliente_id', 'codigo_descuento',"codigo = '{$cod}' COLLATE utf8_bin and activo = 1 and $fecha >= fecha_desde and $fecha <= fecha_hasta and (cantidad - usados > 0)", "");

	/* Si es válido, seguimos con el proceso de cálculo */
	if (is_array($consulta)) {

		if ($consulta[0][6] == 0) {
			$whatfor = $consulta[0][4]; // A qué se le asigna el descuento (Marca[1], Categorías[2], Subcategorías[3]).
			$id = $consulta[0][5]; // Nombre de la marca, categoría, subcategoría.
			$condition;
			if ($whatfor == 1) {
				$dev = consulta_bd('id, nombre', 'marcas', "", "");
				$tbl = 'marcas';
				$condition = 'and p.marca_id = ';
			}elseif($whatfor == 2){
				$dev = consulta_bd('id, nombre', 'categorias', "", "");
				$tbl = 'categorias';
				$condition = 'and cp.categoria_id = ';
			}else if($whatfor == 3){
				$dev = consulta_bd('id, nombre', 'subcategorias', "", "");
				$tbl = 'subcategorias';
				$condition = 'and cp.subcategoria_id = ';
			}else{
				$condition = '';
			}

			$id_dev = 0;
			for ($i=0; $i < sizeof($dev); $i++) { 
				$nom_compare = url_amigables($dev[$i][1]);
				if ($id == $dev[$i][0]) {
					$id_dev = $dev[$i][0];
				}
			}

			if ($condition != ''){
				$condition .= $id_dev;
			}

			/* Si el descuento es por porcentaje o por valor */
			$porcentaje = ($consulta[0][2] > 0) ? true : false;

			$cart = $_COOKIE['cart_alfa_cm'];
			if ($cart) {
				$items = explode(',',$cart);
				$contents = array();
				foreach ($items as $item) {
					$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
				}
				$output[] = '';

				$valorProducto = 0;
				$encontrados = 0;
				$no_descuento = 0;

				/* Recorremos los productos que tiene el carro */
				foreach ($contents as $prd_id=>$qty) {
					$productos = consulta_bd("p.id, pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
					
					$valorProducto = (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
					$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;

					/* Consulta que devuelve el id del producto si es que cumple la condición  */
					$productosDescuento = consulta_bd('p.id', 'productos p join lineas_productos cp on cp.producto_id = p.id', "p.id = {$productos[0][0]} {$condition} group by p.id","");

					if (is_array($productosDescuento)) { // Si el producto pertenece a la condición del descuento
						$encontrados += $qty;
						$valProductosEncontrados += $valorProducto;

						if ($porcentaje) {
							if ($consulta[0][2] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							}else{
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - ' .$consulta[0][2].'%';
								$calculoFinalValor = ($valProductosEncontrados * $consulta[0][2] / 100);
							}
							
						}else{
							if ($consulta[0][1] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							}else{
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - $' .$consulta[0][1];
								$calculoFinalValor = $consulta[0][1];
							}	
						}
					}else{ 
						$no_descuento += $valorProducto;
					}
				} // End foreach

				$total = $calculoFinalValor + $no_descuento;
					
			}
			if ($encontrados > 0) {
				$resultado = true;
			}else{
				$resultado = false;
			}	
		}else{
			// Si el código existe pero tiene a un usuario asociado
			$tieneAssoc = consulta_bd('cd.cliente_id, cd.codigo, c.email', 'codigo_descuento cd join clientes c on c.id = cd.cliente_id', "c.email = '{$_SESSION['correo']}'", '');

			if (is_array($tieneAssoc) > 0) {

				// Recorremos los descuentos que tiene asociado el cliente
				foreach ($tieneAssoc as $codigo) {

					if ($codigo[1] == $cod) {
						/* Si el descuento es por porcentaje o por valor */
						$porcentaje = ($consulta[0][2] > 0) ? true : false;

						// Trabajamos el carro activo
						$cart = $_COOKIE['cart_alfa_cm'];
						if ($cart) {
							$items = explode(',',$cart);
							$contents = array();
							foreach ($items as $item) {
								$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
							}
							$output[] = '';

							$valorProducto = 0;
							$encontrados = 0;
							$no_descuento = 0;

							$contador_ofertas = 0;
							$cont_general = 0;

							/* Recorremos los productos que tiene el carro */
							foreach ($contents as $prd_id=>$qty) {
								$productos = consulta_bd("p.id, pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");

								if ($productos[0][2] > 0) {
									$valorTotal += 0;
									$contador_ofertas++;
								}else{
									$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
								}
								$cont_general++;
								
							} // End foreach carro

							if ($porcentaje) {
								$calculoFinalValor = ($valorTotal * $consulta[0][2] / 100);
							}else{
								$calculoFinalValor = $consulta[0][1];
							} // End if porcentaje

							if ($cont_general == 1) {
								if ($contador_ofertas > 0) {
									$resultado = false;
								}else{
									$resultado = true;
								}
							}else{
								$resultado = true;
							}
							// Si encuentra el código rompo el ciclo.
							break;
						}else{
							$resultado = false;
						}
					} // End if codigo == cod

				} // End foreach tieneAssoc

			}else{ // Else tieneAssoc

				$resultado = false;

			}
		}
		
	}else{ /* Else ---> if cont > 0 */
		$resultado = false;
	}/* End if cont > 0 */

	if ($resultado == true) {
		if (validarMontoDescuento($calculoFinalValor)) {
			$_SESSION['val_descuento'] = $calculoFinalValor;
			$_SESSION['descuento'] = $cod;
			return 1;
		}else{
			unset($_SESSION['descuento']);
			unset($_SESSION['val_descuento']);
			return 0;
		}	
	}else{
		unset($_SESSION['descuento']);
		unset($_SESSION['val_descuento']);
		return 0;
	}
}

function validarMontoDescuento($valor){
	$precio_carro = (int)get_total_price();

	if ($precio_carro < (int)$valor || $precio_carro == (int)$valor) {
		return false;
	}else{
		return true;
	}
}

/* Retorna si un producto pertenece al cyberday
Parámetro => id producto madre */
function is_cyber_product($id){
	$sql = consulta_bd('p.cyber, pd.precio_cyber','productos p join productos_detalles pd on pd.producto_id = p.id', "p.id = $id", '');

	if ($sql[0][0] == 1 AND $sql[0][1] > 0) {
		return true;
	}else{
		return false;
	}
}

// Retorna true si no encuentra al usuario en la tabla primera_compra.
// True: Enviar codigo, False: No enviar codigo
function enviarCodigo($oc){
	$usuario = consulta_bd('cliente_id', 'pedidos', "oc = '{$oc}'", '');
	$id_usuario = $usuario[0][0];

	$tieneDescuento = consulta_bd('cliente_id', 'first_buy', "cliente_id = {$id_usuario}");
	
	if (!is_array($tieneDescuento)) {
		return true;
	}else{
		return false;
	}

	if ($cont < 1) {
		return true;
	}else{
		return false;
	}
}

function enviarCodigoDescuento($oc, $notification){

	// Consulto si ya tiene un codigo de primera compra
	$consulta_pc = consulta_bd("codigo", "first_buy", "oc = '{$oc}'" , "");
	if ($consulta_pc[0][0] == '' OR $consulta_pc[0][0] == NULL) {
		// Si no tiene un código de descuento, se le crea uno (Notificación 1)
		$codigo_desc = explode("_", $oc);
		$codigo = "COD_".$codigo_desc[1];

		$hoy = date('Y-m-d'); // Fecha desde que se envía el código (HOY)

		update_bd("first_buy", "codigo = '{$codigo}', fecha = '{$hoy}'", "oc = '{$oc}'");

		$id_cliente = consulta_bd('c.id', 'clientes c join pedidos p on c.email = p.email', "p.oc = '{$oc}'");

		// +3 meses de duración
	    $nuevaFecha = strtotime('+3 month' , strtotime($hoy));

	    $hasta = date('Y-m-d', $nuevaFecha); // Fecha hasta

	    // y le asignamos un codigo de descuento 
		insert_bd('codigo_descuento', 'cliente_id, codigo, porcentaje, activo, oc, fecha_desde, fecha_hasta, cantidad, descuento_opcion_id, donde_el_descuento', "{$id_cliente[0][0]},'$codigo', 10, 1, '$oc', '$hoy', '$hasta', 1, 4, 'Primera compra'");

	}else{
		$codigo = $consulta_pc[0][0];
	}

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");

	$datos_cliente = consulta_bd("nombre,email,id,direccion, cliente_id, cliente_id","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_cliente = $datos_cliente[0][5];
	
    /*$header = "From: ".$nombre_sitio." <".$noreply.">\nReply-To:".$noreply."\n";
    $header .= "X-Mailer:PHP/".phpversion()."\n";
    $header .= "Mime-Version: 1.0\n";
    $header .= "Content-Type: text/html";*/

    if ($notification == 1) {
    	$asunto = "Código descuento para tu próxima compra";
    	$body_message = '<p>Muchas gracias por su preferencia. Adjuntamos un código por un 10% de descuento. Con él su próxima compra en nuestro sitio web será más conveniente. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="'.$nombre_corto.'">'.$nombre_sitio.'</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />
									
			<br />Recuerda que el código tiene una vigencia de 3 meses a partir de hoy.</p>
			<h2><strong>Código de descuento: '.$codigo.'</strong></h2>';
    }else{
    	$asunto = "Recuerda que tienes un descuento para tu próxima compra";
    	$body_message = '<p>Recuerda que tienes un código de descuento por un 10% en tú próxima compra. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="https://www.mercadojardin.cl">mercadojardin.cl</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />

			<h2><strong>Código de descuento: '.$codigo.'</strong></h2>';
    }
	
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
		<style type="text/css">
				p, ul, a { 
					color:#666; 
					font-family: "Open Sans", sans-serif;
					background-color: #ffffff; 
					font-weight:300;
				}
				strong{
					font-weight:600;
				}
				a {
					color:#666;
				}
			</style>
        </head>
        <body style="background:#f9f9f9;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
            
					<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
						<tr>
							<th align="left" width="50%">
								<p>
									<a href="'.$url_sitio.'" style="color:#8CC63F;">
										<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
									</a>
								</p>
							</th>
						</tr>
					</table>
					<br/><br/>
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   <p><strong>Estimado '.$nombre_cliente.':</strong></p>'.
									$body_message
									.'<p></p>
									<p>Muchas gracias<br /> Atte,</p>
									<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>
	';

	$mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	
    $mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
    //Set an alternative reply-to address

    $mail->addAddress($email_cliente, $nombre_cliente);
    $mail->Subject = $asunto;
    $mail->msgHTML($msg2);
    $mail->AltBody = $msg2;
    $mail->CharSet = 'UTF-8';
    //send the message, check for errors
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return true;
    }
	

}

function get_products($filtro){

	if (isset($filtro['max']) AND $filtro['max'] != false) {
		$campos = 'MAX(IF(pd.descuento > 0, pd.descuento, pd.precio))';
	}else{
		$campos = "DISTINCT(p.id), p.nombre, p.thumbs, pd.precio, pd.descuento, l.nombre, pd.stock, pd.id, p.descripcion, m.nombre, pd.precio_cantidad,min(IF(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, pd.precio_cyber,(case when pd.descuento > '0' then pd.descuento else pd.precio end) as precio_calculado, l.id, p.video, pd.sku, p.ficha_tecnica, IFNULL(pp.posicion, 999999) as posicion, pd.oferta_tiempo_hasta, pd.oferta_tiempo_descuento";
	}
	
	$tablas = 'productos p 
	LEFT JOIN lineas_productos lp ON p.id = lp.producto_id 
	JOIN productos_detalles pd ON pd.producto_id = p.id 
	LEFT JOIN lineas l ON l.id = lp.linea_id 
	LEFT JOIN categorias c ON c.id = lp.categoria_id
	LEFT JOIN posicion_productos pp ON p.id = pp.producto_id 
	JOIN marcas m ON m.id = p.marca_id
	JOIN filtros_productos fp ON fp.producto_id = p.id';
	$where = 'p.publicado = 1';

	if (isset($filtro['orden']) AND $filtro['orden'] != null AND $filtro['orden'] != '') {
		$last .= $filtro['orden'];
	}else if (isset($filtro['random']) AND $filtro['random'] != false) {
		$last .= '';
	}else{
		$last .= 'p.id desc';
	}

	if (isset($filtro['ficha']) AND $filtro['ficha'] != null AND $filtro['ficha'] != '') {
		$where .= ' AND p.id = ' . $filtro['ficha'];
	}

	if (isset($filtro['random']) AND $filtro['random'] != false AND $filtro['random'] != '') {
		$last .= ' RAND()';
	}

	// Limite normal
	if (isset($filtro['limite']) AND $filtro['limite'] != null AND $filtro['limite'] != '') {
		$last .= ' LIMIT '.$filtro['limite'];
	}

	// Límite para paginado
	if (isset($filtro['limit']) AND $filtro['limit'] != null AND $filtro['limit'] != '') {
		$last .= ' '.$filtro['limit'];
	}

	if (isset($filtro['linea']) AND $filtro['linea'] != null AND $filtro['linea'] != '') {
		$where .= ' AND pp.linea_id = ' . $filtro['linea'];
	}

	if (isset($filtro['categoria']) AND $filtro['categoria'] != null AND $filtro['categoria'] != '') {
		$where .= ' AND pp.categoria_id = ' . $filtro['categoria'];
	}

	if (isset($filtro['subcategoria']) AND $filtro['subcategoria'] != null AND $filtro['subcategoria'] != '') {
		$where .= " AND pp.subcategoria_id = {$filtro['subcategoria']}";
	}

	if (isset($filtro['destacados']) AND $filtro['destacados'] != null AND $filtro['destacados'] != false) {
		$where .= " AND p.recomendado = 1";
	}

	if (isset($filtro['home']) AND $filtro['home'] != null AND $filtro['home'] != false) {
		$where .= " AND p.home = 1";
	}

	if (isset($filtro['id_ficha']) AND $filtro['id_ficha'] != null AND $filtro['id_ficha'] != '') {
		$where .= " AND p.id != {$filtro['id_ficha']}";
	}

	if (isset($filtro['rel']) AND $filtro['rel'] != null AND $filtro['rel'] != '') {
		$where .= " AND p.id != " . $filtro['rel'];
	}

	if ( (isset($filtro['ofertas']) AND $filtro['ofertas'] != false AND $filtro['ofertas'] != '' )) {
		$where .= " AND ( pd.descuento > 0 OR (pd.oferta_tiempo_activa = 1 AND pd.oferta_tiempo_hasta > NOW() ) )";
	}

	if (isset($filtro['marca']) AND $filtro['marca'] != NULL AND $filtro['marca'] != '') {
		$where .= " AND p.marca_id = $filtro[marca]";
	}

	if (isset($filtro['cantidad']) AND $filtro['cantidad'] != false AND $filtro['cantidad'] != '') {
		$where .= " AND pd.precio_cantidad = 1";
	}

	$whereMarcas = '';
	if(isset($filtro['marcas']) AND $filtro['marcas'] != NULL AND $filtro['marcas'] != ''){
		$marcas_seleccionadas = $filtro['marcas'];
		$explode_marcas = explode('-', $marcas_seleccionadas);
		$contador_marcas = 0;

		$whereMarcas = ' AND (';
		foreach ($explode_marcas as $item_brand) {
			if ($contador_marcas == 0) {
				$whereMarcas .= "m.id = $item_brand";
			}else{
				$whereMarcas .= " OR m.id = $item_brand";
			}
			$contador_marcas++;
		}
		$whereMarcas .= ')';

	}

	$whereLineas = '';
	if (isset($filtro['lineas']) AND $filtro['lineas'] != NULL AND $filtro['lineas'] != '') {
		$lineas_seleccionadas = $filtro['lineas'];
		$explode_lineas = explode('-', $lineas_seleccionadas);
		$contador_lineas = 0;

		$whereLineas = ' AND (';
		foreach ($explode_lineas as $item_line) {
			if ($contador_lineas == 0) {
				$whereLineas .= "lp.linea_id = $item_line";
			}else{
				$whereLineas .= " OR lp.linea_id = $item_line";
			}
			$contador_lineas++;
		}
		$whereLineas .= ')';
	}

	$whereCategorias = '';
	if (isset($filtro['categorias']) AND $filtro['categorias'] != NULL AND $filtro['categorias'] != '') {
		$categorias_seleccionadas = $filtro['categorias'];
		$explode_categorias = explode('-', $categorias_seleccionadas);

		$contador_categorias = 0;

		$whereCategorias = ' AND (';
		foreach ($explode_categorias as $item_cat) {
			if ($contador_categorias == 0) {
				$whereCategorias .= "lp.categoria_id = $item_cat";
			}else{
				$whereCategorias .= " OR lp.categoria_id = $item_cat";
			}
			$contador_categorias++;
		}
		$whereCategorias .= ')';
	}

	$whereFiltros = '';
	if (isset($filtro['filtros']) AND $filtro['filtros'] != NULL AND $filtro['filtros'] != '') {
		$filtros_seleccionados = $filtro['filtros'];
		$explode_filtros = explode('-', $filtros_seleccionados);

		$contador_filtros = 0;

		$whereFiltros = ' AND (';
		foreach ($explode_filtros as $item_fil) {
			if ($contador_filtros == 0) {
				$whereFiltros .= "fp.filtros_variante_id = $item_fil";
			}else{
				$whereFiltros .= " OR fp.filtros_variante_id = $item_fil";
			}
			$contador_filtros++;
		}
		$whereFiltros .= ')';
	}

	if (isset($filtro['desde']) AND $filtro['desde'] != NULL AND $filtro['desde'] != '') {
		$having .= 'AND precio_calculado >= ' . $filtro['desde'];
	}

	if (isset($filtro['hasta']) AND $filtro['hasta'] != NULL AND $filtro['hasta'] != '') {
		$having .= ' AND precio_calculado <= ' . $filtro['hasta'];
	}

	if (isset($filtro['nuevos']) AND $filtro['nuevos'] != NULL AND $filtro['nuevos'] != false) {
		$where .= ' AND p.nuevos_productos = 1';
	}

	if (isset($filtro['busqueda']) AND $filtro['busqueda'] != null AND trim($filtro['busqueda']) != '') {
		$busqueda = $filtro['busqueda'];
		$where .= " AND (p.nombre LIKE '%$busqueda%' OR pd.sku LIKE '%$busqueda%' OR m.nombre LIKE '%$busqueda%' OR c.nombre LIKE '%$busqueda%' OR l.nombre LIKE '%$busqueda%')";
	}

	// return "SELECT $campos FROM $tablas WHERE $where $whereMarcas $whereLineas $whereCategorias $whereFiltros GROUP BY p.id HAVING MIN(IF(pd.descuento > 0, pd.descuento, pd.precio)) $having ORDER BY $last";

	/* FIN FILTROS */

	if (isset($filtro['max']) AND $filtro['max'] != false) {
		$productos = consulta_bd($campos,$tablas,$where.$whereMarcas.$whereLineas.$whereCategorias.$whereFiltros." HAVING min(IF(pd.descuento > 0, pd.descuento, pd.precio))",$last);
	}else{
		$productos = consulta_bd($campos,$tablas,$where.$where_posicion.$whereMarcas.$whereLineas.$whereCategorias.$whereFiltros." GROUP BY p.id HAVING min(IF(pd.descuento > 0, pd.descuento, pd.precio)) $having",$last);
	}

	// return "SELECT $campos FROM $tablas WHERE $where$where_posicion$whereMarcas$whereLineas$whereCategorias GROUP BY p.id HAVING min(IF(pd.descuento > 0, pd.descuento, pd.precio)) $having ORDER BY $last";
	
	$index = 0;
	$count = 0;

	if (is_array($productos)) {
		if (isset($filtro['max']) AND $filtro['max'] != false) {
			$out['valor_max'] = $productos[0][0];
		}else{
			foreach ($productos as $aux) {
				$out['productos']['producto'][$index]['id_producto'] = $aux[0]; // Madre
				$out['productos']['producto'][$index]['id_hijo'] = $aux[7]; // Hijo
				$out['productos']['producto'][$index]['nombre'] = $aux[1];
				$out['productos']['producto'][$index]['imagen_grilla'] = ($aux[2] != NULL OR $aux[2] != '') ? 'imagenes/productos/'.$aux[2] : 'img/sin-foto.jpg';
				$out['productos']['producto'][$index]['precio'] = $aux[3];
				$out['productos']['producto'][$index]['descuento'] = $aux[4];
				$out['productos']['producto'][$index]['precio_cyber'] = $aux[12];
				// $out['productos']['producto'][$index]['porcentaje'] = (($aux[3] - $aux[4]) / $aux[3]) * 100;
				$out['productos']['producto'][$index]['stock'] = $aux[6];
				$out['productos']['producto'][$index]['descripcion'] = $aux[8];
				$out['productos']['producto'][$index]['nombre_seteado'] = url_amigables($aux[1]);
				$out['productos']['producto'][$index]['marca'] = $aux[9];
				$out['productos']['producto'][$index]['linea'] = $aux[5];
				$out['productos']['producto'][$index]['linea_id'] = $aux[14];
				$out['productos']['producto'][$index]['precio_cantidad'] = $aux[10];
				$out['productos']['producto'][$index]['video'] = $aux[15];
				$out['productos']['producto'][$index]['sku'] = $aux[16];
				$out['productos']['producto'][$index]['ficha_tecnica'] = $aux[17];

				$out['productos']['producto'][$index]['oferta_tiempo_hasta'] = $aux[19];
				$out['productos']['producto'][$index]['oferta_tiempo_descuento'] = $aux[20];

				if ($out['productos']['producto'][$index]['imagen_grilla'] != 'img/sin-foto.jpg') {
					if (!file_exists('imagenes/productos/'.$aux[2])) {
						$out['productos']['producto'][$index]['imagen_grilla'] = 'img/sin-foto.jpg';

						$out['productos']['producto'][$index]['ruta_imagen'] = 'img/'; 
						$out['productos']['producto'][$index]['nombre_imagen'] = 'sin-foto.jpg';
					}else{
						$out['productos']['producto'][$index]['ruta_imagen'] = 'imagenes/productos/'; 
						$out['productos']['producto'][$index]['nombre_imagen'] = $aux[2];
					}
				}

				$index++;
				$count++;
			}
			$out['productos']['total'][] = $count;
		}
	}else{
		return false;
	}

	return $out;
}

function tiene_posicion($columna, $id, $producto_id){
	// return "SELECT posicion FROM posicion_productos WHERE $columna = $id AND producto_id = $producto_id AND posicion IS NOT NULL";
	$query = consulta_bd('posicion', "posicion_productos", "$columna = $id AND producto_id = $producto_id AND posicion IS NOT NULL", '');
	// Si no es arreglo lo elimino en el actions.php
	return is_array($query);
}

function get_categorias($linea_id, $marca = 0, $ofertas = false){
	if ((int)$linea_id > 0) {
		$categorias = consulta_bd('c.id, c.nombre, count(p.id)', 'categorias c join lineas_productos lp on c.id = lp.categoria_id join productos p on p.id = lp.producto_id', "c.linea_id = {$linea_id} and c.publicada = 1 and p.publicado = 1 group by c.id", '');
	}else{
		if ($marca == 0) {
			if (!$ofertas) {
				$categorias = consulta_bd('c.id, c.nombre, count(p.id)', 'categorias c join lineas_productos lp on c.id = lp.categoria_id join productos p on p.id = lp.producto_id', "c.publicada = 1 and p.publicado = 1 group by c.id", '');
			}else{
				$categorias = consulta_bd('c.id, c.nombre, count(p.id)', 'categorias c join lineas_productos lp on c.id = lp.categoria_id join productos p on p.id = lp.producto_id join productos_detalles pd on pd.producto_id = p.id', "c.publicada = 1 and p.publicado = 1 and pd.descuento > 0 group by c.id", '');
			}
		}else{
			$categorias = consulta_bd('c.id, c.nombre, count(p.id)', 'categorias c join lineas_productos lp on c.id = lp.categoria_id join productos p on p.id = lp.producto_id join marcas m on m.id = p.marca_id', "c.publicada = 1 and p.publicado = 1 and m.id = $marca group by c.id", '');
		}	
	}

	if (is_array($categorias)) {
		$i = 0;
		foreach ($categorias as $item) {
			$out[$i]['id'] = $item[0];
			$out[$i]['nombre'] = $item[1];
			$out[$i]['cantidad'] = $item[2];

			$i++;
		}
	}else{
		return null;
	}

	return $out;

}

function get_marcas($linea_id, $categoria = 0, $oferta = false){
	if ($linea_id >= 1) {
		$categorias = consulta_bd('m.id, m.nombre, count(p.id)', 'marcas m join productos p on m.id = p.marca_id join lineas_productos lp on p.id = lp.producto_id', "lp.linea_id = {$linea_id} and p.publicado = 1 group by m.id", '');
	}else{
		if ($categoria == 0) {
			if (!$oferta) {
				$categorias = consulta_bd('m.id, m.nombre, count(p.id)', 'marcas m join productos p on m.id = p.marca_id join lineas_productos lp on p.id = lp.producto_id', "p.publicado = 1 group by m.id", '');
			}else{
				$categorias = consulta_bd('m.id, m.nombre, count(p.id)', 'marcas m join productos p on m.id = p.marca_id join lineas_productos lp on p.id = lp.producto_id join productos_detalles pd on pd.producto_id = p.id', "pd.descuento > 0 and p.publicado = 1 group by m.id", '');
			}
		}else{
			$categorias = consulta_bd('m.id, m.nombre, count(p.id)', 'marcas m join productos p on m.id = p.marca_id join lineas_productos lp on p.id = lp.producto_id', "lp.categoria_id = $categoria AND p.publicado = 1 group by m.id", '');
		}
		
	}

	if (is_array($categorias)) {
		$i = 0;
		foreach ($categorias as $item) {
			$out[$i]['id'] = $item[0];
			$out[$i]['nombre'] = $item[1];
			$out[$i]['cantidad'] = $item[2];

			$i++;
		}
	}else{
		return null;
	}

	return $out;

}

function existe_cliente($id){
	$query = consulta_bd('id', 'clientes', "id = $id", '');

	return is_array($query);
}

function producto_guardado($cliente_id, $producto_id){
	$query = consulta_bd('id', 'productos_guardados', "cliente_id = $cliente_id and producto_id = $producto_id", '');

	return is_array($query);
}

function get_productos_guardados($cliente_id){
	$query = consulta_bd('p.id, p.nombre, p.thumbs, pd.precio, pd.descuento, pd.id', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN productos_guardados pg ON pg.producto_id = p.id', "pg.cliente_id = $cliente_id", 'pg.id desc');


	if (is_array($query)) {

		$i = 0;
		foreach ($query as $p) {
			$out[$i]['id'] = $p[0];
			$out[$i]['id_hijo'] = $p[5];
			$out[$i]['nombre'] = $p[1];
			$out[$i]['imagen'] = ($p[2] != null AND $p[2] != '') ? $p[2] : 'img/sin-foto.jpg';
			$out[$i]['precio'] = $p[3];
			$out[$i]['descuento'] = $p[4];

			$i++;
		}

		return $out;

	}

	return false;

}

function get_pedidos($cliente_id){

	$query = consulta_bd('p.id, p.oc, p.total_pagado, p.fecha_creacion, p.medio_de_pago, p.shares_number, p.mp_cuotas, p.payment_type_code, p.mp_payment_type, p.card_number, p.mp_card_number, p.authorization_code, p.mp_auth_code, p.total, p.descuento, p.valor_despacho, p.retiro_en_tienda', 'pedidos p', "p.cliente_id = $cliente_id AND p.estado_id = 2", 'p.id desc');

	if (is_array($query)) {

		$i++;
		foreach ($query as $pedido) {
			$out['pedidos'][$i]['pedido']['id'] = $pedido[0];
			$out['pedidos'][$i]['pedido']['oc'] = $pedido[1];
			$out['pedidos'][$i]['pedido']['total_pagado'] = $pedido[2];
			$out['pedidos'][$i]['pedido']['fecha_creacion'] = $pedido[3];
			$out['pedidos'][$i]['pedido']['medio_de_pago'] = $pedido[4];

			$num_cuotas = ($pedido[4] == 'webpay') ? $pedido[5] : $pedido[6];
			$method = ($pedido[4] == 'webpay') ? 'tbk' : 'mpago';
			$tipo_pago = ($pedido[4] == 'webpay') ? $pedido[7] : $pedido[8];

			$tipo_pago = tipo_pago($tipo_pago,$num_cuotas, $method);

			$out['pedidos'][$i]['pedido']['cuotas'] = $tipo_pago['cuota'];
			$out['pedidos'][$i]['pedido']['tipo_pago'] = $tipo_pago['tipo_pago'];
			$out['pedidos'][$i]['pedido']['tipo_cuota'] = $tipo_pago['tipo_cuota'];
			$out['pedidos'][$i]['pedido']['numero_tarjeta'] = ($pedido[4] == 'webpay') ? $pedido[9] : $pedido[10];
			$out['pedidos'][$i]['pedido']['auth_code'] = ($pedido[4] == 'webpay') ? $pedido[11] : $pedido[12];
			$out['pedidos'][$i]['pedido']['total'] = $pedido[13];
			$out['pedidos'][$i]['pedido']['descuento'] = $pedido[14];
			$out['pedidos'][$i]['pedido']['despacho'] = $pedido[15];
			$out['pedidos'][$i]['pedido']['retiro_en_tienda'] = $pedido[16];

			$i++;

		}

		return $out;

	}


	return false;

}

function setear_fecha($fecha){

	$dia = date('d', strtotime($fecha));
	$mes = date('F', strtotime($fecha));
	$anio = date('Y', strtotime($fecha));

	switch ($mes) {
		case 'January':
			$mes_es = 'Enero';
		break;
		case 'February':
			$mes_es = 'Febrero';
		break;
		case 'March':
			$mes_es = 'Marzo';
		break;
		case 'April':
			$mes_es = 'Abril';
		break;
		case 'May':
			$mes_es = 'Mayo';
		break;
		case 'June':
			$mes_es = 'Junio';
		break;
		case 'July':
			$mes_es = 'Julio';
		break;
		case 'August':
			$mes_es = 'Agosto';
		break;
		case 'September':
			$mes_es = 'Septiembre';
		break;
		case 'October':
			$mes_es = 'Octubre';
		break;
		case 'November':
			$mes_es = 'Noviembre';
		break;
		case 'December':
			$mes_es = 'Diciembre';
		break;
	}

	return $dia . ' de ' . $mes_es . ' del ' . $anio;

}

function get_filtros($linea_id){

	$query = consulta_bd('id, nombre', 'filtros', "linea_id = $linea_id", '');

	$i = 0;
	foreach ($query as $q) {

		$variantes = consulta_bd('fv.id, fv.nombre', 'filtros_variantes fv join filtros_productos fp on fp.filtros_variante_id = fv.id join productos p on p.id = fp.producto_id', "fv.filtro_id = $q[0] and p.publicado = 1 GROUP BY fv.id", 'fv.posicion asc');

		// $variantes = consulta_bd('id, nombre', 'filtros_variantes', "filtro_id = $q[0]", 'posicion asc');
		if (is_array($variantes)) {
			$out[$i]['id'] = $q[0];
			$out[$i]['nombre'] = $q[1];

			for ($x=0; $x < sizeof($variantes); $x++) { 
				$out[$i]['variantes'][$x]['id'] = $variantes[$x][0];
				$out[$i]['variantes'][$x]['nombre'] = $variantes[$x][1];
			}

			$i++;
			
		}

	}

	return $out;

}

function filtros_aplicados($marcas, $filtro_desde, $filtro_hasta, $categorias, $filtros){
	
	if ($marcas != 0) {
		$explode_brand = explode('-', $marcas);

		$index = 0;
		foreach ($explode_brand as $item) {
			$marca = consulta_bd('id, nombre', 'marcas', "id = $item", '');

			if (is_array($marca)) {
				$out['filtro_marcas'][$index]['id'] = $marca[0][0];
				$out['filtro_marcas'][$index]['nombre'] = $marca[0][1];

				$index++;
			}
		}
	}

	if (!is_null($categorias) AND $categorias != 0) {
		$explode_cat = explode('-', $categorias);
		$index = 0;
		foreach ($explode_cat as $item) {
			$categoria = consulta_bd('id, nombre', 'categorias', "id = $item", '');

			if (is_array($categoria)) {
				$out['filtro_categorias'][$index]['id'] = $categoria[0][0];
				$out['filtro_categorias'][$index]['nombre'] = $categoria[0][1];

				$index++;
			}

		}
	}

	if ($filtros != 0) {
		$explode_filtros = explode('-', $filtros);
		$index = 0;
		foreach ($explode_filtros as $item) {
			
			$data_filtro = consulta_bd('f.nombre, fv.nombre, fv.id', 'filtros_variantes fv JOIN filtros f ON f.id = fv.filtro_id', "fv.id = $item", '');

			if (is_array($data_filtro)) {
				$out['filtro_filtros'][$index]['id'] = $data_filtro[0][2];
				$out['filtro_filtros'][$index]['nombre'] = $data_filtro[0][1];
				$out['filtro_filtros'][$index]['nombre_filtro'] = $data_filtro[0][0];

				$index++;
			}

		}
	}
	

	if ($filtro_hasta != 0) {
		$out['filtro_precios'][0] = 'Desde ' . $filtro_desde . ' hasta ' . $filtro_hasta;
	}

	return $out;
}

function qty_save($cliente_id){
	$query = consulta_bd('count(producto_id)', 'productos_guardados', "cliente_id = $cliente_id", '');
	return (is_array($query)) ? $query[0][0] : 0;
}

function cacheo(){
	return rand(999999999, 99999999999);
}

function GenerarEnvio($pedido_id){

    $pedido     = consulta_bd("oc, nombre, rut, direccion, comuna, email, telefono, total, carrier, comentarios_envio, medio_de_pago, id_envio","pedidos","id = $pedido_id","");

    if (is_null($pedido[0][11]) OR trim($pedido[0][11]) == '') {
    	
    	$medio_de_pago = $pedido[0][10];

	    $pr_pedidos = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id = $pedido_id","");
	    $nombre_pr  = "";
	    $peso_total = 0;
	    $peso_total = 0;
	    $largo      = 0;
	    $altura     = 0;
	    $ancho      = 0;
	    $carrier = $pedido[0][8];

	    foreach($pr_pedidos as $pr){
	        $producto = consulta_bd("nombre, ancho, alto, largo, peso","productos_detalles","id = $pr[0]","");
	        $nombre_producto_pro = preg_replace("/[\r\n|\n|\r]+/", " ", $producto[0][0]);
	        if($nombre_pr){ 
	        	$nombre_pr .= ", ".str_replace('"', '', $nombre_producto_pro); 
	        }else{ 
	        	$nombre_pr = str_replace('"', '', $nombre_producto_pro); 
	        }
	        $peso_total += $producto[0][4]*$pr[1];

	        $largo      = $producto[0][3];
	        $altura     = $producto[0][2];
	        $ancho      = $producto[0][1];

	        $volumen += (($ancho * $largo * $altura) / 4000) * $pr[1];
	    }


	    //DATOS ENVIO           
	    $imported_id            = str_replace("OC_", "", $pedido[0][0]);    // ID DEL ENVIO
	    $order_price            = $pedido[0][7];                            // PRECIO
	    $n_packages             = 1;                                        // NUMERO DE PAQUETES
	    $content_description    = substr($nombre_pr, 0, 200);                               // DESCRIPCION DEL CONTENIDO
	    $type                   = 'delivery';
	    $weight                 = $peso_total;               // PESO
	    $volume                 = $volumen;             // VOLUMEN

	    //DATOS RECEPTOR
	    $name                   = $pedido[0][1];
	    $phone                  = $pedido[0][6];
	    $email                  = $pedido[0][5];

	    //DIRECCIÖN DESPACHO
	    $place                  = $pedido[0][4];
	    $full_address           = preg_replace("/[\r\n|\n|\r]+/", " ", $pedido[0][3]);
	    $comentario_cliente		= str_replace(
	        array("\\", "¨", "º", "~",
	             "#", "@", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "<code>", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	             ".", "*", " "),
	        ' ',
	        $pedido[0][9]
	    );
	    $comentario_cliente = preg_replace("/[\r\n|\n|\r]+/", " ", $comentario_cliente);

	    //BODEGA
	    $warehouse_code         = 'bod1';

	    //CARRIER DE ENVIO
	    $carrier_code           = $carrier;    // CODIGO DE CARRIER
	    $tracking_number        = "";

	    $variables = '{ "shipping_order" : {   
		        "imported_id" : "'.$imported_id.'",
		        "order_price" : "'.$order_price.'",
		        "n_packages"  : "'.$n_packages.'",
		        "content_description" : "'.$content_description.'",
		        "type" : "'.$type.'",
		        "weight" : "'.$weight.'",
		        "volume" : "'.$volume.'"
		    },
		    "shipping_destination" : {          
		        "customer" : { 
		            "name" : "'.$name.'",
		            "phone" : "'.$phone.'",
		            "email" : "'.$email.'"
		        },
		        "delivery_address" : { 
		            "home_address" : { 
		                "place" : "'.$place.'",
		                "full_address" : "'.$full_address.'",
		                "information" : "'.$comentario_cliente.'"
		            }
		        }
		    },
		    "shipping_origin" : {
		        "warehouse_code" : "'.$warehouse_code.'"
		    },
		    "carrier" : {
		        "carrier_code" : "'.$carrier_code.'",
		        "tracking_number": "'.$tracking_number.'"
		    }
		}';


	    $curl = curl_init();

	    curl_setopt_array($curl, array(
	        CURLOPT_URL => "https://api.enviame.io/api/s2/v2/companies/herramientaschile/deliveries",
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_ENCODING => "",
	        CURLOPT_MAXREDIRS => 10,
	        CURLOPT_TIMEOUT => 30,
	        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	        CURLOPT_CUSTOMREQUEST => "POST",
	        CURLOPT_POSTFIELDS => $variables,
	        CURLOPT_HTTPHEADER => array(
	            "Accept: application/json",
	            "Cache-Control: no-cache",
	            "Content-Type: application/json",
	            "api-key: 04a33008f36eb0bfe97501c2db8f0ffd"
	        ),
	    ));

	    

	    $response = curl_exec($curl);
	    $err = curl_error($curl);

	    curl_close($curl);

	    if($err){
	        echo "cURL Error #:" . $err;
	    }
	    
	    // return json_encode($response, true);

	    $resp           = json_decode($response);
	    $identificador  = $resp->data->identifier;
	    $estado_envio   = $resp->data->status->name;

	    if ($medio_de_pago == 'transferencia') {
	    	$update = update_bd("pedidos","id_envio = '$identificador'","id = $pedido_id");
	    }else{
	    	$update = update_bd("pedidos","id_envio = '$identificador', estado_envio = '$estado_envio'","id = $pedido_id");
	    }

    }
    
    // die(print_r($resp));

}

function go_mercadolibre($tabla, $id, $url_global, $op){
	// MercadoLibre
	if ($tabla == 'productos_detalles' OR $tabla == 'productos') {
		$where = ($id == '') ? '' : "(pd.id = $id OR p.id = $id) AND ";
		if ($op == 'addAll') {
			$where .= " (pd.id_meli = '' OR pd.id_meli is NULL) AND ";
		}
		$data_producto = consulta_bd('p.nombre, p.descripcion, pd.precio_mercadolibre, pd.precio, pd.stock, pd.id_meli, p.thumbs, pd.sku, p.marca_id, p.publicado', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', $where . "p.publicado = 1 AND pd.precio_mercadolibre > 0", 'p.id asc limit 5');

		if ($op == 'addOne') {
			if (is_array($data_producto)) {
				$title = $data_producto[0][0];
				$description = htmlspecialchars(trim(strip_tags($data_producto[0][1])));
				// El precio se trabaja dependiendo el ecommerce (Porcentaje o valor).
				$price = $data_producto[0][2];
				$stock = $data_producto[0][4];
				$id_meli = $data_producto[0][5];
				$sku = $data_producto[0][7];
				$imagen_1 = $url_global.'imagenes/productos/'.$data_producto[0][6];
				$status = ($data_producto[0][9] == 1) ? 'active' : 'paused';

				$products['products'][$index]['title'] = $title;
				$products['products'][$index]['price'] = $price;
				$products['products'][$index]['stock'] = $stock;
				$products['products'][$index]['description'] = $description;
				$products['products'][$index]['sku'] = $sku;
				$products['products'][$index]['warranty_text'] = $warranty_text;
				$products['products'][$index]['image_1'] = $imagen_1;
				$products['products'][$index]['image_2'] = $imagen_2;
				$products['products'][$index]['image_3'] = $imagen_3;
				$products['products'][$index]['image_4'] = $imagen_4;
				$products['products'][$index]['image_5'] = $imagen_5;
				$products['products'][$index]['image_6'] = $imagen_6;
				$products['products'][$index]['id_meli'] = $id_meli;
				$products['products'][$index]['status'] = $status;
				// id tienda oficial
				// $products['products'][$index]['official_store_id'] = 386;

				if ($id_meli != NULL OR $id_meli != '') {
					$url = $url_global.'admin/includes/apimeli/edit_products';

				}else{
					$url = $url_global.'admin/includes/apimeli/add_products';	
				}
			}else{
				$data_producto = consulta_bd('pd.id_meli, p.publicado', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id', "pd.id = $id OR p.id = $id", '');
				$id_meli = $data_producto[0][0];
				$status = ($data_producto[0][1] == 1) ? 'active' : 'paused';
				$products['products'][0]['status'] = $status;
				$products['products'][0]['id_meli'] = $id_meli;

				$url = $url_global.'admin/includes/apimeli/paused_products';
			}
		}elseif($op == 'addAll'){
			$index = 0;
			foreach ($data_producto as $aux) {
				$title = $aux[0];
				$description = htmlspecialchars(trim(strip_tags($aux[1])));
				// El precio se trabaja dependiendo el ecommerce (Porcentaje o valor).
				$price = $aux[2];
				$stock = $aux[4];
				$id_meli = $aux[5];
				$sku = $aux[7];
				$status = ($aux[9] == 1) ? 'active' : 'paused';
				$imagen_1 = $url_global.'imagenes/productos/'.$aux[6];

				$products['products'][$index]['title'] = $title;
				$products['products'][$index]['price'] = round($price * 1.19);
				$products['products'][$index]['stock'] = $stock;
				$products['products'][$index]['description'] = $description;
				$products['products'][$index]['sku'] = $sku;
				$products['products'][$index]['warranty_text'] = $warranty_text;
				$products['products'][$index]['image_1'] = $imagen_1;
				$products['products'][$index]['image_2'] = $imagen_2;
				$products['products'][$index]['image_3'] = $imagen_3;
				$products['products'][$index]['image_4'] = $imagen_4;
				$products['products'][$index]['image_5'] = $imagen_5;
				$products['products'][$index]['image_6'] = $imagen_6;
				$products['products'][$index]['id_meli'] = $id_meli;
				$products['products'][$index]['status'] = $status;
				// id tienda oficial
				// $products['products'][$index]['official_store_id'] = 386;
				$index++;
			}

			$url = $url_global.'admin/includes/apimeli/add_products';	
		}elseif($op == 'paused'){
			$index = 0;
			foreach ($data_producto as $aux) {
				if ($aux[5] != NULL OR $aux[5] != '') {
					$products['products'][$index]['status'] = 'paused';
					$products['products'][$index]['id_meli'] = $aux[5];
					$index++;
				}
			}

			$url = $url_global.'admin/includes/apimeli/paused_products';
		}elseif($op == 'active'){
			$index = 0;
			foreach ($data_producto as $aux) {
				if ($aux[5] != NULL OR $aux[5] != '') {
					$products['products'][$index]['status'] = 'active';
					$products['products'][$index]['id_meli'] = $aux[5];
					$index++;
				}
			}

			$url = $url_global.'admin/includes/apimeli/active_products';
		}elseif($op == 'stock'){
			$index = 0;
			foreach ($data_producto as $aux) {
				if ($aux[5] != NULL OR $aux[5] != '') {
					$products['products'][$index]['stock'] = $aux[4];
					$products['products'][$index]['id_meli'] = $aux[5];
					$index++;
				}
			}

			$url = $url_global.'admin/includes/apimeli/edit_stock';
		}

		$products = json_encode($products);

		$userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERAGENT, $userAgent); 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $products);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}
}

function variante_producto($id, $v){
	$query = consulta_bd('id', 'filtros_productos', "producto_id = $id AND filtros_variante_id = $v", '');

	return is_array($query);
}

?>