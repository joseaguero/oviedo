<?php
date_default_timezone_set('America/Santiago');

$p = $_GET[p];

$cart = $_SESSION['cart_alfa_cm'];
if ($active_user)
{
	if ($cart)
	{
		if ($p == 1)
		{
	?>
	<div id="centro" style="width:760px;">
		<div id="ofertas" style="margin-top:0px; width:100%;">
			<h1>Por favor ingrese sus datos</h1>
				<div class="contenedor_productos">
					<div class="centro_productos" style="margin-left:12px; padding-left: 12px; width: 720px; min-height: 500px;">
						<form id="form1" name="form1" action="index.php?op=tc&p=2<?php if (is_numeric($_GET[r])) echo "&r=$_GET[r]";?>" method="post">
					
								<?php
								if ($_GET['error'])
								{
									echo "<div id='error'><p style='font-size:12px;'>$_GET[error]</p></div>";
								}
								
								$filas = consulta_bd("c.direccion, c.comuna, c.ciudad, r.nombre, c.telefono1","clientes c, regiones r","c.id = '$active_user' AND c.region_id = r.id","");
								
								?>
								<h2 style="margin-left:0px;">Dirección registrada</h2>
					
									<strong>Dirección:</strong> 
									<?php echo $filas[0][0];?><br />
					
									<strong>Comuna:</strong>
									<?php echo $filas[0][1];?><br />
					
									<strong>Ciudad:</strong>
									<?php echo $filas[0][2];?><br />
					
									<strong>Región:</strong>
									<?php echo $filas[0][3];?><br />
					
									<strong>Teléfono:</strong>
									<?php echo $filas[0][4];?><br />
							
							<br />
							<input type="checkbox" id="misma" name="misma" checked="checked" onchange="javascript:mostrar('nueva_dir')"/>Utilizar mi dirección registrada
							
							<div id="nueva_dir" style="display:none">
								<table width="690" border="0">
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">Ingrese su dirección de despacho:</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td>Nombre de quien recibe: </td>
										<td><input  type="text" id="nombre_receptor" name="nombre_receptor" value="<?php echo $_GET['nombre_receptor'];?>"/></td>
									</tr>
									<tr>
										<td>Dirección: </td>
										<td><input  type="text" id="dir" name="dir" value="<?php echo $_GET['dir'];?>"/></td>
									</tr>
									<tr>
										<td>Comuna: </td>
										<td><input type="text" id="comuna" name="comuna" value="<?php echo $_GET['comuna'];?>"/></td>
									</tr>
									<tr>
										<td>Ciudad: </td>
										<td><input type="text" id="ciudad" name="ciudad" value="<?php echo $_GET['ciudad'];?>"/></td>
									</tr>
									<tr>
										<td>Región*</td>
										<td>
											<select name="region" id="region" >
												<option value="0">Seleccione...</option>
												<option value="15">ARICA Y PARINACOTA</option>
												<option value="1">TARAPACÁ</option>
												<option value="2">ANTOFAGASTA</option>
												<option value="3">ATACAMA</option>
												<option value="4">COQUIMBO</option>
												<option value="5">VALPARAISO</option>
												<option value="13" selected="selected">METROPOLITANA</option>
												<option value="6">O'HIGGINS</option>
												<option value="7">MAULE</option>
												<option value="8">BIOBIO</option>
												<option value="9">ARAUCANÍA</option>
												<option value="14">LOS RIOS</option>
												<option value="10">LOS LAGOS</option>
												<option value="11">AYSÉN</option>
												<option value="12">MAGALLANES</option>
											</select>
										</td>
									</tr>
								</table>
							</div>
							<table width="690" border="0">
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="4">Comentarios</td>
								</tr>
								<tr>
									<td colspan="4"><textarea id="mensaje" name="comentario" style="width:670px;height:200px;"><?php echo $_GET['comentario'];?></textarea></td>
								</tr>
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="4" align="right" style="padding-left: 575px;">
										<div id="btns">
										      <div class="but_2"><button type="submit" >Continuar</button></div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="4">
											<br />
											<strong>IMPORTANTE: </strong><br /><br />
										    El proceso de compra finaliza cuando Ud ha ingresado los datos y ha dado clic en Terminar Compra.<br /><br />
									</td>
								</tr>
								<tr><tdcolspan="4">&nbsp;</td></tr>
							</table>
						</form>
					</div>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
				</div>
			</div>
		</div>
	<?php
		}
		elseif ($p == 2)
		{
	?>
	<div id="centro" style="width:760px;">
		<div id="ofertas" style="margin-top:0px; width:100%;">
			<h1>Resumen de su pedido</h1>
			<div class="contenedor_productos">
				<div class="centro_productos" style="margin-left:12px; padding-left: 12px; width: 720px; min-height: 500px;">
					<h4>Por favor haga clic en "Finalizar compra" para confirmar su pedido.</h4><br />
					<?php
					
						// Include MySQL class
						require_once('inc/mysql.class.php');
						// Include database connection
						require_once('inc/global.inc.php');
						// Include functions
						require_once('inc/functions.inc.php');
					
						$campos = get_post('');
						
						//Genera número de OC
						$rand = rand(1000, 9999);	
						$date = date('Ymds');
						$oc = "OC_$date$rand";
						
						$items = explode(',',$cart);
						foreach ($items as $item) {
							$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
						}
					
						if (is_numeric($_GET[r]))
						{
							$remate_id = (is_numeric($_GET[r])) ? mysql_real_escape_string($_GET[r]):0;
							if ($remate_id != 0)
							{
								$remate = consulta_bd_por_id("*","remates","",$remate_id);
								$total = $remate['mejor_oferta'];
							}
							else
							{
								die("Remate inválido");
							}
						}
						else
						{
							$total = get_total_price();
						}
						
						if ($_POST[misma])
						{
							$crear_pedido = "INSERT INTO pedidos (cliente_id, fecha, estado_vta_id, comentarios, oc, total) VALUES ('$active_user', NOW(), '1', '$_POST[comentario]', '$oc', '$total')";
						}
						else
						{
							$crear_pedido = "INSERT INTO pedidos (cliente_id, fecha, estado_vta_id, comentarios, oc, total, nombre_receptor, direccion, comuna, ciudad, region_id) VALUES ('$active_user', NOW(), '1', '$_POST[comentario]', '$oc', '$total', '$_POST[nombre_receptor]','$_POST[dir]', '$_POST[comuna]', '$_POST[ciudad]', '$_POST[region]')";
						}
					
						$run = mysql_query($crear_pedido) or die(mysql_error()."<br /><br />$crear_pedido");
						$pedido_id = mysql_insert_id();
						if ($run)
						{
							foreach ($contents as $id=>$qty) {
				/* 					echo "$id => $qty"; */
								if (is_numeric($_GET[r]))
								{
									$precio_pagado = $total;
								}
								else
								{
									$precio_pagado = get_precio($id, 'descuento');
								}
								
								$agregar_item = "INSERT INTO detalle_pedidos (pedido_id, producto_id, cantidad, valor_unitario_pagado) VALUES ('$pedido_id','$id','$qty', '$precio_pagado')";
								$run2 = mysql_query($agregar_item) or die(mysql_error()."<br /><br />$agregar_item");
								if (!$run2)
								{
									$error = "Error, por favor inténtelo nuevamente. No se alcanzó a completar su transacción.&tipo=ventana";
									break;
								}
								else
								{
/*
									$num_vtas = "UPDATE productos SET vtas = vtas+1, stock = stock-$qty WHERE id = '$id'";
									$run = mysql_query($num_vtas);
*/
								}
							}
						}
						else
						{
							$error = "Demasiadas conexiones, por favor inténtelo nuevamente.&tipo=ventana";
						}
				
						
						if ($error != '')
						{
							echo '
							<script>
									pagina = "index.php?op=tc&p=1&error='.$error.''.$campos.'";
									document.location.href=pagina;
							</script>
							';
					/* 		header("location:index.php?op=tc&p=1&error=$error$campos"); */
						}

							
							
							$filas = consulta_bd("nombre, apellidos, rut, email, telefono1, direccion, comuna, ciudad, region_id","clientes","id = '$active_user'","");
							
							$nombre = $filas[0][0];
							$apellido = $filas[0][1];
							$nombre_completo = "$nombre $apellido";
							$direccion = ($_POST['dir']) ? $_POST['dir'] : $filas[0][5];
							$comuna = ($_POST['comuna']) ? $_POST['comuna'] : $filas[0][6];
							$ciudad = ($_POST['ciudad']) ? $_POST['ciudad'] : $filas[0][7];
							$region = ($_POST['region']) ? $_POST['region'] : $filas[0][8];
							$nombre_receptor = ($_POST['nombre_receptor']) ? $_POST['nombre_receptor'] : "$nombre_completo";
							
							//Guardo la región de envío
							$_SESSION['region_id'] = $region;
							//Guardo el monto del despacho
							$despacho = costo_por_despacho($region);
							$update_despacho = update_bd("pedidos","despacho = $despacho","id = $pedido_id","");
							
							if (is_numeric($_GET[r]))
							{
								if ($remate_id != 0)
								{
									echo finalCartRemate($remate_id);
								}
								else
								{
									echo "Remate inválido";
								}
							}
							else
							{
								echo finalCart();
							}
						?>
						
						<HR width="690px" align="left">
						<br />
						
						<table width="690px" border="0">
							<tr>
								<td colspan="2" align="left" width="50%"><h4>Información personal</h4><br /></td>
								<td colspan="2" align="left"><h4>Dirección de entrega</h4><br /></td>
							</tr>
							<tr>
								<td>Nombre</td>
								<td><?php echo $nombre_completo;?></td>
								<td>Nombre de quien recibe</td>
								<td><?php echo $nombre_receptor;?></td>
							</tr>
							<tr>
								<td>Apellidos</td>
								<td><?php echo $filas[0][1];?></td>
								<td>Dirección</td>
								<td><?php echo $direccion;?></td>
							</tr>
							<tr>
								<td>Rut</td>
								<td><?php echo $filas[0][2];?></td>
								<td>Comuna</td>
								<td><?php echo $comuna;?></td>
							</tr>
							<tr>
								<td>E-mail</td>
								<td><?php echo $filas[0][3];?></td>
								<td>Ciudad</td>
								<td><?php echo $ciudad;?></td>
							</tr>
							<tr>
								<td>Teléfono</td>
								<td><?php echo $filas[0][4];?></td>
								<td>Región</td>
								<td><?php
									$reg = consulta_bd("nombre","regiones","id = '$region'","");
									echo $reg[0][0];
								?></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4"><h4>Comentarios:</h4><br /></td>
							</tr>
							<tr>
							<td colspan="4">
								<?php 
									$comentarios = ($_POST['comentario']!='') ? $_POST['comentario'] : "No se escribieron comentarios.";
									echo "<em>$comentarios</em>";
								?>
							</td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
					
						</table>
					</div>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
				</div>
			</div>
		</div>
	
	<?php
		}
	}
	else
	{
			echo '
			<script>
					pagina = "index.php?op=cart";
					document.location.href=pagina;
			</script>
			';
	}
}
else
{
	if (isset($_GET[r]))
	{
		$link = "index.php?op=login&r=$_GET[r]";
	}
	else
	{
		$link = "index.php?op=login";
	}
	echo '
	<script>
			pagina = "'.$link.'";
			document.location.href=pagina;
	</script>
	';
}
?>