<?php
/*
!!! THIS IS JUST AN EXAMPLE !!!, PLEASE USE ImageMagick or some other quality image processing libraries
*/
include("conf.php");
date_default_timezone_set('America/Santiago');

$imgUrl = $_POST['imgUrl'];
$imgInitW = $_POST['imgInitW'];
$imgInitH = $_POST['imgInitH'];
$imgW = $_POST['imgW'];
$imgH = $_POST['imgH'];
$imgY1 = $_POST['imgY1'];
$imgX1 = $_POST['imgX1'];
$cropW = $_POST['cropW'];
$cropH = $_POST['cropH'];

$campo = $_GET['campo'];
$tabla = $_GET['tabla'];
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;

//die($tabla);


//asignamos nombre a la imagen
$nom = substr($imgUrl, 5);
$nom_separado = explode(".", $nom);
$cant = count($nom_separado);
$nombre_final = str_replace(" ", "_", $nom_separado[0]);
$tiempo = date('Ymdhis');

//calidad de la imagen despues de cortarla
$jpeg_quality = 100;

$output_filename = "temp/".$tiempo."_".$nombre_final;

$what = getimagesize($imgUrl);
switch(strtolower($what['mime']))
{
    case 'image/png':
        $img_r = imagecreatefrompng($imgUrl);
		$source_image = imagecreatefrompng($imgUrl);
		$type = '.png';
        break;
    case 'image/jpeg':
        $img_r = imagecreatefromjpeg($imgUrl);
		$source_image = imagecreatefromjpeg($imgUrl);
		$type = '.jpg';
        break;
    case 'image/gif':
        $img_r = imagecreatefromgif($imgUrl);
		$source_image = imagecreatefromgif($imgUrl);
		$type = '.gif';
        break;
    default: die('image type not supported');
}
	
	$resizedImage = imagecreatetruecolor($imgW, $imgH);
	imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, 
				$imgH, $imgInitW, $imgInitH);	
	
	
	$dest_image = imagecreatetruecolor($cropW, $cropH);
	imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, 
				$cropH, $cropW, $cropH);	


	imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
	
	//generamos la ruta de la imagen y la copiamos en nuestra carpeta
	$ruta_imagen = $output_filename.$type;
	$ruta_destino = "../imagenes/$tabla/".$tiempo."_".$nombre_final.$type;
	
	$valor_campo = $tiempo."_".$nombre_final.$type;
	//die($ruta_destino);
	copy($ruta_imagen, $ruta_destino);
	
	$old_img = consulta_bd("$campo",$tabla,"id = $id","");
	$update = update_bd($tabla,"$campo = '$valor_campo'", "id = $id");
	
	if ($update and file_exists("../imagenes/$tabla/".$old_img[0][0])){
		unlink("../imagenes/$tabla/".$old_img[0][0]);
	}
	
	$response = array(
			"status" => 'success',
			"url" => $output_filename.$type ,
			"campo" => $campo,
			"tabla" => $tabla
			
		  );
	 print json_encode($response);

?>