<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
function singular($string){
	$penultima =  substr($string, -2, 1);
	$antepenultima =  substr($string, -3, 1);
	if ($penultima == 'e')
	{
		if ($antepenultima == 't' OR $antepenultima == 'j' OR $antepenultima == 'v')
		{
			$singular = substr($string, 0, -1);
		}
		else
		{
			$singular = substr($string, 0, -2);
		}
	}
	else
	{
		$singular = substr($string, 0, -1);
	}
	return $singular;
}
$id = (isset($_GET[id])) ? mysql_real_escape_string($_GET[id]) : 0;
$tabla = (isset($_GET[tabla])) ? mysql_real_escape_string($_GET[tabla]) : 0;
$id_tabla = singular($tabla)."_id";	

$options = array(
    'delete_type' => 'POST',
    'db_host' => '127.0.0.1',
    'db_user' => 'root',
    'db_pass' => '',
    'db_name' => 'prueba_cms',
    'db_table' => 'img'.$tabla
);

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
$upload_handler = new UploadHandler();

?>