/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';
	
	$.blueimp.fileupload.prototype.processActions.duplicateImage = function (data, options) {
		if (data.canvas) {
			data.files.push(data.files[data.index]);
		}
		return data;
	};

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        //url: 'server/php/',
		url: '../../imagenes/pruebas/',
		processQueue: [
			{
				action: 'loadImage',
				fileTypes: /^image\/(gif|jpeg|png)$/,
				maxFileSize: 20000000 // 20MB
			},
			{
				action: 'resizeImage',
				maxWidth: 800,
				maxHeight: 800
			},
			{action: 'saveImage'}
		]
		
    });

	
	// Load existing files:
	$('#fileupload').addClass('fileupload-processing');
	

	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: $('#fileupload').fileupload('option', 'url'),
		dataType: 'json',
		context: $('#fileupload')[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		$(this).fileupload('option', 'done')
			.call(this, $.Event('done'), {result: result});
	});//fin ajax
    

});
