<?php

/********************************************************\
|  Moldeable  CMS - Edición de contenido.				 |
|  Fecha Modificación: 22/08/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.alfastudio.cl/                             |
\********************************************************/

	//Obtengo las opciones de la tabla
	$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
	$i = 0;
	while($i <= (sizeof($opciones)-1))
	{	
		$nombre = $opciones[$i][0];
		$opcion[$nombre] = $opciones[$i][1];
		$i++;
	}
	if(sizeof($opciones)>0)
	extract($opcion);
	
	if($create == '0' AND !isset($id))
	die("No se permite la creación de entradas en la tabla $tabla.");
	
	if (substr_count($many_to_many,",")>0)
	{
		$many_to_many = explode(',', $many_to_many);
	}
	
	$exclusiones .= (isset($exclusiones)) ? ", id, fecha_creacion, fecha_modificacion, modificado_por, creado_por" : "id, fecha_creacion, fecha_modificacion, modificado_por, creado_por";
	$galerias = ($galerias) ? true : false;
	$exception_update =  "$exclusiones, $archivos";
			
	if (isset($archivos) AND $archivos != '')
	{
		$dir = "../imagenes/$tabla"; //Directorio de imágenes
		$dir_docs = "../docs/$tabla"; //Directorio de imágenes
	}
	
	$nextcropin = ($obligatorios == '') ? 'c' : 'b';
	$actions = (isset($many_to_many_perfiles)) ? "app/perfiles.php" : "app/actions.php" ;

?>


<form id="form" name="form" method="post" enctype="multipart/form-data" action="<?php echo $actions;?>">

<!-- Inicio Botonera -->
<?php include('botonera_edit.php'); ?>
<!-- Fin Botonera -->



<!-- Inicio contenido -->
<?php include ('tips.php');?>
<div id="tabs">
	<ul>
		<li><a href="#tab-1">General</a></li>
		<?php
		if ($many_to_many_perfiles and isset($_GET[id]))
		{
			echo '<li><a href="#tab-2">Opciones perfil</a></li>';
		}
		else
		{
			$m = (isset($m)) ? $m: 2;			
			if (isset($tabs))
			{
					$array = json_decode($tabs, true);				
					$m = 2;
					$p = 0;
					foreach ($array['tabs'] as $val)
					{
						$lista_campos_tabs .= $val['campos'].",";
						echo '<li><a href="#tab-'.$m.'">'.$val['nombre'].'</a></li>';
						$m++;
						$p++;
					}
					
					$lista_tabs = explode(',',$lista_campos_tabs);
	
			}
			
			if (is_array($many_to_many))
			{
				foreach ($many_to_many as $many)
				{
					echo '<li><a href="#tab-'.$m.'">'.get_real_name($many).'</a></li>';
					$m++;
				}
			}
			else
			{
				if (isset($many_to_many) AND isset($id))
				{
					echo '<li><a href="#tab-'.$m.'">'.get_real_name($many_to_many).'</a></li>';
					$m++;
				}
			}
			
			if (isset($idiomas))
			{
				$lista_idiomas = explode(",", $idiomas);
				foreach($lista_idiomas as $idioma)
				{	
					echo '<li><a href="#tab-'.$m.'">'.get_real_name($idioma).'</a></li>';
					$m++;
				}				
			}
			
			if ($galerias and isset($id))
			{
				echo '<li><a href="#tab-'.$m.'">Imágenes</a></li>';
				$m++;
			}
			
			if (isset($has_many))
			{
				$lista_has_many = explode(",", $has_many);
				foreach($lista_has_many as $hm)
				{	
					$id_hm_tabla = get_table_id($hm);
					echo '<li><a href="#tab-'.$m.'">'.get_table_name($id_hm_tabla).'</a></li>';	
					$m++;
				}				
			}
		}
		?>
	</ul>
	
	<?php $t = 1; ?>
	
	<!-- Inicio tab general -->
		<?php include('partials/_general.php');?>
	<!-- Fin tab general -->

	<!-- Inicio otros Tabs -->
		<?php
			if ($tabs)
			{	
				foreach ($array['tabs'] as $val)
				{
					$campos = $val['campos'];
					include('partials/_tabs.php');
					$m++;
					$p++;
				}
			}
		?>
	<!-- Fin Otros Tabs -->
	
	<!-- Inicio tab relaciones many_to_many -->
		<?php
			if ($many_to_many_perfiles and isset($_GET[id]))
			{
				include('partials/_many_to_many_perfiles.php');
			}
			else
			{
				include('partials/_many_to_many.php');
			}
		?>
	<!-- Fin tab relaciones many_to_many -->
	
	<!-- Inicio Tabs de idiomas -->
		<?php include('partials/_idiomas.php');?>
	<!-- Fin tab idiomas -->
	
	<!-- Inicio Tabs de imagenes -->
		<?php include('partials/_imagenes.php');?>
	<!-- Fin tab imagenes -->
	
	<!-- Inicio Has Many -->
		<?php
			if ($has_many)
			{	
				foreach ($lista_has_many as $hm)
				{
					$tabla_hm = $hm;
					include('partials/_has_many.php');
					$m++;
					$p++;
				}
			}
		?>
	<!-- Fin Has Many -->
	
<!-- Fin contenido -->
</form>

<?php 
//Activación de los select dependientes en caso de que exista relación many to many.
if ($many_to_many_parent != NULL) {
	$k = 0;
	foreach ($related_selects_mtmp as $mtmp)
	{
	?>
		<script type="text/javascript">
		$(function(){		
			$("#form").relatedSelects({
				onChangeLoad: 'includes/dependent_selects.php',
				selects: ['<?php echo $many_to_many_parent; ?>', '<?php echo $sel2[$k]; ?>']
			});
		});
		</script>
	<?php
		$k++;
	}
}
elseif ($btp != NULL)
{
	$k = 0;
	foreach ($related_selects_btp as $rbtp)
	{
	?>
		<script type="text/javascript">
		$(function(){		
			$("#form").relatedSelects({
				onChangeLoad: 'includes/dependent_selects_new.php',
				selects: ['<?php echo $rbtp; ?>', '<?php echo $sel2_btp[$k]; ?>']
			});
		});
		</script>
	<?php
	$k++;
	}
}
?>
<script type="text/javascript">
    $(function(){
       $('.check_').click(function(){
           var variables = $(this).attr('rel');
           var valor = $(this).attr('checked');
           alertify.log("Realizando Modificacion");
			
			
			$.ajax({
				type: "POST",
				url: "app/partials/ajax_check_has_many.php",
				data: { campos:variables, marcado:valor },
				success: function(respuesta){
					alertify.success("Modificacion realizada con éxito");
					if(valor == "checked"){
						$('span.'+variables).html('Si');
					} else {
						$('span.'+variables).html('No');
					}
				}, 
				error: function(){
					alertify.error("No se pudo realizar la solicitud");
			  	}
			});
			
       }); 
    });
</script>

<script type="text/javascript">
//Editar campos en linea, no los select ni los checkbox ni ID
$(function(){
	$("td.td_editables").dblclick(function(){
		$(this).find(".form_editar_en_linea").fadeIn(100);
	});
	$("a.btn_editar_linea").click(function(){
		var valor_campo = $(this).parent().find(".campo_editable_lista").val();
		var variables = $(this).parent().find(".campo_editable_lista").attr("name");
		//alert(variables);
		alertify.log("Realizando Modificacion");
		$.ajax({
				type: "POST",
				url: "app/partials/ajax_campos_has_many.php",
				data: { campos:variables, val:valor_campo },
				success: function(respuesta){
					alertify.success("Modificacion realizada con éxito");
					$('span.'+variables).html(valor_campo);
					$(".form_editar_en_linea").fadeOut(100);
				}, 
				error: function(){
					alertify.error("No se pudo realizar la solicitud");
			  	}
			});//fin funcion ajax
	});//fin funcion click
	
}); //fin funcion
</script>


