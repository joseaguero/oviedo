<?php
include('../conf.php');
	$tienda = "Ejemplo moldeable";
	$servicio = consulta_bd("valor","opciones","nombre = 'servicio'","");
	$service;
	$aContar;
            $serv1; $serv2; $serv3; $serv4;
	
    
    $tiendaName = $tienda;


    $totalVentas = ventasRetiroTienda($servicio[0][0]);
    $totalPedidos = PedidosRetiroTienda($servicio[0][0]);
    $totalProductos = cantProductosRetiroTienda($servicio[0][0]);

    $ventasTotales = totalVentas($servicio[0][0]);
    $pedidosTotales = countPedidos($servicio[0][0]);
    $prodTotales = countProductos($servicio[0][0]);

    ksort($totalVentas['despacho']);
    ksort($totalVentas['retiro']);

    ksort($totalPedidos['despacho']);
    ksort($totalPedidos['retiro']);

    ksort($totalProductos['despacho']);
    ksort($totalProductos['retiro']);

    /*echo '<pre>';
    print_r($totalPedidos);
    echo '</pre>';*/
	
?>


	<script type="text/javascript">
	    

// Create the chart
$(function () {
        var ventas = new Highcharts.Chart({
            chart: {
                type: 'column',
                backgroundColor: '#FAFAFA',
                renderTo: 'container'
            },
            title: {
                text: 'Valor Ventas Retiro en tienda y Despacho'
            },
            xAxis: {
                categories: [
                <?php foreach ($totalVentas['retiro'] as $key => $value) {
                    echo $key . ',';
                } ?>
                ]

            },
            yAxis: {
                title: {
                    text: 'Total por mes'
                }
            },
            series: [{
                name: 'Retiro Tienda',
                data: [
                    <?php 
                    foreach ($totalVentas['retiro'] as $key => $value) {
                        echo "{$value[0]} ,";
                    }
                    ?>
                ],
                tooltip: {
                    valuePrefix: '$ '
                }
                }, {
                name: 'Despacho',
                data: [
                    <?php 
                    foreach ($totalVentas['despacho'] as $key => $value) {
                        echo "{$value[0]} ,";
                    }
                    ?>
                ],
                tooltip: {
                    valuePrefix: '$ '
                }
            }
            ]
        });

    
    });

$(function () {
         var pedidos = new Highcharts.Chart({
            chart: {
                type: 'column',
                backgroundColor: '#FAFAFA',
                renderTo: 'torta-pedidos'
            },
            title: {
                text: 'Pedidos Retiro en tienda y Despacho'
            },
            xAxis: {
                categories: [
                <?php foreach ($totalPedidos['retiro'] as $key => $value) {
                    echo $key . ',';
                } ?>
                ]
            },
            yAxis: {
                title: {
                    text: 'Total por mes'
                }
            },
            series: [{
                name: 'Retiro Tienda',
                data: [
                    <?php 
                    foreach ($totalPedidos['retiro'] as $key => $value) {
                        echo "{$value[0]} ,";
                    }
                     ?>
                ]
                }, {
                name: 'Despacho',
                data: [
                    <?php 
                    foreach ($totalPedidos['despacho'] as $key => $value) {
                        echo "{$value[0]} ,";
                    }
                     ?>
                ]}
            ]
        });

    
    });
$(function () {
         var productos = new Highcharts.Chart({
            chart: {
                type: 'column',
                backgroundColor: '#FAFAFA',
                renderTo: 'torta-productos'
            },
            title: {
                text: 'Cantidad Productos Retiro en tienda y Despacho'
            },
            xAxis: {
                categories: [
                <?php foreach ($totalProductos['retiro'] as $key => $value) {
                    echo $key . ',';
                } ?>
                ]

            },
            yAxis: {
                title: {
                    text: 'Total por mes'
                }
            },
            series: [{
                name: 'Retiro Tienda',
                data: [
                    <?php 
                    foreach ($totalProductos['retiro'] as $key => $value) {
                        echo "{$value[0]} ,";
                    }
                     ?>
                ]
                }, {
                name: 'Despacho',
                data: [
                    <?php 
                    foreach ($totalProductos['despacho'] as $key => $value) {
                        echo "{$value[0]} ,";
                    }
                     ?>
                ]}
            ]
        });

    
    });
</script>

	<h2 class="tituloReporte">Productos con retiro en tienda</h2>
	<div class="contenedor">
        
        <div class="total-por-anio">
        	<h1>Total Ventas</h1>
        	<h2>Retiro en Tienda</h2>
        	<div class="box">
                
                
                <table width="100%" border="0" cellpadding="0">
                  <tr style="background-color:#62ACE0; color:#fff;">
                    <td width="60%"><p id="anio">Año</p></td>
                    <td width="40%"><p id="porc">% vs Total Ventas</p></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php 
                  ksort($totalVentas['totales']);
                  foreach ($totalVentas['totales'] as $key => $value): ?>
                      <tr>
                        <td><?= substr($key, 1) ?></td>
                        <td>&nbsp;</td>
                      </tr>

                      <tr>
                        <td><p class="number">
                                <i class="fa fa-usd totales" aria-hidden="true"></i> 
                                <?= number_format($value[0],0,",",".");?>
                            </p>
                        </td>
                        <td>
                            <?php
                            if ($ventasTotales['totales'][$key][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($value[0] * 100) / $ventasTotales['totales'][$key][0], 2) . '%</p>';
                            else:
                                echo '<p class="number porcentaje">0%</p>';
                            endif;
                            ?>
                        </td>
                      </tr>
                      <tr style="border-top: solid 1px #ccc;">
                        <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                        <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                      </tr>
                  <?php endforeach ?>
                </table>
            </div>

        </div>
        <div class="div-g">
            <div id="container"></div>
        </div>

        <div class="separator"></div>

        <div class="total-por-anio">
            <h1>Total Pedidos</h1>
            <h2>Retiro en Tienda</h2>
            <div class="box">
                
                
                <table width="100%" border="0" cellpadding="0">
                  <tr style="background-color:#62ACE0; color:#fff;">
                    <td width="50%"><p id="anio">Año</p></td>
                    <td width="50%"><p id="porc">Promedio por pedido</p></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php 
                  ksort($totalPedidos['totales']);
                  foreach ($totalPedidos['totales'] as $key => $value): ?>
                      <tr>
                        <td><?= substr($key, 1) ?></td>
                        <td>&nbsp;</td>
                      </tr>

                      <tr>
                        <td><p class="number">
                                <i class="fa fa-usd totales" aria-hidden="true"></i> 
                                <?= number_format($value[0],0,",",".");?>
                            </p>
                        </td>
                        <td>
                            <?php
                            if ($pedidosTotales['totales'][$key][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($value[0] * 100) / $pedidosTotales['totales'][$key][0], 2) . '%</p>';
                            else:
                                echo '<p class="number porcentaje">0%</p>';
                            endif;
                            ?>
                        </td>
                      </tr>
                      <tr style="border-top: solid 1px #ccc;">
                        <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                        <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                      </tr>
                  <?php endforeach ?>
                 </table>
                
            </div>

        </div>
        <div class="div-g">
            <div id="torta-pedidos"></div>
        </div>

        <div class="separator"></div>
        
        
        
        
        <!-- DIV PRODUCTOS -->
        <div class="total-por-anio">
            <h1>Total Productos</h1>
            <h2>Retiro en Tienda</h2>
            <div class="box">
                
                
                <table width="100%" border="0" cellpadding="0">
                  <tr style="background-color:#62ACE0; color:#fff;">
                    <td width="40%"><p id="anio">Año</p></td>
                    <td width="60%"><p id="porc">% vs Total Productos</p></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php 
                  ksort($totalProductos['totales']);
                  foreach ($totalProductos['totales'] as $key => $value): ?>
                      <tr>
                        <td><?= substr($key, 1) ?></td>
                        <td>&nbsp;</td>
                      </tr>

                      <tr>
                        <td><p class="number">
                                <i class="fa fa-usd totales" aria-hidden="true"></i> 
                                <?= number_format($value[0],0,",",".");?>
                            </p>
                        </td>
                        <td>
                            <?php
                            if ($prodTotales['totales'][$key][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($value[0] * 100) / $prodTotales['totales'][$key][0], 2) . '%</p>';
                            else:
                                echo '<p class="number porcentaje">0%</p>';
                            endif;
                            ?>
                        </td>
                      </tr>
                      <tr style="border-top: solid 1px #ccc;">
                        <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                        <td style="border-top: solid 1px #ccc;">&nbsp;</td>
                      </tr>
                  <?php endforeach ?>
                </table>
                  
            </div>

        </div>
        <div class="div-g">
            <div id="torta-productos"></div>
        </div>
		
        <div style="clear:both"></div>
    </div>




