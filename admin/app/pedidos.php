<?php
	include('../conf.php');
	$servicio = consulta_bd("valor","opciones","nombre = 'servicio'","");
	//require_once "../functions.php";
	$tienda = "Ejemplo moldeable";//$_GET['tienda'];

	$pedidos = countPedidos($servicio[0][0]);

	$totalVentas = totalVentas($servicio[0][0]);
	$tiendaName = "Ejemplo moldeable";//$result["nombre"];
	
	ksort($pedidos['total_meses']);
	ksort($totalVentas['total_meses']);

	ksort($pedidos['totales']);

	
?>


	<script type="text/javascript">
	$(function () {
	    var pedidos = new Highcharts.chart({
			chart: {
		    zoomType: 'xy',
		    backgroundColor: '#FAFAFA',
		    renderTo: 'grafico_pedidos'
		    },
		    title: {
		        text: 'Cantidad Pedidos por mes Según Año'
		    },
		    xAxis: [{
		        categories:  ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        crosshair: true
		    }],
		    yAxis: [{ // Primary yAxis
		        title: {
		            text: 'Valor Promedio por mes',
		            style: {
		                color: Highcharts.getOptions().colors[1]
		            }
		        }
		    }, { // Secondary yAxis
		        title: {
		        	text: 'Cantidad de Pedidos',
		            style: {
		                color: Highcharts.getOptions().colors[0]
		            }
		        },
		        opposite: true
		    }],
		    tooltip: {
		        shared: true
		    },
		    series: [
		    	<?php foreach ($pedidos['total_meses'] as $item => $value): ?>
	        		{ name: <?=$item?>,
	        		type: 'column',
	        		yAxis: 1,
	        		data: [
	        			<?php 
				        	for($i = 0; $i < 12; $i++):
				        		if ($value[$i] != null) {
				        			echo $value[$i] . ',';
				        		}else{
				        			echo 0 .',';
				        		}
				        		
				        	endfor;
				        ?>
	        		]
			    },
	        	<?php endforeach ?>
	        	<?php foreach ($totalVentas['total_meses'] as $key => $value): ?>
        		{
        			name: 'Valor Promedio Pedidos '+ <?= $key ?>,
        			type: 'spline',
        			data: [
        				<?php 
        				for($i = 0; $i < 12; $i++):
			        		if ($value[$i] == 0):
			        			echo 0 . ',';
			        		 else:
			        			echo round($value[$i] / $pedidos['total_meses'][$key][$i]) . ',';
			        		endif;
		        		endfor;
        				?>
        			],
        			tooltip: {
        				valuePrefix: '$ '
        			}
        		},
	        	<?php endforeach ?>	
		    ]
		});
	});
</script>
<h2 class="tituloReporte">Pedidos por Mes/Año</h2>
	<div class="contenedor">
    	 

        <div class="total-por-anio">
        	<h1>Total Pedidos</h1>
        	<h2>POR AÑO</h2>
        	<table width="100%" border="0" cellpadding="0">
              <tr style="background-color:#62ACE0; color:#fff;">
                <td width="40%"><p id="anio">Pedidos</p></td>
                <td width="60%"><p id="porc">Valor PPP</p></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              	<?php foreach ($pedidos['totales'] as $key => $value): ?>
              	<tr>
                	<td><?= substr($key, 1) ?></td>
                	<td>&nbsp;</td>
            	</tr>
              	<tr>
              		<td><p class="number"><?= $value[0]; ?></p></td>
              		<td>
              			<p class="number porcentaje">
	                    	<i class="fa fa-usd totales" aria-hidden="true"></i> 
							<?php 
							if ($value[0] > 0):
		        				echo number_format(round($totalVentas['totales'][$key][0] / $value[0]),0,",","."); 
		        			else:
		        				echo "0";
		        			endif;        			
		        		?>
		        		</p>
	        		</td>
	        	</tr>
	        	<tr style="border-top: solid 1px #ccc;">
	                <td style="border-top: solid 1px #ccc;">&nbsp;</td>
	                <td style="border-top: solid 1px #ccc;">&nbsp;</td>
	            </tr>
              	<?php endforeach ?>
            </table>
        </div>
        <div class="div-g">
        	<div id="grafico_pedidos" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
        </div><!--fin selectores-->

        <div style="clear:both"></div>
    </div>
