<?php include("../conf.php"); 
//parametros para el filtro
$hoy= date("Y-m-d");
$añoActual = date("Y");
$añoAnterior = date("Y")-1;
//obtengo ventas del mes actual
$ventasAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual'","");
$ventasAñoActualTotal = 0;
for($i=0; $i<sizeof($ventasAñoActual); $i++){
	$ventasAñoActualTotal +=$ventasAñoActual[$i][1];
}

//Obtengo ventas del mes actual
$mesActual = date("m");
$ventasMesActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '$mesActual'","");
$ventasMesActualTotal = 0;
for($i=0; $i<sizeof($ventasAñoActual); $i++){
	$ventasMesActualTotal +=$ventasMesActual[$i][1];
}


$ventasSemanaActual = consulta_bd("id,total","pedidos","estado_id=2 and fecha BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()","");
$ventasSemanaActualTotal = 0;
for($i=0; $i<sizeof($ventasSemanaActual); $i++){
	$ventasSemanaActualTotal +=$ventasSemanaActual[$i][1];
}



//año en curso
$eneroAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '1'","");
$eneroAñoActualTotal = 0;
for($i=0; $i<sizeof($eneroAñoActual); $i++){
	$eneroAñoActualTotal +=$eneroAñoActual[$i][1];
}


$febreroAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '2'","");
$febreroAñoActualTotal = 0;
for($i=0; $i<sizeof($febreroAñoActual); $i++){
	$febreroAñoActualTotal +=$febreroAñoActual[$i][1];
}

$marzoAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '3'","");
$marzoAñoActualTotal=0;
for($i=0; $i<sizeof($marzoAñoActual); $i++){
	$marzoAñoActualTotal +=$marzoAñoActual[$i][1];
}

$abrilAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '4'","");
$abrilAñoActualTotal = 0;
for($i=0; $i<sizeof($abrilAñoActual); $i++){
	$abrilAñoActualTotal +=$abrilAñoActual[$i][1];
}

$mayoAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '5'","");
$mayoAñoActualTotal = 0;
for($i=0; $i<sizeof($mayoAñoActual); $i++){
	$mayoAñoActualTotal +=$mayoAñoActual[$i][1];
}


$junioAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '6'","");
$junioAñoActualTotal = 0;
for($i=0; $i<sizeof($junioAñoActual); $i++){
	$junioAñoActualTotal +=$junioAñoActual[$i][1];
}

$julioAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '7'","");
$julioAñoActualTotal = 0;
for($i=0; $i<sizeof($julioAñoActual); $i++){
	$julioAñoActualTotal +=$julioAñoActual[$i][1];
}


$agostoAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '8'","");
$agostoAñoActualTotal = 0;
for($i=0; $i<sizeof($agostoAñoActual); $i++){
	$agostoAñoActualTotal +=$agostoAñoActual[$i][1];
}


$septiembreAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '9'","");
$septiembreAñoActualTotal = 0;
for($i=0; $i<sizeof($septiembreAñoActual); $i++){
	$septiembreAñoActualTotal +=$septiembreAñoActual[$i][1];
}


$octubreAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '10'","");
$octubreAñoActualTotal = 0;
for($i=0; $i<sizeof($octubreAñoActual); $i++){
	$octubreAñoActualTotal +=$octubreAñoActual[$i][1];
}


$noviembreAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '11'","");
$noviembreAñoActualTotal = 0;
for($i=0; $i<sizeof($noviembreAñoActual); $i++){
	$noviembreAñoActualTotal +=$noviembreAñoActual[$i][1];
}



$diciembreAñoActual = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoActual' and MONTH(fecha) = '12'","");
$diciembreAñoActualTotal = 0;
for($i=0; $i<sizeof($diciembreAñoActual); $i++){
	$diciembreAñoActualTotal +=$diciembreAñoActual[$i][1];
}



//año en anterior
$eneroAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '1'","");
$eneroAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($eneroAñoAnterior); $i++){
	$eneroAñoAnteriorTotal +=$eneroAñoAnterior[$i][1];
}


$febreroAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '2'","");
$febreroAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($febreroAñoAnterior); $i++){
	$febreroAñoAnteriorTotal +=$febreroAñoAnterior[$i][1];
}

$marzoAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '3'","");
$marzoAñoAnteriorTotal=0;
for($i=0; $i<sizeof($marzoAñoAnterior); $i++){
	$marzoAñoAnteriorTotal +=$marzoAñoAnterior[$i][1];
}

$abrilAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '4'","");
$abrilAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($abrilAñoAnterior); $i++){
	$abrilAñoAnteriorTotal +=$abrilAñoAnterior[$i][1];
}

$mayoAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '5'","");
$mayoAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($mayoAñoAnterior); $i++){
	$mayoAñoAnteriorTotal +=$mayoAñoAnterior[$i][1];
}


$junioAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '6'","");
$junioAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($junioAñoAnterior); $i++){
	$junioAñoAnteriorTotal +=$junioAñoAnterior[$i][1];
}

$julioAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '7'","");
$julioAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($julioAñoAnterior); $i++){
	$julioAñoAnteriorTotal +=$julioAñoAnterior[$i][1];
}


$agostoAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '8'","");
$agostoAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($agostoAñoAnterior); $i++){
	$agostoAñoAnteriorTotal +=$agostoAñoAnterior[$i][1];
}


$septiembreAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '9'","");
$septiembreAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($septiembreAñoAnterior); $i++){
	$septiembreAñoAnteriorTotal +=$septiembreAñoAnterior[$i][1];
}


$octubreAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '10'","");
$octubreAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($octubreAñoAnterior); $i++){
	$octubreAñoAnteriorTotal +=$octubreAñoAnterior[$i][1];
}


$noviembreAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '11'","");
$noviembreAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($noviembreAñoAnterior); $i++){
	$noviembreAñoAnteriorTotal +=$noviembreAñoAnterior[$i][1];
}



$diciembreAñoAnterior = consulta_bd("id,total","pedidos","estado_id=2 and YEAR(fecha) = '$añoAnterior' and MONTH(fecha) = '12'","");
$diciembreAñoAnteriorTotal = 0;
for($i=0; $i<sizeof($diciembreAñoAnterior); $i++){
	$diciembreAñoAnteriorTotal +=$diciembreAñoAnterior[$i][1];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Informe de ventas</title>
<link href="../css/informes.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.8.0.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script>
<script type="text/javascript">
 google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Meses', '<?php echo $añoAnterior ?>', '<?php echo $añoActual ?>'],
          ['Enero', <?php echo $eneroAñoAnteriorTotal; ?>, <?php echo $eneroAñoActualTotal; ?>],
          ['Febrero', <?php echo $febreroAñoAnteriorTotal; ?>, <?php echo $febreroAñoActualTotal; ?>],
		  ['Marzo', <?php echo $marzoAñoAnteriorTotal; ?>, <?php echo $marzoAñoActualTotal; ?>],
		  ['Abril', <?php echo $abrilAñoAnteriorTotal; ?>, <?php echo $abrilAñoActualTotal; ?>],
		  ['Mayo', <?php echo $mayoAñoAnteriorTotal; ?>, <?php echo $mayoAñoActualTotal; ?>],
		  ['Junio', <?php echo $junioAñoAnteriorTotal; ?>, <?php echo $junioAñoActualTotal; ?>],
		  ['Julio', <?php echo $julioAñoAnteriorTotal; ?>, <?php echo $julioAñoActualTotal; ?>],
		  ['Agosto', <?php echo $agostoAñoAnteriorTotal; ?>, <?php echo $agostoAñoActualTotal; ?>],
		  ['Septiembre', <?php echo $septiembreAñoAnteriorTotal; ?>, <?php echo $septiembreAñoActualTotal; ?>],
		  ['Octubre', <?php echo $octubreAñoAnteriorTotal; ?>, <?php echo $octubreAñoActualTotal; ?>],
		  ['Noviembre', <?php echo $noviembreAñoAnteriorTotal; ?>, <?php echo $noviembreAñoActualTotal; ?>],
		  ['Diciembre', <?php echo $diciembreAñoAnteriorTotal; ?>, <?php echo $diciembreAñoActualTotal; ?>]
        ]);

        var options = {
          chart: {
            title: 'Informe de ventas',
            subtitle: 'Comparacion entre año actual y anterior',
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#1682CA', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }
</script>
</head>

<body>
    <div class="contenedor">
        <h2>Informe de ventas</h2>
        
        <div class="columnasDatos">
        	<div class="grillaDatos">
            	<h4>Ventas año actual</h4>
                <h5>$<?php echo number_format($ventasAñoActualTotal,0,",","."); ?></h5>
            </div><!--fin grillaDatos-->
            
            <div class="grillaDatos">
            	<h4>Ventas mes actual</h4>
                <h5>$<?php echo number_format($ventasMesActualTotal,0,",","."); ?></h5>
            </div><!--fin grillaDatos-->
            
            <div class="grillaDatos">
            	<h4>Ventas útimos 7 días</h4>
                <h5>$<?php echo number_format($ventasSemanaActualTotal,0,",","."); ?></h5>
            </div><!--fin grillaDatos-->
            
        </div>
        
        <div class="graficos">
            <div id="chart_div"></div>
            <br/>
            <!--<div id="btn-group">
              <button class="button button-blue" id="none">No Format</button>
              <button class="button button-blue" id="scientific">Scientific Notation</button>
              <button class="button button-blue" id="decimal">Decimal</button>
              <button class="button button-blue" id="short">Short</button>
            </div>-->
        </div>
    </div><!--fin contenedor -->
	
    
    
    

</body>
</html>


