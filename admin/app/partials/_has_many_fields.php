<?php
include('../../conf.php');
/********************************************************\
|  Moldeable CMS - Edición de contenido - Has many fields|
|  Fecha Modificación: 20/09/2012                        |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total                  	 |
|  http://www.moldeable.com/                             |
\********************************************************/
/*

include('../../conf.php');
$op = $_GET[op];	
$val_op = get_val_op($op);
$id = (is_numeric($_GET[id])) ? mysql_real_escape_string($_GET[id]):0;
$tabla = obtener_tabla($val_op);
*/

$id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]):0;

?>
	<h2>Agregar nuevo:</h2>

	<table width="100%" border="0">
		    <?php 	
			//Obtengo los valores según el id
			$columnas = show_columns($tabla,$exclusiones);

			$lista_columnas = implode(",", $columnas);
			$filas = consulta_bd($lista_columnas,$tabla,"id = '$id'","");
	
			$i = 0;
			while($i <= (sizeof($columnas)-1))
			{	
				$column = get_real_name($columnas[$i]);
				//Ponemos los * en los campos obligatorios.
				if ($obligatorios_edit != '')
				{
					if (substr_count($opcion['obligatorios_edit'],",")>0)
					{
						$obligatorios_edit = explode(',', $opcion['obligatorios_edit']);
						foreach ($obligatorios_edit as $obl)
						{
							$obl = trim($obl);
							if ($columnas[$i] == $obl)
							{
								$column = "$column *";
							}
						}
					}
					else
					{
						if ($columnas[$i] == trim($obligatorios_edit))
						{
							$column = "$column *";
						}
					}
				}
				
				$val = ($filas[0][$i]!='') ? $filas[0][$i]: $_GET[$columnas[$i]];
				$column_id = $columnas[$i];
				
				$parent = column_is_related($columnas[$i]);
	
				//RELACIONES
				if ($parent)
				{
					//Si el padre tiene una relación, el select debe ser dependiente (belongs_to) o se debe establecer una relación many_to_many
					//Consulto por la opción many_to_many del padre
					$parent_plural = plural($parent);
					$many_to_many_q = consulta_bd("ot.nombre, ot.valor","opciones_tablas ot, tablas t","t.nombre = '$parent_plural' AND ot.tabla_id = t.id AND (ot.nombre = 'many_to_many' OR ot.nombre = 'belongs_to')","");
					$j = 0;
					while($j <= (sizeof($many_to_many_q)-1))
					{	
						$nombre_p = $many_to_many_q[$j][0];
						$opcion_p[$nombre_p."_parent"] = $many_to_many_q[$j][1];
						$j++;
					}
					
					if (isset($belongs_to_parent))
					$belongs_to_parent = NULL;
					
					if ($opcion_p != NULL)
					extract($opcion_p);
					
					$opcion_p = NULL;
	
					if ($belongs_to_parent != '')
					{
						//En caso de que sea una entrada nueva muestro al padre lleno y al hijo vacío
						$sel2 = $columnas[$i];
						if (!isset($id) OR $val == '')
						{
		    				echo '<tr>';
		    				echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
		    				echo '<td>';
		    					select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', '');
		    				echo '</td>';
		    				echo '</tr>';
		    				
		    				echo '<tr>';
		    				echo '<td>'.get_real_name($parent).'</td>';
		    				echo '<td>';
		    					select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', '');
		    				echo '</td>';
		    				echo '</tr>';	    				
						}
						else
						{
							//Obtengo el id del padre de la relación	
							$column_parent = singular($belongs_to_parent)."_id";
							$rel_id_q = consulta_bd($column_parent,plural($parent),"id = $val","");
							$rel_id = $rel_id_q[0][0];
							
		    				echo '<tr>';
		    				echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
		    				echo '<td>';
		    					select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', $rel_id);
		    				echo '</td>';
		    				echo '</tr>';
		    				
		    				echo '<tr>';
		    				echo '<td>'.get_real_name($parent).'</td>';
		    				echo '<td>';
		    					select(plural($parent), 'nombre', 'id', $sel2, '', "", '', '');
		    				echo '</td>';
		    				echo '</tr>';
						}
						$btp = $belongs_to_parent;
						$related_selects_btp[] = $btp;
						$sel2_btp[] = $sel2;
					}
					else
					{
	    				echo '<tr>';
	    				echo '<td>'.get_real_name($parent).'</td>';
	    				echo '<td>';
	    					select(plural($parent), 'nombre', 'id', $tabla.'['.$columnas[$i].']', '', "", '', '');
	    				echo '</td>';
	    				echo '</tr>';
					}	
				}
				elseif($autocompletar != '')
				{
					//Recorro los autocompletar
					$autocompletar_array = explode(';',$autocompletar);
					foreach ($autocompletar_array as $autocompletar_item)
					{
						$datos_autocompletar = explode(',', $autocompletar_item);
						$tabla_autocompletar = trim($datos_autocompletar[0]);
						$columna_autocompletar = trim($datos_autocompletar[1]);
						
						if ($column_id === $columna_autocompletar)
						{
							
							?>
								<script>
									$(function(){
										$('#autocomplete_<?php echo $column_id?>').autocomplete({
											source: "action/autocomplete.php?tabla=<?php echo $tabla_autocompletar;?>",
											minLength: 2,
											select: function(event, ui){
												$('#<?php echo $column_id?>').val(ui.item.id);
											}
										});
									});
								</script>
								<tr>
									<td valign="top"><?php echo $column ?></td>
									<td>
										<input class="campo_texto" type="text" id="autocomplete_<?php echo $column_id; ?>" />
										<input type="hidden" id="<?php echo $column_id; ?>" name="<?php echo $tabla.'['.$column_id.']'; ?>" value="" />
									</td>
								</tr>
							<?php
						}
					}
                    
                    //$autocompletar = '';
					
				}
				elseif (stristr($archivos,$columnas[$i]))
				{

					echo '<tr>';
					echo '<td valign="top">'.$column.'</td>';
					echo '<td><input type="file" id="'.$column_id.'" name="'.$tabla.'_'.$column_id.'" /></td>';
					echo '</tr>';
				}
		        elseif (is_boolean(trim($columnas[$i]), $tabla))
				{
					$publicado = ($val == 1) ? "checked='checked'": "";
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><input type="checkbox" id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'" '.$publicado.'/></td>';
					echo '</tr>';
				}
		        elseif (is_enum(trim($columnas[$i]), $tabla))
				{
					$enum_vals =  enum_vals(trim($columnas[$i]), $tabla);
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><select name="'.$tabla.'['.$column_id.']'.'" id="'.$column_id.'"><option>Seleccione</option>';
					foreach ($enum_vals as $e)
					{
						echo ($val == $e) ? "<option value='$e' selected>".ucwords(str_replace("_", " ", $e))."</option>":"<option value='$e'>".ucwords(str_replace("_", " ", $e))."</option>";
					}
					echo '</select>';
					echo '</td>';
					echo '</tr>';
				}
		        elseif ($columnas[$i] == 'password')
				{
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><input class="campo_texto" type="password" id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'"/></td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>'.$column.' Check</td>';
					echo '<td><input class="campo_texto" type="password" id="'.$column_id.'_check" name="'.$tabla.'['.$column_id.']'.'_check"/></td>';
					echo '</tr>';
				}
				elseif (stristr($tiny_mce,$columnas[$i]))
				{
					echo '<tr>';
					echo '<td valign="top">'.$column.'</td>';
					echo '<td><textarea id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'"></textarea></td>';
					echo '</tr>';
				}
				else
				{
					if ($columnas[$i] == 'rut')
					{
						echo '<tr>';
						echo '<td>'.$column.'</td>';
						echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'"  onblur="javascript:formato(this.value,'."'$column_id'".');"/></td>';
						echo '</tr>';
					}
					else
					{
						echo '<tr>';
						echo '<td>'.$column.'</td>';
						echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$tabla.'['.$column_id.']'.'" /></td>';
						echo '</tr>';
					}
				}
				$i++;
			}
			$t++;
		?>
	</table>
