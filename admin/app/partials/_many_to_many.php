<?php
if (isset($many_to_many) AND isset($id))
{
	if (is_array($many_to_many))
	{
		foreach ($many_to_many as $many)
		{
		?>
		<div id="tab-<?php echo $t;?>">
			<p>Seleccione <?php echo get_real_name($many);?>:</p>
			<script>
				$(document).ready(function(){
					$("#checkchildren_<?php echo $t;?>").checkboxTree({	initializeChecked: 'expanded', initializeUnchecked: 'collapsed'});
				});
			</script>
			<ul id="checkchildren_<?php echo $t;?>" class="unorderedlisttree">
			<?php
				//Se establece el nombre de la tabla relación
				$mtm_table = $many."_".$tabla;
				
				//Veo si la relacion tiene una tabla belongs_to
				$belongs_to_q = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$many'","");
				$belongs_to_id = $belongs_to_q[0][0];
				
				//Busco los nombres y los id de la relación
				$sql = consulta_bd('id, nombre',"$many",'','id DESC');
				$i = 0;
				while($i <= (sizeof($sql)-1))
				{
					$rel1 = singular($tabla)."_id";
					$rel2 = singular($many)."_id";
					
					$id_mtm = $sql[$i][0];
					$nombre_mtm = str_replace(" ", "_", $sql[$i][1]);
					$col = $nombre_mtm."_".$id_mtm;
					
					echo '
						<li>
							<input type="checkbox" name="'.$col.'" ';
							
							$sql2 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel2 = '".$sql[$i][0]."'","");
							$cant_sql2 = $sql2[0][0];
							if ($cant_sql2 > 0)
							{
								echo 'checked="checked"';
							}
							
					echo '/><label for="'.$col.'">'.$sql[$i][1].'</label>';
					
					
					if ($belongs_to_id)
					{
						//Veo que tabla es la belongs_to_id
						$bt_table = obtener_tabla($belongs_to_id);
						$bt_rel = singular($bt_table).'_id';
						$rel3 = $bt_rel;
						$id3 = $belongs_to_id; 
						$belongs_to_items = consulta_bd("id, nombre","$bt_table","$rel2 = $id_mtm","");
						$j = 0;
						echo '<ul>';

						$belongs_to_q2 = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$bt_table'","");
						$belongs_to_id2 = $belongs_to_q2[0][0];
						
						$sql2 = consulta_bd('id, nombre',"$bt_table",'','id ASC');
						


						while($j <= (sizeof($belongs_to_items)-1)) {
							$val = $belongs_to_items[$j][1];
							$id_bt = $belongs_to_items[$j][0];
							$col_bt = $col.'_'.$id_bt;
							echo '<li>';
								echo '<input type="checkbox" name="'.$col_bt.'" id="'.$col_bt.'"';
								$sql3 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $bt_rel = '".$id_bt."' AND $rel2 = '".$sql[$i][0]."'","");
								$cant_sql3 = $sql3[0][0];
								if ($cant_sql3 > 0)
								{
									echo 'checked="checked"';
								}
								echo '/><label for="'.$col_bt.'">'.$val.'</label>';



// -------- TERCERA CATEGORIA IN -------
								
								if ($belongs_to_id2)
								{
									//Veo que tabla es la belongs_to_id
									$bt_table2 = obtener_tabla($belongs_to_id2);
									$bt_rel2 = singular($bt_table2).'_id';
									$rel4 = $bt_rel2;
									$belongs_to_items2 = consulta_bd("id, nombre","$bt_table2","$rel3 = $id_bt","");

									$belongs_to_q3 = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$bt_table2'","");
									$belongs_to_id3 = $belongs_to_q3[0][0];

									$c = 0;
									echo '<ul>';

									while($c <= (sizeof($belongs_to_items2)-1)) {
										$val3 = $belongs_to_items2[$c][1];
										$id_bt3 = $belongs_to_items2[$c][0];
										$col_bt3 = $col_bt.'_'.$id_bt3;
										echo '<li>';
											echo '<input type="checkbox" name="'.$col_bt3.'" id="'.$col_bt3.'"';
											$sql4 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel3 = '".$id_bt."' AND $bt_rel2 = '".$id_bt3."'","");
											$cant_sql4 = $sql4[0][0];
											if ($cant_sql4 > 0)
											{
												echo 'checked="checked"';
											}
											echo '/><label for="'.$col_bt3.'">'.$val3.'</label>';


// -------- CUARTA CATEGORIA IN -------


											if ($belongs_to_id3)
											{
												//Veo que tabla es la belongs_to_id
												$bt_table3 = obtener_tabla($belongs_to_id3);
												$bt_rel3 = singular($bt_table3).'_id';
												$belongs_to_items3 = consulta_bd("id, nombre","$bt_table3","$rel4 = $id_bt3","");

												echo "$id_bt3";

												$d = 0;
												echo '<ul>';

												while($d <= (sizeof($belongs_to_items3)-1)) {
													$val4 = $belongs_to_items3[$d][1];
													$id_bt4 = $belongs_to_items3[$d][0];
													$col_bt4 = $col_bt3.'_'.$id_bt4;

													echo '<li>';
														echo '<input type="checkbox" name="'.$col_bt4.'" id="'.$col_bt4.'"';

														$sql5 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel4 = '".$id_bt3."' AND $bt_rel3 = '".$id_bt4."'","");
														$cant_sql5 = $sql5[0][0];
														if ($cant_sql5 > 0)
														{
															echo 'checked="checked"';
														}
														echo '/><label for="'.$col_bt4.'">'.$val4.'</label>';

														echo '</li>';
													$d++;
												}
												echo '</ul>';
											}


// -------- CUARTA CATEGORIAS OUT -------



										echo '</li>';
										$c++;
									}
									echo '</ul>';
								}


// -------- TERCERA CATEGORIAS OUT -------



							echo '</li>';
							$j++;
						}
						echo '</ul>';
					}

										
					echo '</li>';
						$i++;
				}
			?>
			</ul>
		</div>
		<?php	
			$t++;
		}
	}
	else
	{
	?>
		<div id="tab-<?php echo $t;?>">
			<p>Seleccione <?php echo get_real_name($many_to_many);?>:</p>
			<ul id="checkchildren" class="unorderedlisttree">
			<?php
				//Se establece el nombre de la tabla relación
				$mtm_table = $many_to_many."_".$tabla;
				
				//Veo si la relacion tiene una tabla belongs_to
				$belongs_to_q = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$many_to_many'","");
				$belongs_to_id = $belongs_to_q[0][0];
				
				//Busco los nombres y los id de la relación
				$sql = consulta_bd('id, nombre',"$many_to_many",'','id ASC');
				$i = 0;

// -------- CATEGORIAS IN -------
				while($i <= (sizeof($sql)-1))
				{
					$rel1 = singular($tabla)."_id";
					$rel2 = singular($many_to_many)."_id";
					
					$id_mtm = $sql[$i][0];
					$nombre_mtm = str_replace(" ", "_", $sql[$i][1]);
					$col = $nombre_mtm."_".$id_mtm;
					
					echo '
						<li>
							<input type="checkbox" name="'.$col.'" id="'.$col.'" ';
							
							$sql2 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel2 = '".$sql[$i][0]."'","");
							$cant_sql2 = $sql2[0][0];
							if ($cant_sql2 > 0)
							{
								echo 'checked="checked"';
							}
							
					echo '/><label for="'.$col.'">'.$sql[$i][1].'</label>';

// -------- SUBCATEGORIAS IN ------- 
					if ($belongs_to_id)
					{
						//Veo que tabla es la belongs_to_id
						$bt_table = obtener_tabla($belongs_to_id);
						$bt_rel = singular($bt_table).'_id';
						$rel3 = $bt_rel;
						$id3 = $belongs_to_id; 
						$belongs_to_items = consulta_bd("id, nombre","$bt_table","$rel2 = $id_mtm","");
						$j = 0;
						echo '<ul>';

						$belongs_to_q2 = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$bt_table'","");
						$belongs_to_id2 = $belongs_to_q2[0][0];
						
						$sql2 = consulta_bd('id, nombre',"$bt_table",'','id ASC');
						


						while($j <= (sizeof($belongs_to_items)-1)) {
							$val = $belongs_to_items[$j][1];
							$id_bt = $belongs_to_items[$j][0];
							$col_bt = $col.'_'.$id_bt;
							echo '<li>';
								echo '<input type="checkbox" name="'.$col_bt.'" id="'.$col_bt.'"';
								$sql3 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $bt_rel = '".$id_bt."' AND $rel2 = '".$sql[$i][0]."'","");
								$cant_sql3 = $sql3[0][0];
								if ($cant_sql3 > 0)
								{
									echo 'checked="checked"';
								}
								echo '/><label for="'.$col_bt.'">'.$val.'</label>';



// -------- TERCERA CATEGORIA IN -------
								
								if ($belongs_to_id2)
								{
									//Veo que tabla es la belongs_to_id
									$bt_table2 = obtener_tabla($belongs_to_id2);
									$bt_rel2 = singular($bt_table2).'_id';
									$rel4 = $bt_rel2;
									$belongs_to_items2 = consulta_bd("id, nombre","$bt_table2","$rel3 = $id_bt","");

									$belongs_to_q3 = consulta_bd("tabla_id","opciones_tablas","nombre = 'belongs_to' AND valor = '$bt_table2'","");
									$belongs_to_id3 = $belongs_to_q3[0][0];

									$c = 0;
									echo '<ul>';

									while($c <= (sizeof($belongs_to_items2)-1)) {
										$val3 = $belongs_to_items2[$c][1];
										$id_bt3 = $belongs_to_items2[$c][0];
										$col_bt3 = $col_bt.'_'.$id_bt3;
										echo '<li>';
											echo '<input type="checkbox" name="'.$col_bt3.'" id="'.$col_bt3.'"';
											$sql4 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel3 = '".$id_bt."' AND $bt_rel2 = '".$id_bt3."'","");
											$cant_sql4 = $sql4[0][0];
											if ($cant_sql4 > 0)
											{
												echo 'checked="checked"';
											}
											echo '/><label for="'.$col_bt3.'">'.$val3.'</label>';


// -------- CUARTA CATEGORIA IN -------


											if ($belongs_to_id3)
											{
												//Veo que tabla es la belongs_to_id
												$bt_table3 = obtener_tabla($belongs_to_id3);
												$bt_rel3 = singular($bt_table3).'_id';
												$belongs_to_items3 = consulta_bd("id, nombre","$bt_table3","$rel4 = $id_bt3","");

												echo "$id_bt3";

												$d = 0;
												echo '<ul>';

												while($d <= (sizeof($belongs_to_items3)-1)) {
													$val4 = $belongs_to_items3[$d][1];
													$id_bt4 = $belongs_to_items3[$d][0];
													$col_bt4 = $col_bt3.'_'.$id_bt4;

													echo '<li>';
														echo '<input type="checkbox" name="'.$col_bt4.'" id="'.$col_bt4.'"';

														$sql5 = consulta_bd("count(*)","$mtm_table","$rel1 = '$id' AND $rel4 = '".$id_bt3."' AND $bt_rel3 = '".$id_bt4."'","");
														$cant_sql5 = $sql5[0][0];
														if ($cant_sql5 > 0)
														{
															echo 'checked="checked"';
														}
														echo '/><label for="'.$col_bt4.'">'.$val4.'</label>';

														echo '</li>';
													$d++;
												}
												echo '</ul>';
											}


// -------- CUARTA CATEGORIAS OUT -------



										echo '</li>';
										$c++;
									}
									echo '</ul>';
								}


// -------- TERCERA CATEGORIAS OUT -------



							echo '</li>';
							$j++;
						}
						echo '</ul>';
					}

// -------- SUB CATEGORIAS OUT -------

					echo '</li>';
						$i++;
				}
				$t++;
// -------- CATEGORIAS OUT -------
			?>
			</ul>
		</div>		
	<?php
	}
}
?>