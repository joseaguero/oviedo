<?php
include ('../conf.php');
/********************************************************\
|  Moldeable CMS - Edición de contenido - General		 |
|  Fecha Modificación: 4/04/2014		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total  					 |
|  http://www.alfastudio.cl/                             |
\********************************************************/

?>
<div id="tab-<?php echo $t;?>">

	<!-- Hidden_fields -->
	<?php
		if ($hidden_fields) {
			$hidden_fields_array = json_decode($hidden_fields, true);
			foreach ($hidden_fields_array['hidden_fields'] as $val)
			{
				if ($val['valor'][0] == '$')
				{
					$resto_palabra = substr($val['valor'], 1);
					$valor =  $$resto_palabra;
				}
				else
				{
					$valor = $val['valor'];
				}
			
				?>
					<input type="hidden" name="<?php echo $val['nombre'];?>" value="<?php echo $valor;?>" />
				<?php
			}
		}
	?>
	<!-- Fin Hidden_fields -->

	<table width="100%" border="0">
    	<tr>
        	<td width="100"></td>
            <td></td>
        </tr>
		    <?php 	
			//Obtengo los valores según el id
			$columnas = show_columns($tabla,$exclusiones);

			if ($tabs)
			{
				$exclusiones_tabs = $exclusiones;
				//Buscamos las columnas terminadas en el idioma
				foreach ($columnas AS $col)
				{
					//Encuentra las columnas que están dentro de lista_tabs.
					foreach($lista_tabs AS $tab)
					{
						$tab = trim($tab);
						if ($col==$tab)
						{
							//Agrego a las exclusiones el nombre de la columna en el idioma
							$exclusiones_tabs .= ", ".$col;
						}
					}	
				}
				$columnas = show_columns($tabla,$exclusiones_tabs);
			}
			
			if ($idiomas)
			{
				$exclusiones_l = $exclusiones;
				//Buscamos las columnas terminadas en el idioma
				$r = 0;
				foreach ($columnas AS $col)
				{
					//Veo si existe un _ en el nombre de la columna
					if (strpos($col, '_'))
					{
						//Encuentra las columnas terminadas en cada idioma.
						foreach($lista_idiomas AS $idioma)
						{
							$buscar = "_".trim($idioma);
							if (strrpos($col, $buscar))
							{
								//Agrego a las exclusiones el nombre de la columna en el idioma
								$exclusiones_l .= ", ".$col;
							}
							$r++;
						}	
					}
				}
				$columnas = show_columns($tabla,$exclusiones_l);
			}
	
			$lista_columnas = implode(",", $columnas);
			$filas = consulta_bd($lista_columnas,$tabla,"id = '$id'","");
	
			$i = 0;
			while($i <= (sizeof($columnas)-1))
			{	
				$parent = column_is_related($columnas[$i]);
				$parent_name = $parent;
				$column = get_real_name($columnas[$i]);
				
				//Ponemos los * en los campos obligatorios.
				if ($obligatorios_edit != '')
				{
					$nombre_sin_id = str_replace('_id', '', $columnas[$i]);
					if (substr_count($opcion['obligatorios_edit'],",")>0)
					{
						$obligatorios_edit = explode(',', $opcion['obligatorios_edit']);
						foreach ($obligatorios_edit as $obl)
						{
							$obl = trim($obl);
							if ($columnas[$i] == $obl)
							{
								$column = "$column *";
								if ($parent)
								{
									$parent_name = "$parent_name *";
								}
							}
						}
					}
					else
					{
						if ($nombre_sin_id == trim($obligatorios_edit))
						{
							$column = "$column *";
							if ($parent)
							{
								$parent_name = "$parent_name *";
							}
						}
					}
				}
				
				$val = ($filas[0][$i]!='') ? $filas[0][$i]: $_GET[$columnas[$i]];
				$column_id = $columnas[$i];
					
				if ($tienda) {
					if ($columnas[$i] == 'tipo')
					{
						$tipo = $val;
					}
				}
				
				$parent = column_is_related($columnas[$i]);
	
				//RELACIONES
				if ($parent)
				{
					//Si el padre tiene una relación, el select debe ser dependiente (belongs_to) o se debe establecer una relación many_to_many
					//Consulto por la opción many_to_many del padre
					$parent_plural = plural($parent);
					$many_to_many_q = consulta_bd("ot.nombre, ot.valor","opciones_tablas ot, tablas t","t.nombre = '$parent_plural' AND ot.tabla_id = t.id AND (ot.nombre = 'many_to_many' OR ot.nombre = 'belongs_to')","");
					$j = 0;
					while($j <= (sizeof($many_to_many_q)-1))
					{	
						$nombre_p = $many_to_many_q[$j][0];
						$opcion_p[$nombre_p."_parent"] = $many_to_many_q[$j][1];
						$j++;
					}
					
					if (isset($belongs_to_parent))
					$belongs_to_parent = NULL;
					
					if ($opcion_p != NULL)
					extract($opcion_p);
					
					$opcion_p = NULL;
	
					if ($many_to_many_parent != NULL)
					{
						//En caso de que sea una entrada nueva muestro al padre lleno y al hijo vacío
						$sel2 = $columnas[$i];
						if (!isset($id))
						{
		    				echo '<tr>';
		    				echo '<td>'.get_real_name(singular($many_to_many_parent)).'</td>';
		    				echo '<td>';
		    					select($many_to_many_parent, 'nombre', 'id', $many_to_many_parent, '', '', '', '');
		    				echo '</td>';
		    				echo '</tr>';
		    				
		    				echo '<tr>';
		    				echo '<td>'.get_real_name($parent_name).'</td>';
		    				echo '<td>';
		    					select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', '');
		    				echo '</td>';
		    				echo '</tr>';
						}
						else
						{
							//Obtengo el id del padre de la relación
							$rel_table = $many_to_many_parent."_".plural($parent);
							$rel_id_q = consulta_bd(singular($many_to_many_parent)."_id",$rel_table,$parent."_id = $val","");
							$rel_id = $rel_id_q[0][0];
						
		    				echo '<tr>';
		    				echo '<td>'.get_real_name(singular($many_to_many_parent)).'</td>';
		    				echo '<td>';
		    					select($many_to_many_parent, 'nombre', 'id', $many_to_many_parent, '', '', '', $rel_id);
		    				echo '</td>';
		    				echo '</tr>';
		    				
		    				echo '<tr>';
		    				echo '<td>'.get_real_name($parent_name).'</td>';
		    				echo '<td>';
		    					select(plural($parent), 'nombre', 'id', $sel2, '', "", '', $val);
		    				echo '</td>';
		    				echo '</tr>';
						}
					}
					elseif ($belongs_to_parent != '')
					{
						//En caso de que sea una entrada nueva muestro al padre lleno y al hijo vacío
						$sel2 = $columnas[$i];
						if (!isset($id) OR $val == '')
						{
							//Obtengo el id de la tabla btp
							$btp_id = consulta_bd("id", "tablas", "nombre = '$belongs_to_parent'", "");
							$btp_id = $btp_id[0][0];
							 
							//Pregunto si hay relación 1
							$gran_parent = consulta_bd("valor","opciones_tablas","tabla_id = $btp_id AND nombre = 'belongs_to'","");
							$gran_parent_exists = mysqli_affected_rows($conexion);
							if ($gran_parent_exists > 0) {
								
								$gran_parent_tabla = $gran_parent[0][0];
								$grand_parent_rel_id = singular($gran_parent_tabla)."_id";
								
								//Obtengo el id de la tabla btp
								$btgp_id = consulta_bd("id", "tablas", "nombre = '$gran_parent_tabla'", "");
								$btgp_id = $btgp_id[0][0];
								
								//Pregunto si hay relación 2
								$gran_gran_parent = consulta_bd("valor","opciones_tablas","tabla_id = $btgp_id AND nombre = 'belongs_to'","");
								$gran_gran_parent_exists = mysqli_affected_rows($conexion);
								if ($gran_gran_parent_exists > 0) {
									
									$gran_gran_parent_tabla = $gran_gran_parent[0][0];
									$grand_gran_parent_rel_id = singular($gran_gran_parent_tabla)."_id";
									
				    				echo '<tr>';
				    				echo '<td>'.get_real_name(singular($gran_gran_parent_tabla)).'</td>';
				    				echo '<td>';
				    					select($gran_gran_parent_tabla, 'nombre', 'id', $gran_gran_parent_tabla, '', '', '', '');
				    				echo '</td>';
				    				echo '</tr>';
				    				$related_selects_btp[] = $gran_gran_parent_tabla;
				    				$sel2_btp[] = $gran_parent_tabla;
								}
								
			    				echo '<tr>';
			    				echo '<td>'.get_real_name(singular($gran_parent_tabla)).'</td>';
			    				echo '<td>';
			    					select($gran_parent_tabla, 'nombre', 'id', $gran_parent_tabla, '', '', '', '');
			    				echo '</td>';
			    				echo '</tr>';
			    				$related_selects_btp[] = $gran_parent_tabla;
			    				$sel2_btp[] = $belongs_to_parent;
							}
							
		    				echo '<tr>';
		    				echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
		    				echo '<td>';
		    					select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', '');
		    				echo '</td>';
		    				echo '</tr>';
		    				
		    				echo '<tr>';
		    				echo '<td>'.get_real_name($parent_name).'</td>';
		    				echo '<td>';
		    					select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', '');
		    				echo '</td>';
		    				echo '</tr>';	    				
						}
						else
						{
							//Obtengo el id del padre de la relación	
							$column_parent = singular($belongs_to_parent)."_id";
							
							//Consulto si existe relación en un nivel superior.
							//Obtengo el id de la tabla btp
							$btp_id = consulta_bd("id", "tablas", "nombre = '$belongs_to_parent'", "");
							$btp_id = $btp_id[0][0];
							
							//Consulto el valor del padre en la tabla (cuestión)
							//SELECT cuestion_id FROM articulos WHERE id = 1
							$rel_id_q = consulta_bd($column_parent,plural($parent),"id = $val","");
							$rel_id = $rel_id_q[0][0];

							//Pregunto si hay relación
							$gran_parent = consulta_bd("valor","opciones_tablas","tabla_id = $btp_id AND nombre = 'belongs_to'","");
							$gran_parent_exists = mysqli_affected_rows($conexion);
							if ($gran_parent_exists > 0) {
								
								$gran_parent_tabla = $gran_parent[0][0];
								$grand_parent_rel_id = singular($gran_parent_tabla)."_id";
								
								//Obtengo el valor del select. (tema)
								//SELECT tema_id FROM cuestiones WHERE id = 1
								$rel_id_gran_parent = consulta_bd("$grand_parent_rel_id","$belongs_to_parent","id = $rel_id","");
								$rel_id_gran_parent = $rel_id_gran_parent[0][0];
								
								//Obtengo el id de la tabla btp
								$btgp_id = consulta_bd("id", "tablas", "nombre = '$gran_parent_tabla'", "");
								$btgp_id = $btgp_id[0][0];
								
								//Pregunto si hay relación 2
								$gran_gran_parent = consulta_bd("valor","opciones_tablas","tabla_id = $btgp_id AND nombre = 'belongs_to'","");
								$gran_gran_parent_exists = mysqli_affected_rows($conexion);
								if ($gran_gran_parent_exists > 0) {
									
									$gran_gran_parent_tabla = $gran_gran_parent[0][0];
									$gran_gran_parent_rel_id = singular($gran_gran_parent_tabla)."_id";
									
									//Obtengo el valor del select. (parte)
									//SELECT parte_id FROM temas WHERE id = 2
									$rel_id_gran_gran_parent = consulta_bd("$gran_gran_parent_rel_id","$gran_parent_tabla","id = $rel_id_gran_parent","");
									$rel_id_gran_gran_parent = $rel_id_gran_gran_parent[0][0];
									
									$gran_gran_parent_tabla = $gran_gran_parent[0][0];
									$grand_gran_parent_rel_id = singular($gran_gran_parent_tabla)."_id";
									
				    				echo '<tr>';
				    				echo '<td>'.get_real_name(singular($gran_gran_parent_tabla)).'</td>';
				    				echo '<td>';
				    					select($gran_gran_parent_tabla, 'nombre', 'id', $gran_gran_parent_tabla, '', '', '', $rel_id_gran_gran_parent);
				    				echo '</td>';
				    				echo '</tr>';
				    				$related_selects_btp[] = $gran_gran_parent_tabla;
				    				$sel2_btp[] = $gran_parent_tabla;
								}
								
								
			    				echo '<tr>';
			    				echo '<td>'.get_real_name(singular($gran_parent_tabla)).'</td>';
			    				echo '<td>';
			    					select($gran_parent_tabla, 'nombre', 'id', $gran_parent_tabla, '', '', '', $rel_id_gran_parent);
			    				echo '</td>';
			    				echo '</tr>';
			    				$related_selects_btp[] = $gran_parent_tabla;
			    				$sel2_btp[] = $belongs_to_parent;
							}
							
							//Cuestion
		    				echo '<tr>';
		    				echo '<td>'.get_real_name(singular($belongs_to_parent)).'</td>';
		    				echo '<td>';
		    					select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', $rel_id);
		    				echo '</td>';
		    				echo '</tr>';
		    				
		    				//Articulo
		    				echo '<tr>';
		    				echo '<td>'.get_real_name($parent_name).'</td>';
		    				echo '<td>';
		    					select(plural($parent), 'nombre', 'id', $sel2, '', "", '', $val);
		    				echo '</td>';
		    				echo '</tr>';
						}
						$btp = $belongs_to_parent;
						$related_selects_btp[] = $btp;
						$sel2_btp[] = $sel2;
					}
					else
					{
	    				echo '<tr>';
	    				echo '<td>'.get_real_name($parent_name).'</td>';
	    				echo '<td>';
	    					select(plural($parent), 'nombre', 'id', $columnas[$i], '', "", '', $val);
	    				echo '</td>';
	    				echo '</tr>';
					}	
				}
				elseif (stristr($archivos,$columnas[$i]))
				{
					$file_ext = strtolower(substr(strrchr($val, '.'), 1));
					
					if ($file_ext == 'jpg' or $file_ext == 'gif' or $file_ext == 'png' or $file_ext == 'jpeg' or $file_ext == 'webp')
					{
						$f = $dir.'/'.$val;
						$size=getimagesize($f); 
						$width=$size[0]; 
						$height=$size[1];
						
						if ($width>300)
						{
							$tam = "width='320'";
							$width = 300;
						}
						elseif($height>300)
						{	
							$tam = "height='300'";
						}
						else
						{
							$tam = '';
							$width = $width-40;
						}
						
						echo '<tr class="fila_imagen_'.$columnas[$i].'">';
						echo '<td valign="top">'.$column.' actual:</td>';
						echo '<td>';
						echo	  '<a class="img_admin" href="javascript:void(0)" alt="Borrar" title="Borrar">
										<img class="btn_borrar_imagen" onclick="delete_img(';
										echo "'$id', '$tabla', '$columnas[$i]'";
										echo ')"
											src="pics/delete.png" border="0"/>
										
										<img src="'.$f.'" border="0" '.$tam.' id="cont_img_actual_'.$columnas[$i].'"/>
									</a>';
						echo 	'</td>';
						echo '</tr>';
						
						
					}
					elseif ($file_ext != '')
					{
						echo '<tr>';
					    echo '<td valign="bottom">'.$column.' actual:</td>';
						echo '<td>
								<div id="file_name_for">
									<a href="'.$dir_docs.'/'.$val.'">'.$val.'</a>
									<a href="#" onclick="javascript:delete_file(';
									echo "'$id','$tabla','$columnas[$i]','$val'";
									echo ')" >
									<img src="pics/delete.jpg" border="0" alt="Eliminar Archivo" title="Eliminar Archivo">
								</div>
								</a>
							  </td>';	
						echo '</tr>';
						
					}
					
					
					$paso1 = consulta_bd("id","tablas","nombre='$tabla'","id desc");
					$paso4 = consulta_bd("valor","opciones_tablas","tabla_id=".$paso1[0][0]." and nombre ='crop'","id desc");
					$campos_separados = explode(",", $paso4[0][0]);
					
					if(has_array($campos_separados, $columnas[$i])){
							
							//die("1");
						} else {
							//die("2");
							echo '<tr class="'.$columnas[$i].' '.$oculto.'">';
								echo '<td valign="top">'.$column.'</td>';
								echo '<td><input type="file" id="'.$column_id.'" name="'.$column_id.'" /></td>';
							echo '</tr>';
						}
						
					/* for($k=0; $k<sizeof($campos_separados); $k++){
						if($campos_separados[$k] != $columnas[$i]){
							if($val != ''){
								$oculto = 'oculto';
							} else {
								$oculto = '';
							}
							echo '<tr class="'.$columnas[$i].' '.$oculto.'">';
								echo '<td valign="top">'.$column.'</td>';
								echo '<td><input type="file" id="'.$column_id.'" name="'.$column_id.'" /></td>';
							echo '</tr>';
						}
					}  */
					
					
					for($k=0; $k<sizeof($campos_separados); $k++){
						if($campos_separados[$k] == $columnas[$i]){
							$medidas = consulta_bd("valor","opciones_tablas","nombre='$campos_separados[$k]' and tabla_id=".$paso1[0][0],"");
							//echo $medidas[0][0];
							if($val != ''){
								$oculto = 'oculto';
							} else {
								$oculto = '';
							}
							$medidas_separadas = explode(",", $medidas[0][0]);
							$ancho_crop = trim($medidas_separadas[0]);
							$alto_crop = trim($medidas_separadas[1]);
							echo '<tr class="'.$columnas[$i].' '.$oculto.'" >
									<td valign="top">'.$column.'</td>
									<td>
										<div id="malla" class="cont_crop" style="float:left; width:'.$ancho_crop.'px; height: '.$alto_crop.'px" >
											<div class="cropContainerModal" id="'.$column_id.'_cropContainerModal" rel="'.$f.'"></div>
										</div>
									</td>
								</tr>';
						}
					}//fin for
					
				}
		        elseif (is_boolean(trim($columnas[$i]), $tabla))
				{
					$publicado = ($val == 1) ? "checked='checked'": "";
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><input type="checkbox" id="'.$column_id.'" name="'.$column_id.'" '.$publicado.'/></td>';
					echo '</tr>';
				}
		        elseif (is_enum(trim($columnas[$i]), $tabla))
				{
					$enum_vals =  enum_vals(trim($columnas[$i]), $tabla);
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><select name="'.$column_id.'" id="'.$column_id.'"><option>Seleccione</option>';
					foreach ($enum_vals as $e)
					{
						echo ($val == $e) ? "<option value='$e' selected>".ucwords(str_replace("_", " ", $e))."</option>":"<option value='$e'>".ucwords(str_replace("_", " ", $e))."</option>";
					}
					echo '</select>';
					echo '</td>';
					echo '</tr>';
				}
				elseif($autocompletar != '')
				{
					//Recorro los autocompletar
					$autocompletar_array = explode(';',$autocompletar);
					foreach ($autocompletar_array as $autocompletar_item)
					{
						$datos_autocompletar = explode(',', $autocompletar_item);
						$tabla_autocompletar = trim($datos_autocompletar[0]);
						$columna_autocompletar = trim($datos_autocompletar[1]);
						
						if ($columnas[$i] == $columna_autocompletar)
						{
							?>
								<script>
									$(function(){
										$('#autocomplete_<?php echo $column_id?>').autocomplete({
											source: "action/autocomplete.php?tabla=<?php echo $tabla_autocompletar;?>",
											minLength: 2,
											select: function(event, ui){
												$('#<?php echo $column_id?>').val(ui.item.id);
											}
										});
									});
								</script>
								<tr>
									<td valign="top"><?php echo $column ?></td>
									<td>
										<input type="text" class="campo_texto" id="autocomplete_<?php echo $column_id; ?>" />
										<input type="hidden" id="<?php echo $column_id; ?>" name="<?php echo $column_id; ?>" value="" />
									</td>
								</tr>
							<?php
						}
					}
					
				}
		        elseif ($columnas[$i] == 'password')
				{
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><input class="campo_texto" type="password" id="'.$column_id.'" name="'.$column_id.'"/></td>';
					echo '</tr>';
					echo '<tr>';
					echo '<td>'.$column.' Check</td>';
					echo '<td><input class="campo_texto" type="password" id="'.$column_id.'_check" name="'.$column_id.'_check"/></td>';
					echo '</tr>';
				}
				elseif ($tienda and $columnas[$i] == 'valor' and $tipo == "Texto")
				{
					echo '<tr>';
					echo '<td valign="top">'.$column.'</td>';
					echo '<td><textarea id="valor_opcion_tienda" name="'.$column_id.'">'.$val.'</textarea></td>';
					echo '</tr>';
				}
				elseif (stristr($tiny_mce,$columnas[$i]))
				{
					echo '<tr>';
					echo '<td valign="top">'.$column.'</td>';
					echo '<td><textarea id="'.$column_id.'" name="'.$column_id.'">'.$val.'</textarea></td>';
					echo '</tr>';
				}
				else
				{
					echo '<tr>';
					echo '<td>'.$column.'</td>';
					echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$column_id.'" value="'.$val.'"/></td>';
					echo '</tr>';
				}
				$i++;
			}
			$t++;
		?>
        <!--<tr>
            <td valign="top">Malla</td>
            <td>
                <div id="malla" class="cont_crop" style="float:left; width:500px;">
                    <div id="cropContainerModal"></div>
                </div>
            </td>
        </tr>-->
        
	</table>
</div>