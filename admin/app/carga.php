<?php  ?>

<form action="action/upload_excel.php" method="post" enctype="multipart/form-data">
	<select name="tipo" class="tipo_excel">
		<option value="0">Seleccionar carga</option>
		<option value="1">Stock y precio</option>
		<option value="2">Productos</option>
	</select>
	<input type="file" data-buttonText="Seleccione archivo" name="excel">
	<input type="submit" name="enviar" value="Subir">
</form>

<a href="" style="display: none; margin-top: 10px;" class="link_plantilla" data-base="<?= $url_base ?>">Descargar plantilla</a>

<?php $historial = consulta_bd('fecha_subida, archivo, tipo', 'historial_upload_excel', '', 'id desc'); ?>

<h2>Historial de cargas</h2>
<table class="table-excel" border="0" cellpadding="10" cellspacing="0">
	<thead>
		<tr>
			<th>Tipo excel</th>
			<th>Fecha subida</th>
			<th>Acción</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($historial as $row): ?>
			<tr>
				<td><?= $row[2] ?></td>
				<td><?= $row[0] ?></td>
				<td><a href="<?= $url_base . 'admin/excel/' . $row[1] ?>">Descargar</a></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>