<?php
	include('../conf.php');
	$servicio = consulta_bd("valor","opciones","nombre = 'servicio'","");
	
	$tienda = "Ejemplo moldeable";//$_GET['tienda'];
	$service = "";
	$serv1 = ""; $serv2 = ""; $serv3 = ""; $serv4 = "";

	
	$totalVentas = totalVentas($servicio[0][0]);

	$tiendaName = "Ejemplo moldeable";//$result["nombre"];
	ksort($totalVentas['total_meses']);
?>

	<script type="text/javascript">
	$(function () {
	    var ventas = new Highcharts.Chart({
	        chart: {
	            type: 'column',
	            backgroundColor: '#FAFAFA',
	            renderTo: 'grafico_ventas'
	        },
	        title: {
	            text: 'Valor ventas por mes y año'
	        },
	        xAxis: {
	            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ]

	        },
	        yAxis: {
	            title: {
	                text: 'Total por mes'
	            }
	        },
	        series: [
	        	<?php foreach ($totalVentas['total_meses'] as $item => $value): ?>
	        		{ name: <?=$item?>,
	        		data: [
	        			<?php 
				        	for($i = 0; $i < 12; $i++):
				        		if ($value[$i] != null) {
				        			echo $value[$i] . ',';
				        		}else{
				        			echo 0 .',';
				        		}
				        		
				        	endfor;
				        ?>
	        		],
			        tooltip: {
			            valuePrefix: '$ '
			        }
			    },
	        	<?php endforeach ?>
	        ]
	    });

	
	});
</script>

        
    <h2 class="tituloReporte">Ventas por Mes/Año</h2>
    
    <div class="contenedor" style="margin-top:20px;">
        
        <div class="total-por-anio">
        	<h1>Total Ventas</h1>
        	<h2>POR AÑO</h2>

        	<?php 
        	ksort($totalVentas['totales']);
        	foreach ($totalVentas['totales'] as $key => $value): ?>
        		<div class="box">
	        		<p><?= substr($key, 1) ?></p>
	        		<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
	        		<?php echo number_format($value[0],0,",","."); ?>
	        		</p>
	        	</div>
        	<?php endforeach ?>
        </div>
        <div class="div-g">
        	<div id="grafico_ventas" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
        </div><!--fin selectores-->
        <div style="clear:both"></div>
    </div>
