<?php
	include('../conf.php');
	include('includes/tienda/cart/inc/functions.inc.php');

	$region_id = 0;
	$comuna_id = 0;
	$localidad_id = 0;
	$calle = '';
	$numero = '';

?>

<style type="text/css">
	#principal{
		display: table;
		clear: both;
	}
	h2{
		text-align: center;
		width: 100%;
	}
	h4{
	    margin: -28px 0 10px;
	    background: #fff;
	    float: left;
	    padding: 0 5px;
	}
	.datos{
		width: 100%;
		border: 1px solid #ccc;
		padding: 20px 20px 15px;
		box-sizing: border-box;
		margin-bottom: 20px;
		float: left;
	}
	.datos > div{
		float: left;
		width: 100%;
	}
	.datos input{
		margin-bottom: 5px;
		box-sizing: border-box;
		float: left;
		width: calc(25% - 8px);
		margin-right: 10px;
	}
	.datos input:nth-child(4n){
		margin-right: 0;
	}
	.datos .selector{
		float: left;
		width: calc(25% - 8px);
		margin-right: 10px;
		box-sizing: border-box;
		margin-bottom: 5px;
		height: auto;
	}
	.productos{
		width: 100%;
	}
	.productos td{
		border: 1px solid #d7d7d7;	
		padding: 0 5px;
	}
	.tipo_compra > div{
		float: left;
		width: 100px;
	}
	.tipo_compra > div input{
		margin: 1px;
	}
	.factura{
		margin-top: 10px;
	}
	.tipo_despacho > div{
		float: left;
		width: 115px;
		margin-bottom: 15px;
	}
	.tipo_despacho > div input{
		margin: 1px;
	}

	.add_prd{
		float: left;
		width: 100%;
	}
	.add_prd label{
		float: left;
		width: 100%;
		margin-bottom: 5px;
	}
	.totales{
		float: right;
	}
	.totales td{
		border: 1px solid #d7d7d7;	
		padding: 10px;
		width: 100px;
	}
	.contRadioDireccion input{
		margin-bottom: 5px;
		box-sizing: border-box;
		float: left;
		width: 50px;
		margin-right: 10px;
	}
	.btn{
		float: left;
	    margin: 5px;
	    font-size: 12px;
	    padding: 10px 20px;
	    border-radius: 3px;
	    background: #fff;
	    cursor: pointer;
	}
	.btn.comprar{
		background: #28a745;
		border-color: #28a745;
		color: #fff;
	}
</style>

<h2>Modulo de ventas</h2>

<div class="datos">
	<h4>Datos cliente</h4>
	<div>
		<input type="text" id="nombre" name="nombre" placeholder="Nombre y Apellido" class="campo_texto">
		<input type="text" id="email" name="email" placeholder="Email" class="campo_texto">
		<input type="text" id="telefono" name="telefono" placeholder="Teléfono" class="campo_texto">
		<input type="text" id="run" name="run" placeholder="Rut" class="campo_texto">
	</div>
</div>

<div class="datos">
	<h4>Envio / Retiro en tienda</h4>

	<div class="tipo_despacho">
		<div>
			<label for="envio">Envio</label>
			<input type="radio" name="tipo_despacho" id="envio" value="envio" checked/>
		</div>
		<div>
			<label for="retiro">Retiro en tienda</label>
			<input type="radio" name="tipo_despacho" id="retiro" value="retiro" />
		</div>
	</div>


	<form name="formRetiro" class="retiro"  style="display: none;">
		<?php $sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, s.latitud, s.longitud, c.nombre, r.nombre, s.link_mapa","sucursales s, comunas c, ciudades ciu, regiones r","s.publicado = 1 and s.retiro_en_tienda=1 and s.comuna_id = c.id and c.ciudad_id = ciu.id and ciu.region_id = r.id","s.posicion asc");
	   for($i=0; $i<sizeof($sucursales); $i++) {
	   ?> 
	        <div class="cont100 filaSucursalCarro">
	        	<div class="contRadioDireccion">
	            	<input type="radio" name="retiro" id="sucursal<?= $sucursales[$i][0]; ?>" value="<?= $sucursales[$i][0]; ?>" <?php if($i == 0){echo 'checked="checked"';}?> />
	            </div>
	             <div class="contNombreSucursal">
	             	<span class="nombreSucursal"><?= $sucursales[$i][1]; ?></span>
	                <span><?= $sucursales[$i][2]; ?>, <?= $sucursales[$i][6]; ?>, <?= $sucursales[$i][7]; ?></span>
	             </div>
	        </div>
        <?php } ?>
	</form>


	<div class="envio">
		<?php $regiones = consulta_bd("id, nombre","regiones","","id desc");?>
        <select name="region" id="region" class="select">
            <option value="0">Seleccione region</option>
            <?php for($i=0; $i<sizeof($regiones); $i++){ ?>
            	<option value="<?php echo $regiones[$i][0]; ?>"><?php echo $regiones[$i][1]; ?></option>
            <?php } ?>
        </select>
        
        <select name="comuna" id="comuna" class="select">
            <option value="0">Seleccione Comuna</option>
            <?php for($i=0; $i<sizeof($comunas); $i++){ ?>
            	<option value="<?php echo $comunas[$i][0]; ?>"><?php echo $comunas[$i][1]; ?></option>
            <?php } ?>
        </select>
        
        <input type="text" name="direccion" id="direccion" value="" placeholder="Dirección" class="campo_texto" />
	</div>
</div>


<div class="datos">
	<h4>Tipo Compra</h4>
	<div class="tipo_compra">
		<div>
			<label for="boleta">Boleta</label>
			<input type="radio" name="tipo_compra" id="boleta" value="boleta" checked/>
		</div>
		<div>
			<label for="factura">Factura</label>
			<input type="radio" name="tipo_compra" id="factura" value="factura" />
		</div>
	</div>
	<div class="factura" style="display: none;">
		<input type="text" name="rut" placeholder="Rut" id="rut" class="campo_texto">
		<input type="text" name="giro" placeholder="Giro" id="giro" class="campo_texto">
		<input type="text" name="razon_social" placeholder="Razon social" id="razon_social" class="campo_texto">
		<input type="text" name="direccion" placeholder="Direccion" id="direccion_factura" class="campo_texto">
	</div>
</div>


<div class="datos">
	<h4>Carro</h4>
	<div class="add_prd">
		<label>Agregar producto</label>
		<input type="text" name="sku" placeholder="SKU" id="SKU" class="campo_texto">
		<button class="btn-add"><i class="fas fa-plus"></i> Agregar</button>
	</div>
	
	<div class="table-prod">
		<?php require('partials/_cart-manual.php'); ?>
	</div>
		
	<div class="btns">
		<button class="btn venta" rel="G">Guardar</button>
		<button class="btn comprar venta" rel="V">Generar comprar</button>
	</div>

</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#factura').on('change', function(){
			if($('#factura').is(':checked')){ $('.factura').slideDown(); }
			else{ $('.factura').slideUp(); }
		});
		$('#boleta').on('change', function(){
			if($('#factura').is(':checked')){ $('.factura').slideDown(); }
			else{ $('.factura').slideUp(); }
		});

		$('#retiro').on('change', function(){
			if($('#retiro').is(':checked')){
				$('.envio').slideUp(function(){
					$('.retiro').slideDown();
					despacho 	= false;
					desp_id 	= 0;
					$('.table-prod').load('app/partials/_cart-manual.php?d='+despacho+'&i='+desp_id);
				});
			}
		});
		$('#envio').on('change', function(){
			if($('#envio').is(':checked')){
				$('.retiro').slideUp(function(){
					$('.envio').slideDown();
					despacho 	= true;
					desp_id 	= $('#comuna').val();
					$('.table-prod').load('app/partials/_cart-manual.php?d='+despacho+'&i='+desp_id);
				});
			}
		});

		$('.btn-add').on('click', function(e){
			e.preventDefault();
			var sku 		= $('#SKU').val();
			var cantidad 	= 1;

			if($('#retiro').is(':checked')){
				despacho 	= false;
				desp_id 	= 0;
			}else{
				despacho 	= true;
				desp_id 	= $('#comuna').val();
			}

			if(sku){
				$.ajax({
					type: 'GET',
					url: 'app/partials/ajax_cart_manual.php',
					data: {id:0, sku:sku, action:"addSKU", qty:cantidad},
					cache: false,
					success:function(resp) {
						console.log(resp);
						$('.table-prod').load('app/partials/_cart-manual.php?d='+despacho+'&i='+desp_id);
						alertify.success("Producto Agregado");
					}
				});
			}else{
				alertify.error("Ingresa un SKU");
			}
		});

		$('#region').on('change', function(){
	        var $this   = $(this);
	        var id      = $this.val();
	        $.ajax({
	            type    : 'POST',
	            url     : 'app/partials/ajax_comunas.php',
	            data    : { value:id, action:'com' },
	            cache   : false,
	            success : function(comunas){
	                var $select = $('#comuna');
	                $select.empty();
	                $select.append($("<option/>").attr("value", 0).text('Seleccione'));
	                $(comunas).each(function (index, o) { 
	                    var $option = $("<option/>").attr("value", o[0]).text(o[1]);
	                    $select.append($option);
	                });
	            }
	        });
	    });

		$('#comuna').on('change', function(){
			despacho 	= true;
			desp_id 	= $('#comuna').val();
			$('.table-prod').load('app/partials/_cart-manual.php?d='+despacho+'&i='+desp_id);
		});

		$('.venta').on('click', function(){
			var venta = $(this).attr('rel');

			//CLIENTE
			var cliente = {
				nombre 		: $('#nombre').val(),
				email 		: $('#email').val(),
				telefono 	: $('#telefono').val(),
				rut 		: $('#run').val()
			};

			if(!cliente.nombre){ alertify.error("Ingrese Nombre cliente"); return false; }
			if(!cliente.email){ alertify.error("Ingrese Email cliente"); return false; }
			if(!cliente.telefono){ alertify.error("Ingrese Teléfono cliente"); return false; }
			if(!cliente.rut){ alertify.error("Ingrese Rut cliente"); return false; }

			//ENVIO - RETIRO
			if($('#envio').is(':checked')){
				tipo_envio 	= 'envio';
				region 		= $('#region').val();
				comuna 		= $('#comuna').val();
				direccion 	= $('#direccion').val();
				sucursal 	= 0;

				if(!region){ alertify.error("Seleccione Región"); return false; }
				if(!comuna){ alertify.error("Seleccione Comuna"); return false; }
				if(!direccion){ alertify.error("Ingrese Dirección"); return false; }

			}else{
				tipo_envio = 'retiro';
				region 		= 0;
				comuna 		= 0;
				direccion 	= "";
				sucursal 	= document.forms['formRetiro'].retiro.value;
			}

			var envio = {
				tipo 		: tipo_envio,
				region 		: region,
				comuna 		: comuna,
				direccion 	: direccion,
				sucursal 	: sucursal
			};

			//TIPO COMPRA
			if($('#boleta').is(':checked')){
				tipo 			= "boleta";
				rut 			= "";
				giro 			= "";
				razon_social 	= "";
				direccion_fac 	= "";
			}else{
				tipo 			= "factura";
				rut 			= $('#rut').val();
				giro 			= $('#giro').val();
				razon_social 	= $('#razon_social').val();
				direccion_fac 	= $('#direccion_factura').val();

				if(!rut){ alertify.error("Ingrese Rut"); return false; }
				if(!giro){ alertify.error("Ingrese Giro"); return false; }
				if(!razon_social){ alertify.error("Ingrese Razón Social"); return false; }
				if(!direccion_fac){ alertify.error("Ingrese Dirección Factura"); return false; }
			}
			
			var tipo_compra = {
				rut 		:rut,
				giro 		:giro,
				razon_social:razon_social,
				direccion 	: direccion_fac
			};

			if(venta == 'G'){
				var msg = "Compra Guardada";
			}else{
				var msg = "Compra Relizada";
			}

			$.ajax({
				type: 'POST',
				url: 'app/partials/ajax_proceso_compra.php',
				data: {cliente:cliente, envio:envio, tipo_compra:tipo_compra, venta:venta},
				cache: false,
				success:function(resp) {
					console.log(resp);
					if(resp.trim() == 'SUCCESS'){
						alertify.alert(msg,function(){
							location.reload();
						});
					}else{
						alertify.alert(resp);
					}
				}
			});


		});

	});
</script>

