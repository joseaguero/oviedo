<?php 

include('../conf.php');
include('../includes/tienda/cart/inc/functions.inc.php');

$id = mysqli_real_escape_string($conexion, $_GET['id']);

// Lineas a las que pertenece el producto
$lineas_producto = consulta_bd('lp.linea_id, l.nombre', 'lineas_productos lp join lineas l on l.id = lp.linea_id', "lp.producto_id = $id group by lp.linea_id", '');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Asignar Filtro</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="../js/alertify.min.js"></script>

	<link rel="stylesheet" href="../css/alertify.core.css" />
	<link rel="stylesheet" href="../css/alertify.default.css" id="toggleCSS" />

	<style type="text/css">
		body{
			padding: 45px 0 0 0;
			margin: 0;
			font-family: 'Roboto', sans-serif;
		}

		.botonera_filtros{
			width: 97.5%;
			background: #eee;
			padding: 10px;
			position: fixed;
			top: 0;
			left: 0;
		}

		.botonera_filtros h2{
			margin: 0;
			float: left;
		}

		.content_button{
			float: right;
			margin-right: 60px;
			margin-top: 0px;
		}

		.content_button .btnFiltro{
			background: #1682ca;
			display: block;
			padding: 5px 10px;
			color: #fff;
			text-decoration: none;
			font-size: 14px;
		}

		.center_content{
			width: 90%;
			margin: auto;
		}

		.center_content h3{
			border: 1px solid #e1e1e1;
			border-radius: 5px 5px 0 0;
			font-size: 14px;
			padding: 10px;
		}

		.box_filtros{
			padding: 10px;
			border: 1px solid #e1e1e1;
			border-top: 0;
			border-radius: 0 0 5px 5px;
		}

		.head_filtro{
			margin-top: 15px;
			margin-bottom: 5px;
		}

		.box_filtros .head_filtro:first-child{
			margin-top: 0;
		}

		.box_filtros .row{
			color: #5d5d5d;
			font-size: 14px;
			margin-bottom: 5px;
		}
	</style>
</head>
<body>

<div class="botonera_filtros">
	<h2>Asignar filtros:</h2>

	<div class="content_button">
		<a href="javascript:void(0)" class="btnFiltro" data-id="<?= $id ?>">Guardar</a>
	</div>
</div>

<div class="center_content">
	<?php 
	if (is_array($lineas_producto)) {
		foreach ($lineas_producto as $lp) {

			echo '<h3 style="margin-bottom:0;">Filtros para linea '.$lp[1].':</h3>';
		
			$filtro_principal = consulta_bd('id, nombre', 'filtros', "linea_id = $lp[0]");

			if (is_array($filtro_principal)) {
				echo '<div class="box_filtros">';
				foreach ($filtro_principal as $filtro) {
					echo '<div class="head_filtro">--- ' . $filtro[1] . '</div>';
					$variantes = consulta_bd('id, nombre', 'filtros_variantes', "filtro_id = {$filtro[0]}", 'posicion asc');
					foreach ($variantes as $v) {
						$checked = variante_producto($id, $v[0]);
						$checked_r = ($checked) ? 'checked' : '';
						echo '<div class="row">';
						echo '<input type="checkbox" value="'.$v[0].'" id="v'.$v[0].'" class="chk_filtros" '.$checked_r.'>';
						echo '<label for="v'.$v[0].'">' . $v[1] . '</label>';
						echo '</div>';
					}
				}
				echo '</div>';
			}

		}
	}else{
		echo 'Debe asignar el producto a una linea para agregar sus filtros.';
	}

	?>
</div>

<script type="text/javascript">
	
	$(function(){
		$('.btnFiltro').on('click', function(){
			var chk = $('.chk_filtros:checked'),
				id = $(this).attr('data-id'),
				ides = '';

			chk.each(function(i, x){
				if (ides == '') {
					ides = $(this).val();
				}else{
					ides += ',' + $(this).val();
				}
			});

			$.ajax({
				url: '../action/save_filtros.php',
				type: 'post',
				data: {chk: ides, id: id},
			})
			.done(function(res) {
				// console.log(res);
				alertify.success('Filtros agregados correctamente');
			})
			.fail(function(res) {
				console.log(res);
			});
		})
	})

</script>

</body>
</html>