<?php 

// $lista = consulta_bd('pp.id, pp.posicion, p.nombre', 'posicion_productos pp JOIN productos p ON p.id = pp.producto_id', '', 'posicion');

$linea = (isset($_GET['line'])) ? $_GET['line'] : NULL;
$categoria = (isset($_GET['category'])) ? $_GET['category'] : NULL;
$subcat = (isset($_GET['subcategory'])) ? $_GET['subcategory'] : NULL;

if ($linea != NULL OR $categoria != NULL OR $subcat != NULL) {
	if ($linea != NULL) {
		$where = 'pp.linea_id = '.$linea;
		$change = 'linea-'.$linea;
	}elseif($categoria != NULL){
		$where = 'pp.categoria_id = '.$categoria;
		$change = 'categoria-'.$categoria;
	}elseif($subcat != NULL){
		$where = 'pp.subcategoria_id = '.$subcat;
		$change = 'subcategoria-'.$subcat;
	}

	$lista = consulta_bd('p.id, p.nombre, p.thumbs, IFNULL(pp.posicion, 999999) posicion, pd.precio, pd.descuento, l.nombre, pd.stock, pd.id, p.descripcion, m.nombre, pd.precio_cantidad,
	min(IF(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, pd.precio_cyber,
	(case when pd.descuento > 0 then pd.descuento else pd.precio end) as precio_calculado, 
	l.id', "productos p LEFT JOIN lineas_productos lp ON p.id = lp.producto_id 
	JOIN productos_detalles pd ON pd.producto_id = p.id 
	LEFT JOIN lineas l ON l.id = lp.linea_id 
	LEFT JOIN categorias c ON c.id = lp.categoria_id
	LEFT JOIN posicion_productos pp ON p.id = pp.producto_id 
	JOIN marcas m ON m.id = p.marca_id
	", "p.publicado = 1 AND $where GROUP BY p.id", 'posicion asc');

	// var_dump("SELECT p.id, p.nombre, p.thumbs, IFNULL(pp.posicion, 9999) posicion FROM productos p LEFT JOIN posicion_productos pp ON p.id = pp.producto_id LEFT JOIN lineas_productos lp ON p.id = lp.producto_id JOIN marcas m ON m.id = p.marca_id WHERE p.publicado = 1 AND $where GROUP BY p.id ORDER BY posicion asc");

	$cantLista = count($lista);
}

$lineas = consulta_bd('id, nombre', 'lineas', 'publicado = 1', 'posicion');
// var_dump($lista);

?>
<div id="botonera">
	<div id="titulo"><h3>Ordenar Productos </h3></div>
	<div id="botones">
		<a href="javascript:void(0)" id="guardar-orden" data-where="<?=$change?>">Guardar</a>
	</div>
</div>
<div class="menu-orden">
	<ul>
		<?php foreach ($lineas as $linea) {
			$categorias = consulta_bd('id, nombre', 'categorias', "linea_id = $linea[0]", 'posicion');
			if (is_array($categorias)) {
				echo 
				'<li><a href="line='.$linea[0].'">'.$linea[1].'</a>
					<ul class="submenu">';
					foreach ($categorias as $categoria) {
						$subcategorias = consulta_bd('id, nombre', 'subcategorias', "categoria_id = $categoria[0]", 'posicion');
						if (is_array($subcategorias)) {
							echo 
							'<li><a href="category='.$categoria[0].'">'.$categoria[1].'</a>
							<ul class="subsubmenu">';
							foreach ($subcategorias as $subcategoria) {
								echo '<li><a href="subcategory='.$subcategoria[0].'">'.$subcategoria[1].'</a></li>';
							}
							echo '</ul>
							</li>';
						}else{
							echo '<li><a href="category='.$categoria[0].'">'.$categoria[1].'</a></li>';
						}
					}					
				echo '</ul>
				</li>';
			}else{
				echo '<li><a href="line='.$linea[0].'">'.$linea[1].'</a></li>';
			}
			
		} ?>
	</ul>
	<div class="btn-list"></div>
</div>

<?php 
if($cantLista < 1){echo '<h2>Debe asignar productos a las categorias para poder ordenarlos</h2>'; } else { ?>
<ol class="cont-order">
	<?php 
	foreach ($lista as $producto) { 
		$imagen = ($producto[2] != NULL OR $producto[2] != '') ? "../imagenes/productos/{$producto[2]}" : '../img/sin-foto.jpg';
	?>
		<li class="row" data-id="<?=$producto[0]?>">
			<div class="img"><img src="<?=$imagen?>" alt=""></div>
			<div class="nombreOrden"><?=$producto[1]?></div>
		</li>
	<?php } ?>
</ol>
<?php } ?>
<div style="clear:both"></div>

<!-- JAVASCRIPT -->
<script>
	$( function() {
		$( ".cont-order" ).sortable();
		$( ".cont-order" ).disableSelection();
	} );

	$('#guardar-orden').on('click', function(e){
		e.preventDefault();
		var donde_cambia = $(this).attr('data-where');
		var arreglo = [];
		$('.cont-order > .row').each(function(i, e){
			var id_prod = $(this).attr('data-id');
			var pos = i + 1;	

			arreglo.push( [id_prod, pos] );
		})
		$.ajax({
			url: 'action/cambiar-orden.php',
			type: 'POST',
			dataType: 'json',
			data: {donde_cambia: donde_cambia, productos: JSON.stringify(arreglo)},
			beforeSend: function(){
				$('#guardar-orden').html('Guardando...');
			},
			success: function(res){
				console.log(res);
				$('#guardar-orden').html('Guardar');
				alertify.success('Orden guardado correctamente');
			}
		}).fail(function(res){
			console.log(res);
		})
	})
	$('.menu-orden ul li > a').on('click', function(e){
		e.preventDefault();
		var go = $(this).attr('href');
		location.href = 'index.php?op=order'+'&'+go;
	});

	$('.btn-list').on('click', function(){
		$(this).toggleClass('btn-list-columns');
		$('.cont-order').toggleClass('r-order');
	})
</script>

