<?php
	include('../conf.php');
	$tienda = "Ejemplo moldeable";//$_GET['tienda'];
	$service;
	$servicio = consulta_bd("valor","opciones","nombre = 'servicio'","");
	
	
	$totalVentas = ventasTipoPago($servicio[0][0]);
	$totalPedidos = pedidosTipoPago($servicio[0][0]);
	$totalProductos = productosTipoPago($servicio[0][0]);

	ksort($totalVentas['total_meses']);
	ksort($totalPedidos['total_meses']);
	ksort($totalProductos['total_meses']);

	$tiendaName = $tienda;

	/*echo '<pre>';
	print_r($totalVentas);
	echo '</pre>';*/

?>

	<script type="text/javascript">
		// Create the chart
	$(function () {
	    var ventas = new Highcharts.Chart({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'container'
    },
    title: {
        text: 'Valor Venta tipo pago'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Monto',
        data: [
        	<?php 
        		echo "['Ventas Debito', {$totalVentas['total'][0]}],
			        ['Ventas Normales', {$totalVentas['total'][1]}],
			        ['Ventas Cuotas', {$totalVentas['total'][2]}],
			        ['3 cuotas sin interés', {$totalVentas['total'][3]}],
		            ['2 cuotas sin interés', {$totalVentas['total'][4]}],
		            ['X cuotas sin interés', {$totalVentas['total'][5]}]";
        	?>
        ],
        tooltip: {
        		valuePrefix: '$ '
        }
    }]
});
});

$(function () {
	  var pedidos = new Highcharts.Chart({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'torta-pedidos'
    },
    title: {
        text: 'Cantidad de Pedidos Según tipo pago'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Cantidad',
        data: [
            <?php 
        		echo "['Ventas Debito', {$totalPedidos['total'][0]}],
			            ['Ventas Normales', {$totalPedidos['total'][1]}],
			            ['Ventas Cuotas', {$totalPedidos['total'][2]}],
			            ['3 cuotas sin interés', {$totalPedidos['total'][3]}],
			            ['2 cuotas sin interés', {$totalPedidos['total'][4]}],
			            ['X cuotas sin interés', {$totalPedidos['total'][5]}]";
         	?>
        ]
    }]
});
});

$(function () {
	 var ventas = new Highcharts.Chart({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'torta-productos'
    },
    title: {
        text: 'Cantidad de Productos Según tipo pago'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Cantidad',
        data: [
            <?php 
        		echo "['Ventas Debito', {$totalProductos['total'][0]}],
			            ['Ventas Normales', {$totalProductos['total'][1]}],
			            ['Ventas Cuotas', {$totalProductos['total'][2]}],
			            ['3 cuotas sin interés', {$totalProductos['total'][3]}],
			            ['2 cuotas sin interés', {$totalProductos['total'][4]}],
			            ['X cuotas sin interés', {$totalProductos['total'][5]}]";
         	?>
        ]
    }]
});
});
</script>


	<h2 class="tituloReporte">Tipo de pago</h2>
	<div class="contenedor">
        
		<!-- GRAFICO TORTA DE VENTAS -->
		<div class="totales-menu">
			<h2>Ventas Tipo Pago</h2>
			<h3>POR AÑOS</h3>
			<ul>
				<?php foreach ($totalVentas['total_meses'] as $key => $value): ?>
				<li class="nav-totales"><?= $key ?> <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child">
						<li>Debito <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[0],0,",",".");
						?></p></li>
						<li>Normales <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[1],0,",",".");
						?></p></li>
						<li>Cuotas <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[2],0,",",".");
						?></p></li>
						<li>3 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[3],0,",",".");
						?></p></li>
						<li>2 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[4],0,",",".");
						?></p></li>
						<li>X C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[5],0,",",".");
						?></p></li>
					</ul>
				</li>
				<?php endforeach ?>
			</ul>
		</div>
        <div class="div-g">
            <div id="container"></div>
        </div>

        <div class="separator"></div>
		
		<!-- GRAFICO TORTA DE PEDIDOS -->
        <div class="totales-menu">
			<h2>Pedidos Tipo Pago</h2>
			<h3>POR AÑOS</h3>
			<ul>
				<?php foreach ($totalPedidos['total_meses'] as $key => $value): ?>
				<li class="nav-totales"><?= $key ?> <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child">
						<li>Debito <p><?php echo number_format($value[0],0,",",".");
						?></p></li>
						<li>Normales <p><?php echo number_format($value[1],0,",",".");
						?></p></li>
						<li>Cuotas <p><?php echo number_format($value[2],0,",",".");
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo number_format($value[3],0,",",".");
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo number_format($value[4],0,",",".");
						?></p></li>
						<li>X C/Sin Interés <p><?php echo number_format($value[5],0,",",".");
						?></p></li>
					</ul>
				</li>
				<?php endforeach ?>
			</ul>
		</div>
        <div class="div-g">
            <div id="torta-pedidos"></div>
        </div>

        <div class="separator"></div>

        <!-- GRAFICO TORTA DE PRODUCTOS -->
        <div class="totales-menu">
			<h2>Productos Tipo Pago</h2>
			<h3>POR AÑOS</h3>
			<ul>
				<?php foreach ($totalProductos['total_meses'] as $key => $value): ?>
				<li class="nav-totales"><?= $key ?> <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child">
						<li>Debito <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[0],0,",",".");
						?></p></li>
						<li>Normales <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[1],0,",",".");
						?></p></li>
						<li>Cuotas <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[2],0,",",".");
						?></p></li>
						<li>3 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[3],0,",",".");
						?></p></li>
						<li>2 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[4],0,",",".");
						?></p></li>
						<li>X C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo number_format($value[5],0,",",".");
						?></p></li>
					</ul>
				</li>
				<?php endforeach ?>
			</ul>
		</div>
        <div class="div-g">
            <div id="torta-productos"></div>
        </div>
        <div style="clear:both"></div>
</div>