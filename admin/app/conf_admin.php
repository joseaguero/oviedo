<h2 class="tituloReporte">Configuraciones Cliente</h2>
  
<div class="contenedor" style="margin-top:20px;">
	<form method="post" action="actualizarOpciones.php" style="float:left; width:100%;">
    	
        <div class="cont100 separador">
        	<h2 class="titulosConf">Configuracion General</h2>
        	<div class="cont50">
                <div class="cont100 filaCampo">
                    <label class="label">Dominio</label>
                    <input type="text" name="dominio" value="<?= opciones("dominio"); ?>" class="campo_texto" />
                </div>
            </div>
            
            <div class="cont50">
                <div class="cont100 filaCampo">
                    <label class="label">Nombre Cliente</label>
                    <input type="text" name="nombre_cliente" value="<?= opciones("nombre_cliente"); ?>" class="campo_texto" />
                </div>
            </div>
        </div>
        
        
        
        <div class="cont100 separador">
        	<h2 class="titulosConf">Configuracion emails</h2>
        	<div class="cont50">
                <div class="cont100 filaCampo">
                    <label for="logo_mail" class="label">Logo Email</label>
                    <input type="text" name="logo_mail" value="<?= opciones("logo_mail"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo bordeCorreo">
                    <label for="correo_admin1" class="label">Correo notificación compras</label>
                    <input type="text" name="correo_admin1" value="<?= opciones("correo_admin1"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo bordeCorreo">
                    <label for="correo_admin2" class="label">Correo notificación compras</label>
                    <input type="text" name="correo_admin2" value="<?= opciones("correo_admin2"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo">
                    <label for="correo_admin3" class="label">Correo notificación compras</label>
                    <input type="text" name="correo_admin3" value="<?= opciones("correo_admin3"); ?>" class="campo_texto" />
                </div>
            </div>
            
            <div class="cont50">
                <div class="cont100 filaCampo">
                    <label for="color_logo" class="label">Color Logo</label>
                    <input type="text" name="color_logo" value="<?= opciones("color_logo"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo bordeCorreo">
                    <label for="nombre_correo_admin1" class="label">Nombre correo notificación compras</label>
                    <input type="text" name="nombre_correo_admin1" value="<?= opciones("nombre_correo_admin1"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo bordeCorreo">
                    <label for="nombre_correo_admin2" class="label">Nombre correo notificación compras</label>
                    <input type="text" name="nombre_correo_admin2" value="<?= opciones("nombre_correo_admin2"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo">
                    <label for="nombre_correo_admin3" class="label">Nombre correo notificación compras</label>
                    <input type="text" name="nombre_correo_admin3" value="<?= opciones("nombre_correo_admin3"); ?>" class="campo_texto" />
                </div>
            </div>
        </div>
        
        
        
        <div class="cont100 separador">
        	<h2 class="titulosConf">SEO General</h2>
        	<div class="cont50">
                <div class="cont100 filaCampo">
                    <label for="seo_titulo" class="label">Título</label>
                    <input type="text" name="seo_titulo" value="<?= opciones("seo_titulo"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo contAdicional">
                    <label for="seo_keywords" class="label">Keywords</label>
                    <textarea class="textarea" name="seo_keywords"><?= opciones("seo_keywords"); ?></textarea>
                </div>
            </div>
            
            <div class="cont50">
                <div class="cont100 filaCampo">
                    <label for="seo_url_imagen" class="label">Imagen</label>
                    <input type="text" name="seo_url_imagen" value="<?= opciones("seo_url_imagen"); ?>" class="campo_texto" />
                </div>
                <div class="cont100 filaCampo contAdicional">
                    <label for="seo_descripcion" class="label">Descripción</label>
                    <textarea class="textarea" name="seo_descripcion"><?= opciones("seo_descripcion"); ?></textarea>
                </div>
            </div>
        </div>
        
        
        <div class="cont100 separador">
        	<h2 class="titulosConf">Chat</h2>
        	<div class="cont50">
                <div class="cont100 filaCampo contAdicional">
                    <label for="chat" class="label">Chat</label>
                    <textarea class="textarea" name="chat"><?= opciones("chat"); ?></textarea>
                </div>
            </div>
            
           
        </div>

        <div class="cont100 separador">
            <h2 class="titulosConf">Productos</h2>
            <?php if(get_option('ultimas_unidades')){ ?>
                <div class="cont50">
                    <div class="cont100 filaCampo contAdicional">
                        <label for="ultimas_uni" class="label">Ultimas unidades</label>
                        <input type="text" id="ultimas_uni" class="campo_texto" name="cant_ultimas_unidades" value="<?= get_option('cant_ultimas_unidades'); ?>">
                    </div>
                </div>
            <?php } ?>
        </div>
        
        <div class="cont100 filaCampo">
        	<input type="submit" name="guardar" value="Guardar" />
        </div>
    </form>
</div>
<div style="clear:both"></div>