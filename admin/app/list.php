<?php
/********************************************************\
|  Moldeable CMS - Listado de entradas.		             |
|  Fecha Modificación: 4/04/2014		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

//Obtengo las opciones de la tabla
$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
require_once 'paginador/paginator.class.php';

$i = 0;
while($i <= (sizeof($opciones)-1))
{	
	$nombre = $opciones[$i][0];
	$opcion[$nombre] = $opciones[$i][1];
	$i++;
}
if(sizeof($opciones)>0)
extract($opcion);

if (isset($_GET[id]) and isset($_GET[r]))
{
    $submenu_table = consulta_bd_por_id("submenu_table","tablas","",$val_op);
    $submenu_table = $submenu_table['submenu_table'];
    $id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]):0;
    if(substr_count($submenu_table, ",")!=0){
        $tablas_menu = explode(",", $submenu_table);
        $tablas_menu_reverse_1 = array_reverse($tablas_menu);
        $abc = array('a','b','c','d','e');
        array_unshift($tablas_menu, $tabla);
        array_unshift($tablas_menu_reverse_1, $tabla);
        $tablas_menu_reverse = array_reverse($tablas_menu);
        for($c=0;$c<count($tablas_menu);$c++)
        {
           
           $tablas_consulta[] = $tablas_menu_reverse_1[$c]." ".$abc[$c];
           
           if ($c == (count($tablas_menu)-1))
           {
               $filtro .= $abc[$c].".id = ".$id;
           }
           else
           {
               $tabla_singular = singular(trim($tablas_menu_reverse[$c]));
               $filtro .= $abc[$c].".".$tabla_singular."_id = ".$abc[$c+1].".id";
               $filtro .= " and ";
           }
        }    
        $tablas_consulta = implode(',',$tablas_consulta);
        //c.id = 2 and c.id = b.edifio_id and b.id = a.tipo_id
    }
    else
    {
	//$submenu_table = (isset($_GET[r])) ? mysql_real_escape_string($_GET[r]):0;
	$filtro = singular($submenu_table)."_id = $id";
    }
    $tipo = consulta_bd_por_id("nombre",$_GET[r],"",$id);
    $tipo = $tipo['nombre'];

}
else
{
	$filtro_q = consulta_bd("filtro, save_in","tablas","id = $val_op","");
	$save_in = $filtro_q[0][1];
	$filtro_a = explode(" ",$filtro_q[0][0]);
	foreach ($filtro_a as $k => $f)
	{
		if ($f[0] == '$')
		{
			$resto_palabra = substr($f, 1);
			$filtro_a[$k] =  $$resto_palabra;
		}
	}
	$filtro = implode(" ", $filtro_a);
}

if ($sm_table[0][0] != '')
{
    $rid = "&rid=$id";
}


if($tabla == 'productos'){
	$linea_id_get = (isset($_GET['line'])) ? $_GET['line'] : NULL;
	$categoria_id_get = (isset($_GET['category'])) ? $_GET['category'] : NULL;
	$subcat_id_get = (isset($_GET['subcategory'])) ? $_GET['subcategory'] : NULL;

	if ($linea_id_get != NULL OR $categoria_id_get != NULL OR $subcat != NULL) {
		if ($linea_id_get != NULL) {
			$where_productos = 'cp.linea_id = '.$linea_id_get;
			$change = 'linea-'.$linea_id_get;
		}elseif($categoria_id_get != NULL){
			$where_productos = 'cp.categoria_id = '.$categoria_id_get;
			$change = 'categoria-'.$categoria_id_get;
		}elseif($subcat_id_get != NULL){
			$where_productos = 'cp.subcategoria_id = '.$subcat_id_get;
			$change = 'subcategoria-'.$subcat_id_get;
		}
	}else{
		$where_productos = 'p.id > 0';
	}

	$lineas = consulta_bd('id, nombre', 'lineas', 'publicado = 1', 'posicion');
	// var_dump($lista);
}


?>


    
  


<div id="botonera">
	<div id="titulo"><h3>Listado de <?php echo get_table_name($val_op); echo ($tipo) ? ' - '.$tipo : ''; ?></h3></div>
	<div id="botones">
		<?php if ($val_op == 270 AND !$permiso_lectura): ?>
			<a href="javascript:;" id="btn_transferencias">Transferencias</a>
			<div class="bg_mdl">
				<div class="content_mdl">
					<h3>Transferencias</h3>

					<div class="form-group-mdl">
						<label>Orden de compra:</label>
						<input type="text" name="oc" class="ocMdl">
					</div>

					<div class="form-group-mdl">
						<label>Acción:</label>
						<select class="selectMdl">
							<option value="0">Seleccionar acción</option>
							<option value="1">Validar pedido</option>
							<option value="2">Anular pedido</option>
						</select>
					</div>

					<div class="form-group-mdl">
						<a href="javascript:;" class="saveTransferencias">Enviar</a>
					</div>
				</div>
			</div>
		<?php endif ?>
		<?php
			$update = consulta_bd("valor","opciones_tablas op, (SELECT id FROM tablas WHERE nombre = '$tabla') t","nombre = 'obligatorios' AND tabla_id = t.id","");
			$obligatorios = $update[0][0];
			if ($obligatorios == '')
			{
				$next = 'c';
			}
			else
			{
				$next = ($solo_update) ? 'c': 'b'.$rid;
			}
			
			if ($create != '0' OR !isset($create))
			{
				if ($save_in)
				{
					$filtro_url = str_replace(" ", '', $filtro);
					$next_vars = ($filtro) ? $save_in.$next."&".$filtro_url."&last=".$val_op : $save_in.$next."&last=".$val_op;
				}
				else
				{
					$next_vars = $val_op.$next;
				}
			
			?>
				<button type="button" class="boton3 agregar_nuevo" style="color:red" onclick="javascript:volver('index.php?op=<?php echo "$next_vars";?>')">Nueva entrada</button>
			<?php
			}
		?>
	</div>
	<?php 
	if (strtolower(get_table_name($val_op)) == 'productos') { ?>
		<div class="meli">
			<a href="javascript:void(0)" class="logo-ml">MercadoLibre</a>
			<div class="dropdown-ml">
				<a href="javascript:void(0)" data="add">Agregar todos</a>
				<a href="javascript:void(0)" data="stock">Actualizar stock</a>
				<a href="javascript:void(0)" data="paused">Pausar todos</a>
				<a href="javascript:void(0)" data="active">Activar todos</a>
			</div>
		</div>
		<div class="loading-meli">
			<img src="pics/loading.gif" alt="Loading..." />
		</div>
	<?php } ?>
</div>


<?php if($tabla == 'productos'){ ?>
	<div class="menu-orden">
		<ul>
			<?php foreach ($lineas as $linea) {
				$categorias = consulta_bd('id, nombre', 'categorias', "linea_id = $linea[0]", 'posicion');
				if (is_array($categorias)) {
					echo 
					'<li><a href="index.php?op='.$val_op.'a&line='.$linea[0].'">'.$linea[1].'</a>
						<ul class="submenu">';
						foreach ($categorias as $categoria) {
							$subcategorias = consulta_bd('id, nombre', 'subcategorias', "categoria_id = $categoria[0]", 'posicion');
							if (is_array($subcategorias)) {
								echo 
								'<li><a href="index.php?op='.$val_op.'a&category='.$categoria[0].'">'.$categoria[1].'</a>
								<ul class="subsubmenu">';
								foreach ($subcategorias as $subcategoria) {
									echo '<li><a href="index.php?op='.$val_op.'a&subcategory='.$subcategoria[0].'">'.$subcategoria[1].'</a></li>';
								}
								echo '</ul>
								</li>';
							}else{
								echo '<li><a href="index.php?op='.$val_op.'a&category='.$categoria[0].'">'.$categoria[1].'</a></li>';
							}
						}					
					echo '</ul>
					</li>';
				}else{
					echo '<li><a href="index.php?op='.$val_op.'a&line='.$linea[0].'">'.$linea[1].'</a></li>';
				}
				
			} ?>
		</ul>
		<div class="btn-list"></div>
	</div>
<?php } ?>



<br />
<div id="loader"></div>
<div id="tablewrapper" style="display:none">
		<!--<div id="tableheader">
	    	<div class="search">
	    		<em>Buscar por:</em><br />
	            <select id="columns" class="select" onchange="sorter.search('query')"></select>
	            <input type="text" id="query" onkeyup="sorter.search('query')" />
	        </div>
	        <span class="details">
				<div>Filas <span id="startrecord"></span>-<span id="endrecord"></span> de <span id="totalrecords"></span></div>
	    		<div><a href="javascript:sorter.reset()">Quitar Filtros</a></div>
	    	</span>
	    </div>-->
	    <div id="tableheader">
	    	<div class="search">
	    		<form action="index.php" method="GET">
		            <select name="columnas" class="select" id="select_columna">
                        <option value="all">Todos los campos</option>  
                        <?php 
                            $columnas = show_columns($tabla, $exclusiones_list); 
                            //var_dump($columnas);
                            for($h=0; $h<sizeof($columnas); $h++) {
                              if($_GET['columnas'] == $columnas[$h]){
                                 echo '<option value="'.$columnas[$h].'" selected="selected">'.$columnas[$h].'</option>';  
                              } else {
                                 echo '<option value="'.$columnas[$h].'">'.$columnas[$h].'</option>';  
                              }
                                 
                            }
                            
                        ?>
                        
                    </select>
                    <input type="text" id="query" name="buscar" value="<?php echo $_GET['buscar'];?>"/>
                    <input type="hidden" name="op" value="<?php echo $val_op; ?>a"/>
                    
		            <input type="submit" class="botonBuscar" value="Buscar" />
		            <input type="button" onClick="location.href='index.php?op=<?php echo $val_op;?>a';" class="botonBuscar" value="Ver Todos" />
	        	</form>
	        </div>
	    </div>
    <table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
        <thead>
            <tr>
            	<?php 
            		$columnas = show_columns($tabla, $exclusiones_list);

                    if(substr_count($submenu_table, ",")!=0){
                        foreach ($columnas as $c)
                        {
                          $columnas_sm[] = "a.$c";  
                        }
                        $lista_columnas = implode(",", $columnas_sm);
                    }
                    else
                    {
                        $lista_columnas = implode(",", $columnas);
                    }
            		
					//Columnas con Autocompletar
					$columna_autocompletar_items = array();
					$tabla_autocompletar_items = array();
					$id_col_autocompletar = array();
					if ($autocompletar != ''){
						$autocompletar_array = explode(';',$autocompletar);
						$j=0;
						foreach ($autocompletar_array as $autocompletar_item)
						{
							$datos_autocompletar = explode(',', $autocompletar_item);
							$tabla_autocompletar_items[$j] = trim($datos_autocompletar[0]);
							$columna_autocompletar_items[$j] = trim($datos_autocompletar[1]);
							$j++;
						}
					}   
					//Fin columnas con autocompletar 
            		
            		$i = 0;
            		while($i <= (sizeof($columnas)-1))
            		{	
            		
            			$related[$i] = column_is_related($columnas[$i]);
            		
            			$colum = strtoupper(get_real_name($columnas[$i]));
            			if ($i == 0)
            			{
            				echo '<th class="nosort"><h3>'.$colum.'</h3></th>';
            			}
            			else
            			{
            				if ($related[$i])
            				{
            					
            					$belong_to_table = check_if_item_has_belongs_to($columnas[$i]);
            					if ($belong_to_table)
            					{
            						//Consulto si hay otra relacion
            						$columna_bt = singular($belong_to_table)."_id";
            						$belongs_to_parent = check_if_item_has_belongs_to($columna_bt);
            						if ($belongs_to_parent)
            						{
	            						//Consulto si hay otra relacion
	            						$columna_btp = singular($belongs_to_parent)."_id";
	            						$belongs_to_gran_parent = check_if_item_has_belongs_to($columna_btp);
	            						if ($belongs_to_gran_parent)
	            						{
		            						echo '<th><h3>'.strtoupper(get_real_name($belongs_to_gran_parent)).'</h3></th>';	
	            						}
	            						echo '<th><h3>'.strtoupper(get_real_name($belongs_to_parent)).'</h3></th>';	
            						}
	            					echo '<th><h3>'.strtoupper(get_real_name($belong_to_table)).'</h3></th>';
            					}
            					echo '<th><h3>'.strtoupper(get_real_name($related[$i])).'</h3></th>';
            				}
            				else
            				{
            					echo '<th><h3>'.$colum.'</h3></th>';
            				}
            			}           			
            			
						if ($columnas[$i] == 'modificado_por') {
	            			$id_modificado_por = $i;
	            		}
	            		elseif ($columnas[$i] == 'creado_por') {
	            			$id_creado_por = $i;
	            		}
	            		
						if (in_array($columnas[$i], $columna_autocompletar_items))
						$id_col_autocompletar[] .= $i;
	            		
            			$i++;
            		}
            		
            		if($asociados)
            		{
        				echo '<th><h3>'.strtoupper($asociados).' ASOCIADOS</h3></th>';
    				}
    				
            	?>
            	<?php if (!isset($edit) or $edit == 1) { ?>
                <th class="nosort" width="30"><h3>&nbsp;</h3></th>
                <th class="nosort" width="30"><h3>&nbsp;</h3></th>
                <?php } ?>
            </tr>
            <!--<tr>
            	<td><?php print_r($columnas); ?></td>
            </tr>-->
        </thead>
        <tbody>
        	<?php if(isset($_GET['buscar'])){ ?>
	        	<?php
	        		$textoBuscado = $_GET['buscar'];

	        		if($filtro != ''){
	        			$filtro .= " and ";	
	        		}	
                    
                    //si existe la columna, busco solo en esa columna, de lo contrario
                    if(isset($_GET['columnas']) && $_GET['columnas'] != 'all'){
                        $filtro .= $_GET['columnas']." like '%".$textoBuscado."%'";	
                    } else {
                        for($t=0;$t<count($columnas);$t++){
                            $filtro .= $columnas[$t]." like '%".$textoBuscado."%'";	
                            if($columnas[$t+1] != ''){
                                $filtro .= " or ";
                            }
                        }
                    }
    
	        		
					

					$filas  = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro,'') : consulta_bd($lista_columnas,$tabla,$filtro,'');				
				    $total  = mysqli_affected_rows($conexion);

					$pages = new Paginator;
				    $pages->items_total = $total;
				    $pages->mid_range = 7; 
                    if(isset($_GET[columnas])){
                        $varCol = "columnas=".$_GET[columnas]."&";
                    } else {
                        $varCol = "";
                    }
                    if(isset($_GET[buscar])){
                        $varBuscar = "buscar=".$_GET[buscar]."&";
                    } else {
                        $varBuscar = "";
                    }
    
				    $pages->paginate("index.php?".$varCol.$varBuscar."op=".$val_op."a");

					if ($order_list_by != '')
					{
						$order_by = $order_list_by;
					}
					else
					{
						$order_by = "id DESC $pages->limit";
					}

					if ($nombre == 'filtro')
					{
						$filtro = $opcion[$nombre]." LIKE '$letter%'";	
					}

				    $sql = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro,$order_by) : consulta_bd($lista_columnas,$tabla,$filtro,$order_by);				
				    $cant = mysqli_affected_rows($conexion);

					
				 	
					$i = 0;
					while($i <= (sizeof($sql)-1))
					{
						$id = $sql[$i][0];
						
					 	if ($i%2!=0)
						{
						 	echo '<tr>';
						}
						else
						{
						 	echo '<tr bgcolor="#ddd">';
						}
						
						$m = $i+1;
						
						for($j = 0; $j <= sizeof($columnas)-1; $j++)
						{
							if (is_boolean($columnas[$j], $tabla))
							{
								//dejo editables por ajax los checkbox
								$val_checkbox = ($sql[$i][$j] == 1) ? "Si":"No";
								if ($sql[$i][$j] == 1){
									$marcado = 'checked="checked"';
								} else{
									$marcado = '';
								} 
								$val_rel = $columnas[$j].'-'.$sql[$i][0].'-'.$tabla;
								echo '<td><input type="checkbox" class="check_" name="" value="'.$sql[$i][$j].'" '.$marcado.' rel="'.$val_rel.'" /><span class="'.$val_rel.'">'.$val_checkbox.'</span></td>';
							}
							elseif(in_array($j, $id_col_autocompletar))
							{
								//Recorro los autocompletar
								foreach ($autocompletar_array as $autocompletar_item)
								{
									$datos_autocompletar = explode(',', $autocompletar_item);
									$tabla_autocompletar = trim($datos_autocompletar[0]);
									$columna_autocompletar = trim($datos_autocompletar[1]);
									
									if ($columnas[$j] == $columna_autocompletar)
									{
										$val_autocompletar = consulta_bd_por_id("nombre","$tabla_autocompletar","",$sql[$i][$j]);
										echo '<td>'.$val_autocompletar['nombre'].'</td>';
									}
								}	
							}
							elseif (is_date($sql[$i][$j]))
							{
								//se muestran las fechas de creacion y mofificacion
								$date = date("d/m/Y H:i:s", strtotime($sql[$i][$j]));
								echo '<td class="fechas">'.$date.'</td>';
							}
							else
							{           				
								if ($related[$j]) //Arituculos
								{
									$id = $sql[$i][$j];
									if ($id != null)
									{
										$belong_to_table = check_if_item_has_belongs_to($columnas[$j]);
										if ($belong_to_table) //cuestiones
										{
										
											//Valor del articulo
											$id_related = singular($belong_to_table)."_id";
											$val_related = consulta_bd_por_id("nombre, $id_related", plural($related[$j]), '', $id);
											
											if ($belongs_to_parent) //temas
											{								
											
												//Valor de la cuestion
												$id_related_padre = singular($belongs_to_parent)."_id";
												$val_related_padre = consulta_bd_por_id("nombre, $id_related_padre", $belong_to_table, '', $val_related["$id_related"]);
												$data_padre = ($val_related_padre['nombre'] == '') ? "&nbsp;": $val_related_padre['nombre'];
														
												if ($belongs_to_gran_parent) //partes
												{
													//Valor del tema
													$id_related_parent = singular($belongs_to_gran_parent)."_id";
													$val_related_parent = consulta_bd_por_id("nombre, $id_related_parent", $belongs_to_parent, '', $val_related_padre["$id_related_padre"]);
												
													//Valor del parte
													$val_related_gran_parent = consulta_bd_por_id("nombre", $belongs_to_gran_parent, '', $val_related_parent["$id_related_parent"]);
													$data = ($val_related_gran_parent['nombre'] == '') ? "&nbsp;": $val_related_gran_parent['nombre'];
													//Mostrar el parte
													echo '<td>'.ucwords($data).'</td>';
												}
												else
												{
													//Valor del tema
													$val_related_parent = consulta_bd_por_id("nombre", $belongs_to_parent, '', $val_related_padre["$id_related_padre"]);

												}
												
												//Mostrar el tema
												$data = ($val_related_parent['nombre'] == '') ? "&nbsp;": $val_related_parent['nombre'];
												echo '<td>'.ucwords($data).'</td>';
											}
											else
											{
												//Valor de la cuestion
												$val_related_padre = consulta_bd_por_id("nombre", $belong_to_table, '', $val_related["$id_related"]);
												$data_padre = ($val_related_padre['nombre'] == '') ? "&nbsp;": $val_related_padre['nombre'];
											}
											
											//Mostrar la cuestion
											echo '<td>'.ucwords($data_padre).'</td>';
											
											//Mostrar el articulo
											$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
											echo '<td>'.ucwords($data).'</td>';
											
										}
										else
										{
											// se muestran en la lista los select
											$val_related = consulta_bd_por_id("nombre", plural($related[$j]), '', $id);
											$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
											echo '<td>'.ucwords($data).'</td>';
										}
									}
									else
									{
										echo '<td>&nbsp;</td>';
									}
								}
								elseif  ((isset($id_modificado_por) AND $j == $id_modificado_por) OR (isset($id_creado_por) AND $j == $id_creado_por))
								{	
									$val_uid = (int)$sql[$i][$j];
									$user_modificado_por = consulta_bd_por_id("nombre, apellido_paterno","administradores","",$val_uid);
									$user_name = $user_modificado_por['nombre']." ".$user_modificado_por['apellido_paterno'];
									echo '<td class="usuarios">'.$user_name.'</td>';
								}
								else
								{
									//campos de textos con opcion de editar
									$data = ($sql[$i][$j] == '') ? "&nbsp;": $sql[$i][$j];
									if($colum == 'id'){
										echo '<td>'.$data.'</td>';
									} else {
										echo '<td rel="'.$columnas[$j].'-'.$sql[$i][0].'-'.$tabla.'" style="position:relative">
												<span class="'.$columnas[$j].'-'.$sql[$i][0].'-'.$tabla.'">'.$data.'</span>
												<form method="post" class="form_editar_en_linea">';
												//if (is_numeric($data)){ $tipo_dato = 'int'; } else { $tipo_dato = '';}
													
										echo 		'<input class="campo_editable_lista" id="" type="text" name="" value="'.$data.'" />';
													
										echo	   '<a href="javascript:void(0)" class="btn_editar_linea">Guardar</a>
												</form>
											  </td>';
									}
									/////////////////////////////
								}
							}
							
						}
						if($asociados)
						{
							$data = get_asociados($tabla, $asociados, $sql[$i][0]);
							echo '<td>'.ucwords($data).'</td>';
						}
					
						if (!isset($edit) or $edit == 1) {
							//con esto se edita y elimina desde la linea
							echo '<td>
									<a href="index.php?op='.$val_op.'c&id='.$sql[$i][0].$rid.'" class="boton4 btn_editar" target="_blank"><i class="fas fa-edit"></i></a>
								  </td>
								  <td>'; 
							?>
									<a onclick="javascript:confirmar('<?php echo $sql[$i][0].$rid; ?>','del','','<?php echo $tabla; ?>','','')" class="boton4 btn_eliminar"><i class="far fa-trash-alt"></i></a>
							<?php		
							echo  '</td>
								</tr>';
						}
						$i++;
					}
					
				  ?>

			<?php }else{ ?>

				<?php
					

					if($tabla == 'productos'){
						if ($linea_id_get != NULL OR $categoria_id_get != NULL) {

							$tabla_consulta_join = "productos p JOIN lineas_productos cp ON p.id = cp.producto_id LEFT JOIN posicion_productos pp ON p.id = pp.producto_id";
							$filtro = $where_productos;

							$lista = explode(',', $lista_columnas);
							$lista_columnas = "";
							
							foreach($lista as $l){
								if($lista_columnas) $lista_columnas .= ', p.'.$l;
								else $lista_columnas .= 'p.'.$l;
							}

							$filas  = (isset($tablas_consulta) AND $tablas_consulta != NULL) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro,'') : consulta_bd($lista_columnas,$tabla_consulta_join,$filtro. ' GROUP BY p.id','');

						}else{
							$filas  = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,'', '') : consulta_bd($lista_columnas,$tabla,'', '');
						}
						
					}else{
						$filas  = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,'', '') : consulta_bd($lista_columnas,$tabla,'', '');
					}
				    $total  = mysqli_affected_rows($conexion);

					$pages = new Paginator;
				    $pages->items_total = $total;
				    $pages->mid_range = 7; 
				    $pages->paginate("index.php?op=".$val_op."a");

					if ($order_list_by != '')
					{
						$order_by = $order_list_by;
					}
					else
					{
						$order_by = "id DESC $pages->limit";
					}

					if ($nombre == 'filtro')
					{
						$filtro = $opcion[$nombre]." LIKE '$letter%'";	
					}

					$tabla = obtener_tabla($val_op);

					if ($tabla == 'productos') {
						if ($linea_id_get != NULL OR $categoria_id_get != NULL) {
							$filtro = $where_productos;

							$order_by = "p.id DESC $pages->limit";

							$tabla_consulta_join = "productos p JOIN lineas_productos cp ON p.id = cp.producto_id LEFT JOIN posicion_productos pp ON p.id = pp.producto_id";

							$sql  = (isset($tablas_consulta) AND $tablas_consulta != NULL) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro . ' GROUP BY p.id',$order_by) : consulta_bd($lista_columnas,$tabla_consulta_join,$filtro. ' GROUP BY p.id',$order_by);
						}else{
							$sql = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro. ' GROUP BY p.id',$order_by) : consulta_bd($lista_columnas,$tabla,$filtro,$order_by);	
						}
						
					}else{
						$sql = (isset($tablas_consulta)) ? consulta_bd($lista_columnas,$tablas_consulta,$filtro. ' GROUP BY p.id',$order_by) : consulta_bd($lista_columnas,$tabla,$filtro,$order_by);	
					}
				    			
				    $cant = mysqli_affected_rows($conexion);
				 	
					$i = 0;
					while($i <= (sizeof($sql)-1))
					{
						$id = $sql[$i][0];
						
					 	if ($i%2!=0)
						{
						 	echo '<tr>';
						}
						else
						{
						 	echo '<tr bgcolor="#ddd">';
						}
						
						$m = $i+1;
						
						for($j = 0; $j <= sizeof($columnas)-1; $j++)
						{	
							if (is_boolean($columnas[$j], $tabla))
							{
								//dejo editables por ajax los checkbox
								$val_checkbox = ($sql[$i][$j] == 1) ? "Si":"No";
								if ($sql[$i][$j] == 1){
									$marcado = 'checked="checked"';
								} else{
									$marcado = '';
								} 
								$val_rel = $columnas[$j].'-'.$sql[$i][0].'-'.$tabla;
								echo '<td><input type="checkbox" class="check_" name="" value="'.$sql[$i][$j].'" '.$marcado.' rel="'.$val_rel.'" /><span class="'.$val_rel.'">'.$val_checkbox.'</span></td>';
							}
							elseif(in_array($j, $id_col_autocompletar))
							{
								//Recorro los autocompletar
								foreach ($autocompletar_array as $autocompletar_item)
								{
									$datos_autocompletar = explode(',', $autocompletar_item);
									$tabla_autocompletar = trim($datos_autocompletar[0]);
									$columna_autocompletar = trim($datos_autocompletar[1]);
									
									if ($columnas[$j] == $columna_autocompletar)
									{
										$val_autocompletar = consulta_bd_por_id("nombre","$tabla_autocompletar","",$sql[$i][$j]);
										echo '<td>'.$val_autocompletar['nombre'].'</td>';
									}
								}	
							}
							elseif (is_date($sql[$i][$j]))
							{
								//se muestran las fechas de creacion y mofificacion
								$date = date("d/m/Y H:i:s", strtotime($sql[$i][$j]));
								echo '<td class="fechas">'.$date.'</td>';
							}
							else
							{           				
								if ($related[$j]) //Arituculos
								{
									$id = $sql[$i][$j];
									if ($id != null)
									{
										$belong_to_table = check_if_item_has_belongs_to($columnas[$j]);
										if ($belong_to_table) //cuestiones
										{
										
											//Valor del articulo
											$id_related = singular($belong_to_table)."_id";
											$val_related = consulta_bd_por_id("nombre, $id_related", plural($related[$j]), '', $id);
											
											if ($belongs_to_parent) //temas
											{								
											
												//Valor de la cuestion
												$id_related_padre = singular($belongs_to_parent)."_id";
												$val_related_padre = consulta_bd_por_id("nombre, $id_related_padre", $belong_to_table, '', $val_related["$id_related"]);
												$data_padre = ($val_related_padre['nombre'] == '') ? "&nbsp;": $val_related_padre['nombre'];
														
												if ($belongs_to_gran_parent) //partes
												{
													//Valor del tema
													$id_related_parent = singular($belongs_to_gran_parent)."_id";
													$val_related_parent = consulta_bd_por_id("nombre, $id_related_parent", $belongs_to_parent, '', $val_related_padre["$id_related_padre"]);
												
													//Valor del parte
													$val_related_gran_parent = consulta_bd_por_id("nombre", $belongs_to_gran_parent, '', $val_related_parent["$id_related_parent"]);
													$data = ($val_related_gran_parent['nombre'] == '') ? "&nbsp;": $val_related_gran_parent['nombre'];
													//Mostrar el parte
													echo '<td>'.ucwords($data).'</td>';
												}
												else
												{
													//Valor del tema
													$val_related_parent = consulta_bd_por_id("nombre", $belongs_to_parent, '', $val_related_padre["$id_related_padre"]);

												}
												
												//Mostrar el tema
												$data = ($val_related_parent['nombre'] == '') ? "&nbsp;": $val_related_parent['nombre'];
												echo '<td>'.ucwords($data).'</td>';
											}
											else
											{
												//Valor de la cuestion
												$val_related_padre = consulta_bd_por_id("nombre", $belong_to_table, '', $val_related["$id_related"]);
												$data_padre = ($val_related_padre['nombre'] == '') ? "&nbsp;": $val_related_padre['nombre'];
											}
											
											//Mostrar la cuestion
											echo '<td>'.ucwords($data_padre).'</td>';
											
											//Mostrar el articulo
											$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
											echo '<td>'.ucwords($data).'</td>';
											
										}
										else
										{
											// se muestran en la lista los select
											$val_related = consulta_bd_por_id("nombre", plural($related[$j]), '', $id);
											$data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
											echo '<td>'.ucwords($data).'</td>';
										}
									}
									else
									{
										echo '<td>&nbsp;</td>';
									}
								}
								elseif  ((isset($id_modificado_por) AND $j == $id_modificado_por) OR (isset($id_creado_por) AND $j == $id_creado_por))
								{	
									$val_uid = (int)$sql[$i][$j];
									$user_modificado_por = consulta_bd_por_id("nombre, apellido_paterno","administradores","",$val_uid);
									$user_name = $user_modificado_por['nombre']." ".$user_modificado_por['apellido_paterno'];
									echo '<td class="usuarios">'.$user_name.'</td>';
								}
								else
								{
									//campos de textos con opcion de editar
									$data = ($sql[$i][$j] == '') ? "&nbsp;": $sql[$i][$j];
									if($colum == 'id'){
										echo '<td>'.$data.'</td>';
									} else {
										echo '<td rel="'.$columnas[$j].'-'.$sql[$i][0].'-'.$tabla.'" style="position:relative">
												<span class="'.$columnas[$j].'-'.$sql[$i][0].'-'.$tabla.'">'.$data.'</span>
												<form method="post" class="form_editar_en_linea">';
												//if (is_numeric($data)){ $tipo_dato = 'int'; } else { $tipo_dato = '';}
													
										echo 		'<input class="campo_editable_lista" id="" type="text" name="" value="'.$data.'" />';
													
										echo	   '<a href="javascript:void(0)" class="btn_editar_linea">Guardar</a>
												</form>
											  </td>';
									}
									/////////////////////////////
								}
							}
							
						}
						if($asociados)
						{
							$data = get_asociados($tabla, $asociados, $sql[$i][0]);
							echo '<td>'.ucwords($data).'</td>';
						}
					
						if (!isset($edit) or $edit == 1) {
							//con esto se edita y elimina desde la linea
							echo '<td>
									<a href="index.php?op='.$val_op.'c&id='.$sql[$i][0].$rid.'" class="boton4 btn_editar" target="_blank"><i class="fas fa-edit"></i></a>
								  </td>
								  <td>'; 
							?>
									<a onclick="javascript:confirmar('<?php echo $sql[$i][0].$rid; ?>','del','','<?php echo $tabla; ?>','','')" class="boton4 btn_eliminar"><i class="far fa-trash-alt"></i></a>
							<?php		
							echo  '</td>
								</tr>';
						}
						$i++;
					}
					
				  ?>
			<?php } ?>
        </tbody>
    </table>
    <div id="tablefooter">
      	<!--<div id="tablenav">
        	<div>
                <img src="css/images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="css/images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="css/images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="css/images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div>
            	<select class="select" id="pagedropdown"></select>
			</div>
            <div>
            	<a class="ver_todo" href="javascript:sorter.showall()">Ver Todo</a>
            </div>
        </div>-->
		<div id="tablelocation">
        	<div>

        		<div class="paginacion">
		            <div class="main">
		                <?php 
		                    echo $pages->display_pages();
		                ?>
		            </div>
		        </div>

            </div>
            <div class="page">Página<span id="currentpage"></span> de <span id="totalpages"></span></div>
        </div>        
    </div>
</div>

<script type="text/javascript">
//editar en linea los checkbox

    $(function(){
		
		$('.check_').click(function(){
           var variables = $(this).attr('rel');
           var valor = $(this).attr('checked');
           alertify.log("Realizando Modificacion");
			
			$.ajax({
				type: "POST",
				url: "app/partials/ajax_check_has_many.php",
				data: { campos: variables, marcado: valor },
				success: function(respuesta){
					//console.log(respuesta);
					alertify.success("Modificacion realizada con éxito");
					if(valor == "checked"){
						//marcar si
						$('span.'+variables).html('Si');
					} else {
						$('span.'+variables).html('No');
						//marcar no
					};
				}, 
				error: function(){
					alertify.error("No se pudo realizar la solicitud");
			  		return false;
				}
			});
			
		}); 
	   
    });
</script>


<script type="text/javascript">
//Editar campos en linea, no los select ni los checkbox ni ID
$(function(){
	$("td").dblclick(function(){
		$(this).find(".form_editar_en_linea").fadeIn(100);
	});
	$("a.btn_editar_linea").click(function(){
		var valor_campo = $(this).parent().find(".campo_editable_lista").val();
		var variables = $(this).parent().parent().attr("rel");
		alertify.log("Realizando Modificacion");
		
		$.ajax({
				type: "POST",
				url: "app/partials/ajax_campos_list.php",
				data: { campos:variables, val:valor_campo },
				success: function(respuesta){
					//console.log(respuesta);
					alertify.success("Modificacion realizada con éxito");
					$('span.'+variables).html(valor_campo);
					$(".form_editar_en_linea").fadeOut(100);
				}, 
				error: function(){
					alertify.error("No se pudo realizar la solicitud");
			  		return false;
				}
			});
	});
	
});
</script>

<script type="text/javascript" src="js/tinytable.js"></script>
<script type="text/javascript">
function cargado()
{
	$('#loader').hide();
	$('#tablewrapper').fadeIn(500);
}
cargado();
<?php if ($cant > 0) { ?>
var sorter = new TINY.table.sorter('sorter','table',{
	headclass:'head',
	ascclass:'asc',
	descclass:'desc',
	evenclass:'evenrow',
	oddclass:'oddrow',
	evenselclass:'evenselected',
	oddselclass:'oddselected',
	paginate:false,
	size:10,
	colddid:'columns',
	currentid:'currentpage',
	totalid:'totalpages',
	startingrecid:'startrecord',
	endingrecid:'endrecord',
	totalrecid:'totalrecords',
	hoverid:'selectedrow',
	pageddid:'pagedropdown',
	navid:'tablenav',
	init:true
}, cargado);
<?php }else{ ?>
	cargado();
<?php } ?>
</script>
<script type="text/javascript">
	$('.bg_mdl').on('click', function(){
		$('.bg_mdl').fadeOut();
	})

	$('.content_mdl').on('click', function(e){
		e.stopPropagation();
	})
	$('#btn_transferencias').on('click', function(){
		$('.bg_mdl').fadeIn();
	})

	$('.saveTransferencias').on('click', function(){
		var oc = $('.ocMdl').val();
		var accion = $('.selectMdl').val();

		if (oc.trim() != '' && accion != 0) {
			$.ajax({
				url: 'action/correoTransferencias.php',
				type: 'post',
				dataType: 'json',
				data: { oc: oc, accion: accion },
				beforeSend: function(){
					$('.saveTransferencias').html('<i class="fas fa-circle-notch fa-spin" style="color: #fff;"></i>');
				}
			}).done(function(res){
				$('.saveTransferencias').html('ENVIAR');
				if (res['status'] == 'success') {
					alertify.success('Operación exitosa.');
					$('.ocMdl').val('');
					$('.selectMdl').val(0);
				}else{
					alertify.error('El pedido ya ha sido validado.');
				}
			}).fail(function(res){
				console.log(res);
			})
		}else{
			alertify.error('Todos los campos son obligatorios');
		}
	})

	$('.cancelarCompras').on('click', function(){
		var oc = $('.ocMdl').val();
		var accion = $('.selectMdl').val();

		if (oc.trim() != '' && accion != 0) {
			$.ajax({
				url: 'action/cancelarPedido.php',
				type: 'post',
				dataType: 'json',
				data: { oc: oc, accion: accion },
				beforeSend: function(){
					$('.cancelarCompras').html('<i class="fas fa-circle-notch fa-spin" style="color: #fff;"></i>');
				}
			}).done(function(res){
				$('.cancelarCompras').html('ENVIAR');
				if (res['status'] == 'success') {
					alertify.success('Operación exitosa.');
					$('.ocMdl').val('');
					$('.selectMdl').val(0);
				}else{
					alertify.error('Error, intente más tarde.');
				}
			})
		}else{
			alertify.error('Todos los campos son obligatorios');
		}
	})
</script>
<p></p>


