<?php
	include("../conf.php");
	header('Content-type: text/xml');
	//header("Content-Type: text/plain");
	// header('Content-Type: application/json');

	$url_sitio 				= get_option('url_sitio');
	$tienda_facebook 		= get_option('tienda_facebook');
	$nombre_setFrom_mail	= get_option('nombre_setFrom_mail');
	$categoria_google 		= get_option('categoria_google');

	if($tienda_facebook){

		$select = "p.id, p.nombre, m.nombre, p.descripcion_seo, pd.precio, IF(pd.descuento > 0, pd.descuento, pd.precio), p.thumbs, pd.sku";
		$from = "productos p join productos_detalles pd on p.id = pd.producto_id join marcas m on p.marca_id = m.id";
		$where = "p.publicado = 1 and pd.publicado = 1 and p.thumbs != '' and p.thumbs is not null and pd.stock > 0 and pd.precio > 0 group by id";


		$productos 	= consulta_bd($select,$from,$where,$order);

		echo '<?xml version="1.0"?>';
		echo '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">';
			echo '<channel>';
				echo '<title>'.$nombre_setFrom_mail.'</title>';
				echo '<link>'.$url_sitio.'</link>';
				echo '<description></description>';

				$contador = 0;
				foreach($productos as $pr){
					$imagen 		= $url_sitio.'/imagenes/productos/'.$pr[6];
					$description 	= strip_tags($pr[3]);
					$marca = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', html_entity_decode($pr[2]));

					$nombre_producto = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', html_entity_decode($pr[1]));

					$final_description = ($description != '' AND $descripcion != NULL) ? substr(preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', html_entity_decode($description)), 0, 5000) : ucwords(strtolower($nombre_producto));

					if($imagen){
						echo '<item>';
							echo '<g:id>'.$pr[0].'</g:id>';
							echo '<g:availability>in stock</g:availability>';
							echo '<g:condition>new</g:condition>';
							echo '<g:description>'.$final_description.'</g:description>';
							echo '<g:image_link>'.$imagen.'</g:image_link>';
							echo '<g:link>'.$url_sitio.'/ficha/'.$pr[0].'/'.url_amigables($pr[1]).'</g:link>';
							echo '<g:title>'.ucwords(mb_strtolower($nombre_producto, 'UTF-8')).'</g:title>';
							echo '<g:price>'.round($pr[4]).' CLP</g:price>';
							echo '<g:sale_price>'.round($pr[5]).' CLP</g:sale_price>';
							echo '<g:mpn>'.$pr[7].'</g:mpn>';
							echo '<g:brand>'.$marca.'</g:brand>';
							echo '<g:google_product_category>'.$categoria_google.'</g:google_product_category>';
						echo '</item>';
					}

					$contador++;
					// if ($contador == 200) {
					// 	break;
					// }
				}


			echo '</channel>';
		echo '</rss>';

	}


	function getIMGPrd($pd){
		$url_sitio 	= get_option('url_sitio');
		$img 		= consulta_bd("p.thumbs","productos_detalles pd, productos p","p.id = pd.producto_id AND pd.id = $pd","");
		$src 		= $url_sitio.'/imagenes/productos/'.$img[0][0];

		if (getimagesize($src)){
			$return = $src;
		}else{
			$return = false;
		}
		return $return;
	}
	function getPrecio($pd){
		$row = consulta_bd("precio, descuento","productos_detalles","id = $pd","");
		if(hasDescuento($pd)){
			$precio = (int)$row[0][1];
		}else{
			$precio = (int)$row[0][0];
		}
		return $precio*1.19;
	}
	function getMarca($id){
		$row = consulta_bd("nombre","marcas","id = $id","");
		return $row[0][0];
	}
	function hasDescuento($pd){
		$row = consulta_bd("descuento","productos_detalles","id = $pd","");
		if((int)$row[0][0] > 0) return true;
		else return false;
	}
	function getPrecioNormal($pd){
		$row = consulta_bd("precio","productos_detalles","id = $pd","");
		return $row[0][0];
	}
?>