<?php


/*Archivo para procesar las vistas previas de un newsletter*/

require('conf.php');

function sanitize_output($buffer) {

    $search = array(
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/' // Remove HTML comments
    );

    $replace = array(
        '>',
        '<',
        '\\1',
        ''
    );

    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}

if(isset($_GET['id'])) {

  $id = htmlentities($_GET['id']);
  $id = intval($id);
/*  if(isset($_GET['n2'])) {
      $consulta = consulta_bd('id,nombre','newsletterses', 'id = '.$id);
  }
  else {
    $consulta = consulta_bd('id,titulo','newsletters', 'id = '.$id);
  }*/

  $consulta = consulta_bd('id,nombre','creador_newsletteres', 'id = '.$id);

  if($consulta == null) {
    echo 'No se encuentra el elemento.';
  }
  else {
    /*ob_start();
    include("newsletters/plantilla_1.php");
	$contenido = ob_get_contents();
	ob_end_clean();
	$contenido = htmlentities($contenido);*/
	
	ob_start();
	include("newsletters/plantilla_1.php");
	$contenido = ob_get_contents();
	ob_end_clean();
	
	//$contenido = htmlentities($contenido);
	$contenido = sanitize_output($contenido);
	echo '<div style="box-sizing:border-box;padding:30px;width:calc(100% - 6px);height:calc(100% - 6px);"><textarea class="focustextarea" style="width:calc(100% - 30px);height:calc(100% - 30px);padding:15px;margin:0 auto;">'.str_replace('í', '&iacute;',$contenido).'</textarea></div>';
  }
}
else {
  header('Location: index.php');
}






?>
