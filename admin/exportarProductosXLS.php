<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');
include("conf.php");

require_once 'PHPExcel-1.8/Classes/PHPExcel.php';
require_once 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$productos = consulta_bd("id, publicado, pack, nombre, marca_id, thumbs, fecha_creacion, fecha_modificacion","productos","","");
//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Publicado');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Pack');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Marca_id');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Thumbs');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Fecha creacion');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Fecha modificacion');

for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $productos[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $productos[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $productos[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $productos[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $productos[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $productos[$i][5]);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$linea, $productos[$i][6]);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$linea, $productos[$i][6]);
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Productos');






// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);


//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Producto_id');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Publicado');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SKU');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Precio');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Descuento');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Stock');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Stock reserva');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Fecha creacion');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Fecha modificacion');

$PD = consulta_bd("id, producto_id, publicado, nombre, sku, precio, descuento, stock, stock_reserva, fecha_creacion, fecha_modificacion","productos_detalles","","");
for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $PD[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $PD[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $PD[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $PD[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $PD[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $PD[$i][5]);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$linea, $PD[$i][6]);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$linea, $PD[$i][7]);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$linea, $PD[$i][8]);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$linea, $PD[$i][9]);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$linea, $PD[$i][10]);
}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Productos detalles');





// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="productos.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>
