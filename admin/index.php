<?php

/********************************************************\
|  Moldeable CMS 										 |
|  Fecha Modificación: 14/03/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2011   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                              |
| 														 |
|  Moldeable no se hace responsable por errores en el    |
|  sistema en caso de que se modifiquen los archivos     |
|  contenidos dentro de esta carpeta.					 |
\********************************************************/


	if(file_exists('conf.php'))
	{
		(file_exists('install.php')) ? include("install.php"):include("system.php");
	}
	else
	{
		die("<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Advertencia:</span> <br /><br />Aparentemente falta el archivo de configuración conf.php necesario para poder ejecutar este software.</p>");
	}
	
?>