<?php
//conf
include('../admin/conf.php');
//funciones carro
// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

require_once "mercadopago/lib/mercadopago.php";
include('mercadopago/claves.php');

$despacho_retiro = mysqli_real_escape_string($conexion, $_POST['despacho_retiro']);

if (isset($_COOKIE['usuario_id'])) {
	$cliente_id = $_COOKIE['usuario_id'];

	$datos_cliente = consulta_bd('nombre, rut, telefono, email', 'clientes', "id = $cliente_id", '');

	$info_email = $datos_cliente[0][3];
	$info_tele  = $datos_cliente[0][2];
	$info_name  = $datos_cliente[0][0];
	$info_rut	= $datos_cliente[0][1];

	$current_user = $cliente_id;

}else{

	$info_email = mysqli_real_escape_string($conexion, $_POST['email_ident']);

	if ($despacho_retiro == 2) {
		$info_name  = mysqli_real_escape_string($conexion, $_POST['nombreretiro']);
		$info_tele  = '';
	}else{
		$info_name  = mysqli_real_escape_string($conexion, $_POST['nombre']);
		$info_tele  = mysqli_real_escape_string($conexion, $_POST['telefono']);
	}
	
	$info_rut	= '';
	
	$clientes = consulta_bd("id","clientes","LOWER(email)='$info_email'","");
	$cantClientes = mysqli_affected_rows($conexion);
	if($cantClientes > 0){
		$current_user = $clientes[0][0];
	} else {
		$inserCliente = insert_bd("clientes","nombre,email,telefono","'$info_name','$info_email','$info_tele'");
		$idClienteInsert = mysqli_insert_id($conexion);
		$current_user = $idClienteInsert;
	}
	//usuario no logueado
}


$date = date('Ymdhis');
$oc = "OC_$date";

//GET ID ADDRESS
$id_direccion = mysqli_real_escape_string($conexion, $_POST[address_id]);
$tiendaRetiro = mysqli_real_escape_string($conexion, $_POST[retiro]);

if ($despacho_retiro == 1) {

	if (isset($_COOKIE['usuario_id'])) {
		$cliente_id = $_COOKIE['usuario_id'];
		// Usuario logueado revisamos si enviamos a la dirección principal o a una distinta.
		
		if (isset($_POST['row_select_dir'])) {

			$direccion_id = (int)mysqli_real_escape_string($conexion, $_POST['row_select_dir']);
			$direccion_check = consulta_bd('d.id, r.nombre, c.nombre, d.calle, d.numero, d.piso, d.comuna_id', 'clientes_direcciones d JOIN regiones r ON d.region_id = r.id JOIN comunas c ON d.comuna_id = c.id', "d.id = $direccion_id", '');

			$region 	= $direccion_principal[0][1];
			$comuna 	= $direccion_principal[0][2];
			$direccion 	= $direccion_principal[0][3] . ' ' . $direccion_principal[0][4] . ' ' . $direccion_principal[0][5];

			$comuna_id = $direccion_principal[0][6];

			$despacho = valorDespacho($comuna_id);
			$retiroEnTienda = 0;
		    $despacho_id = 0;
		    $despacho_por_pagar = (is_numeric(valorDespacho($comuna_id))) ? 0 : 1;
			
		} else {

			// Dirección principal
			$direccion_principal = consulta_bd('d.id, r.nombre, c.nombre, d.calle, d.numero, d.piso, d.comuna_id', 'clientes_direcciones d JOIN regiones r ON d.region_id = r.id JOIN comunas c ON d.comuna_id = c.id', "d.cliente_id = $cliente_id AND d.principal = 1", '');

			$region 	= $direccion_principal[0][1];
			$comuna 	= $direccion_principal[0][2];
			$direccion 	= $direccion_principal[0][3] . ' ' . $direccion_principal[0][4] . ' ' . $direccion_principal[0][5];

			$comuna_id = $direccion_principal[0][6];

		    $despacho = ( is_numeric(valorDespacho($comuna_id)) ) ? valorDespacho($comuna_id) : 0 ;
			$retiroEnTienda = 0;
		    $despacho_id = 0;
		    $despacho_por_pagar = (is_numeric(valorDespacho($comuna_id))) ? 0 : 1;

		}

	} else {

		$region_id = mysqli_real_escape_string($conexion, $_POST['region']);
		$comuna_id = mysqli_real_escape_string($conexion, $_POST['comuna']);
		$numero = mysqli_real_escape_string($conexion, $_POST['numero']);
		$piso = mysqli_real_escape_string($conexion, $_POST['piso']);
		$calle = mysqli_real_escape_string($conexion, $_POST['direccion']);

		$region_comuna = consulta_bd('r.nombre, c.nombre', 'comunas c join regiones r on r.id = c.region_id', "c.id = $comuna_id", '');

		$region = $region_comuna[0][0];
		$comuna = $region_comuna[0][1];


		$direccion = $calle . ' ' . $numero . ' ' . $piso;

		$despacho = (is_numeric(valorDespacho($comuna_id))) ? valorDespacho($comuna_id) : 0 ;
		$retiroEnTienda = 0;
	    $despacho_id = 0;
	    $despacho_por_pagar = (is_numeric(valorDespacho($comuna_id))) ? 0 : 1;

	}

	$carrier = check_carrier();
	
}elseif($despacho_retiro == 2){

	$sucursal_id = (int)mysqli_real_escape_string($conexion, $_POST['sucursal']);

	$sucursal = consulta_bd('s.id, s.direccion, r.nombre, c.nombre, s.comuna_id', 'sucursales s JOIN comunas c ON s.comuna_id = c.id JOIN regiones r ON r.id = c.region_id', "s.id = $sucursal_id", '');

	$region 	= $sucursal[0][2];
	$comuna 	= $sucursal[0][3];
	$direccion 	= $sucursal[0][1];

	$comuna_id 	= $sucursal[0][4];

	$despacho = 0;
	$retiroEnTienda = 1;
    $despacho_id = $sucursal_id;
    $despacho_por_pagar = 0;

    $carrier = '';
}

//datos retiro en tienda
$nombre_retiro = (isset($_POST[quien_retira])) ? mysqli_real_escape_string($conexion, $_POST[quien_retira]) : 0;
$rut_retiro = (isset($_POST[rut_retiro])) ? mysqli_real_escape_string($conexion, $_POST[rut_retiro]) : 0;

//Comentarios envios
/*$comentarios_envio = (isset($_POST[comentarios_envio])) ? mysqli_real_escape_string($conexion, $_POST[comentarios_envio]) : 0;*/


//facturacion
$factura  = (isset($_POST[factura])) ? mysqli_real_escape_string($conexion, $_POST[factura]) : 0;

if($factura == 0){
	$giro = 'Particular';
	$factura = 0;
	$razon_social = '';
	$rut_factura = '';
	$direccion_factura = '';
	$nombre_factura = '';
	$telefono_factura = '';
}else{
	$giro = (isset($_POST[giro])) ? mysqli_real_escape_string($conexion, $_POST[giro]) : 0;
	$factura = 1;
	$razon_social = (isset($_POST[razon_social])) ? mysqli_real_escape_string($conexion, $_POST[razon_social]) : 0;
	$rut_factura = (isset($_POST[rut_empresa])) ? mysqli_real_escape_string($conexion, $_POST[rut_empresa]) : 0;
	$direccion_factura = (isset($_POST[direccion_empresa])) ? mysqli_real_escape_string($conexion, $_POST[direccion_empresa]) : 0;
	$nombre_factura = (isset($_POST[nombre_factura])) ? mysqli_real_escape_string($conexion, $_POST[nombre_factura]) : 0;
	$telefono_factura = (isset($_POST[telefono_factura])) ? mysqli_real_escape_string($conexion, $_POST[telefono_factura]) : 0;
}


$subtotal = round(get_total_price($comuna_id));

$cod = 'null';
//descuento
if($_SESSION["descuento"]){
	$valor_descuento = $_SESSION['val_descuento'];
	// Condición session descuento
	$cod = $_SESSION['descuento'];
} else {
	$valor_descuento = 0;
	$cod = '';
}

							
$medioPago  = (isset($_POST[metodopago])) ? mysqli_real_escape_string($conexion, $_POST[metodopago]) : 0;

//TOTAL
$total = $subtotal+$despacho-$valor_descuento;

//CART EXPLODE AND COUNT ITEMS
if(!isset($_COOKIE[cart_alfa_cm])){
	setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
$cart = $_COOKIE[cart_alfa_cm];
	

$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}



////////////////// ----------- REVISA STOCK TEMPORAL ----------- //////////////////

	foreach ($contents as $id=>$qty){

		$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

		if($isPack[0][0]){

			$codes_pack = explode(',', $isPack[0][1]);
			foreach ($codes_pack as $value){
				$value 			= trim($value);
				$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku='$value'","");
				$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
				if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
					header("location: mi-carro?stock=$id");
					die();
				}
			}

		}else{
			$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
			$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
			if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
				header("location: mi-carro?stock=$id");
				die();
			}	
		}

		
	}

////////////////// ----------- ////////////////// ----------- //////////////////




$nombre_retiro = (isset($_POST[nombre_retiro])) ? mysqli_real_escape_string($conexion, $_POST[nombre_retiro]) : 0;
$apellidos_retiro = (isset($_POST[apellidos_retiro])) ? mysqli_real_escape_string($conexion, $_POST[apellidos_retiro]) : 0;
$rut_retiro = (isset($_POST[rut_retiro])) ? mysqli_real_escape_string($conexion, $_POST[rut_retiro]) : 0;


if ($_COOKIE['cart_alfa_cm']!="") {
	
	$estado_pendiente = 1;
	$campos = "oc, 
	cliente_id, 
	fecha, 
	estado_id, 
	codigo_descuento, 
	descuento, 
	total, 
	valor_despacho, 
	total_pagado, 
	regalo, 
	nombre, 
	telefono, 
	email, 
	direccion, 
	comuna, 
	region, 
	ciudad, 
	localidad, 
	giro, 
	retiro_en_tienda, 
	retiro_en_tienda_id,
	factura, razon_social,
	rut_factura, 
	medio_de_pago, id_notificacion, nombre_retiro, apellidos_retiro, rut_retiro, despacho_por_pagar, carrier, rut, direccion_factura, telefono_factura, nombre_factura";
	$values = "NULL, '$current_user', NOW(), $estado_pendiente, '$cod', '$valor_descuento', $subtotal, $despacho, $total, 1, '$info_name', '$info_tele', '$info_email', '$direccion', '$comuna', '$region', '$ciudad', '$localidad', '$giro', $retiroEnTienda, $despacho_id, $factura, '$razon_social', '$rut_factura', '$medioPago', 0, '$nombre_retiro', '$apellidos_retiro','$rut_retiro', $despacho_por_pagar, '$carrier', '$info_rut', '$direccion_factura', '$telefono_factura', '$nombre_factura'";
	
    $generar_pedido = insert_bd("pedidos","$campos","$values");
			
	$pedido_id = mysqli_insert_id($conexion);

	$new_oc = $oc.$pedido_id;
	update_bd('pedidos', "oc = '$new_oc'", "id = $pedido_id");

	/* CARRO ABANDONADO */
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
	$pdca = $_COOKIE[cart_alfa_cm];
	update_bd("carros_abandonados","oc='$new_oc'","productos = '$pdca' and correo = '$info_email'");
	$notificacion = (isset($_SESSION[notificacion])) ? mysqli_real_escape_string($conexion, $_SESSION[notificacion]) : 0;

	$cantTotal = 0;

		foreach ($contents as $id=>$qty) {
			//consulto el precio segun la lista de precios
			$prod = consulta_bd("pd.precio, pd.descuento, p.pack, p.codigos, pd.sku, pd.id, p.id, pd.precio_cyber","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
			////Veo si el producto es un pack, lo manejo de cierta forma///////////////////////
			if($prod[0][2] == 1){
				
				$precio_final = $prod[0][0];
				$codigo_pack = $prod[0][4];
				$codigosProductos = trim($prod[0][3]);
				$productosPack = explode(",", $codigosProductos);
				
				
				//saco la suma de los productos reales
				$totalReal = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle = consulta_bd("precio","productos_detalles","sku='$skuPack'","");
					$totalReal = $totalReal + $valorProdDetalle[0][0];
				}
				//obtengo el porcentaje de descuento
				$descPorcentajeProducto = ($totalReal - $precio_final)/$totalReal;
				//$descPorcentajeProducto2 = (($precio_final - $totalReal)/$totalReal) * -1;
				//die("$descPorcentajeProducto /////-///// $descPorcentajeProducto2");
				
				$retorno = "";
				$idPd = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle2 = consulta_bd("precio, id","productos_detalles","sku='$skuPack'","");
					
					$precioUnitario = $valorProdDetalle2[0][0];
					$idProductoDetalle = $valorProdDetalle2[0][1];
					
					$precioDescuento = $valorProdDetalle2[0][0] * (1 - $descPorcentajeProducto);
					$precioTotal = round($precioDescuento * $qty);
					
					$desc = 1 - $descPorcentajeProducto;
					//die("$precioUnitario  --  $precioTotal");
					$sku = $skuPack;
					
					
					$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, descuento, codigo, precio_total, fecha_creacion, codigo_pack";
					$values = "$pedido_id,$idProductoDetalle,'$qty',$precioUnitario, $precioDescuento, '$sku', $precioTotal, NOW(), '$codigo_pack'";
					$detalle = insert_bd("productos_pedidos","$campos","$values");
					$cantTotal = $cantTotal + $qty;
				}//fin foreach
				
				
				
			} else {
			///////////////////////////
			
				/*if($prod[0][1] > 0){
					$precio_final = $prod[0][1];
					$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
				}else{
					$precio_final = $prod[0][0];
					$descuento = 0;
				}*/
				$descuento = 0;
                if ($is_cyber AND is_cyber_product($id)) {
					$precios = get_cyber_price($id);
					$precio_final = $precios['precio_cyber'];
					$is_qty_price = 0;
				}else{
					$pd = consulta_bd("precio_cantidad","productos_detalles","id = $id","");
					if($pd[0][0]){
						$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $id","rango DESC");
						if($precios_cantidad){
							$pc = $precios_cantidad[0];
							$rango 		= $pc[1];
							$descuento 	= $pc[2];
							$precio_final = (getPrecio($id) - (getPrecio($id) * ($descuento / 100)));
							$is_qty_price = 1;
						}else{
							$precio_final = getPrecio($id);
							$is_qty_price = 0;
						}
					}else{
						$precio_final = getPrecio($id);
						$is_qty_price = 0;
					}
				}
				
                
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario * $qty;
				$porcentaje_descuento = $descuento;//$prod[0][1];//$valoresFinales[descuento];
				
				$all_sku = consulta_bd("sku","productos_detalles","id=$id","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */
	
				$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion, descuento, compra_dc";
				$values = "$pedido_id, $id, $qty, $precioUnitario,'$sku', $precioTotal, NOW(), $porcentaje_descuento, $is_qty_price";
				$detalle = insert_bd("productos_pedidos","$campos","$values");
				$cantTotal = $cantTotal + $qty;
			}//fin del ciclo


			////////////////// ----------- INSERTO STOCK TEMPORAL ----------- //////////////////


				$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

				if($isPack[0][0]){
					$codes_pack = explode(',', $isPack[0][1]);
					foreach ($codes_pack as $value){
						$value 			= trim($value);
						$stock  = consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku='$value'","");
						$stock_temporal = insert_bd("stock_temporal","oc, sku, stock, fecha_creacion","'$new_oc','".$stock[0][3]."', $qty, NOW()");
					}

				}else{

		        	$stock  = consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
					$stock_temporal = insert_bd("stock_temporal","oc, sku, stock, fecha_creacion","'$new_oc','".$stock[0][3]."', $qty, NOW()");

				}


			////////////////// ----------- ////////////////// ----------- //////////////////
		}

		$qtyFinal = update_bd("pedidos","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$pedido_id");

        unset($_SESSION["notificacion"]);

        $token = generateToken($new_oc, $total);

    if ($medioPago == 'mercadopago') {
    	// mpago 
		$mp = new MP ($keys['client_id'], $keys['client_secret']);

		$preference_data = array(
			"items" => array(
				array(
					"id" => $new_oc,
					"title" => 'Compra de productos '.opciones('nombre_cliente'),
					"currency_id" => "CLP",
					"picture_url" =>"https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
					// "description" => "Description",
					// "category_id" => "Category",
					"quantity" => 1,
					"unit_price" => $total
				)
			),
			"payment_methods" => array(
				"excluded_payment_methods" => array(
					array(
						"id" => "khipu"
					)
				)
			),
			"payer" => array(
		        "first_name" => $info_email,
		        "email" => $info_name
		    ),
			"back_urls" => array(
				"success" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=success&cod={$cod}",
				"failure" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=failure&cod={$cod}",
				"pending" => $url_base . "tienda/mercadopago/casepay.php?oc={$new_oc}&case=pending&cod={$cod}"
			),	
			"auto_return" => "approved",
		   "notification_url" => "{$url_base}tienda/mercadopago/notificacion.php?oc={$new_oc}&cod={$cod}",
		   "expires" => false,
		   "expiration_date_from" => null,
		   "expiration_date_to" => null

		);

		$preference = $mp->create_preference($preference_data);
    }elseif($medioPago == 'transferencia'){

    	include('../PHPMailer/PHPMailerAutoload.php');
		include('../vendor/autoload.php');
		include('../tienda/mailchimp.class.php');

		update_bd('pedidos', "estado_envio = 'Pendiente de pago'", "oc = '$new_oc'");

		//obtengo el id del pedido
	    $id_pedido = consulta_bd("id, retiro_en_tienda","pedidos","oc='$new_oc'","");
	    $reducirStock = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id=".$id_pedido[0][0],"");

		for($i=0; $i<sizeof($reducirStock); $i++){
	        $cantActual = consulta_bd("id,stock","productos_detalles","id=".$reducirStock[$i][0],"");
	        $qtyFinal = $cantActual[0][1]- $reducirStock[$i][1];
		
	        update_bd("productos_detalles","stock=$qtyFinal","id=".$reducirStock[$i][0]);
	    }

	    //si existe codigo de descuento, lo descuento del total y elimino la sesion
			
		if($_SESSION["descuento"]){
			$codigo = $_SESSION["descuento"];
			$codigoActual = consulta_bd("oc","codigo_descuento","codigo = '$codigo'","");
			$ocActualizada = $codigoActual[0][0].$new_oc.',';
			$desactivoCodigo = update_bd("codigo_descuento","fecha_modificacion = NOW(),fecha_uso = NOW(), oc = '$ocActualizada', usados = usados +1","codigo = '$codigo'");
			unset($_SESSION["descuento"]);
	        unset($_SESSION["val_descuento"]);
		}

		//funciones para avisar al cliente y al administrador de la venta 
	    // Se ejecutan con la funcion exec para agilizar el proceso de carga y que el envío de correo pase a segundo plano.
			enviarComprobanteCliente($new_oc);
			enviarComprobanteAdmin($new_oc);

		//elimino la cookie del carro de compras
		setcookie('cart_alfa_cm', null, -1, '/');

		header('Location: '.opciones("url_sitio").'/exito?oc='.$new_oc);
		die();

    }

} else {
	header("location:404");
}
?>
<div style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:999; background-image: url(<?= $url_base; ?>tienda/trama.png);">
    <div style="margin: 0 auto; width: 100%;text-align:center;font-family:verdana;font-size: 12px;padding-top:200px;">
        <img src="<?php echo $url_base; ?>img/logo.svg" border="0" /><br /><br />
        <?php if ($medioPago == 'webpay'): ?>
        	Estamos redireccionando a transbank...
        <?php elseif($medioPago == 'mercadopago'): ?>
        	Estamos redireccionando a mercado pago...
        <?php endif ?>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<?php if($medioPago == 'webpay'){ ?>
<form id="form" action="tienda/webpay/tbk-normal.php" method="post" >
    <input type="hidden" name="TBK_MONTO" value="<?php echo $token; ?>" />
    <input type="hidden" name="TBK_ORDEN_COMPRA" value="<?php echo $new_oc;?>" />
    <input type="hidden" name="TBK_ID_SESION" value="<?php echo $new_oc;?>" />
    <input type="hidden" name="URL_RETURN" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal.php?action=result" />
    <input type="hidden" name="URL_FINAL" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal.php?action=end" />
</form>
<?php } else { ?>
    <script type="text/javascript">
		setTimeout(function(){
		window.location.href = "<?=$preference["response"]["init_point"]?>";
		}, 1000); // 2Segs
	</script>
<?php } ?>

<?php mysqli_close($conexion); ?>

<script type="text/javascript">
$(function(){
	$('#form').submit();
});
</script>