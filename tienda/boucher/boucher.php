<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div class="contenedor">
    	<div class="header"><img src="logoCliente.jpg" width="187" /></div>
        <h2>¡MUCHAS GRACIAS POR COMPRAR CON NOSOTROS!</h2>
        <h3>SU ORDEN DE COMPRA ES:</h3>
        <h4>OC_ODC456388SD</h4>
        
        <div class="titulo"><span>DATOS DE COMPRA</span></div>
        
        <div class="filaDato">
        	<div class="tituloDato">Nombre comprador:</div>
            <div class="valorDato">Hernan Torres</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Rut:</div>
            <div class="valorDato">12637468-5</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Fecha y hora de Venta:</div>
            <div class="valorDato">30/07/2015  /  15: 03:57</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Tipo de transacción:</div>
            <div class="valorDato">Venta</div>
        </div>
        
        <div class="filaDato">
        	<div class="tituloDato">Tipo de pago:</div>
            <div class="valorDato">Red Compra - debito</div>
        </div>
        
        <div class="filaDato filaGris">
        	<div class="tituloDato">Tarjeta terminada en:</div>
            <div class="valorDato">**** **** **** 6623</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Número de coutas:</div>
            <div class="valorDato">4 coutas</div>
        </div>
        <div class="filaDato filaGris">
        	<div class="tituloDato">Valor pagado:</div>
            <div class="valorDato">$15.000</div>
        </div>
        <div class="filaDato">
        	<div class="tituloDato">Código de autorización:</div>
            <div class="valorDato">238452</div>
        </div>
        
        <div class="totales">
         	<div class="filaDato">
            	<div class="valorDato">14</div>
                <div class="tituloDato">cantidad de productos:</div>
            </div>
            <div class="filaDato">
            	<div class="valorDato">$10004</div>
                <div class="tituloDato">Sub total:</div>
            </div>
            <div class="filaDato">
            	<div class="valorDato">$104</div>
                <div class="tituloDato">Valor IVA:</div>
            </div>
            <div class="filaDato">
            	<div class="valorDato">$0</div>
                <div class="tituloDato">Valor envío:</div>
            </div>
            <div class="filaDato">
            	<div class="valorDato total">$144.928</div>
                <div class="tituloDato total">Total</div>
            </div>
        </div><!--fin totales-->
        
        
        <div class="footer">
        	Todos los derechos reservados. IMAHE. 2015
        </div>
        
        <div style="clear:both"></div>
    </div>
</body>
</html>