<?php
include("../../admin/conf.php");
require_once('../../PHPMailer/PHPMailerAutoload.php');
require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

require('../../vendor/autoload.php');
include('../../tienda/mailchimp.class.php');
/**
 * @author     Allware Ltda. (http://www.allware.cl)
 * @copyright  2015 Transbank S.A. (http://www.tranbank.cl)
 * @date       Jan 2015
 * @license    GNU LGPL
 * @version    1.0
 */

require_once( 'libwebpay/webpay.php' );
require_once( 'certificates/cert-normal.php' );

/* Configuracion parametros de la clase Webpay */
//$sample_baseurl = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

$monto = mysqli_real_escape_string($conexion, $_POST[TBK_MONTO]); 
$oc = $_POST[TBK_ORDEN_COMPRA];  
$idSesion = $_POST[TBK_ID_SESION];  
$result = $_POST[URL_RETURN];  
$final = $_POST[URL_FINAL];
$exito = opciones("url_sitio")."/exito?oc=";
$fracaso = opciones("url_sitio")."/fracaso?oc=";


if(isset($_POST[TBK_ORDEN_COMPRA]) and $_POST[TBK_ORDEN_COMPRA] != ''){
    $_SESSION['ocRechazo'] = $_POST[TBK_ORDEN_COMPRA];
   // echo $_SESSION['ocRechazo']."Siexiste";
} else {
    //echo $_SESSION['ocRechazo']."noexiste";
}


//echo '<h1>'.$_SESSION['ocRechazo'].'<h1>';
//var_dump($_SESSION['ocRechazo']);

function write_log($tipo,$cadena){
	if (file_exists("log/TBK-WS_".date("Y-m-d").".log")) {
        //si existe no hago nada
    } else {
        //si no existe lo creo
        $fileName = "log/TBK-WS_".date("Y-m-d").".log";
        $contenidoInicial = "Log transbank ".date("Y-m-d")."\n";
        file_put_contents($fileName, $contenidoInicial);
    }
    $arch = fopen("log/TBK-WS_".date("Y-m-d").".log", "a+"); 
    fwrite($arch, $tipo.": ".$cadena."\n");
	fclose($arch);
}



$webpay_settings = array(
	"MODO" => "PRODUCCION",
	"PRIVATE_KEY" => $certificate['private_key'],
	"PUBLIC_CERT" => $certificate['public_cert'],
	"WEBPAY_CERT" => $certificate['webpay_cert'],
	"COMMERCE_CODE" => $certificate['commerce_code'],
	"URL_RETURN" => $result,
	"URL_FINAL" => $final
);

/* Creacion Objeto Webpay */
$webpay = new WebPaySOAP($webpay_settings); // Crea objeto WebPay
$webpay = $webpay->getNormalTransaction(); // Crea Transaccion Normal

$action = isset($_GET["action"]) ? $_GET["action"]: 'init';
$montoAPagar = consulta_bd("total_pagado","pedidos","oc='$oc'","");

switch ($action) {

	default: 
 		$tx_step = "Init";

        $error = 0;
        if(is_numeric($monto)){
            $message  = "======================================================================= \r\n";
            $message .= "ERROR!: La variable monto es numérica (Posible intento de hackeo). OC = {$oc} \r\n";
            $message .= "=======================================================================";
            write_log($message);
            $error = 1;
        }else{
            $monto_decode = consulta_bd('monto, token', 'checkout_token', "token = '$monto'","");
            $fecha = date("d-m-Y H");

            if ($monto_decode < 1) {
                $message = "======================================================================== \r\n";
                $message .= "ERROR!: Token no autorizado para la compra (Posible intento de hackeo). \r\n";
                $message .= "------------------------------------------------------------------------ \r\n";
                $message .= "Token Ingresado: {$monto_decode[0][0]} \r\n";
                $message .= "Fecha Operación: {$fecha} \r\n";
                $message .= "Orden de compra: {$oc} \r\n";
                $message .= "=======================================================================";
                write_log($message);
                $error = 1;
            }
        }

        if ($error == 1) {
            del_bd_generic('checkout_token', 'oc', "{$oc}");
            header("Location: $fracaso".$_SESSION['ocRechazo']);
            die();
        }

        $request = array(
  			"amount"    => $monto_decode[0][0],      // monto a cobrar
  			"buyOrder"  => $oc,    // numero orden de compra
  			"sessionId" => $idSesion, // idsession local
  		);
		
        // Iniciamos Transaccion
 		$result = $webpay->initTransaction($request["amount"], $request["sessionId"], $request["buyOrder"]);

        //die(print_r($result));

        $webpay_token = $result["token_ws"];
        
        
       // Verificamos respuesta de inicio en webpay
		if (strlen($webpay_token)) {
			$message = "Sesion iniciada con exito en Webpay";
			$next_page = $result["url"];
            write_log("     Response Mensaje","$message");
        } else {
			$message = "webpay no disponible";
            write_log("     Response Mensaje","$message");
            //al no coinidir los certificados retorno al fracaso
            header("Location: $fracaso".$_SESSION['ocRechazo']);
		}

		break;

	case "result":
 		$tx_step = "Get Result";
		if (!isset($_POST["token_ws"])) break;
		
		$webpay_token = $_POST["token_ws"];
		$request = array( 
			"token"  => $_POST["token_ws"]
		);

		// Rescatamos resultado y datos de la transaccion
		$result = $webpay->getTransactionResult($request["token"], $fracaso);  
       
        //datos que retornan de transbank
        $accountingDate = $result->accountingDate;
            $buyOrder = $result->buyOrder;
            $cardNumber = $result->cardDetail->cardNumber;
            $cardExpirationDate = $result->cardDetail->cardExpirationDate;
            
            $authorizationCode = $result->detailOutput->authorizationCode;
            $paymentTypeCode = $result->detailOutput->paymentTypeCode;
            $responseCode = $result->detailOutput->responseCode;
            $sharesNumber = $result->detailOutput->sharesNumber;
            $amount = $result->detailOutput->amount;
            $commerceCode = $result->detailOutput->commerceCode;
            
            $transactionDate = $result->transactionDate;   
            $VCI = $result->VCI;
            $token = $_POST["token_ws"];
        
        
            ////////////////// ----------- ELIMINO STOCK TEMPORAL ----------- //////////////////

                $stock_temporal = del_bd_generic("stock_temporal","oc","$buyOrder");

            ////////////////// ----------- ////////////////// ----------- //////////////////


		// Verificamos resultado del pago
        //si el pago es exitoso
		if ($result->detailOutput->responseCode===0) {
			$message = "Pago ACEPTADO por webpay (se deben guardatos para mostrar voucher)";
			$next_page = $result->urlRedirection;
			
            write_log("Resultado","");
            write_log("     Exito","$message");
            
            $next_page_title = "Finalizar Pago";

            $tipo_pago_ov = payment_type('webpay', $paymentTypeCode);
            
            $estado = 2;
            //actualizao la bd con los datos recibidos
            update_bd("pedidos","accounting_date = '$accountingDate', card_number = '$cardNumber', card_expiration_date = '$cardExpirationDate', authorization_code = '$authorizationCode', payment_type_code = '$paymentTypeCode', response_code = '$responseCode', shares_number = '$sharesNumber', amount = '$amount', commerce_code = '$commerceCode', transaction_date = '$transactionDate', vci = '$VCI', token = '$token', estado_id = $estado, tipo_pago = '$tipo_pago_ov'","oc='$buyOrder'");
            
            //obtengo el id del pedido
            $id_pedido = consulta_bd("id, retiro_en_tienda","pedidos","oc='$buyOrder'","");
            $reducirStock = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id=".$id_pedido[0][0],"");
            //redusco el stock de los productos comprados.
            for($i=0; $i<sizeof($reducirStock); $i++){
                $cantActual = consulta_bd("id,stock","productos_detalles","id=".$reducirStock[$i][0],"");
                $qtyFinal = $cantActual[0][1]- $reducirStock[$i][1];
				
				//si no hay productos disponibles despublico la entrada.
				if($qtyFinal == 0){
					$despublico = ', publicado = 0';
				}
                update_bd("productos_detalles","stock=$qtyFinal $despublico","id=".$reducirStock[$i][0]);
            }
			
			//si existe codigo de descuento, lo descuento del total y elimino la sesion
			
			if($_SESSION["descuento"]){
				$codigo = $_SESSION["descuento"];
				$codigoActual = consulta_bd("oc","codigo_descuento","codigo = '$codigo'","");
				$ocActualizada = $codigoActual[0][0].$buyOrder.',';

                // $codigo_primera = update_bd("first_buy", "usado = usado+1", "codigo = '$codigo'");

				$desactivoCodigo = update_bd("codigo_descuento","fecha_modificacion = NOW(),fecha_uso = NOW(), oc = '$ocActualizada', usados = usados +1","codigo = '$codigo'");
				unset($_SESSION["descuento"]);
                unset($_SESSION["val_descuento"]);
			}
			
            //funciones para avisar al cliente y al administrador de la venta
			//en el envio del cliente se guarda en mailchimp el correo 

            if ($id_pedido[0][1] == 0) {
                exec("php -f ../../ajax/postEnviame.php {$id_pedido[0][0]}", $output, $return_var);
            }else{
                update_bd('pedidos', "estado_envio = 'Retiro en tienda'", "oc = '$buyOrder'");
            }

            //funciones para avisar al cliente y al administrador de la venta 
            // Se ejecutan con la funcion exec para agilizar el proceso de carga y que el envío de correo pase a segundo plano.
            // exec("php -f ../../ajax/enviarCorreoAdmin.php $buyOrder", $output, $return_var);
            // exec("php -f ../../ajax/enviarCorreoCliente.php $buyOrder", $output, $return_var);
            
            enviarComprobanteAdmin($buyOrder);
            enviarComprobanteCliente($buyOrder);
            
            //elimino la cookie del carro de compras
            setcookie('cart_alfa_cm', null, -1, '/');

            /* Proceso email primera compra */
            /*$seEnvia = enviarCodigo($buyOrder);

            if ($seEnvia) {

                $id_cliente = consulta_bd('c.id', 'clientes c join pedidos p on c.email = p.email', "p.oc = '{$buyOrder}'");

                $hoy = date('Y-m-d'); // Fecha desde

                // Agrego al usuario a la tabla primera compra.
                insert_bd('first_buy', 'cliente_id, fecha_compra, oc', "{$id_cliente[0][0]}, '{$hoy}', '{$buyOrder}'");

            }*/
			
			//elimino la cookie del carro de compras
			// setcookie('cart_alfa_cm', null, -1, '/');
			
            //generar correo con codigo de descuento para segunda compra
			//enviarCodigoDescuento($buyOrder);
			
			
			
		} else {
			$message = "Pago RECHAZADO por webpay - ".utf8_decode($result->detailOutput->responseDescription);
            write_log("Resultado","");
            write_log("     Rechazo","$message");
            
            
            $next_page= $fracaso.$_SESSION['ocRechazo'];
            $estado = 3;
            update_bd("pedidos","accounting_date = '$accountingDate', card_number = '$cardNumber', card_expiration_date = '$cardExpirationDate', authorization_code = '$authorizationCode', payment_type_code = '$paymentTypeCode', response_code = '$responseCode', shares_number = '$sharesNumber', amount = '$amount', commerce_code = '$commerceCode', transaction_date = '$transactionDate', vci = '$VCI', token = '$token', estado_id = $estado","oc='$buyOrder'");
            //header("Location: $fracaso.$buyOrder");

            save_in_mailchimp($_SESSION['ocRechazo'], 'fracaso');
            save_in_mailchimp($_SESSION['ocRechazo'], 'todas_compras');
		}

		break;
		

	case "end":
        $token_ws = $_POST[token_ws];
        $oc = consulta_bd("oc","pedidos","token='$token_ws' and vci <> ''","");
        $cantresult = mysqli_affected_rows($conexion);
        if($cantresult > 0){
            
            $tx_step = "End";
            write_log("Transaccion exitosa","$tx_step");

            $request= '';
            $result = $_POST;
            $message = "Transacion Finalizada";
            $next_page = $exito.$oc[0][0];
            break;
        } else {
            $tx_step = "End";
            write_log("Transaccion Cancelada","$tx_step");
            $request= '';
            $result = $_POST;
            $message = "Transacion Finalizada";
            $next_page= $fracaso.$_SESSION['ocRechazo'];

            save_in_mailchimp($_SESSION['ocRechazo'], 'pendiente');
            save_in_mailchimp($_SESSION['ocRechazo'], 'todas_compras');
            
            //header("Location: $fracaso".$_POST[TBK_ORDEN_COMPRA]);
            break;
        }
       
}

del_bd_generic('checkout_token', 'oc', "{$oc}");

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!--<h2><?php echo "Step: ".$tx_step ?> <?php echo $_SESSION['ocRechazo']; ?></h2>

<div style="background-color:lightyellow;">
	<h3>request</h3>
	<?php  //var_dump($request); ?>
</div>
<div style="background-color:lightgrey;">
	<h3>result</h3>
	<?php  //var_dump($result); ?>
</div>
<p><samp><?php  echo $message; ?></samp></p>-->
<?php 
    if (strlen($next_page)) {    
?>
<form style="display:none;" action="<?php echo $next_page; ?>" method="post" id="formProceso">
	<input type="hidden" name="token_ws" value="<?php echo $webpay_token; ?>">
	<input type="submit" value="Continuar &raquo;">
</form>
<?php } ?>


<script type="text/javascript">
    $(function(){
        $('#formProceso').submit();	
    });
</script>
