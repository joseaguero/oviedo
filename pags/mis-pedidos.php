<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "inicio-sesion"</script>';
    }

$pedidos = get_pedidos($_COOKIE['usuario_id']);

?>

<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">cuenta</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">mis pedidos</span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="content_gray">
  
  <div class="content-dashboard">
    <h2 class="title-dash">Mi cuenta</h2>
    <div class="grid-dashboard">
      <div class="col">

        <?php include("pags/menuMiCuenta.php"); ?>

      </div>
      <div class="col">
        
        <div class="main_dash">
          
            <div class="title">Mis pedidos</div>
            <div class="subtitle">
            Podrás ver tus pedidos, historial de compra y <br>editar tus datos personales y de envío.
            </div>
          
            <?php $index = 0; foreach ($pedidos['pedidos'] as $ped): ?>
            <div class="row_pedidos">
                <div class="head">
                    <div class="col">
                        <div class="titlesmall">Fecha del pedido</div>
                        <div class="data"><?= setear_fecha($ped['pedido']['fecha_creacion']); ?></div>
                    </div>
                    <div class="col">
                        <div class="titlesmall">Total pagado</div>
                        <div class="data f14">$<?= number_format($ped['pedido']['total_pagado'], 0, ',', '.') ?></div>
                    </div>
                    <div class="col">
                        <div class="titlesmall"><span>ORDEN:</span> <?= $ped['pedido']['oc'] ?></div>
                        <a href="javascript:void(0)" class="button_view">Ver</a>
                    </div>
                </div>

                <div class="content-detalles">
                    <div class="detalles">
                        <div class="col">
                            <?php if ($ped['pedido']['retiro_en_tienda'] == 1): ?>
                                <div class="title">Retiro en tienda</div>
                            <?php else: ?>
                                <div class="title">Despacho</div>
                            <?php endif ?>
                        </div>
                        <div class="col">
                            <div class="title">Pago</div>
                            <div class="data">
                                <span>Medio de pago: <?= $ped['pedido']['medio_de_pago'] ?> </span>
                                <span>Tipo de transacción: Venta </span>
                                <span>N° de Cuotas: <?= $ped['pedido']['cuotas'] ?> </span>
                                <span>Tipo de cuota: <?= $ped['pedido']['tipo_cuota'] ?> </span>
                                <span>Tarjeta terminada en:**** **** **** <?= $ped['pedido']['numero_tarjeta'] ?> </span>
                                <span>Tipo de pago: <?= $ped['pedido']['tipo_pago'] ?> </span>
                                <span>Fecha y hora de transacción: <?= date('Y-m-d | H:i:s', strtotime($ped['pedido']['fecha_creacion'])); ?></span>
                                <span>Valor pagado: $<?= number_format($ped['pedido']['total_pagado'], 0, ',', '.') ?> </span>
                                <span>Código de autorización: <?= $ped['pedido']['auth_code'] ?> </span>
                                <span>Tipo de moneda: CLP / PESO Chileno </span>
                            </div>

                            <div class="title mtop-30">Totales</div>
                            <div class="data">
                                <span>Subtotal <small>$<?= number_format($ped['pedido']['total'], 0, ',', '.') ?></small></span>
                                <span>Descuentos <small>$<?= number_format($ped['pedido']['descuento'], 0, ',', '.') ?></small></span>
                                <span>Envio <small>$<?= number_format($ped['pedido']['despacho'], 0, ',', '.') ?></small></span>
                                <span>Total cancelado <small class="redTotal">$<?= number_format($ped['pedido']['total_pagado'], 0, ',', '.') ?></small></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php 

                $productos = consulta_bd('p.id, pp.productos_detalle_id, p.thumbs, p.nombre, pp.precio_unitario, pp.compra_dc, pd.precio, pd.descuento, pd.precio_cyber', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN productos_pedidos pp ON pd.id = pp.productos_detalle_id', "pp.pedido_id = ".$ped['pedido']['id'], 'pp.id desc');

                ?>
                <?php foreach ($productos as $prd): ?>
                    <div class="body">
                        <div class="col">
                            <?php if ($prd[5] == 1): ?>
                                <div class="texto_cantidad">descuento por cantidad.</div>
                            <?php endif ?>
                            <div class="thumb_col">
                                <?php if (file_exists('imagenes/productos/'.$prd[2])) { 
                                    $thumb = imagen("imagenes/productos/", $prd[2]); ?>
                                    
                                    <img src="<?= $thumb ?>">

                                <?php }else{ ?>
                                    <img src="img/sin-foto.jpg">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="nombre">
                                <?= $prd[3] ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="precio">
                                <small class="txt_dis">Compraste en:</small>
                                $<?= number_format($prd[4], 0, ',', '.') ?>
                                <small class="txt_dis">Precio actual:</small>
                                <?php if ($is_cyber AND is_cyber_product($prd[1])): ?>
                                    $<?= number_format($prd[8], 0, ',', '.') ?>
                                <?php endif ?>
                                <?php if ($prd[7] > 0): ?>
                                    $<?= number_format($prd[7], 0, ',', '.') ?>
                                <?php else: ?>
                                    $<?= number_format($prd[6], 0, ',', '.') ?>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col">
                            <a href="javascript:void(0)" class="volver_a_comprar btn" data-id="<?= $prd[1] ?>">volver a comprar</a>
                        </div>
                    </div>
                <?php endforeach ?>
                
            </div>
            <?php $index++; endforeach ?>
        
        </div>

      </div>
    </div>
  </div>
  
</div>
