<?php 

$sucursales = consulta_bd('s.id, s.nombre, s.direccion, r.nombre, c.nombre, s.telefono, s.contacto, s.mayoristas, s.imagen', 'sucursales s JOIN comunas c ON c.id = s.comuna_id JOIN regiones r ON r.id = c.region_id', 's.publicado = 1 and s.retiro_en_tienda = 1', 's.id asc');

?>
<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">Tiendas</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="container mtop-20 mbottom-20">
    <div class="titulo_p">Tiendas</div>

    <div class="grid-sucursales mtop-20">
        <?php foreach ($sucursales as $sucursal):
            $img_s = imagen("imagenes/sucursales/", $sucursal[8]); ?>
            <div class="col">
                <div class="imagen_s">
                    <img src="<?= $img_s ?>">
                </div>

                <span class="title_s"><?= $sucursal[1] ?></span>
                <span class="data"><?= $sucursal[2] ?></span>
                <span class="data"><?= $sucursal[4] ?>, <?= $sucursal[3] ?></span>
                <?php if ($sucursal[5] != NULL AND $sucursal[5] != ''): ?>
                    <span class="data"><strong>Teléfono:</strong> <?= $sucursal[5] ?></span>
                <?php endif ?>
                <?php if ($sucursal[6] != NULL AND $sucursal[6] != ''): ?>
                    <span class="data"><strong>Contacto:</strong> <?= $sucursal[6] ?> </span>
                <?php endif ?>
                <?php if ($sucursal[7] != NULL AND $sucursal[7] != ''): ?>
                    <span class="data"><strong>Mayoristas:</strong> <?= $sucursal[7] ?> </span> 
                <?php endif ?>
            </div>
        <?php endforeach ?>
    </div>

</div>
