<?php 
    if(!isset($_COOKIE['usuario_id'])){
        echo '<script>location.href = "inicio-sesion"</script>';
    }

    $productos = get_productos_guardados($_COOKIE['usuario_id']);
?>

<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">cuenta</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">productos guardados</span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="content_gray">
  
  <div class="content-dashboard">
    <h2 class="title-dash">Mi cuenta</h2>
    <div class="grid-dashboard">
      <div class="col">

        <?php include("pags/menuMiCuenta.php"); ?>

      </div>
      <div class="col">
        
        <div class="main_dash">
          
            <div class="title mbottom-20">Productos guardados</div>
              <!-- <div class="subtitle">
                Podrás ver tus pedidos, historial de compra y <br>editar tus datos personales y de envío.
              </div> -->
              
            <?php if ($productos != false): ?>
                <?php foreach ($productos as $p): ?>

                    <div class="row_guardados">
                        <div class="body">
                            <div class="col">
                                <div class="thumb_col">
                                    <?php if ($p['imagen'] != 'img/sin-foto.jpg'): ?>
                                        <?php if (file_exists("imagenes/productos/{$p['imagen']}")): 
                                            $thumb = imagen("imagenes/productos/", $p['imagen']); ?>
                                            <img src="<?= $thumb ?>">
                                        <?php else: ?>
                                            <img src="img/sin-foto.jpg">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?= $p['imagen'] ?>">
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="col">
                                <div class="nombre">
                                    <?= $p['nombre'] ?>
                                </div>
                            </div>
                            <div class="col">
                                <div class="precio">
                                    <span>Total item</span>
                                    <?php if ($p['descuento'] > 0): ?>
                                        $<?= number_format($p['descuento'], 0, ',', '.') ?>
                                    <?php else: ?>
                                        $<?= number_format($p['precio'], 0, ',', '.') ?>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="col">
                                <div class="accionesGuardados">
                                    <a href="javascript:void(0)" class="eliminarGuardado" data-id="<?= $p['id'] ?>">Eliminar</a> | 
                                    <a href="javascript:void(0)" class="moverAlCarro" onclick="moveToCart(<?= $p['id'] ?>, 'dash')">Mover al carro</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                <?php endforeach ?>
            <?php else: ?>
                <div>No tienes productos en favoritos.</div>
            <?php endif ?>
            
            </div>

      </div>
    </div>
  </div>
  
</div>

<div class="bg_popup"></div>
<div class="popup_acciones">
  <div class="title_pop">¿Estás seguro de eliminar esta publicación?</div>

  <div class="content_buttons">
    <a href="javascript:void(0)" class="confirmPopF" data-action="delete" data-id="0" data-where="dash">Confirmar</a>
    <a href="javascript:void(0)" class="cancelPop">Cancelar</a>
  </div>
</div>

<?php if (isset($_SESSION['flash'])): ?>
    <script type="text/javascript">
        $(function(){
            swal('', "<?= $_SESSION['flash']['message'] ?>", "<?= $_SESSION['flash']['status'] ?>");
        })
    </script>
<?php unset($_SESSION['flash']); endif ?>