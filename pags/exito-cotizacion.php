<script>
<?php 
	if(opciones("cotizador") != 1){
		echo 'location.href = "404"';
	}
?>
</script>
<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Cotizaciona exitosa</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100">
    <div class="cont100Centro">
        <div class="ctndr100">

        <h2 class="titulos_interiores">COTIZACIÓN EXITOSA</h2><!--fin titulos_interiores -->


		<div class="top-identificacion">
            <div>¡MUCHAS GRACIAS POR COTIZAR CON NOSOTROS!</div>

            <span>Pronto nos comunicaremos con usted para responder su solicitud</span>
            <div class="codigo-4 rojo"><?php echo $oc; ?></div>
        </div>
		
       
		</div>
    </div><!--fin cont100Centro-->
</div><!--Fin breadcrumbs -->