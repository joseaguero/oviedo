<?php
    $oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
    
    $PA = consulta_bd("id, estado_id, oc","pedidos","oc='$oc'","");
	$cant = mysqli_affected_rows($conexion);
	if($oc == 'anulada'){
        update_bd("pedidos","estado_id = 3","oc='$oc'");
    } else if ($cant == 0){
		echo '<script>parent.location = "'.$url_base.'404";</script>';
	} else {
		update_bd("pedidos","estado_id = 3","oc='$oc' and payment_type_code IS NULL");
	}
	
?>

<div class="bg_exito mbottom-100">
  
    <img src="img/fracaso.png" class="exitoimg">
    
    <div class="container_small ohidden mbottom-20">
        <div class="cont100">
                <h2 class="titulos_interiores" style="position:relative;">FRACASO EN LA COMPRA</h2><!--fin titulos_interiores -->

            <div class="top-identificacion" style="margin-bottom:20px;">
                <div>Transacción Rechazada <span class="rojo"><?= $oc; ?></span><br />
                    Las posibles causas de este rechazo son:
                </div> <br><br>
                
                    <div class="txt-exito"> - Error en el ingreso de los datos de su tarjeta de crédito o Debito (fecha y/o código de seguridad).</div> <br><br>
                    <div class="txt-exito"> - Su tarjeta de crédito o debito no cuenta con el cupo necesario para cancelar la compra.</div> <br><br>
                    <div class="txt-exito"> -Tarjeta aún no habilitada en el sistema financiero. </div>
                    
                </div>
                
                <div class="cont100 contBtnFracaso">
                    <a href="home">IR AL HOME</a> <a href="mi-carro">VOLVER A INTENTARLO</a>
                </div>
        </div><!--Fin mensaje --> 
    </div>

</div>
<!--
<script type="text/javascript">
	ga('ec:setAction','checkout', {'step': 4});
</script>-->