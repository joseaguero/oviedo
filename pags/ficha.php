<?php 

$id = mysqli_real_escape_string($conexion, $_GET['id']);
$nombre = mysqli_real_escape_string($conexion, $_GET['nombre']);

$producto = consulta_bd('p.nombre, pd.sku, pd.stock, pd.precio, pd.descuento, pd.precio_cyber, m.nombre, p.descripcion_corta, p.descripcion, p.especificaciones, p.garantia, pd.id, pd.oferta_tiempo_descuento, pd.oferta_tiempo_hasta, pd.precio_cantidad, p.descripcion_seo, p.ficha_tecnica', 'productos p JOIN productos_detalles pd ON p.id = pd.producto_id JOIN marcas m ON m.id = p.marca_id LEFT JOIN lineas_productos lp ON lp.producto_id = p.id LEFT JOIN lineas l ON lp.linea_id = l.id LEFT JOIN categorias c ON lp.categoria_id = c.id', "p.id = $id AND p.publicado = 1 AND pd.publicado = 1 AND pd.principal = 1", '');

if (!is_array($producto)) {
    echo '<script>location.href="home";</script>';
}

$categoria_producto = consulta_bd('c.id, c.nombre, l.id, l.nombre', 'lineas_productos lp LEFT JOIN categorias c ON c.id = lp.categoria_id LEFT JOIN lineas l ON l.id = lp.linea_id', "lp.producto_id = $id", '');

$id_hijo = $producto[0][11];

$descripcion_corta          = $producto[0][7];
$descripcion                = $producto[0][8];
$especificaciones           = $producto[0][9];
$garantia                   = $producto[0][10];
$ficha_tecnica              = $producto[0][16];

$oferta_tiempo_descuento    = $producto[0][12];
$oferta_tiempo_hasta        = $producto[0][13];

$precio_cantidad            = $producto[0][14];

$imagenes_ficha = consulta_bd('archivo', 'img_productos', "producto_id = $id", 'posicion asc');

?>
<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">Productos</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="lineas/<?= $categoria_producto[0][2] ?>/<?= url_amigables($categoria_producto[0][3]) ?>" itemprop="item">
                <span itemprop="name"><?= $categoria_producto[0][3] ?></span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
        
        <?php if ($categoria_producto[0][0] != ''): ?>
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="categorias/<?= $categoria_producto[0][0] ?>/<?= url_amigables($categoria_producto[0][1]) ?>" itemprop="item">
                    <span itemprop="name"><?= $categoria_producto[0][1] ?></span>
                    <meta itemprop="position" content="4" />
                </a>
            </li>

            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="ficha/<?= $id ?>/<?= url_amigables($producto[0][0]) ?>" itemprop="item">
                    <span itemprop="name"><?= $producto[0][0] ?></span>
                    <meta itemprop="position" content="5" />
                </a>
            </li>
        <?php else: ?>
            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="ficha/<?= $id ?>/<?= url_amigables($producto[0][0]) ?>" itemprop="item">
                    <span itemprop="name"><?= $producto[0][0] ?></span>
                    <meta itemprop="position" content="4" />
                </a>
            </li>
        <?php endif ?>
        
    </ul>
</div>

<div class="container mtop-20 ohidden mbottom-20">

    <section class="grid_ficha">
        <div class="col">
            
            <div class="slider-ficha-nav">
                <?php if (is_array($imagenes_ficha)): ?>
                    <?php foreach ($imagenes_ficha as $img): ?>
                        <div class="item">
                            <img src="imagenes/productos/<?= $img[0] ?>">
                        </div>
                    <?php endforeach ?>
                <?php else: ?>
                    <div class="item">
                        <img src="img/not-imagef.jpg">
                    </div>
                <?php endif ?>
            </div>
            
            <div class="principal">
                <!-- Porcentaje descuento  -->
                <?php if ( ofertaTiempo($id_hijo) ): ?>
                    <div class="porcentaje_descuento">-<?= round(100 - ($oferta_tiempo_descuento * 100) / $producto[0][3]) ?>%</div>
                <?php elseif($is_cyber AND $producto[0][5] > 0): ?>
                    <div class="porcentaje_descuento">-<?= round(100 - ($producto[0][5] * 100) / $producto[0][3]) ?>%</div>
                <?php elseif( $producto[0][4] > 0 ): ?>
                    <div class="porcentaje_descuento">-<?= round(100 - ($producto[0][4] * 100) / $producto[0][3]) ?>%</div>
                <?php endif ?>
                    
                <?php if ($precio_cantidad == 1): ?>
                    <div class="descuento_cantidad">descuento por cantidad</div>
                <?php endif ?>

                <div class="slider-ficha-for">
                    <?php if (is_array($imagenes_ficha)): ?>
                        <?php foreach ($imagenes_ficha as $img_f): ?>
                            <div class="item">
                                <?php 
                                if (file_exists("imagenes/productos/{$img_f[0]}")):
                                    $ficha_img = imagen("imagenes/productos/", $img_f[0]);
                                else:
                                    $ficha_img = "img/not-imagef.jpg";
                                endif ?>
                                <a href="<?= $ficha_img ?>" class="fancybox hover_zoom" data-fancybox="gallery">
                                    <i class="fas fa-search-plus"></i>
                                </a>
                                <img src="<?= $ficha_img ?>">
                            </div>
                        <?php endforeach ?>
                    <?php else: ?>
                        <div class="item">
                            <img src="img/not-imagef.jpg">
                        </div>
                    <?php endif ?>
                    
                </div>
                
                <?php if(ofertaTiempo($id_hijo)){ ?>
                    <div class="countdown" rel="<?= $oferta_tiempo_hasta ?>">
                        <div class="col">
                            <small>Hora</small>
                            <div class="box" data-rel="hour">00</div>
                        </div>
                        <div class="col">
                            <small>Min.</small>
                            <div class="box" data-rel="minute">00</div>
                        </div>
                        <div class="col">
                            <small>Seg.</small>
                            <div class="box" data-rel="second">00</div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
        <div class="col info">
            
            <div class="brand"><?= $producto[0][6] ?></div>
            <div class="box_price_ajx">
                <?php if ($producto[0][4] > 0): ?>
                    <div class="discount">
                        Antes $<?= number_format($producto[0][3], 0, ',', '.') ?>
                    </div>
                <?php elseif(ofertaTiempo($id_hijo)): ?>
                    <div class="discount">
                        Antes $<?= number_format($producto[0][3], 0, ',', '.') ?>
                    </div>
                <?php endif ?>
                
                <div class="price ohidden">
                    <?php 
                    if ( ofertaTiempo($id_hijo) ):
                        $precio_final = number_format($oferta_tiempo_descuento, 0, ',', '.');
                        $precioFinalSinPunto = $oferta_tiempo_descuento;
                    else: 
                        if ($producto[0][4] > 0):
                            $precio_final = number_format($producto[0][4], 0, ',', '.');
                            $precioFinalSinPunto = $producto[0][4];
                        else:
                            $precio_final = number_format($producto[0][3], 0, ',', '.');
                            $precioFinalSinPunto = $producto[0][3];
                        endif;
                    endif 
                    ?>
                     <span>$<?= $precio_final ?></span>
                    <span>IVA Incluido</span>
                </div>
                
                <div class="cuotasMP">
                    <div class="logoMP">
                        <img src="img/mpagologo.png" width="100%">
                    </div>
                    <div class="txtMP">
                        Paga en 6 cuotas precio contado<br>
                        <span class="valorCuota">$<?= number_format(round($precioFinalSinPunto/6), 0, ',', '.'); ?></span>
                    </div>
                </div>
            </div>
            

            <div class="name">
                <?= strtoupper($producto[0][0]) ?>
            </div>

            <div class="sku">SKU: <?= $producto[0][1] ?></div>
            
            <?php if ($producto[0][2] > 0): ?>
                <div class="disponibilidad">Stock disponible</div>
            <?php else: ?>
                <div class="disponibilidad no-stock">Stock no disponible</div>
            <?php endif ?>

            <div class="views">Actualmente <?= rand(1, 10) ?> personas estan viendo este producto</div>

            <div class="descripcion_corta">
                <?= $descripcion_corta ?>
            </div>

        </div>
        <div class="col">
            
            <div class="box_main_qty">
                
                
                <?php if ($producto[0][2] > 0){ ?>
                <div class="box_qty" data-id="<?= $id_hijo ?>">
                    <span>Cantidad</span>
                    <input type="text" class="cantidad_ficha" value="1" readonly>
                    <a href="javascript:void(0)" rel="<?= $id_hijo ?>" id="btn_agregarCarro" class="addToCart btn">Agregar al carro</a>
                </div>
                <?php } else { ?>
                    <div class="box_qty">
                        <a href="whatsapp://send?phone=+56939127048" id="" class="btnWhatsApp"><i class="fab fa-whatsapp"></i> <span>Consultar por este producto</span></a>
                    </div>
                <?php } ?>
                
                <div class="box_select_stock">

                    <?php if ($producto[0][2] > 10): ?>
                        
                        <?php for ($i=0; $i < 10; $i++) { ?>
                            <div class="row_stock_ficha" data-val="<?= $i+1 ?>"><?= $i+1 ?></div>
                        <?php } ?>

                        <div class="row_stock_ficha" data-val="add">+ más</div>
                    <?php else: ?>
                        <?php for ($i=0; $i < $producto[0][2]; $i++) { ?>
                            <div class="row_stock_ficha" data-val="<?= $i+1 ?>"><?= $i+1 ?></div>
                        <?php } ?>
                    <?php endif ?>
                    
                </div>
            </div>
            
            <?php if ($producto[0][2] > 0): ?>
                <div class="box_retiro">
                    <div class="column">
                        <img src="img/home.svg" alt="Despacho">
                        <span class="title">Despacho a domicilio</span>
                        <span class="d_dis">Disponible</span>
                    </div>
                    <div class="column">
                        <img src="img/store.svg" alt="Despacho">
                        <span class="title">Retiro en tienda</span>
                        <span class="d_dis">Disponible</span>
                    </div>
                </div>
            <?php else: ?>
                <div class="box_retiro">
                    <div class="column">
                        <img src="img/home.svg" alt="Despacho">
                        <span class="title">Despacho a domicilio</span>
                        <span class="d_not">No disponible</span>
                    </div>
                    <div class="column">
                        <img src="img/store.svg" alt="Despacho">
                        <span class="title">Retiro en tienda</span>
                        <span class="d_not">No disponible</span>
                    </div>
                </div>
            <?php endif ?>         

            <!-- <div class="envio_gratis">Envío <strong>GRATIS</strong> en pedidos superiores a <strong>$100.000</strong></div> -->

            <!-- <div class="mensaje_envio">Recíbelo entre el ju., 8 ag. - vi., 9 ag</div> -->
            
            <?php if (isset($_COOKIE['usuario_id'])): ?>
                <?php if (producto_guardado($_COOKIE['usuario_id'], $id)): ?>
                    <a href="javascript:void(0)" class="agregarFavorito wishlist" data-id="<?= $id ?>" data-action="remove"><i class="fas fa-heart"></i></i><span>Añadido a la lista de deseos</span></a>
                <?php else: ?>
                    <a href="javascript:void(0)" class="agregarFavorito wishlist" data-id="<?= $id ?>" data-action="add"><i class="far fa-heart"></i><span>Añadir a la lista de deseos</span></a>
                <?php endif ?>
            <?php else: ?>
                <a href="javascript:void(0)" class="agregarFavorito wishlist" data-id="<?= $id ?>" data-action="add"><i class="far fa-heart"></i><span>Añadir a la lista de deseos</span></a>
            <?php endif ?>

            <?php $precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","productos_detall_id =".$id_hijo,""); ?>

            <?php if($precios_cantidad){ ?>
                <div class="content_cantidades">
                    <?php foreach($precios_cantidad as $pc){ ?>
                        <div class="preciocant"> 
                            <div class="col">
                                <span class="si_compras">Si compras</span> 
                                <span class="nombre"><?= $pc[0]; ?></span>
                            </div>
                            <div class="col">
                                <div class="precio"> 
                                    <small>$</small>
                                    <span>
                                        <?= number_format(round($precioFinalSinPunto - (($precioFinalSinPunto * $pc[2]) / 100)), 0, ',', '.'); ?>
                                    </span>
                                </div>
                                <span class="x_unidad">x unidad</span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

        </div>
    </section>


    <section class="acordeon_ficha">
        <?php if ($descripcion != NULL AND trim($descripcion) != ''): ?>
            <div class="box">
                <div class="title"><i class="material-icons">add_circle</i> Descripción</div>
                <div class="body">
                    <?= $descripcion ?>
                </div>
            </div>
        <?php endif ?>


        <?php 
        $fichas = consulta_bd('nombre,valor', 'fichas_tecnicas', "producto_id = $id", 'id asc'); 
        ?>
        <?php if (is_array($fichas)): ?>
            <div class="box">
                <div class="title"><i class="material-icons">add_circle</i> Ficha técnica</div>
                <div class="body">
                    <table border="1" class="tabla_fichas" cellspacing="0" cellpadding="10">
                        <tbody>
                            <?php foreach ($fichas as $f): ?>
                                <tr>
                                    <td><?= $f[0] ?></td>
                                    <td><?= $f[1] ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif ?>

        <?php if ($especificaciones != NULL AND trim($especificaciones) != ''): ?>
            <div class="box">
                <div class="title"><i class="material-icons">add_circle</i> Especificaciones</div>
                <div class="body">
                    <?= $especificaciones ?>
                </div>
            </div>
        <?php endif ?>
        
        <?php if ($garantia != NULL AND trim($garantia) != ''): ?>
            <div class="box">
                <div class="title"><i class="material-icons">add_circle</i> Garantía</div>
                <div class="body">
                    <?= $garantia ?>
                </div>
            </div>
        <?php endif ?>
    </section>

    <?php 

    $filtro_rel['id_ficha'] = $id;
    $filtro_rel['categoria'] = $categoria_producto[0][0];
    $filtro_rel['limite'] = 4;
    $filtro_rel['random'] = true;

    $productos_rel = get_products($filtro_rel);

    ?>

    <section class="productos_relacionados mtop-20">
        <div class="home_title">
            Los clientes también vieron
        </div>

        <div class="grid_products grid_four mtop-20">
            <?php foreach ($productos_rel['productos']['producto'] as $item): 
                $url_producto = 'ficha/' . $item['id_producto'] . '/' . $item['nombre_seteado'];
                ?>
                <div class="col">
                    <?php if ($item['precio_cantidad'] == 1): ?>
                        <div class="descuento_cantidad">descuento por cantidad</div>
                    <?php endif ?>
                    
                    <?php if (isset($_COOKIE['usuario_id'])): ?>
                        <?php if (producto_guardado($_COOKIE['usuario_id'], $item['id_producto'])): ?>
                            <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="remove"><i class="fas fa-heart"></i></div>
                        <?php else: ?>
                            <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                        <?php endif ?>
                    <?php else: ?>
                        <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                    <?php endif ?>

                    <!-- Porcentaje descuento  -->
                    <?php if ( ofertaTiempo($item['id_hijo']) ): ?>
                        <div class="porcentaje_descuento">-<?= round(100 - ($item['oferta_tiempo_descuento'] * 100) / $item['precio']) ?>%</div>
                    <?php elseif($is_cyber AND $item['precio_cyber'] > 0): ?>
                        <div class="porcentaje_descuento">-<?= round(100 - ($item['precio_cyber'] * 100) / $item['precio']) ?>%</div>
                    <?php elseif( $item['descuento'] > 0 ): ?>
                        <div class="porcentaje_descuento">-<?= round(100 - ($item['descuento'] * 100) / $item['precio']) ?>%</div>
                    <?php endif ?>
                    
                    <div class="thumb">
                        <a href="<?= $url_producto ?>">
                            <?php if ($item['imagen_grilla'] != 'img/sin-foto.jpg'): 
                                $thumb = imagen("imagenes/productos/", $item['nombre_imagen']); ?>
                                <img src="<?= $thumb ?>">
                            <?php else: ?>
                                <img src="<?= $item['imagen_grilla'] ?>">
                            <?php endif ?>
                            
                        </a>
                        <?php if(ofertaTiempo($item['id_hijo'])){
                            $oferta_tiempo_hasta = $item['oferta_tiempo_hasta'];
                         ?>
                            <div class="countdown" rel="<?= $oferta_tiempo_hasta ?>">
                                <div class="col">
                                    <small>Hora</small>
                                    <div class="box" data-rel="hour">00</div>
                                </div>
                                <div class="col">
                                    <small>Min.</small>
                                    <div class="box" data-rel="minute">00</div>
                                </div>
                                <div class="col">
                                    <small>Seg.</small>
                                    <div class="box" data-rel="second">00</div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <a href="<?= $url_producto ?>">
                        <span class="nombre"><?= $item['nombre'] ?></span>
                        
                        <?php if ( ofertaTiempo( $item['id_hijo'] ) ): ?>
                            <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                            <span class="precio"><small class="signo">$</small><?= number_format(getPrecio($item['id_hijo']),0,",",".") ?> <small class="iva">IVA Incluido
                        <?php else: ?>
                            <?php if ($item['descuento'] > 0): ?>
                                <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                                <span class="precio"><small class="signo">$</small><?= number_format($item['descuento'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                            <?php else: ?>
                                <span class="descuento"></span>
                                <span class="precio"><small class="signo">$</small><?= number_format($item['precio'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                            <?php endif ?>
                        <?php endif; ?>
                        
                        
                        </small></span>
                    </a>

                </div>
            <?php endforeach ?>
            </div>

        </div>
    </section>
    
</div>

<div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto[0][1] ?>" />
        <meta itemprop="name" content="<?= $producto[0][0] ?>" />
        <?php if (is_array($imagenes_ficha)): ?>
            <?php for($i=0; $i<sizeof($imagenes_ficha); $i++){ ?>
                <link itemprop="image" href="imagenes/productos/<?= $imagenes_ficha[$i][0] ?>" />
            <?php } ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto[0][15]) ?>" />
        
        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto[0][2] > 0): ?>
            <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else: ?>
            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto[0][1] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto[0][6] ?>" />
        </div>
    </div> 
</div>