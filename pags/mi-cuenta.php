<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo '<script>location.href = "inicio-sesion"</script>';
	}
?>

<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">cuenta</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">mis datos</span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="content_gray">
  
  <div class="content-dashboard">
    <h2 class="title-dash">Mi cuenta</h2>
    <div class="grid-dashboard">
      <div class="col">

        <?php include("pags/menuMiCuenta.php"); ?>


      </div>
      <div class="col">
        
        <div class="main_dash">
          
          <div class="title">Mis datos</div>
          <div class="subtitle">
            Podrás ver tus pedidos, historial de compra y <br>editar tus datos personales y de envío.
          </div>

          <div class="group-form">
            <label>Nombre y apellido<small>*</small></label>
            <input type="text" name="nombre" value="<?= $cliente[0][1] ?>">
          </div>
          
          <div class="double-form">
            <div class="group-form">
              <label>Email<small>*</small></label>
              <input type="email" name="email" value="<?= $cliente[0][4] ?>">
            </div>

            <div class="group-form">
              <label>Teléfono<small>*</small></label>
              <input type="text" name="telefono" value="<?= $cliente[0][3] ?>" onkeypress="return justNumbers(event);" maxlength="9" placeholder="Ej. 912345678">
            </div>
          </div>

          <!-- <div class="group-form">
            <label>Rut<small>*</small></label>
            <input type="text" name="rut" value="<?= $cliente[0][2] ?>" maxlength="12">
          </div> -->

          <div class="box_buttons">
            <a href="javascript:void(0)" class="update btn">Actualizar datos</a>
            <a href="javascript:void(0)" class="cambiar_password">Cambiar contraseña</a>
          </div>

        </div>

      </div>
    </div>
  </div>
  
</div>

<div class="bg_password"></div>
<div class="content_password">
  <h2>Cambiar contraseña</h2>
  
  <div class="form-group">
    <label>Contraseña actual</label>
    <input type="password" name="actual_password">
  </div>

  <div class="form-group">
    <label>Contraseña nueva</label>
    <input type="password" name="nueva_password">
  </div>

  <div class="form-group">
    <label>Repetir contraseña nueva</label>
    <input type="password" name="r_nueva_password">
  </div>

  <a href="javascript:void(0)" class="btn btnUpdatePasswordPost">Actualizar</a>

</div>
