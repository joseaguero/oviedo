<div id="cambioPrecio"></div>


<div class="container_small ohidden mtop-20 mbottom-20">

    <div class="title_carro">Mi carro</div>

    <div class="mtop-20 grid-xs-cart">
    	
		<div class="contCarro">
        
            <div id="contFilasCarro">
                <?= ShowCart(); ?>
            </div>
            
            <?php if (isset($_COOKIE['usuario_id'])): ?>
                <div id="contProductosGuardados">
                    <?= saveForLater(); ?>
                </div>
            <?php endif ?>
            
        </div><!-- fin contCarro-->
        
        
        
        <div class="contTotales">
            <div id="contValoresResumen">
                <?= resumenCompra(); ?>
            </div><!--fin contValoresResumen-->
            
            
            <!-- <a href="home" class="btnAgregarMasProductos">agregar mas productos</a> -->
            
            <div class="contUltimosVistos">
            	<?= vistosRecientemente("lista","4"); ?>
            </div><!--fin contUltimosVistos -->
            
        </div><!-- Fin contTotales-->

	</div>
</div>

<?php if($_GET['stock']){ ?>
    <script type="text/javascript">
        swal("","Uno de los productos en el carro, ya no cuenta con stock","warning");
    </script>
<?php } ?>
            


<?php if(qty_pro() > 0){ ?>
<script type="text/javascript">
<?php
    if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}  
	$cart = $_COOKIE['cart_alfa_cm'];  
	    $items = explode(',',$cart);
	foreach ($items as $item) {
        $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
    }
    foreach ($contents as $prd_id=>$qty) {
        $filas = consulta_bd("pd.id, pd.sku, pd.nombre, p.nombre, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $prd_id","");
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] != '' and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
        ga('ec:addProduct', {'id': '<?= $filas[0][0]; ?>','name': '<?= $filas[0][2]; ?>','brand': 'no informada','price': '<?= $valor; ?>','quantity': <?= $qty; ?>});
		<?php
    }
?>
</script>
<script type="text/javascript">ga('ec:setAction','checkout', {'step': 1});</script>
<?php } //valido que el carro tenga productos para mostrar el analytics ?>


<div class="fixedResponsive">
	<?= resumenCompra(); ?>
</div>