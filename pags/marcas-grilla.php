<?php 
require_once 'paginador/paginator.class.php';

    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 0;
    $ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 4;
    
    $id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
    $nombreLinea = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;

    $linea = consulta_bd('l.id, l.nombre, c.id, c.nombre', 'lineas l join categorias c on c.linea_id = l.id', "c.id = $id", '');

    $marca_info = consulta_bd('id, nombre', 'marcas', "id = $id", "");
    
    $orden = (isset($_GET['orden']) AND $_GET['orden'] != '') ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
    $categorias = (isset($_GET['cat']) AND $_GET['cat'] != '') ? mysqli_real_escape_string($conexion, $_GET['cat']) : 0;
    $filtro_desde = (isset($_GET['desde']) AND $_GET['desde'] != '') ? mysqli_real_escape_string($conexion, $_GET['desde']) : 0;
    $filtro_hasta = (isset($_GET['hasta']) AND $_GET['hasta'] != '') ? mysqli_real_escape_string($conexion, $_GET['hasta']) : 0;

    if ($orden != 0 OR $categorias != 0 OR $filtro_desde != 0 OR $filtro_hasta != 0) {
        $conteo_de_filtros = 1;
    }

    $explode_categorias = explode('-', $categorias);
    $explode_marcas = explode('-', $marcas);
    $explode_filtros = explode('-', $filtros);

    if($orden === "desc"){
        $orderSql = ' valorMenor desc';
    } else if($orden === "asc"){
        $orderSql = ' valorMenor asc';
    } else if($orden === 'rel'){
        $orderSql = "posicion asc";
    } else {
        $orderSql = "posicion asc";
    }

    if ($categorias != 0) {
        $filtros_grilla['categorias'] = $categorias;
        $filtro_max['categorias'] = $categorias;
        $categorias_display = true;
    }

    if ($filtro_desde != 0) {
        $filtros_grilla['desde'] = $filtro_desde;
        $precios_display = true;
    }

    if ($filtro_hasta != 0) {
        $filtros_grilla['hasta'] = $filtro_hasta;
        $precios_display = true;
    }

    $filtros = 0;

    $filtros_grilla['marca'] = $id;

    $productos = get_products($filtros_grilla);

    $total = $productos['productos']['total'][0];

    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 8; 
    $rutaRetorno = "marcas/$id/$nombreLinea?cat=$categorias&desde=$filtro_desde&hasta=$filtro_hasta&orden=$orden";
    $pages->paginate($rutaRetorno);

    $filtros_grilla['limit'] = $pages->limit;
    $filtros_grilla['orden'] = $orderSql;
    $productos = get_products($filtros_grilla);

    $filtro_max['marca'] = $id;
    $filtro_max['max'] = true;
    $maximo_valor = get_products($filtro_max);

    $maximo_actual = ($filtro_hasta != 0) ? $filtro_hasta : $maximo_valor['valor_max']+1000;
?>
 
<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="marcas" itemprop="item">
                <span itemprop="name">Marcas</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="marcas/<?= $id ?>/<?= $nombreLinea ?>" itemprop="item">
                <span itemprop="name"><?= $marca_info[0][1] ?></span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="container_grid">
    <div class="col">
        <div class="title_filtro filtros_col" data-ref="marcas">Filtros</div>
        
        <?php if ($conteo_de_filtros == 1): ?>
            <div class="filtros_aplicados">
                <span class="filter_apply">Filtros aplicados</span>
                <?php $filtros_seleccionados = filtros_aplicados($marcas, $filtro_desde, $filtro_hasta, $categorias, $filtros); ?>

                <?php foreach ($filtros_seleccionados['filtro_categorias'] as $item): ?>
                    <a href="javascript:void(0)" class="aplicado cat_aplicadas" data-id="<?= $item['id'] ?>" data-action="cat"><span><?= $item['nombre'] ?></span> <i class="material-icons">close</i></a>
                <?php endforeach ?>
                
                <?php if ($filtros_seleccionados['filtro_precios']): ?>
                    <a href="javascript:void(0)" class="aplicado_otros precios_aplicados" data-action="precio"><span><?= $filtros_seleccionados['filtro_precios'][0] ?> <i class="material-icons">close</i></span></a>
                <?php endif ?>

                <div class="clearfix"></div>
                <a href="marcas/<?= $id ?>/<?= $nombreLinea ?>" class="limpiar_filtros">Limpiar filtros</a>

            </div>
        <?php endif ?>

        <div class="box_filtro">
            <div class="title">
                Categorías
            </div>
            
            <?php $categorias_filtros = get_categorias(0, $id) ?>
            <div class="box_check">
                <?php foreach ($categorias_filtros as $cat_chk): 
                    $checked_c = (in_array($cat_chk['id'], $explode_categorias)) ? "checked" : ''; ?>
                    <div class="row_otro">
                        <input type="checkbox" name="chk_cat" class="chk" value="<?=$cat_chk['id']?>" id="c_<?=$cat_chk['id']?>" <?= $checked_c ?>>
                        <label for="c_<?=$cat_chk['id']?>"><?= $cat_chk['nombre'] ?></label>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <div class="box_filtro precio_filtro" data-max="<?= $maximo_valor['valor_max']+1000 ?>" data-max-actual=<?= $maximo_actual ?>>
            <div class="title">
                Precio
            </div>

            <div class="box_check">
                <div class="box_price dblock" data-ref="prices">
                    <span class="min-filtro" data-info="<?= $filtro_desde ?>" data-valor="0"></span> - 
                    <span class="max-filtro" data-info="<?= $filtro_hasta ?>" data-valor="<?= $maximo_actual ?>"></span>
                    <div class="clearfix"></div>
                    <div id="slider-range"></div>
                </div>
            </div>
        </div>

        <!-- <a href="#" class="btn btn_ofertas">ofertas</a> -->

    </div>
    <div class="col">

        <div class="button_filtros_xs">
            <div class="title_filtro filtros_col" data-ref="lineas">Filtros</div>
        </div>
        
        <div class="buttom_b">
            <div class="orderby" data-order="<?= $orden ?>">
                <div class="title">Ordenar por</div>
                <div class="title-xs">Ordenar por</div>

                <div class="col"><a href="<?= "marcas/$id/$nombreLinea?cat=$categorias&desde=$filtro_desde&hasta=$filtro_hasta&orden=rel&page=1" ?>" <?= ($orden === 'rel' || $orden == '0') ? 'class="active"' : '' ?>>Relevante</a></div>
                <div class="col"><a href="<?= "marcas/$id/$nombreLinea?cat=$categorias&desde=$filtro_desde&hasta=$filtro_hasta&orden=desc&page=1" ?>" <?= ($orden === 'desc') ? 'class="active"' : '' ?>>Mayor precio</a></div>
                <div class="col"><a href="<?= "marcas/$id/$nombreLinea?cat=$categorias&desde=$filtro_desde&hasta=$filtro_hasta&orden=asc&page=1" ?>" <?= ($orden === 'asc') ? 'class="active"' : '' ?>>Menor precio</a></div>

            </div>
        </div>

        <div class="bg-order"></div>
        <div class="orderby-xs">
            <div class="row-order"><a href="<?= "ofertas?cat=$categorias&marca=$marcas&filtros=$filtros&desde=$filtro_desde&hasta=$filtro_hasta&orden=rel&page=1" ?>" <?= ($orden === 'rel' || $orden == '0') ? 'class="active"' : '' ?>>Relevante</a></div>
            <div class="row-order"><a href="<?= "ofertas?cat=$categorias&marca=$marcas&filtros=$filtros&desde=$filtro_desde&hasta=$filtro_hasta&orden=desc&page=1" ?>" <?= ($orden === 'desc') ? 'class="active"' : '' ?>>Mayor precio</a></div>
                <div class="row-order"><a href="<?= "ofertas?cat=$categorias&marca=$marcas&filtros=$filtros&desde=$filtro_desde&hasta=$filtro_hasta&orden=asc&page=1" ?>" <?= ($orden === 'asc') ? 'class="active"' : '' ?>>Menor precio</a></div>
        </div>

        <div class="clearfix"></div>

        
        <div class="grid_products mtop-20">
            <?php if (is_array($productos)): ?>
                <?php foreach ($productos['productos']['producto'] as $item): 
                    $url_producto = 'ficha/' . $item['id_producto'] . '/' . $item['nombre_seteado'];
                    ?>
                    <div class="col">
                        <?php if ($item['precio_cantidad'] == 1): ?>
                            <div class="descuento_cantidad">descuento por cantidad</div>
                        <?php endif ?>
                        
                        <?php if (isset($_COOKIE['usuario_id'])): ?>
                            <?php if (producto_guardado($_COOKIE['usuario_id'], $item['id_producto'])): ?>
                                <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="remove"><i class="fas fa-heart"></i></div>
                            <?php else: ?>
                                <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                            <?php endif ?>
                        <?php else: ?>
                            <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                        <?php endif ?>

                        <!-- Porcentaje descuento  -->
                        <?php if ( ofertaTiempo($item['id_hijo']) ): ?>
                            <div class="porcentaje_descuento">-<?= round(100 - ($item['oferta_tiempo_descuento'] * 100) / $item['precio']) ?>%</div>
                        <?php elseif($is_cyber AND $item['precio_cyber'] > 0): ?>
                            <div class="porcentaje_descuento">-<?= round(100 - ($item['precio_cyber'] * 100) / $item['precio']) ?>%</div>
                        <?php elseif( $item['descuento'] > 0 ): ?>
                            <div class="porcentaje_descuento">-<?= round(100 - ($item['descuento'] * 100) / $item['precio']) ?>%</div>
                        <?php endif ?>
                        
                        <div class="thumb">
                            <a href="<?= $url_producto ?>">
                                <?php if ($item['imagen_grilla'] != 'img/sin-foto.jpg'): 
                                    $thumb = imagen("imagenes/productos/", $item['nombre_imagen']); ?>
                                    <img src="<?= $thumb ?>">
                                <?php else: ?>
                                    <img src="<?= $item['imagen_grilla'] ?>">
                                <?php endif ?>
                                
                            </a>
                            <?php if(ofertaTiempo($item['id_hijo'])){
                                $oferta_tiempo_hasta = $item['oferta_tiempo_hasta'];
                             ?>
                                <div class="countdown" rel="<?= $oferta_tiempo_hasta ?>">
                                    <div class="col">
                                        <small>Hora</small>
                                        <div class="box" data-rel="hour">00</div>
                                    </div>
                                    <div class="col">
                                        <small>Min.</small>
                                        <div class="box" data-rel="minute">00</div>
                                    </div>
                                    <div class="col">
                                        <small>Seg.</small>
                                        <div class="box" data-rel="second">00</div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <a href="<?= $url_producto ?>">
                            <span class="nombre"><?= $item['nombre'] ?></span>
                            
                            <?php if ( ofertaTiempo( $item['id_hijo'] ) ): ?>
                                <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                                <span class="precio"><small class="signo">$</small><?= number_format(getPrecio($item['id_hijo']),0,",",".") ?> <small class="iva">IVA Incluido
                            <?php else: ?>
                                <?php if ($item['descuento'] > 0): ?>
                                    <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                                    <span class="precio"><small class="signo">$</small><?= number_format($item['descuento'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                                <?php else: ?>
                                    <span class="descuento"></span>
                                    <span class="precio"><small class="signo">$</small><?= number_format($item['precio'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                                <?php endif ?>
                            <?php endif; ?>
                            
                            
                            </small></span>
                        </a>

                    </div>
                <?php endforeach ?>
            <?php else: ?>
                No se encontraron productos
            <?php endif ?>
        </div>

    </div>
</div>
    
<div class="container ohidden">
    <div class="paginador">
        <?= $pages->display_pages(); ?>
    </div>
</div>

<div class="bg_filtros_xs"></div>