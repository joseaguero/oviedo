<script>
<?php 
	if(opciones("cotizador") != 1){
		echo 'location.href = "404"';
	}
?>
</script>

<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Carro de cotización</li>
        </ul>
    </div>
</div>


<div class="cont100">
    <div class="cont100Centro">
    	
		<div class="contCarroCotizacionFinal">
        	<div class="filaTitulos cont100">
                <span class="ancho50">Producto</span>
                <span class="ancho20">Precio unitario</span>
                <span class="ancho10">Cantidad</span>
                <span class="ancho20 tituloTotal"><span>Total item</span></span>
            </div>
            <div id="contFilasCarro">
                <?= ShowCartCotizacion(); ?>
            </div>
            
        </div><!-- fin contCarro-->
        
        
        
        <div class="contTotalesCotizacion">
            <div class="tituloColumnas">Complete sus datos</div>
            <?php 
			if(isset($_COOKIE[usuario_id])){
				$usuario_id = $_COOKIE[usuario_id];
				$usuario = consulta_bd("nombre, apellido, telefono, email, rut","clientes","id = $usuario_id","");
				$nombre = $usuario[0][0];
				$apellidos = $usuario[0][1];
				$telefono = $usuario[0][2];
				$email = $usuario[0][3];
				$rut = $usuario[0][4];
				
				} else {
					$usuario_id = 0;
					$nombre = "";
					$telefono = "";
					$email = "";
					$rut = "";
				}
			?>
            <form method="post" action="tienda/procesarCotizacion.php" id="formCotizacion">
            	<!--<label for="nombre">Nombre</label>-->
                <input class="campoPasoFinal" type="text" name="nombre" value="<?= $nombre; ?>" id="nombre" placeholder="Nombre" />
                
                <!--<label for="apellido">Apellidos</label>-->
                <input class="campoPasoFinal" type="text" name="apellido" value="<?= $apellidos; ?>" id="apellidos" placeholder="Apellidos" />
                
                <!--<label for="telefono">Teléfono</label>-->
                <input class="campoPasoFinal" type="text" name="telefono" value="<?= $telefono; ?>" id="telefono" placeholder="Teléfono" />
                
                <!--<label for="email">Email</label>-->
                <input class="campoPasoFinal" type="text" name="email" value="<?= $email; ?>" id="email" placeholder="Email" />
                
                <!--<label for="rut">Rut</label>-->
                <input class="campoPasoFinal" type="text" name="rut" value="<?= $rut; ?>" id="Rut" placeholder="Rut" />
                
                <!--<label for="comentarios">Comentarios</label>-->
                <textarea class="textareaPasoFinal"  name="comentarios" placeholder="Ingrese sus comentarios"></textarea>
                <?php if(qty_proCotizacion() > 0){ ?>
                <a href="javascript:void(0)" class="btnTerminarCarro" id="btnCotizacion">Solicitar cotización</a>
                <?php } ?>
            </form>
            
            
        </div><!-- Fin contTotales-->

	</div>
</div>

            
            

<?php /*?><script type="text/javascript">
<?php
    
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}  
	$cart = $_COOKIE[cart_alfa_cm];  
    $items = explode(',',$cart);
    foreach ($items as $item) {
        $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
    }
    
    foreach ($contents as $prd_id=>$qty) {
        $filas = consulta_bd("pd.id, pd.sku, pd.nombre, m.nombre, pd.precio, pd.descuento","productos p, productos_detalles pd, marcas m","p.id = pd.producto_id and pd.id = ".$prd_id." and p.marca_id = m.id","");
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] != '' and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
        
            ga('ec:addProduct', {
              'id': '<?php echo $filas[0][0]; ?>',
              'name': '<?php echo $filas[0][2]; ?>',
              'brand': '<?php echo $filas[0][3]; ?>',
              'price': '<?php echo $valor; ?>',
              'quantity': <?php echo $qty; ?>
            });
			console.log("<?php echo $filas[0][0]; ?>,<?php echo $filas[0][2]; ?>,<?php echo $filas[0][3]; ?>,<?php echo $valor; ?>,<?php echo $qty; ?>");
        
        <?php
    }
?>
</script>
<script type="text/javascript">
	ga('ec:setAction','checkout', {'step': 1});
</script>
<?php */?>
