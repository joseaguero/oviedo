<?php 

$cliente_id = $_COOKIE['usuario_id'];

$direccion_principal = consulta_bd('d.id, d.nombre, r.nombre, c.nombre, d.numero, cl.nombre, cl.telefono, d.calle, cl.email, c.id', 'clientes_direcciones d join regiones r on d.region_id = r.id join comunas c on d.comuna_id = c.id join clientes cl on d.cliente_id = cl.id', "d.cliente_id = $cliente_id and d.principal = 1", 'd.id desc');

$direcciones = consulta_bd('d.id, d.nombre, r.nombre, c.nombre, d.numero, d.calle, c.id', 'clientes_direcciones d join regiones r on d.region_id = r.id join comunas c on d.comuna_id = c.id', "d.cliente_id = $cliente_id and d.principal = 0", 'd.id desc');


$comuna_principal = (is_array($direccion_principal)) ? $direccion_principal[0][9] : 0;

?>

<div class="container_small ohidden mtop-20 mbottom-20">

    <div class="title_carro">Finaliza tu compra</div>
    <div class="subtitle_carro">Compra de manera rápida, fácil y sin tener cuenta.</div>
    
    <form action="checkout" method="post" id="checkout" data-valid="false">

        <div class="grid_enviopago ohidden">
            <div class="col mtop-20">

                <div class="content_tabs_carro">

                    <div class="tab_carro" data-op="1" id="despachoClick" data-active="true">Datos de despacho</div>
                    <div class="tab_carro disabled_right" data-op="2" id="retiroClick" data-active="false">Retiro en tienda</div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="despacho_retiro" value="1" id="despacho_retiro">
                    <div class="datos_ajx">
                        <div class="content_datos">
                            <?php if (!is_array($direccion_principal)): 
                                $datos_cliente = consulta_bd('id, nombre, telefono, email', 'clientes', "id = $cliente_id", '');
                            ?>
                                <div class="box_despacho_c">
                                    <span class="row_text"><strong><?= $datos_cliente[0][1] ?></strong></span>
                                    <span class="row_text"><?= $datos_cliente[0][2] ?></span>
                                    <span class="row_text"><?= $datos_cliente[0][3] ?></span>
                                </div>
                            <?php else: ?>
                                <div class="box_despacho_c">
                                    <span class="row_text"><?= $direccion_principal[0][5] ?></span>
                                    <span class="row_text"><?= $direccion_principal[0][6] ?></span>
                                    <span class="row_text"><?= $direccion_principal[0][8] ?></span>
                                    <span class="row_text direccion_prl"><strong><?= $direccion_principal[0][1] ?></strong></span>
                                    <span class="row_text"><?= $direccion_principal[0][7] ?> <?= $direccion_principal[0][4] ?></span>
                                    <span class="row_text"><?= ucwords(strtolower($direccion_principal[0][3])) ?>, <?= $direccion_principal[0][2] ?></span>

                                    <div class="actions_row_principal" data-id="<?=$direccion_principal[0][9]?>">
                                        <a href="javascript:void(0)" onclick="editDirOne(<?=$direccion_principal[0][0]?>, 0)"><i class="far fa-edit"></i> Editar envío</a>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="box_retiro_c">
                        
                        <div class="double-form">
                            <div class="form-carro">
                                <label>Sucursal <small class="obl">*</small></label>
                                <?php $sucursales = consulta_bd('id, nombre', 'sucursales', "retiro_en_tienda = 1", 'posicion asc'); ?>
                                <select name="sucursal" id="sucursal_checkout">
                                    <option value="0">Seleccionar sucursal</option>
                                    <?php foreach ($sucursales as $s): ?>
                                        <option value="<?= $s[0] ?>"><?= $s[1] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <div class="form-carro">
                                <label>¿Quién retira? <small class="obl">*</small></label>
                                <input type="text" name="quien_retira" placeholder="Nombre y apellido" id="quien_retira">
                            </div>
                        </div>

                        <div class="form-carro">
                            <label>Rut <small class="obl">*</small></label>
                            <input type="text" name="rut_retiro" placeholder="Ingresa tu RUT" id="rut_retiro" maxlength="12">
                        </div>

                    </div>
                </div>

                <div class="content_factura">
                    <a href="javascript:void(0)" class="btnFactura">Necesito factura</a>

                    <div class="form_factura">
                        <div class="three_form">
                            
                            <div class="form-carro">
                                <label>Giro <small class="obl">*</small></label>
                                <input type="text" name="giro" id="giro_cheackout">
                            </div>

                            <div class="form-carro">
                                <label>Razón social <small class="obl">*</small></label>
                                <input type="text" name="razon_social" id="razon_social_cheackout">
                            </div>

                            <div class="form-carro">
                                <label>Rut empresa<small class="obl">*</small></label>
                                <input type="text" name="rut_empresa" id="rut_empresa" maxlength="12">
                            </div>

                            <div class="form-carro">
                                <label>Dirección de Facturación<small class="obl">*</small></label>
                                <input type="text" name="direccion_empresa" id="direccion_empresa">
                            </div>
                            
                            <div class="form-carro">
                                <label>Nombre<small class="obl">*</small></label>
                                <input type="text" name="nombre_factura" id="nombre_factura">
                            </div>

                            <div class="form-carro">
                                <label>Teléfono<small class="obl">*</small></label>
                                <input type="text" name="telefono_factura" id="telefono_factura" onkeypress="return justNumbers(event);" maxlength="9">
                            </div>

                            <input type="hidden" name="factura" value="0">

                        </div>
                    </div>
                </div>
                
                <div class="direcciones_ajx">
                    <div class="content_direcciones">
                        <?php if (is_array($direcciones)): ?>
                            <div class="seleccionar_direccion">
                                <div class="title_select_dir">Seleccionar otra dirección</div>
                                
                                <?php foreach ($direcciones as $dir): ?>
                                    <div class="row_direc_select ohidden">
                                        <input type="radio" name="row_select_dir" value="<?= $dir[0] ?>" id="r<?= $dir[0] ?>" onclick="selectDirChk(<?= $dir[6] ?>, <?= $dir[0] ?>, this)">

                                        <label for="r<?= $dir[0] ?>" onclick="selectDirChk(<?= $dir[6] ?>, <?= $dir[0] ?>, this)">
                                            <span class="alias"><?= $dir[1] ?></span>
                                            <span class="normal_text"><?= $dir[5] ?> <?= $dir[4] ?></span>
                                            <span class="normal_text"><?= ucwords(strtolower($dir[3])) ?>, <?= $dir[2] ?></span>
                                            <span class="normal_text">Chile</span>
                                        </label>

                                        <div class="actions_row">
                                            <a href="javascript:void(0)" onclick="delDir(<?=$dir[0]?>, 0)"><i class="far fa-trash-alt"></i> Eliminar</a>
                                            <a href="javascript:void(0)" onclick="editDirOne(<?=$dir[0]?>, 0)"><i class="far fa-edit"></i> Editar envío</a>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>

                        <a href="javascript:void(0)" onclick="addFade()" class="btn_agregar_direccion btn mtop-20">
                            Agregar dirección
                        </a>

                        <div class="box_agregar_direccion">
                            <div class="titulo_add">Agregar dirección</div>
                            <div class="double-form">
                                <div class="form-carro">
                                    <?php $regiones = consulta_bd('id, nombre', 'regiones', '', 'posicion asc'); ?>
                                    <label>Región <small class="obl">*</small></label>
                                    <select name="region" id="region_dir">
                                        <option value="0">Seleccionar región</option>
                                        <?php foreach ($regiones as $r): ?>
                                            <option value="<?= $r[0] ?>"><?= $r[1] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-carro">
                                    <label>Comuna <small class="obl">*</small></label>
                                    <select name="comuna" id="comuna_dir">
                                        <option value="0">Seleccionar comuna</option>
                                    </select>
                                </div>
                            </div>

                            <div class="double-form">
                                <div class="form-carro">
                                    <label>Calle <small class="obl">*</small></label>
                                    <input type="text" name="direccion" id="calle_dir">
                                </div>

                                <div class="double-cart">
                                    <div class="form-carro">
                                        <label>Número <small class="obl">*</small></label>
                                        <input type="text" name="numero" id="numero_dir">
                                    </div>

                                    <div class="form-carro">
                                        <label>Piso o Dpto.</label>
                                        <input type="text" name="piso" id="piso_dir">
                                    </div>
                                </div>
                            </div>

                            <div class="form-carro">
                                <label>Asignar alías <small class="tip">(Ejemplo: Oficina)</small></label>
                                <input type="text" name="alias" id="alias_dir">
                            </div>

                            <div class="ohidden">
                                <input type="checkbox" class="chk" name="principal" id="chkPrincipal">
                                <label for="chkPrincipal" class="not_label">Guardar como dirección principal</label>
                            </div>
                            
                            <a href="javascript:void(0)" onclick="addDir()" class="button btnAdd btn">Agregar</a>
                            <a href="javascript:void(0)" onclick="editDir()" data-id="0" class="button btnEdit btn">Editar</a>
                            <a href="javascript:void(0)" onclick="cancelAdd()" class="button btnCancel">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mtop-20">
                
                <div class="bx_pagos">
                    <div class="ajx_precio_xs">
                        <div class="totales_xs">
                            <span class="tituloxs">Total:</span>
                            <div class="precioxs">$<?= number_format(get_precio_total($comuna_principal, 0), 0, ',', '.') ?></div>
                        </div>
                    </div>
                    <div class="boton_pago_session">
                        <a href="javascript:void(0)"><span>Comprar productos</span></a>
                    </div>
                </div>
                <div class="metodos ohidden">
                    <div class="title_box">Forma de pago</div>
                    <div class="group_radio">
                        <input type="radio" name="metodopago" value="webpay" checked>
                        <label><img src="img/webpay.png"></label>
                    </div>

                    <div class="group_radio">
                        <input type="radio" name="metodopago" value="mercadopago">
                        <!--<label><img src="img/mpago.png" style="margin-top: 3px;"></label>-->
                        <label><img src="img/logompago.png" width="80" style="margin-top: 3px;"></label>
                    </div>

                    <div class="group_radio">
                        <input type="radio" name="metodopago" value="transferencia">
                        <label style="height: 15px;margin-top: 6px;">Transferencia</label>
                    </div>
                </div>

                <!-- Fixed boton -->
                <div class="fixed-button">
                    <div class="container_small">
                        <div class="boton_pago_session">
                            <a href="javascript:void(0)"><span>Comprar productos</span></a>
                        </div>
                        <!-- <div class="bx_term_fixed">
                            <input type="checkbox" name="terminos" id="terminos">
                            <label for="terminos">Al comprar acepto los términos y <br>condiciones</label>
                        </div> -->
                    </div>
                </div>

                <div class="chk_box_terminos">
                    <input type="checkbox" name="terminos" id="terminos">
                    <label for="terminos" class="lblTerminos">Al comprar acepto los términos y <br>condiciones</label>
                </div>
                <div class="clearfix"></div>
                
                <div class="ajx_despacho">
                    <div class="bx_despacho mtop-20 ohidden">
                        <div class="title_box">Tipo de despacho</div>
                        
                        <?php if (is_array($direccion_principal)): 
                            $valor_despacho = valorDespacho($direccion_principal[0][9]); ?>
                            
                            <div class="group_radio_despacho">
                                <input type="radio" name="tipodespacho" value="1" id="despachoNormal" checked>
                                <label for="despachoNormal" style="height: 15px;margin-top: 5px;">Normal: <span>$<?= number_format($valor_despacho, 0, ',', '.') ?></span> <small>de 3 a 5 días</small></label>
                            </div>

                            <!-- <div class="group_radio_despacho">
                                <input type="radio" name="tipodespacho" value="1" id="despachoExpress">
                                <label for="despachoExpress" style="height: 15px;margin-top: 5px;">Express: <span>$10.000</span> <small>Durante el día</small></label>
                            </div> -->

                        <?php else: ?>
                            <span class="text_despacho">Seleccione una comuna para ver las opciones de despacho</span>
                        <?php endif ?>
                    </div>
                </div>

                
                <div class="bx_descuento">
                    <?php if($_SESSION["descuento"]){
                        $codigo = $_SESSION["descuento"];
                        descuentoBy($codigo);
                        $estadoInput = 'disabled="disabled"';
                        $icono = '<i class="fas fa-check"></i>';
                    } else {
                        $estadoInput = '';
                    } ?>
                    <div class="codigoOK"><?php echo $icono; ?></div>
                    <input type="text" name="codigo_descuento" class="descuento_box" value="<?php echo $codigo; ?>" placeholder="Agregar Código de Descuento" <?php echo $estadoInput ?>>

                    <a href="javascript:void(0)" class="btnValidarCodigo btn">Agregar</a>
                </div>

                <div id="contDatosCartVariables">
                    <div class="contCartCompraRapida">
                        <?php 
                        echo resumenCompraShort($comuna_principal, 0); ?>
                    </div>
                    <!--<div class="mensajesDespacho"></div>-->
                    
                    
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
    </form>



    <!-- <div class="compraYAceptaTerminos">
        <div class="txtTerminosAcepto">
            <input type="checkbox" name="terminos" id="terminos" value="1" />
            <a href="<?php echo $url_base; ?>terminos-y-condiciones">
                 Al comprar acepto los Términos y condiciones
            </a>
        </div>
        
        <a href="javascript:void(0)" class="btnTerminarCarro" id="comprar" />Comprar productos</a>
    </div> -->    
</div>

<div class="bg_checkout"></div>


<div class="bg_popup"></div>
<div class="popup_acciones">
  <div class="title_pop">¿Estás seguro de eliminar esta dirección?</div>

  <div class="content_buttons">
    <a href="javascript:void(0)" class="confirmPop" data-action="delete" data-id="0" data-where="cart">Confirmar</a>
    <a href="javascript:void(0)" class="cancelPop">Cancelar</a>
  </div>
</div>

<script type="text/javascript" src="js/fixedCompra.js"></script>
