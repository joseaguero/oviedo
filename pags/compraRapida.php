<div class="container_small ohidden mtop-20 mbottom-20">

    <div class="title_carro">Finaliza tu compra</div>
    <div class="subtitle_carro">Compra de manera rápida, fácil y sin tener cuenta.</div>
    
    <form action="checkout" method="post" id="checkout" data-valid="false">

        <div class="grid_enviopago ohidden">
            <div class="col">
                <div class="box_ident ohidden">
                    <div class="disabled_checkout"></div>
                    <span class="t_ident">Identificate</span>

                    <span>Ingresa tu correo</span>
                    <input type="email" name="email_ident" class="emailIdent" data-valid="false">
                    <a href="javascript:void(0)" class="btnValidar btn">validar</a>
                </div>

                <div class="content_tabs_carro">
                    <div class="disabled_checkout"></div>

                    <div class="tab_carro" data-op="1" id="despachoClick" data-active="true">Datos de despacho</div>
                    <div class="tab_carro disabled_right" data-op="2" id="retiroClick" data-active="false">Retiro en tienda</div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="despacho_retiro" value="1" id="despacho_retiro">
                    <div class="box_despacho_c">
                        <div class="double-form">
                            <div class="form-carro">
                                <label>Nombre y apellido <small class="obl">*</small></label>
                                <input type="text" name="nombre" id="nombre_checkout">
                            </div>

                            <div class="form-carro">
                                <label>Teléfono</label>
                                <input type="text" name="telefono" id="telefono_checkout" onkeypress="return justNumbers(event);" maxlength="9">
                            </div>
                        </div>

                        <div class="double-form">
                            <div class="form-carro">
                                <?php $regiones = consulta_bd('id, nombre', 'regiones', '', 'posicion asc'); ?>
                                <label>Región <small class="obl">*</small></label>
                                <select name="region" id="region_checkout">
                                    <option value="0">Seleccionar región</option>
                                    <?php foreach ($regiones as $r): ?>
                                        <option value="<?= $r[0] ?>"><?= $r[1] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <div class="form-carro">
                                <label>Comuna <small class="obl">*</small></label>
                                <select name="comuna" id="comuna_checkout">
                                    <option value="0">Seleccionar comuna</option>
                                </select>
                            </div>
                        </div>

                        <div class="double-form">
                            <div class="form-carro">
                                <label>Calle <small class="obl">*</small></label>
                                <input type="text" name="direccion" id="calle_checkout">
                            </div>

                            <div class="double-cart">
                                <div class="form-carro">
                                    <label>Número <small class="obl">*</small></label>
                                    <input type="text" name="numero" id="numero_cheackout">
                                </div>

                                <div class="form-carro">
                                    <label>Piso o Dpto.</label>
                                    <input type="text" name="piso" id="piso_checkout">
                                </div>
                            </div>
                        </div>

                        <div class="box-chk ohidden">
                            <input type="checkbox" name="news" id="news">
                            <label for="news">Quiero recibir el newsletter con promociones.</label>
                        </div>
                    </div>

                    <div class="box_retiro_c">

                        <div class="double-form">
                            <div class="form-carro">
                                <label>Nombre y apellido <small class="obl">*</small></label>
                                <input type="text" name="nombreretiro" id="nombrer_checkout">
                            </div>
                        </div>
                        
                        <div class="double-form">
                            <div class="form-carro">
                                <label>Sucursal <small class="obl">*</small></label>
                                <?php $sucursales = consulta_bd('id, nombre', 'sucursales', "retiro_en_tienda = 1", 'posicion asc'); ?>
                                <select name="sucursal" id="sucursal_checkout">
                                    <option value="0">Seleccionar sucursal</option>
                                    <?php foreach ($sucursales as $s): ?>
                                        <option value="<?= $s[0] ?>"><?= $s[1] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <div class="form-carro">
                                <label>¿Quién retira? <small class="obl">*</small></label>
                                <input type="text" name="quien_retira" placeholder="Nombre y apellido" id="quien_retira">
                            </div>
                        </div>

                        <div class="form-carro">
                            <label>Rut <small class="obl">*</small></label>
                            <input type="text" name="rut_retiro" placeholder="Ingresa tu RUT" id="rut_retiro" maxlength="12">
                        </div>

                    </div>

                </div>

                <div class="content_factura">
                    <div class="disabled_checkout"></div>
                    <a href="javascript:void(0)" class="btnFactura">Necesito factura</a>

                    <div class="form_factura">
                        <div class="three_form">
                            
                            <div class="form-carro">
                                <label>Giro <small class="obl">*</small></label>
                                <input type="text" name="giro" id="giro_cheackout">
                            </div>

                            <div class="form-carro">
                                <label>Razón social <small class="obl">*</small></label>
                                <input type="text" name="razon_social" id="razon_social_cheackout">
                            </div>

                            <div class="form-carro">
                                <label>Rut empresa<small class="obl">*</small></label>
                                <input type="text" name="rut_empresa" id="rut_empresa" maxlength="12">
                            </div>

                            <div class="form-carro">
                                <label>Dirección de Facturación<small class="obl">*</small></label>
                                <input type="text" name="direccion_empresa" id="direccion_empresa">
                            </div>
                            
                            <div class="form-carro">
                                <label>Nombre<small class="obl">*</small></label>
                                <input type="text" name="nombre_factura" id="nombre_factura">
                            </div>

                            <div class="form-carro">
                                <label>Teléfono<small class="obl">*</small></label>
                                <input type="text" name="telefono_factura" id="telefono_factura" onkeypress="return justNumbers(event);" maxlength="9">
                            </div>

                            <input type="hidden" name="factura" value="0">

                        </div>
                    </div>
                </div>

            </div>
            <div class="col mtop-20">
                
                <div class="content_fixed">
                    <div class="bx_pagos">
                        <div class="ajx_precio_xs">
                            <div class="totales_xs">
                                <span class="tituloxs">Total:</span>
                                <div class="precioxs">$<?= number_format(get_precio_total(0, 0), 0, ',', '.') ?></div>
                            </div>
                        </div>
                        <div class="boton_pago">
                            <a href="javascript:void(0)"><span><img src="img/lock.svg">Comprar productos</span></a>
                        </div>
                    </div>
                    <div class="metodos ohidden">
                        <div class="title_box">Forma de pago</div>
                        <div class="group_radio">
                            <input type="radio" name="metodopago" value="webpay" checked>
                            <label><img src="img/webpay.png"></label>
                        </div>

                        <div class="group_radio">
                            <input type="radio" name="metodopago" value="mercadopago">
                             <label><img src="img/logompago.png" width="80" style="margin-top: 3px;"></label>
                        </div>

                        <div class="group_radio">
                            <input type="radio" name="metodopago" value="transferencia">
                            <label style="height: 15px;margin-top: 6px;">Transferencia</label>
                        </div>
                    </div>
                </div>

                <!-- Fixed boton -->
                <div class="fixed-button">
                    <div class="container_small">
                        <div class="boton_pago">
                            <a href="javascript:void(0)"><span><img src="img/lock.svg">Comprar productos</span></a>
                        </div>
                        <!-- <div class="bx_term_fixed">
                            <input type="checkbox" name="terminos" id="terminos">
                            <label for="terminos">Al comprar acepto los términos y <br>condiciones</label>
                        </div> -->
                    </div>
                </div>

                <div class="chk_box_terminos">
                    <input type="checkbox" name="terminos" id="terminos">
                    <label for="terminos" class="lblTerminos">Al comprar acepto los términos y <br>condiciones</label>
                </div>

                <div class="clearfix"></div>
                
                <div class="ajx_despacho">
                    <div class="bx_despacho mtop-20 ohidden">
                        <div class="title_box">Tipo de despacho</div>

                        <span class="text_despacho">Seleccione una comuna para ver las opciones de despacho</span>

                        <!-- <div class="group_radio_despacho">
                            <input type="radio" name="tipodespacho" value="1" id="despachoNormal" checked>
                            <label for="despachoNormal" style="height: 15px;margin-top: 5px;">Normal: <span>$5.000</span> <small>de 3 a 5 días</small></label>
                        </div> -->

                        <!-- <div class="group_radio_despacho">
                            <input type="radio" name="tipodespacho" value="1" id="despachoExpress">
                            <label for="despachoExpress" style="height: 15px;margin-top: 5px;">Express: <span>$10.000</span> <small>Durante el día</small></label>
                        </div> -->
                    </div>
                </div>
                
                <div class="bx_descuento">
                    <?php if($_SESSION["descuento"]){
                        $codigo = $_SESSION["descuento"];
                        descuentoBy($codigo);
                        $estadoInput = 'disabled="disabled"';
                        $icono = '<i class="fas fa-check"></i>';
                    } else {
                        $estadoInput = '';
                    } ?>
                    <input type="text" name="codigo_descuento" class="descuento_box" value="<?php echo $codigo; ?>" placeholder="Agregar Código de Descuento" <?php echo $estadoInput ?>>

                    <a href="javascript:void(0)" class="btnValidarCodigo btn">Agregar</a>
                </div>

                <div id="contDatosCartVariables">
                    <div class="contCartCompraRapida">
                      <?php echo resumenCompraShort(0, 1); ?>
                    </div>
                    <!--<div class="mensajesDespacho"></div>-->
                    
                    
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
    </form>



    <!-- <div class="compraYAceptaTerminos">
        <div class="txtTerminosAcepto">
            <input type="checkbox" name="terminos" id="terminos" value="1" />
            <a href="<?php echo $url_base; ?>terminos-y-condiciones">
                 Al comprar acepto los Términos y condiciones
            </a>
        </div>
        
        <a href="javascript:void(0)" class="btnTerminarCarro" id="comprar" />Comprar productos</a>
    </div> -->    
</div>
<script type="text/javascript" src="js/fixedCompra.js"></script>
            