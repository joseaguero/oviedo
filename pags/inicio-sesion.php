<script>
<?php 
  if(isset($_COOKIE['usuario_id'])){
    echo 'location.href = "cuenta/mis-datos"';
  }
?>
</script>

<div class="button_xs">
  <div class="tab_login" data-tab="inicio">Iniciar sesión</div>
  <div class="tab_login disabled_tab" data-tab="registro">Regístrate</div>
</div>

<div class="container cont_login">
  <div class="tabs_login">
    <div class="bx active" data-action="1">
      Iniciar sesión
    </div>
    <div class="bx" data-action="2">
      Regístrate
    </div>
  </div>
  <div class="center-login">
    <div class="col" data-tab="1">
      <h2>Iniciar sesión</h2>

      <?php 
        if(isset($_COOKIE[nombreUsuario]) and isset($_COOKIE[claveUsuario])){
          $checked = 'checked="checked"';
          $usuarioCookie = $_COOKIE[nombreUsuario];
          $claveCookie = base64_decode($_COOKIE[claveUsuario]);
          } else {
            $checked = '';
            }
          ?>
      <form action="ajax/login.php" method="post" class="formularioLogin" id="formularioLogin">
        <div class="form-group">
          <label for="email">Correo electrónico</label>
          <input type="text" id="email" name="email" class="campoGrande" value="<?= $usuarioCookie; ?>" />
        </div>

        <div class="form-group">
          <label for="clave">Contraseña</label>
          <input type="password" id="clave" name="clave" class="campoGrande" value="<?= $claveCookie; ?>" />
        </div>

        <input type="hidden" name="origen" value="inicio-sesion" />
        <div class="clearfix"></div>
        <div class="text-center term-login">
          <a href=javascript:void(0)" class="btnRecuperarClave">¿Has olvidado tu contraseña?</a>
        </div>
         
         <button id="inicioSesion" class="btnFormCompraRapida btn">Iniciar sesión</button>

        <div class="text-center term-login">
          <p>
            Al iniciar sesión, aceptas las <a href="politicas-de-privacidad">Políticas de privacidad</a> y los <a href="terminos-y-condiciones">Términos y condiciones</a>
          </p>
        </div>
      </form>
    </div>

    <div class="col" data-tab="2">
      <h2>Regístrate</h2>
      
      <form action="registro" method="post" id="formRegistroPagLogin">
        <div class="form-group">
          <label for="nombre">Nombre y apellido</label>
          <input type="text" name="nombre" />
        </div>

        <div class="double-form">
          <div class="form-group">
            <label for="nombre">Correo electrónico</label>
            <input type="email" name="email-registro" />
          </div>

          <div class="form-group">
            <label for="nombre">Repetir correo electrónico</label>
            <input type="email" name="email-re" />
          </div>
        </div>

        <div class="double-form">
          <div class="form-group">
            <label for="nombre">Contraseña</label>
            <input type="password" name="password" />
          </div>

          <div class="form-group">
            <label for="nombre">Repetir contraseña</label>
            <input type="password" name="password-re" />
          </div>
        </div>
        
        <button class="btnFormCompraRapida btn mtop-30" id="btnRegistroLogin">Registrarse</button>
      </form>
    </div>
  </div>
</div>

<section class="servicios">
    <div class="col">
        <img src="img/s1.svg">
        <span>Compra online y retira en tienda</span>
    </div>
    <div class="col">
        <img src="img/s2.svg">
        <span>Entrega a todo Chile</span>
    </div>
    <div class="col">
        <img src="img/s3.svg">
        <span>Compra fácil y segura</span>
    </div>
    <div class="col">
        <img src="img/s4.svg">
        <span>Todo medio de pago</span>
    </div>
</section>

<section class="newsletter">
    <div class="container content_news">
        <p>Infórmate de lo último de Herramientas Chile. Nuestras ofertas y novedades directamente a tu e-mail.</p>

        <div class="form_newsletter">
            <form method="post" action="ajax/newsletter.php" id="formNewsletter">
                <input type="text" name="newsletter" value="" class="campoGrande" id="newsletter" placeholder="Ingrese su correo" />
                <a href="javascript:void(0)" id="btnNewsletter"><img src="img/arrow_white.svg"></a>
            </form>
        </div>
    </div>
</section>

<div class="bg_popup_login"></div>
<div class="content_popup_login">
  <div class="t">¿Has olvidado tu contraseña?</div>
  <div class="s">Introduce tu dirección de correo electrónico y te enviaremos las instrucciones para restablecer tu contraseña</div>

  <div class="form-group mtop-20">
    <label>Correo electrónico</label>
    <input type="text" name="email" class="emailRec">
    <a href="javascript:void(0)" class="restablecerPass btn">Restablecer contraseña</a>
  </div>
</div>
