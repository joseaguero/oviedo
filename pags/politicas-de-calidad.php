<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">políticas de privacidad</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="container mtop-20">
    <div class="max-plains">
        <div class="titulo_p">Políticas de calidad</div>

        <p class="subt_p">Ferretería Oviedo S.A.</p>
        <p class="body_p">Ferretería Oviedo, empresa dedicada a la comercialización de productos industriales, se ha propuesto que todos sus productos y servicios den satisfacción a los requerimientos y necesidades de nuestros clientes, a través de: <br><br>

    	1. La instauración de Procesos Logísticos Planificados y Eficaces <br><br>

    	2. La realización de un asesoramiento al Cliente en función de sus necesidades y conforme al uso previsto de los productos, cumpliendo con los requisitos de calidad y funcionalidad acorde a sus requerimientos.<br><br>


		3. El cumplimiento de los requisitos legales y reglamentarios aplicables a la organización.<br><br>


		Nos comprometemos a mejorar continuamente la eficacia del sistema de gestión de la calidad, basado en la Norma ISO 9001 versión 2015, contando con un equipo profesional que asegura la realización de las actividades en forma eficaz y entregando siempre productos de calidad.</p>
    </div>
</div>
