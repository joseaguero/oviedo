<?php 
	include("../admin/conf.php");
	include("../funciones.php");
	require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

	$id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
    $qty = (isset($_GET[qty])) ? mysqli_real_escape_string($conexion, $_GET[qty]) : 0;
	$producto = consulta_bd("p.nombre, p.thumbs, pd.precio, pd.descuento, p.id, pd.precio_cyber, m.nombre","productos p, productos_detalles pd, marcas m","p.id = pd.producto_id and p.marca_id = m.id and pd.id = $id","");
	
	if ($is_cyber AND is_cyber_product($producto[0][4])) {
        $precios = get_cyber_price($id);
        $precio = $precios['precio'];
        $precio_cyber = $precios['precio_cyber'];
        $precio_normal = $precios['precio'];

        $pd = consulta_bd("precio_cantidad","productos_detalles","id = $id","");
        if($pd[0][0]){
            $precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $id","rango DESC");
            if($precios_cantidad){
                $pc = $precios_cantidad[0];
                $rango      = $pc[1];
                $descuento  = $pc[2];

                $precio = $precio_cyber - ($precio_cyber * ($descuento / 100));
            }else{
                $precio = $precio_cyber;
            }   
        }else{
            $precio = $precio_cyber;
        }

        $valores = '<div class="valorAntFilaArticulo floatLeft ancho100">$'.number_format($precio_normal,0,",",".").'</div>
                        <div class="valorAhoraFilaArticulo floatLeft ancho100">$'.number_format($precio,0,",",".").'</div>';
        $total_pedido = "";

    }else{
        $precio = $producto[0][2];
        $descuento_principal = $producto[0][3];
        $precio_normal = $producto[0][2];
        $pd = consulta_bd("precio_cantidad","productos_detalles","id = $id","");
        if($pd[0][0]){
            $precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $id","rango DESC");
            if($precios_cantidad){
                $pc = $precios_cantidad[0];
                $rango      = $pc[1];
                $descuento  = $pc[2];
                if ($descuento_principal > 0) {
                    $precio = $descuento_principal - ($descuento_principal * ($descuento / 100));
                }else{
                    $precio = $precio - ($precio * ($descuento / 100));
                }
            }else{
                $precio = $producto[0][2];
            }   
        }else{
            $precio = ($descuento_principal > 0) ? $descuento_principal : $precio_normal;
        }

        if($descuento_principal > 0){
            $valores = '<div class="valorAntFilaArticulo floatLeft ancho100">$'.number_format($precio_normal,0,",",".").'</div>
                        <div class="valorAhoraFilaArticulo floatLeft ancho100">$'.number_format($precio,0,",",".").'</div>';
            $total_pedido = "";
        } else {
            $valores = '<div class="valorAhoraFilaArticulo floatLeft ancho100">$'.number_format($precio,0,",",".").'</div>';
            $total_pedido = "";
        }
    }
?>
<div class="fondoPopUp">
    <div class="contPop">
        <a href="javascript:cerrar()" class="cerrar"><img src="img/close-pop.svg"></a>
        <h3><i class="fas fa-check"></i> Añadido al carro de compras</h3>

        <div class="contFilasArticulos floatLeft ancho100">
            <div class="filaArtPopUp floatLeft ancho100">
                <div class="contImgFilaArticulo">
                	<img src="imagenes/productos/<?= $producto[0][1]; ?>" width="100%" />
                </div>
                <div class="contDatosFilaArt">
                    <span class="marca"><?= $producto[0][6] ?></span>
                    <div class="NomFilaArticulo floatLeft ancho100"><?= $producto[0][0]; ?></div>
                    <?= $valores; ?>
                </div>

                <div class="botonesPop">
                    <a href="javascript:cerrar()" class="btnPopUpSeguirComprando">seguir comprando</a>
                    <a href="mi-carro" class="btnPopUpComprar">Comprar</a>
                </div>
            </div>
        </div><!--fin contFilasArticulos -->
        
        <!-- <div class="box_relacionados">
            <h3>Algunnos clientes también compraron</h3>
        </div> -->
    </div>

</div>
<?php mysqli_close($conexion);?>