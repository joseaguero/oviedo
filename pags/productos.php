<?php 
$id = (is_numeric($_GET[id])) ? mysql_real_escape_string($_GET[id]) : 0;
/*$productos = consulta_bd("p.id, p.nombre, p.modelo, p.potencia, p.tipo_motor, p.descripcion, p.detalle_producto, p.manual, p.descripcion_manual, p.ficha_tecnica, p.descripcion_ficha, p.manual_desarme, p.descripcion_manual_desarme, c.nombre, c.id", "productos p, categorias c", "p.categoria_id = c.id and p.id=".$id, "p.id desc");*/


$productos = consulta_bd("p.id, p.nombre, p.modelo, p.potencia, p.tipo_motor, p.descripcion, s.id, p.manual, p.descripcion_manual, p.ficha_tecnica, p.descripcion_ficha, p.manual_desarme, p.descripcion_manual_desarme, c.nombre, c.id", "productos p, categorias c, subcategorias s", "c.id = s.categoria_id and p.subcategoria_id = s.id and p.id=".$id, "p.id desc");


$galeria = consulta_bd("archivo", "img_productos", "producto_id=".$id, "posicion asc");
?>



<div class="contenido">
	<h2><?php echo $productos[0][1];?>
        <div class="ruta">
            <a href="index.php?op=home">INICIO</a> / <a href="index.php?op=categorias&cat=<?php echo $productos[0][14];?>"><?php echo $productos[0][13];?></a> / <a class="activado"><?php echo $productos[0][2];?></a>
        </div>
    </h2>
    
    <div class="detalle_producto">
    	<div class="img_detalle_producto">
        	<?php for($i=0; $i<sizeof($galeria); $i++) {?>
            <?php if ($i == '0'){?>
            <a href="imagenes/productos/<?php echo $galeria[$i][0];?>" rel="example2">
            	<img src="imagenes/productos/<?php echo $galeria[$i][0];?>" width="239" />
            </a>
            <?php } else {?>
            	<a class="oculta" href="imagenes/productos/<?php echo $galeria[$i][0];?>" rel="example2"></a>
            <?php }}?>	
        </div>
        <div class="descripcion_producto">
            <h2><?php echo $productos[0][1];?></h2>
            <h3><?php echo $productos[0][2];?></h3>
            <?php echo $productos[0][5];?>
            
            <a class="cotizar" href="index.php?op=donde_comprar" >DONDE COMPRAR</a><!--onclick="javascript:abrir_cotizador()" -->
            
            <div class="cotizador"><!--inicio cotizador -->
            	<p><strong style="color:#333;">PRODUCTO A COTIZAR</strong><br />
                <?php echo $productos[0][1];?></p>
                <a class="cerrar" onclick="javascript:cerrar()">X</a>
                <form action="cotizar.php?cat=<?php echo $_GET[cat];?>&id=<?php echo $_GET[id];?>&nom_producto=<?php echo $productos[0][1];?>" method="post">
                	<table width="390" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top">
                      	<td width="103"><label for="nombre">NOMBRE</label></td>
                        <td width="287"><input type="text" name="nombre" class="campo_cotizar" /></td>
                      </tr>
                      <tr valign="top">
                        <td><label for="empresa">EMPRESA</label></td>
                        <td><input type="text" name="empresa" class="campo_cotizar" /></td>
                      </tr>
                      <tr valign="top">
                        <td><label for="telefono">TELÉFONO</label></td>
                        <td><input type="text" name="telefono" class="campo_cotizar" /></td>
                      </tr>
                      <tr valign="top">
                        <td><label for="email">E-MAIL</label></td>
                        <td><input type="text" name="email" class="campo_cotizar" /></td>
                      </tr>
                      <tr valign="top" height="40">
                        <td>CANTIDAD</td>
                        <td>
                            <select name="cantidad" class="default">
                              <option selected="selected" value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="ENVIAR COTIZACIÓN" name="enviar" class="enviar_cotizacion" /></td>
                      </tr>
                    </table>
                </form> 
            </div><!--fin cotizador -->
        </div>
    </div><!--fin detalle producto -->
    
    
    
    
    
    <div class="detalle_producto" style="min-height:400px;"><!--inicio tabs -->
    	<div id="tabs">
            <ul>
                <?php /*?><li><a href="#tabs-1">descripción</a></li><?php */?>
                <li><a href="#tabs-3">datos técnicos</a></li>
                <li><a href="#tabs-2">manuales y fichas</a></li>
                
            </ul>
            <?php /*?><div id="tabs-1"><!-- inicio tabs detalle producto -->
                <?php echo $productos[0][6];?>
            </div><!-- fin tabs detalle producto --><?php */?>
            
            <div id="tabs-2"><!--inicio manuales y fichas -->
                <div class="cont_fichas">
                	<?php if($productos[0][7] != ''){ ?>
                	<div class="fila_archivos">
                    	<div class="archivo">
                            <h6>archivo</h6>
                            <?php echo $productos[0][7];?>
                        </div>
                        <div class="descripcion_archivo">
                            <h6>Descripción</h6>
                            <?php echo $productos[0][8];?>
                        </div>
                        <div class="descarga_archivo">
                        	<a href="docs/productos/<?php echo $productos[0][7];?>" target="_blank">
                            	<img src="img/btn_descargar_ficha.png" width="145" height="40" />
                            </a>
                        </div>
                    </div><!--fin fila archivos -->
                    <?php }?>
                    <?php if ($productos[0][9] != ''){?>
                    <div class="fila_archivos">
                    	<div class="archivo">
                            <h6>archivo</h6>
                            <?php echo $productos[0][9];?>
                        </div>
                        <div class="descripcion_archivo">
                            <h6>Descripción</h6>
                            <?php echo $productos[0][10];?>
                        </div>
                        <div class="descarga_archivo">
                        	<a href="docs/productos/<?php echo $productos[0][9];?>" target="_blank">
                            	<img src="img/btn_descargar_ficha.png" width="145" height="40" />
                            </a>
                        </div>
                    </div><!--fin fila archivos -->
                    <?php }?>
                    
                    <?php if ($productos[0][11] != ''){?>
                    <div class="fila_archivos">
                    	<div class="archivo">
                            <h6>archivo</h6>
                            <?php echo $productos[0][11];?>
                        </div>
                        <div class="descripcion_archivo">
                            <h6>Descripción</h6>
                            <?php echo $productos[0][12];?>
                        </div>
                        <div class="descarga_archivo">
                        	<a href="docs/productos/<?php echo $productos[0][11];?>" target="_blank">
                            	<img src="img/btn_descargar_ficha.png" width="145" height="40" />
                            </a>
                        </div>
                    </div><!--fin fila archivos -->
                    <?php }?>
                    
                    
                </div><!--fin contenedor fichas -->
                <div style="clear:both"></div>
            </div> <!--fin tabs manuales y ficha -->
            
            <div id="tabs-3">
            	<div class="cont_fichas">
                	<?php 
					$fichas = consulta_bd("id, nombre, datos", "fichas", "producto_id=".$id, "id desc");?>
                    <?php for($i=0; $i<sizeof($fichas); $i++) {?>
                    <div class="fila_ficha_tecnica">
                    	<div class="iz_titulo">
                        	<?php echo $fichas[$i][1];?> >>
                        </div> <!--fin titulo_izquierdo -->
                        <div class="cont_tabla"><!--class="tabla_ficha"-->
                        	<?php echo $fichas[$i][2];?>
                        </div><!--fin cont tabla -->
                    </div><!--fin fila ficha tecnica -->
                    <?php }?>
                    
                    
              </div><!--fin fichas -->
                <div style="clear:both"></div>
            </div><!--fin tabs ficha tecnica-->
        </div>
    	
    </div><!--fin tabs productos -->
</div> <!--fin contenido -->