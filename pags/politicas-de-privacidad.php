<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">políticas de privacidad</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="container mtop-20">
    <div class="max-plains">
        <div class="titulo_p">Políticas de privacidad</div>

        <p class="subt_p">Declaración de Privacidad</p>
        <p class="body_p">HERRAMIENTASCHILE.CL mantendrá siempre tus datos aislados de terceras empresas o personas que puedan hacer usos de esta información para otros fines. Se garantiza confidencialidad, privacidad y seguridad en la manipulación y uso de cualquier información personal de los clientes inscritos en el sitio web.</p>
        
        <p class="subt_p">Confidencialidad de datos</p>
        <p class="body_p">Actualmente en todo el mundo, la manera más eficaz de resguardar la seguridad de las compras es vía certificados SSL. Estos certificados son los encargados de cifrar o codificar los datos de las transacciones, impidiendo que estas sean intervenidas y descifradas por terceros. Un certificado SSL presente y activo en un sitio web, entrega a los compradores la tranquilidad necesaria para saber que sus datos no serán obtenidos por una tercera persona.</p>

        <p class="subt_p">Compromiso de Seguridads</p>
        <p class="body_p">HERRAMIENTASCHILE.CL velará energéticamente por el uso, la mantención y mejora de los estándares de seguridad y privacidad hacia sus clientes registrados en el sitio web. Se mantendrán los estándares requeridos, tanto en infraestructura, herramientas y dotación para cumplir con este compromiso de resguardo de toda información otorgada en los formularios de registros.</p>

        <p class="subt_p">Finalidad de datos</p>
        <p class="body_p">Las informaciones requeridas en el formulario de registro tiene por finalidad el identificar a los clientes, mantenerlos informados, solucionar reclamos, revisar pedidos, consultar por despachos o recibir consultas y sugerencias. Esto permite tener un contacto más cercano, rápido y detallado que ayuda a un accionar más rápido y eficiente en cualquiera de las situaciones.</p>

        <p class="subt_p">Datos Personales</p>
        <p class="body_p">Conforme a la Ley Nº 19.628 sobre protección de datos de carácter personal, cualquier usuario podría en todo momento modificar, revisar o eliminar su información personal de nuestros registros. Esto puede hacerse por las siguientes vías: <br><br>

        Escribiendo a <a href="mailto:ventaweb@herramientaschile.cl">ventaweb@herramientaschile.cl</a> <br><br>

        Llamando al teléfono 24601800 en horario hábil.</p>
    </div>
</div>
