<?php
$popup = consulta_bd('activo', 'active_popup', '', '');  
$nombre_cliente = opciones('nombre_cliente');
$nombre_popup = 'POPUP'.strtoupper(str_replace(' ', '', $nombre_cliente));
if ($popup[0][0] == 1 AND !isset($_COOKIE[$nombre_popup])) { ?>
    <script>
        setTimeout(function(){
            $('.bg-popup').fadeIn();
            $('.popup-news').fadeIn();
        }, 2000)
    </script>
<?php }

$titulos = consulta_bd('nombre', 'titulos', '', 'posicion asc');
$slider = consulta_bd('imagen, imagen_tablet, imagen_movil, link', 'slider', 'publicado = 1', 'posicion asc');

?>
<div class="bg-popup" data-cliente="<?= $nombre_popup ?>">
    <div class="popup-news">
        <div class="contenido">
            <div class="close"></div>
            <h3 style="text-align: center;">ACÁ EL LOGO</h3>
            <p>¡Suscríbete a nuestro newsletter y <br>entérate primero de nuestras ofertas, novedades y más!</p>
            <div class="datos">
                <input type="text" name="nombre" placeholder="Nombre" id="nombre-suscribe">
                <input type="email" name="email" placeholder="Email" id="email-suscribe">
                <a href="#" id="suscribe-newsletter">SUSCRIBIRME</a>
                <h3 style="text-align: center;">LOGO PEQUEÑO</h3>
            </div>
        </div>
    </div>
</div>

<section id="slider">
    <div class="slider-home">
        <?php foreach ($slider as $sl):
            $thumb_desk = imagen("imagenes/slider/", $sl[0]);
            $thumb_tablet = imagen("imagenes/slider/", $sl[1]);
            $thumb_movil = imagen("imagenes/slider/", $sl[2]); ?>
            <div class="item">
                <a href="<?= $sl[3] ?>" class="s-desktop">
                    <img src="<?= $thumb_desk ?>">
                </a>

                <a href="<?= $sl[3] ?>" class="s-tablet">
                    <img src="<?= $thumb_tablet ?>">
                </a>

                <a href="<?= $sl[3] ?>" class="s-movil">
                    <img src="<?= $thumb_movil ?>">
                </a>
            </div>
        <?php endforeach ?>
    </div>
    <div class="slider-progress">
        <div class="progress"></div>
    </div>
</section>

<?php 
$oferta_home = consulta_bd('imagen_hotsale, imagen_producto', 'oferta_home', "id = 1 and publicado = 1", ''); 
$producto_off_home = consulta_bd('p.id, p.nombre, pd.oferta_tiempo_descuento, pd.id, pd.oferta_tiempo_hasta', 'productos p join productos_detalles pd on p.id = pd.producto_id', "pd.oferta_home = 1", "");
if (is_array($oferta_home) AND is_array($producto_off_home)) { 
    $fecha_hoy = date("Y-m-d H:i:s", time());
    if ($producto_off_home[0][4] > $fecha_hoy) { ?>
        <section class="hot_sale">
            <div class="container grid_oferta_home">
                <div class="col">
                    <div class="img_hotsale">
                        <img src="imagenes/oferta_home/<?= $oferta_home[0][0] ?>">
                    </div>
                </div>
                <div class="col">
                    
                    <div class="content_tiempo">
                        <img src="img/clock.svg">

                        <span>Ofertas por poco tiempo</span>

                        <div class="timer" data-fecha="<?= $producto_off_home[0][4] ?>">
                            <div class="box_time" data-hour="hora">
                                <div class="time">
                                    00
                                </div>

                                <span class="text">horas</span>
                            </div>

                            <div class="box_time" data-hour="minuto">
                                <div class="time">
                                    00
                                </div>

                                <span class="text">minutos</span>
                            </div>

                            <div class="box_time" data-hour="segundos">
                                <div class="time">
                                    00
                                </div>

                                <span class="text">segundos</span>
                            </div>
                        </div>

                        <span class="mtop-10">hasta agotar stock</span>
                    </div>

                </div>
                <div class="col">
                    <img src="imagenes/oferta_home/<?= $oferta_home[0][1] ?>">
                </div>
                <div class="col">
                    <div class="content_info">
                        <div class="nombre">
                            <?= $producto_off_home[0][1] ?>
                        </div>

                        <div class="precio">$ <?= number_format($producto_off_home[0][2], 0, ',', '.') ?></div>

                        <a href="ficha/<?= $producto_off_home[0][0] ?>/<?= url_amigables($producto_off_home[0][1]) ?>" class="btn button_oferta">Comprar</a>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
<?php } ?>

<section class="buy_category">
    <div class="home_title">
        <?= $titulos[0][0] ?>
    </div>

    <?php $lineas_home = consulta_bd('id,nombre,imagen', 'lineas', '', 'posicion asc'); ?>

    <div class="grid owl-carousel owl-theme">
        <?php foreach ($lineas_home as $lhome): 
            $img_linea = imagen("imagenes/lineas/", $lhome[2]); ?>
            <div class="item col">
                <a href="lineas/<?=$lhome[0]?>/<?=url_amigables($lhome[1])?>">
                    <div class="imagen">
                        <img src="<?= $img_linea ?>">
                    </div>

                    <div class="nombre">
                        <?= $lhome[1] ?>
                    </div>
                </a>
            </div>
        <?php endforeach ?>
    </div>
</section>

<?php 

$banner_one = consulta_bd('id, nombre, imagen, link', 'banner_uno', '', 'posicion asc');

?>
<section class="banners_one">
    <div class="home_title">
        <?= $titulos[1][0] ?>
    </div>

    <div class="grid">
        <div class="col">
            <a href="<?= $banner_one[0][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[0][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_one[1][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[1][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_one[2][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[2][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_one[3][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[3][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_one[4][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[4][2]) . '" />' ?>
            </a>
        </div>
    </div>

    <div class="banner-slider">
        <div class="item">
            <a href="<?= $banner_one[2][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[2][2]) . '" />' ?>
            </a>
        </div>

        <div class="item">
            <a href="<?= $banner_one[3][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[3][2]) . '" />' ?>
            </a>
        </div>

        <div class="item">
            <a href="<?= $banner_one[4][3] ?>">
                <?php echo '<img src="' . imagen("imagenes/banner_uno/", $banner_one[4][2]) . '" />' ?>
            </a>
        </div>
    </div>
</section>


<section class="home_products">
    <div class="home_title">
        <?= $titulos[2][0] ?>
    </div>

    <?php 
    $filtro_home['home'] = true; 
    $productos_home = get_products($filtro_home);
    ?>

    <div class="productos_relacionados mtop-20">

        <div class="grid_products grid_four mtop-20">
            <?php foreach ($productos_home['productos']['producto'] as $item): 
                $url_producto = 'ficha/' . $item['id_producto'] . '/' . $item['nombre_seteado'];
                ?>
                <div class="col">
                    <?php if ($item['precio_cantidad'] == 1): ?>
                        <div class="descuento_cantidad">descuento por cantidad</div>
                    <?php endif ?>
                    <?php if (isset($_COOKIE['usuario_id'])): ?>
                        <?php if (producto_guardado($_COOKIE['usuario_id'], $item['id_producto'])): ?>
                            <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="remove"><i class="fas fa-heart"></i></div>
                        <?php else: ?>
                            <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                        <?php endif ?>
                    <?php else: ?>
                        <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                    <?php endif ?>
                    

                    <!-- Porcentaje descuento  -->
                    <?php if ( ofertaTiempo($item['id_hijo']) ): ?>
                        <div class="porcentaje_descuento">-<?= round(100 - ($item['oferta_tiempo_descuento'] * 100) / $item['precio']) ?>%</div>
                    <?php elseif($is_cyber AND $item['precio_cyber'] > 0): ?>
                        <div class="porcentaje_descuento">-<?= round(100 - ($item['precio_cyber'] * 100) / $item['precio']) ?>%</div>
                    <?php elseif( $item['descuento'] > 0 ): ?>
                        <div class="porcentaje_descuento">-<?= round(100 - ($item['descuento'] * 100) / $item['precio']) ?>%</div>
                    <?php endif ?>
                    
                    <div class="thumb">
                        <a href="<?= $url_producto ?>">
                            <?php if ($item['imagen_grilla'] != 'img/sin-foto.jpg'): 
                                $thumb = imagen("imagenes/productos/", $item['nombre_imagen']); ?>
                                <img src="<?= $thumb ?>">
                            <?php else: ?>
                                <img src="<?= $item['imagen_grilla'] ?>">
                            <?php endif ?>
                            
                        </a>
                        <?php if(ofertaTiempo($item['id_hijo'])){
                            $oferta_tiempo_hasta = $item['oferta_tiempo_hasta'];
                         ?>
                            <div class="countdown" rel="<?= $oferta_tiempo_hasta ?>">
                                <div class="col">
                                    <small>Hora</small>
                                    <div class="box" data-rel="hour">00</div>
                                </div>
                                <div class="col">
                                    <small>Min.</small>
                                    <div class="box" data-rel="minute">00</div>
                                </div>
                                <div class="col">
                                    <small>Seg.</small>
                                    <div class="box" data-rel="second">00</div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <a href="<?= $url_producto ?>">
                        <span class="nombre"><?= $item['nombre'] ?></span>
                        
                        <?php if ( ofertaTiempo( $item['id_hijo'] ) ): ?>
                            <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                            <span class="precio"><small class="signo">$</small><?= number_format(getPrecio($item['id_hijo']),0,",",".") ?> <small class="iva">IVA Incluido
                        <?php else: ?>
                            <?php if ($item['descuento'] > 0): ?>
                                <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                                <span class="precio"><small class="signo">$</small><?= number_format($item['descuento'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                            <?php else: ?>
                                <span class="descuento"></span>
                                <span class="precio"><small class="signo">$</small><?= number_format($item['precio'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                            <?php endif ?>
                        <?php endif; ?>
                        
                        
                        </small></span>
                    </a>

                </div>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</section>

<?php 

$banner_dos = consulta_bd('imagen, link, imagen_movil', 'banner_dos', '', 'posicion asc');

?>

<section class="banners_two mtop-20">
    <div class="home_title">
        <?= $titulos[3][0] ?>
    </div>
    
    <div class="grid mtop-20">
        <div class="col">
            <a href="<?= $banner_dos[0][1] ?>" class="gdesk">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[0][0]) . '" />' ?>
            </a>
            <a href="<?= $banner_dos[0][1] ?>" class="gmovil">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[0][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_dos[1][1] ?>" class="gdesk">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[1][0]) . '" />' ?>
            </a>
            <a href="<?= $banner_dos[1][1] ?>" class="gmovil">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[1][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_dos[2][1] ?>" class="gdesk">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[2][0]) . '" />' ?>
            </a>
            <a href="<?= $banner_dos[2][1] ?>" class="gmovil">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[2][2]) . '" />' ?>
            </a>
        </div>
        <div class="col">
            <a href="<?= $banner_dos[3][1] ?>" class="gdesk">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[3][0]) . '" />' ?>
            </a>
            <a href="<?= $banner_dos[3][1] ?>" class="gmovil">
                <?php echo '<img src="' . imagen("imagenes/banner_dos/", $banner_dos[3][2]) . '" />' ?>
            </a>
        </div>
    </div>
</section>

<section class="servicios">
    <div class="col">
        <img src="img/s1.svg">
        <span>Compra online y retira en tienda</span>
    </div>
    <div class="col">
        <img src="img/s2.svg">
        <span>Entrega a todo Chile</span>
    </div>
    <div class="col">
        <img src="img/s3.svg">
        <span>Compra fácil y segura</span>
    </div>
    <div class="col">
        <img src="img/s4.svg">
        <span>Todo medio de pago</span>
    </div>
</section>

<section class="newsletter">
    <div class="container content_news">
        <p>Infórmate de lo último de Herramientas Chile. Nuestras ofertas y novedades directamente a tu e-mail.</p>

        <div class="form_newsletter">
            <form method="post" action="ajax/newsletter.php" id="formNewsletter">
                <input type="text" name="newsletter" value="" class="campoGrande" id="newsletter" placeholder="Ingrese su correo" />
                <a href="javascript:void(0)" id="btnNewsletter"><img src="img/arrow_white.svg"></a>
            </form>
        </div>
    </div>
</section>