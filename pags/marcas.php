<?php 

$marcas = consulta_bd('id, imagen,nombre', 'marcas', "imagen is not null AND imagen != ''", '');

?>
<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="marcas" itemprop="item">
                <span itemprop="name">Marcas</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="container mtop-20 mbottom-20">
    <div class="cont-brand">

        <?php foreach ($marcas as $marca): 
            $imagen_marca = imagen("imagenes/marcas/", $marca[1]); ?>
            <div class="col-brand">
                <a href="marcas/<?=$marca[0]?>/<?=url_amigables($marca[2])?>">
                    <img src="<?= $imagen_marca ?>">
                </a>
            </div>
        <?php endforeach ?>

    </div>
</div>
