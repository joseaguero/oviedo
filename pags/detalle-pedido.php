<div class="bannerFicha cont100"></div>
<script>
<?php 
$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
    if(!isset($_COOKIE['usuario_id'])){
        echo 'location.href = "inicio-sesion"';
    }
?> 
</script>

<?php 

    $pedidos = consulta_bd("
    id,
    oc,
    total_pagado, 
    fecha_creacion, 
    medio_de_pago, 
    retiro_en_tienda, 
    direccion, 
    comuna, 
    region, 
    total, 
    valor_despacho, 
    total_pagado, 
    accounting_date, 
    card_number, 
    card_expiration_date, 
    authorization_code, 
    payment_type_code, 
    shares_number, 
    amount, 
    transaction_date, 
    mp_payment_type, 
    mp_payment_method, 
    mp_auth_code, 
    mp_paid_amount, 
    mp_card_number, 
    mp_id_paid, 
    mp_cuotas, 
    mp_valor_cuotas, 
    mp_transaction_date, 
    descuento","pedidos","estado_id = 2 and oc = '$oc' and cliente_id = ".$_COOKIE['usuario_id'],"fecha_creacion desc");
    
    $medio_de_pago = ($pedidos[0][4] == 'webpay') ? 'tbk' : 'mp';
    $num_cuotas    = ($pedidos[0][4] == 'webpay') ? $pedidos[0][17] : $pedidos[0][26];
    $tipo_pago     = ($pedidos[0][4] == 'webpay') ? $pedidos[0][16] : $pedidos[0][20];
    $tipo_pago = tipo_pago($tipo_pago,$num_cuotas,$medio_de_pago);

    $fecha = ($pedidos[0][4] == 'webpay') ? date('d/m/Y', time($pedidos[0][19])) : date('d/m/Y', time($pedidos[0][28]));
        $hora_pago = ($pedidos[0][4] == 'webpay') ? date('H:s:i', time($pedidos[0][19])) : date('H:s:i', time($pedidos[0][28]));

    $total_pagado = ($pedidos[0][4] == 'webpay') ? $pedidos[0][18] : $pedidos[0][23];
    
?>

<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li><a href="mis-pedidos">Mis Pedidos</a></li>
          <li class="active"><?= $oc; ?></li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">Detalle pedido: <?= $oc; ?></div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">

       
        
        <?php for($i=0; $i<sizeof($pedidos); $i++){ ?>
        <div class="detallePedido">
                <div class="encabezado">
                    <div class="ancho20">
                        <span class="cont100"><strong>Fecha del pedido</strong></span>
                        <span class="cont100"><?= formato_fecha(substr($pedidos[$i][3], 0, 10), "esp"); ?></span>
                    </div>
                    <div class="ancho20">
                        <span class="cont100"><strong>TOTAL PAGADO</strong></span>
                        <span class="cont100">$<?= number_format($pedidos[$i][2],0,",",".");?></span>
                    </div>
                    <div class="floatRight ancho20">
                        <span class="cont100"><strong>ORDEN: <?= $pedidos[$i][1]?></strong></span>
                    </div>
                </div><!--fin encabezado -->
                
                <!-- Datos pedido /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// ///////////////////-->
                <div class="cont100 contDatosPago">
                    <div class="ancho33">
                        <?php if($pedidos[0][5] == 1){ ?>
                                <h3>Retiro en tienda</h3>
                            <?php } else { ?>
                                <h3>Despacho</h3>
                            <?php } ?>
                        <div class="cont100">
                            <div class="cont100"><?= $pedidos[0][6]."<br />".$pedidos[0][7].", ".$pedidos[0][8]; ?></div>
                        </div>
                    </div>
                    
                    <div class="ancho33">
                        <h3>Pago</h3>
                        <div class="cont100">
                            <div class="cont100"><span class="nvalor">Medio de pago:</span> <?= ucwords($pedidos[0][4]) ?></div>
                            <?php if($pedidos[0][4] == "webpay"){ ?>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de transacción:</span> Venta
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">N° de Cuotas:</span> <?= $tipo_pago[cuota]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de cuota:</span> <?= $tipo_pago[tipo_cuota]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tarjeta terminada en:</span>**** **** **** <?= $pedidos[0][13]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de pago:</span> <?= $tipo_pago['tipo_pago']; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Fecha y hora de transacción:</span> <?= $fecha; ?> | <?= $hora_pago; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Valor pagado:</span> $<?= number_format($pedidos[0][18], 0, ',', '.'); ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Código de autorización:</span> <?= $pedidos[0][15]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de moneda:</span>CLP / PESO Chileno
                                </div>
                            <?php } else { ?> 
                                <div class="cont100">
                                    <span class="nvalor">Tipo de transacción:</span> Venta
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">N° de Cuotas:</span> <?= $tipo_pago[cuota]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de cuota:</span> <?= $tipo_pago[tipo_cuota]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tarjeta terminada en:</span>**** **** **** <?= $pedidos[0][24]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de pago:</span> <?= $tipo_pago['tipo_pago']; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Fecha y hora de transacción:</span> <?= $fecha; ?> | <?= $hora_pago; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Valor pagado:</span> $<?= number_format($pedidos[0][23], 0, ',', '.'); ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Código de autorización:</span> <?= $pedidos[0][22]; ?>
                                </div>
                                <div class="cont100">
                                    <span class="nvalor">Tipo de moneda:</span>CLP / PESO Chileno
                                </div>
                            <?php } ?>
                            
                            
                        </div>
                    </div>
                    
                    <div class="ancho33">
                        <h3>Totales</h3>
                        <div class="cont100">
                            <div class="cont50"><span class="nvalor">Subtotal</span></div>
                            <div class="cont50 textAlignRight">$<?= number_format($pedidos[0][9],0,",","."); ?></div>
                        </div>
                        <div class="cont100">
                            <div class="cont50"><span class="nvalor">Descuentos</span></div>
                            <div class="cont50 textAlignRight">$<?= number_format($pedidos[0][29],0,",","."); ?></div>
                        </div>
                        <div class="cont100">
                            <div class="cont50"><span class="nvalor">Envio</span></div>
                            <div class="cont50 textAlignRight">$<?= number_format($pedidos[0][10],0,",","."); ?></div>
                        </div>
                        <div class="cont100 totalDetallePedido">
                            <div class="cont50"><span class="nvalor">Total cancelado</span></div>
                            <div class="cont50 textAlignRight">$<?= number_format($total_pagado,0,",","."); ?></div>
                        </div>
                    </div>
                </div><!--Datos pedido /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// ///////////////////-->
                
                
                <div class="cont100">
                    <div class="ancho70Pedido">
                        <h3 class="tituloPedidoGuardado">Estado del pedido: Procesando</h3>
                        
                        <?php 
                        $idPedido = $pedidos[$i][0];
                        $detalles = consulta_bd("pd.id, p.nombre, pd.sku, pp.cantidad, pp.precio_unitario, p.thumbs, pd.descuento, pd.precio, pd.publicado, p.id","productos_detalles pd, productos_pedidos pp, productos p","pp.pedido_id = $idPedido and pp.productos_detalle_id = pd.id and pd.producto_id = p.id","");
                        for($j=0; $j<sizeof($detalles); $j++){
                            ?>
                        <div class="cont100 filaProductoComprado">
                            <a href="ficha/<?= $detalles[$j][9]; ?>/<?= url_amigables($detalles[$j][1]); ?>" target="_blank" class="thumbsProducto">
                                <img src="imagenes/productos/<?= $detalles[$j][5]; ?>" width="100%" />
                            </a>
                            <div class="infoProdDetalle">
                                <span><strong><?= $detalles[$j][1]; ?></strong></span>
                                <span>SKU: <?= $detalles[$j][2]; ?></span>
                                <span>Cantidad: <?= $detalles[$j][3]; ?></span>
                                <span class="rojo"><strong>Valor pagado: $<?= number_format($detalles[$j][4],0,",","."); ?></strong></span>
                                <?php if($detalles[$j][8] == 1){
                                    if($detalles[$j][6] > 0){
                                        $valorActual = $detalles[$j][6];
                                        } else {
                                            $valorActual = $detalles[$j][7];
                                            }
                                    if($valorActual < $detalles[$j][4]){
                                        $mensajeBoton = "Cómpralo ahora por: $.". number_format($detalles[$j][6],0,",",".");
                                        } else {
                                            $mensajeBoton = "Cómpralo ahora";
                                            }
                                    
                                    ?>
                                <a class="btnComprarAhora" rel="<?= $detalles[$j][0]; ?>" href="javascript:void(0);"><?= $mensajeBoton; ?></a>
                                <?php } ?>
                            </div>
                        </div><!--Fin producto -->
                        <?php } ?>
                    </div>
                    <div class="contBotones">
                        <!--<a class="btnPedidos" href="javascript:void(0);">Seguimiento despacho</a>-->
                    </div>
                </div>
            </div>
        <?php } ?>
            <!--fin cont100 -->
            
            
            
            
            
            
            
       
       
       </div><!--fin contenidoMiCuenta-->
    
    </div>
</div>
