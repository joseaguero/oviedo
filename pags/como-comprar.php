<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">cómo comprar</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="container mtop-20">
    <div class="max-plains-grid">
        <div class="col">
            
            <div class="titulo_p">Cómo comprar</div>

            <p class="body_p">
                <span>1.</span> Vitrinea y luego selecciona el producto que quieras comprar y agrégalo al carro de compras.  Puedes revisar tu carro de compras las veces que quieras, para agregar o quitar productos. <br><br>

                <span>2.</span> Encontrarás el resumen de tu compra donde estarán él o los productos agregados al carro.  Si tienes un Cupón de Descuento o Gift Card, actívalo en este paso. <br><br>

                <span>3.</span> Proceso de identificación, envío, documento de compra y pago = TODO EN UN SOLO PASO. <br><br>

                Debes ingresar tu correo electrónico para luego completar tus datos, seleccionar el documento de compra (boleta o factura), dirección de envío o retiro en tienda; según sea tu elección. <br><br> 

                Si ya has realizado compras anteriores tus datos y dirección de envío ya estarán registrados, en el caso que la quisieras editar, debes solicitar una clave que será enviada a tu email.  <br><br>

                Revisa bien tus datos que aparecen en el resumen antes de hacer click en comprar. Una vez que presiones el botón comprar irás al sistema seguro de pago Webpay.
            </p>

        </div>

        <div class="col">
            <img src="img/cc.svg">
        </div>
        
    </div>
</div>
