<?php 

$cliente_id = (int)$_COOKIE['usuario_id'];
$cliente = consulta_bd('id, nombre, rut, telefono, email', 'clientes', "id = $cliente_id", '');

?>

<div class="welcome">
    <small>Hola,</small>
    <span><?= $cliente[0][1] ?></span>
</div>

<div class="btn_dash_xs">
    <i class="material-icons">menu</i> 
    <span>Menú</span>
</div>

<div class="menuMiCuenta">

    <a class="<?php if($op == "mi-cuenta"){echo "current";}?>" href="cuenta/mis-datos">
    	<img src="img/card.svg" style="margin-top: 4px;"> <span>Mis Datos</span>
    </a>
    <a class="<?php if($op == "mis-direcciones"){echo "current";}?>" href="cuenta/mis-direcciones">
    	<img src="img/house.svg" style="width: 17px;"> <span>Mis Direcciones</span>
    </a>
    <a class="<?php if($op == "mis-pedidos" || $op == "detalle-pedido"){echo "current";}?>" href="cuenta/mis-pedidos">
    	<img src="img/box.svg" style="width: 17px;"> <span>Mis Pedidos</span>
    </a>
    <a class="<?php if($op == "productos-guardados"){echo "current";}?>" href="cuenta/productos-guardados">
        <img src="img/heart.svg" style="margin-top: 3px;"> <span>Productos Guardados</span>
    </a>
    
    <a href="ajax/cerrar-sesion.php">
        <img src="img/logout.svg" style="margin-top: 3px;"> Cerrar sesión
    </a>

</div>