<?php 
  if(!isset($_COOKIE['usuario_id'])){
    echo '<script>location.href = "inicio-sesion"</script>';
  }

  $cliente_id = $_COOKIE['usuario_id'];
  $id = mysqli_real_escape_string($conexion, $_GET['id']);

  $cliente_direccion = consulta_bd('id', 'clientes_direcciones', "id = $id AND cliente_id = $cliente_id", '');

  if (!is_array($cliente_direccion)) {
    echo '<script>location.href = "cuenta/mis-direcciones"</script>';
  }else{

    $direcciones = consulta_bd('id, nombre, region_id, comuna_id, numero, calle, piso, principal', 'clientes_direcciones', "id = $id", '');

    $comunas_region = consulta_bd('id, nombre', 'comunas', "region_id = {$direcciones[0][2]}", 'nombre asc');
  }

?>

<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">cuenta</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">mis direcciones</span>
                <meta itemprop="position" content="3" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">editar dirección</span>
                <meta itemprop="position" content="4" />
            </a>
        </li>
    </ul>
</div>

<div class="content_gray">
  
  <div class="content-dashboard">
    <h2 class="title-dash">Mi cuenta</h2>
    <div class="grid-dashboard">
      <div class="col">

        <?php include("pags/menuMiCuenta.php"); ?>

      </div>
      <div class="col">
        
        <div class="main_dash">
          
          <div class="title">Mis direcciones</div>
          <div class="subtitle">
            Podrás ver tus pedidos, historial de compra y <br>editar tus datos personales y de envío.
          </div>
          
            <div class="double-form">
                <div class="form-carro">
                    <?php $regiones = consulta_bd('id, nombre', 'regiones', '', 'posicion asc'); ?>
                    <label>Región <small class="obl">*</small></label>
                    <select name="region" id="region_dir">
                        <option value="0">Seleccionar región</option>
                        <?php foreach ($regiones as $r): ?>

                            <?php if ($direcciones[0][2] == $r[0]): ?>
                                <option value="<?= $r[0] ?>" selected><?= $r[1] ?></option>
                            <?php else: ?>
                                <option value="<?= $r[0] ?>"><?= $r[1] ?></option>
                            <?php endif ?>
                            
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-carro">
                    <label>Comuna <small class="obl">*</small></label>
                    <select name="comuna" id="comuna_dir">
                        <option value="0">Seleccionar comuna</option>
                        <?php foreach ($comunas_region as $comuna): ?>
                            <?php if ($direcciones[0][3] == $comuna[0]): ?>
                                <option value="<?= $comuna[0] ?>" selected><?= $comuna[1] ?></option>
                            <?php else: ?>
                                <option value="<?= $comuna[0] ?>"><?= $comuna[1] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

            <div class="double-form">
                <div class="form-carro">
                    <label>Calle <small class="obl">*</small></label>
                    <input type="text" name="direccion" id="calle_dir" value="<?= $direcciones[0][5] ?>">
                </div>

                <div class="double-cart">
                    <div class="form-carro">
                        <label>Número <small class="obl">*</small></label>
                        <input type="text" name="numero" id="numero_dir" value="<?= $direcciones[0][4] ?>">
                    </div>

                    <div class="form-carro">
                        <label>Piso o Dpto.</label>
                        <input type="text" name="piso" id="piso_dir" value="<?= $direcciones[0][6] ?>">
                    </div>
                </div>
            </div>

            <div class="form-carro">
                <label>Asignar alías <small class="tip">(Ejemplo: Oficina)</small></label>
                <input type="text" name="alias" id="alias_dir" value="<?= $direcciones[0][1] ?>">
            </div>

            <div class="ohidden">
                <?php if ($direcciones[0][7] == 1): ?>
                    <input type="checkbox" class="chk" name="principal" id="chkPrincipal" checked>
                <?php else: ?>
                    <input type="checkbox" class="chk" name="principal" id="chkPrincipal">
                <?php endif ?>
                
                <label for="chkPrincipal" class="not_label">Guardar como dirección principal</label>
            </div>
            
            <a href="javascript:void(0)" class="button_dash btnEditDash btn" data-id="<?= $direcciones[0][0] ?>">Actualizar</a>
            <a href="cuenta/mis-direcciones" class="button_dash btnCancelDash">Cancelar</a>
          </div>
        
        </div>

      </div>
    </div>
  </div>
  
</div>

<div class="bg_popup"></div>
<div class="popup_acciones">
  <div class="title_pop">¿Estás seguro de eliminar esta publicación?</div>

  <div class="content_buttons">
    <a href="javascript:void(0)" class="confirmPop" data-action="delete" data-id="0" data-where="dash">Confirmar</a>
    <a href="javascript:void(0)" class="cancelPop">Cancelar</a>
  </div>
</div>
