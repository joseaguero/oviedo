<?php
include("../admin/conf.php");
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
include("../funciones.php");

if(isset($_GET[op])){
    $op=$_GET[op];
    } else{
        $op="home";
    }
	include("../php-html-css-js-minifier.php");
	
include("../includes/cookies.php");

?>
<body>
<div class="cont_loading">
	<div class="lds-dual-ring"></div>
</div>

<?php 
	echo 'aaaaa';
?>
 
<!-- JavaScript -->
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
<script src="js/jquery.fancybox.min.js"></script>   
	<script type="text/javascript" src="js/jquery.Rut.js"></script>
	<script type="text/javascript" src="admin/js/jquery.numeric.js"></script>
    <script type="text/javascript">$(function(){$("#telefono, #phone").numeric();});</script>
    <script type="text/javascript" src="js/validacionCompraRapida.js"></script>
    <script type="text/javascript" src="js/agrega_desde_ficha.js"></script>
    <script type="text/javascript" src="js/cotiza_desde_ficha.js"></script>
    <?php 
		$codigoDescuento = file_get_contents('js/codigoDescuento.js');
		echo '<script>';
		echo minify_css($codigoDescuento);
		echo '</script>';
	?>
	<script type="text/javascript" src="js/js.cookie.min.js"></script>
    <script type="text/javascript" src="js/funciones.js"></script>
    <script type="text/javascript">function cerrar(){ $(".fondoPopUp").remove();}</script>
    <script type="text/javascript" src="js/agrega_quita_elimina_ShowCart.js"></script>
	<script type="text/javascript" src="js/agrega_quita_elimina_ShowCartCotizacion.js"></script>
    <script type="text/javascript" src="js/lista_productos.js"></script>
    <script type="text/javascript" src="js/validacionCotizacion.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    
    <?php if($op == "ficha"){?><script type="text/javascript" src="js/fixedFicha.js"></script><?php } ?>
    <?php if($op == "carro-resumen"){?>
		<script type="text/javascript" src="js/fixedCarro1.js"></script>
        <script type="text/javascript" src="js/cambiosDePrecio.js"></script>
	<?php } ?>
    
    
	<?php if ($_GET[msje]) {
		if($_GET[a] == 1){$type = "success";} else {$type = "error";}
		?>
	<script type="text/javascript">
        $(function() { 
				swal('','<?= $_GET[msje];?>','<?= $type; ?>'); 
			});
    </script>     
    <?php } ?>
    
	
   <script type="text/javascript">ga('send', 'pageview');</script> 
</body>
</html>
<?php mysqli_close($conexion);?>