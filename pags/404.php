<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">Error 404</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>
    </ul>
</div>

<div class="container mtop-20 mbottom-20">
    <div class="title_search">Lo sentimos, no se encontró la página que buscabas</div>
    <div class="subtitle_search">Prueba utilizando nuestro buscador o vuelve al HOME
y navega en las distintas categorías.</div>

    <div class="buscador_search">
        <form method="get" action="busquedas" style="float:none;">
          <input type="text" name="buscar" class="campo_buscador" placeholder="<?php if(isset($_GET[buscar])){echo $_GET[buscar];} else {echo 'Buscar producto';}?>">
           <input class="btn_search" type="submit" value="">
        </form>
    </div>
</div>
<div class="sepdiv"></div>

<?php 
$filtros_search['random'] = true;
$filtros_search['limite'] = 4;
$productos_search = get_products($filtros_search); 
?>

<div class="container mbottom-20">
    <div class="title2_search">Te recomendamos</div>

    <div class="grid_products grid_four">
        <?php foreach ($productos_search['productos']['producto'] as $item): 
            $url_producto = 'ficha/' . $item['id_producto'] . '/' . $item['nombre_seteado'];
            ?>
            <div class="col">
                <?php if ($item['precio_cantidad'] == 1): ?>
                    <div class="descuento_cantidad">descuento por cantidad</div>
                <?php endif ?>
                
                <?php if (isset($_COOKIE['usuario_id'])): ?>
                    <?php if (producto_guardado($_COOKIE['usuario_id'], $item['id_producto'])): ?>
                        <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="remove"><i class="fas fa-heart"></i></div>
                    <?php else: ?>
                        <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                    <?php endif ?>
                <?php else: ?>
                    <div class="icon_save" onclick="addToWish(<?=$item['id_producto']?>, this)" data-action="add"><i class="far fa-heart"></i></div>
                <?php endif ?>

                <!-- Porcentaje descuento  -->
                <?php if ( ofertaTiempo($item['id_hijo']) ): ?>
                    <div class="porcentaje_descuento">-<?= round(100 - ($item['oferta_tiempo_descuento'] * 100) / $item['precio']) ?>%</div>
                <?php elseif($is_cyber AND $item['precio_cyber'] > 0): ?>
                    <div class="porcentaje_descuento">-<?= round(100 - ($item['precio_cyber'] * 100) / $item['precio']) ?>%</div>
                <?php elseif( $item['descuento'] > 0 ): ?>
                    <div class="porcentaje_descuento">-<?= round(100 - ($item['descuento'] * 100) / $item['precio']) ?>%</div>
                <?php endif ?>
                
                <div class="thumb">
                    <a href="<?= $url_producto ?>">
                        <?php if ($item['imagen_grilla'] != 'img/sin-foto.jpg'): 
                            $thumb = imagen("imagenes/productos/", $item['nombre_imagen']); ?>
                            <img src="<?= $thumb ?>">
                        <?php else: ?>
                            <img src="<?= $item['imagen_grilla'] ?>">
                        <?php endif ?>
                        
                    </a>
                    <?php if(ofertaTiempo($item['id_hijo'])){
                        $oferta_tiempo_hasta = $item['oferta_tiempo_hasta'];
                     ?>
                        <div class="countdown" rel="<?= $oferta_tiempo_hasta ?>">
                            <div class="col">
                                <small>Hora</small>
                                <div class="box" data-rel="hour">00</div>
                            </div>
                            <div class="col">
                                <small>Min.</small>
                                <div class="box" data-rel="minute">00</div>
                            </div>
                            <div class="col">
                                <small>Seg.</small>
                                <div class="box" data-rel="second">00</div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <a href="<?= $url_producto ?>">
                    <span class="nombre"><?= $item['nombre'] ?></span>
                    
                    <?php if ( ofertaTiempo( $item['id_hijo'] ) ): ?>
                        <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                        <span class="precio"><small class="signo">$</small><?= number_format(getPrecio($item['id_hijo']),0,",",".") ?> <small class="iva">IVA Incluido
                    <?php else: ?>
                        <?php if ($item['descuento'] > 0): ?>
                            <span class="descuento">Antes $<?= number_format($item['precio'], 0, ',', '.') ?></span>
                            <span class="precio"><small class="signo">$</small><?= number_format($item['descuento'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                        <?php else: ?>
                            <span class="descuento"></span>
                            <span class="precio"><small class="signo">$</small><?= number_format($item['precio'], 0, ',', '.') ?> <small class="iva">IVA Incluido
                        <?php endif ?>
                    <?php endif; ?>
                    
                    
                    </small></span>
                </a>

            </div>
        <?php endforeach ?>
        </div>

    </div>
</div>