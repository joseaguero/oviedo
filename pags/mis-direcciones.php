<?php 
  if(!isset($_COOKIE['usuario_id'])){
    echo '<script>location.href = "inicio-sesion"</script>';
  }

  $cliente_id = $_COOKIE['usuario_id'];

  $direcciones = consulta_bd('d.id, d.nombre, r.nombre, c.nombre, d.numero, d.calle', 'clientes_direcciones d join regiones r on d.region_id = r.id join comunas c on d.comuna_id = c.id', "d.cliente_id = $cliente_id", 'd.principal desc');

?>

<div class="breadcrumbs">  
    <ul itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="home" itemprop="item">
                <span itemprop="name">Home</span>
                <meta itemprop="position" content="1" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">cuenta</span>
                <meta itemprop="position" content="2" />
            </a>
        </li>

        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="javascript:void(0)" itemprop="item">
                <span itemprop="name">mis direcciones</span>
                <meta itemprop="position" content="3" />
            </a>
        </li>
    </ul>
</div>

<div class="content_gray">
  
  <div class="content-dashboard">
    <h2 class="title-dash">Mi cuenta</h2>
    <div class="grid-dashboard">
      <div class="col">

        <?php include("pags/menuMiCuenta.php"); ?>

      </div>
      <div class="col">
        
        <div class="main_dash">
          
          <div class="title">Mis direcciones</div>
          <div class="subtitle">
            Podrás ver tus pedidos, historial de compra y <br>editar tus datos personales y de envío.
          </div>
          
          
          <?php if (is_array($direcciones)): ?>
            <?php foreach ($direcciones as $dir): ?>
              <div class="row_direccion">
                <div class="alias"><?= $dir[1] ?></div>
                <div class="direccion-dash"><?= $dir[5] ?>, <?= $dir[4] ?></div>
                <div class="region"><?= ucwords(strtolower($dir[3])) ?>, <?= $dir[2] ?></div>

                <div class="options">
                  <a href="javascript:void(0)" class="delDir" data-id="<?= $dir[0] ?>">
                    <img src="img/delete.svg">
                    <span>Eliminar</span>
                  </a>

                  <a href="cuenta/mis-direcciones/editar/<?= $dir[0] ?>" class="editDir" data-id="<?= $dir[0] ?>">
                    <img src="img/edit.svg">
                    <span>Editar</span>
                  </a>
                </div>
              </div>
            <?php endforeach ?>
          <?php else: ?>
            <div>No tienes direcciones guardadas actualmente.</div>
          <?php endif ?>

          <a href="cuenta/mis-direcciones/agregar" class="agregarDireccion btn">
            Agregar nueva dirección
          </a>
        
        </div>

      </div>
    </div>
  </div>
  
</div>

<div class="bg_popup"></div>
<div class="popup_acciones">
  <div class="title_pop">¿Estás seguro de eliminar esta dirección?</div>

  <div class="content_buttons">
    <a href="javascript:void(0)" class="confirmPop" data-action="delete" data-id="0" data-where="dash">Confirmar</a>
    <a href="javascript:void(0)" class="cancelPop">Cancelar</a>
  </div>
</div>
