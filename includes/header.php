<header>
    <div class="top ohidden">
        <div class="container-top">
            <div class="fl">
                <a href="whatsapp://send?phone=+56939127048" class="atop">
                    <i class="fab fa-whatsapp"></i>
                    <span>+56 9 3912 7048</span>
                </a>

                <a href="mailto:hablemos@herramientaschile.cl" class="atop">
                    <i class="fas fa-envelope"></i>
                    <span>hablemos@herramientaschile.cl</span>
                </a>
            </div>
            <div class="fr">
                <?php if (!isset($_COOKIE['usuario_id'])): ?>
                    <a href="javascript:void(0)" class="atop save_products not-user">
                        <i class="fas fa-heart"></i>
                        <span class="qty_save">Productos guardados (0)</span>
                        <small>0</small>
                    </a>
                <?php else: ?>
                    <a href="cuenta/productos-guardados" class="atop save_products">
                        <i class="fas fa-heart"></i>
                        <span class="qty_save">Productos guardados (<?= qty_save($_COOKIE['usuario_id']) ?>)</span>
                        <small><?= qty_save($_COOKIE['usuario_id']) ?></small>
                    </a>
                <?php endif ?>
                
            </div>
        </div>
    </div>

    <div class="content_header container">
        <div class="col">
            <div class="btn_menu">
                <!-- <img src="img/btnmenu.svg"> -->
                <div class="hamburger">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <h1 class="logo">
                <a href="home">
                    <img src="img/logo.svg">
                </a>
            </h1><!--Fin logo-->
        </div>

        <div class="col">
            <div class="buscador">
                <form method="get" action="busquedas" style="float:none;">
                  <input type="text" name="buscar" class="campo_buscador" placeholder="Buscar por productos, marcas y más...">
                   <input class="btn_search" type="submit" value="">
                </form>
            </div>
        </div>

        <div class="col">
            <?php if(!isset($_COOKIE['usuario_id'])){?>
                <a href="login" class="aoption account_h">
                    <img src="img/i-user.svg">
                    <span>Inicia sesión o Registrate</span>
                </a>
            <?php }else{ 
                $cliente = consulta_bd("nombre","clientes","id=".$_COOKIE['usuario_id'],""); ?>
                <a href="cuenta/mis-datos" class="aoption account_h">
                    <img src="img/i-user.svg">
                    <span><?= $cliente[0][0] ?></span>
                </a>
            <?php } ?>

            <a href="mi-carro" class="aoption carro_h">
                <img src="img/i-cart.svg">
                <small class="cantidad_carro_xs"><?= qty_pro(); ?></small>
                <span>Mi carro (<small class="cantidad_carro"><?= qty_pro(); ?></small>)</span>
            </a>

            <a href="javascript:void(0)" class="aoption isearch-xs">
                <img src="img/i-search.svg">
            </a>
        </div>
        
    </div>
</header>

<nav id="main_menu">
    <?php $lineas_header = consulta_bd('id, nombre', 'lineas', 'publicado = 1', 'posicion asc'); ?>
    <ul style="grid-template-columns: repeat(<?=count($lineas_header) + 2?>, auto);">
        <li class="inicio_xs principal_nav">
            <?php if(!isset($_COOKIE['usuario_id'])){?>
                <a href="login" class="aoption">
                    <img src="img/i-user.svg">
                    <span>Inicia sesión o Registrate</span>
                </a>
            <?php }else{ 
                $cliente = consulta_bd("nombre","clientes","id=".$_COOKIE['usuario_id'],""); ?>
                <a href="cuenta/mis-datos" class="aoption">
                    <img src="img/i-user.svg">
                    <span><?= $cliente[0][0] ?></span>
                </a>
            <?php } ?>
        </li>
        <?php foreach ($lineas_header as $l): 
            $link_linea = 'lineas/'.$l[0].'/'.url_amigables($l[1]);
            // $cats_line = consulta_bd('id, nombre', 'categorias', "linea_id = $l[0]", 'posicion asc');
            $cats_line = get_categorias($l[0]); ?>
            <li class="principal_nav">
                <div class="sep_menu">
                    <a href="<?= $link_linea ?>"><?= $l[1] ?></a>
                    <?php if (is_array($cats_line)): ?>
                        <div class="btnNext">
                            <i class="material-icons">arrow_forward_ios</i>
                        </div>
                    <?php endif ?>
                </div>
                
                <?php if (is_array($cats_line)): ?>
                    <ul class="submenu">
                        <li class="title_submenu">
                            <i class="material-icons">arrow_back_ios</i>
                            <a href="javascript:void(0)"><?= $l[1] ?></a>
                        </li>
                        <?php foreach ($cats_line as $sb): ?>
                            <li>
                                <a href="categorias/<?=$sb[id]?>/<?=url_amigables($sb[nombre])?>">
                                    <?= $sb['nombre'] ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                <?php endif ?>
            
            </li>
        <?php endforeach ?>
        <li class="principal_nav">
            <div class="sep_menu">
                <a href="marcas">Marcas</a>
                <div class="btnNext">
                    <i class="material-icons">arrow_forward_ios</i>
                </div>
            </div>
            <!-- <ul class="submenu">
                <li class="title_submenu">
                    <i class="material-icons">arrow_back_ios</i>
                    <a href="javascript:void(0)">Marcas</a>
                </li>
                <?php for ($i=0; $i < 3; $i++) { ?>
                    <li>
                        <a href="#">
                            Submenu <?= $i+1 ?>
                        </a>
                    </li>
                <?php } ?>

            </ul> -->
        </li>
        <li class="principal_nav">
            <div class="sep_menu">
                <a href="ofertas" class="offer">Ofertas</a>
            </div>
        </li>
    </ul>
</nav>

<div class="search_xs">
    <form method="get" action="busquedas" style="float:none;">
        <input type="text" name="buscar" class="campo_buscador" placeholder="Buscar por productos, marcas y más...">
        <a href="javascript:void(0)" class="close_search_xs"><img src="img/close.svg"></a>
    </form>
</div>