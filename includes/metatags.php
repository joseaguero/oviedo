<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <base href="<?php echo $url_base; ?>" />
    
    <META NAME="Author" CONTENT="Moldeable S.A.">
    <META NAME="DC.Author" CONTENT="Moldeable S.A.">
    <META NAME="DC.Creator" CONTENT="Moldeable S.A.">
    <META NAME="Generator" CONTENT="Moldeable CMS">
    <META NAME="authoring.tool" CONTENT="Moldeable CMS">
    <META NAME="VW96.objecttype" CONTENT="Homepage">
    <META NAME="resource-type" CONTENT="Homepage">
    <META NAME="doc-type" CONTENT="Homepage">
    <META NAME="Classification" CONTENT="General">
    <META NAME="RATING" CONTENT="General">
    <META NAME="Distribution" CONTENT="Global">
    <META NAME="Language" CONTENT="Spanish">
    <META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
    <META NAME="Robots" CONTENT="index,follow">
    <META NAME="Revisit-after" CONTENT="15 days">
    <META NAME="CREDITS" CONTENT="Diseño y Prgramación: Moldeable S.A., www.moldeable.com">

    <?php 

    $descripcion_seo_default = "Todo en Herramientas, Soldadoras, Maquinarias, insumos industriales y materiales de construcción. Los mejores precios, marcas y con despacho a todo Chile.";
    $imagen_seo_default = $url_base . "images/default_seo_image.jpg";
    switch ($op) {
        case 'lineas':
            $menu_id            = (int)mysqli_real_escape_string($conexion, $_GET['id']);
            $row                = consulta_bd('titulo_seo, descripcion_seo, imagen_seo, nombre', 'lineas', "id = $menu_id", '');
            $titulo_seo         = ($row[0][0] != NULL AND trim($row[0][0]) != '') ? $row[0][0] : $row[0][3];
            $descripcion_seo    = ($row[0][1] != NULL AND trim($row[0][1]) != '') ? strip_tags($row[0][1]) : $descripcion_seo_default;
            $imagen_seo         = $row[0][2];

            if ($imagen_seo != null AND trim($imagen_seo) != '') {
                $imagen_seo = $url_base . 'imagenes/lineas/' . $imagen_seo;
            }else{
                $imagen_seo = $imagen_seo_default;
            }
            break;
        case 'categorias':
            $menu_id            = (int)mysqli_real_escape_string($conexion, $_GET['id']);
            $row                = consulta_bd('titulo_seo, descripcion_seo, imagen_seo, nombre', 'categorias', "id = $menu_id", '');
            $titulo_seo         = ($row[0][0] != NULL AND trim($row[0][0]) != '') ? $row[0][0] : $row[0][3];
            $descripcion_seo    = ($row[0][1] != NULL AND trim($row[0][1]) != '') ? strip_tags($row[0][1]) : $descripcion_seo_default;
            $imagen_seo         = $row[0][2];

            if ($imagen_seo != null AND trim($imagen_seo) != '') {
                $imagen_seo = $url_base . 'imagenes/categorias/' . $imagen_seo;
            }else{
                $imagen_seo = $imagen_seo_default;
            }
            break;
    }

    ?>
    
	<?php if($op == "ficha"){ 
		$idProd = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
		$prod = consulta_bd("nombre, descripcion_corta, thumbs, descripcion_seo, keywords","productos","id=$idProd","");
	?>
        <meta property="og:title" content="<?= $prod[0][0]; ?>" />
        <?php if($prod[0][3] != ""){ ?>
        <meta property="og:description" content="<?= strip_tags($prod[0][3]); ?>" />
        <?php } else { ?>
        <meta property="og:description" content="<?= strip_tags($prod[0][1]); ?>" />
        <?php } ?>
        <meta property="og:image" content="<?php echo $url_base; ?>imagenes/productos/<?= $prod[0][2]; ?>" />
        <meta property="og:site_name" content="<?= opciones("url_sitio"); ?>" />
        <link rel="canonical" href="<?= obtenerURL(); ?>">
        
		<?php if($prod[0][4] != ""){ ?>
        <META NAME="Keywords" CONTENT="<?= $prod[0][4]; ?>">
		<?php } else { ?>
		<META NAME="Keywords" CONTENT="<?= opciones("seo_keywords"); ?>">
		<?php } ?>       
    <?php }elseif($op == 'lineas' OR $op == 'categorias'){ ?>
        <meta property="og:title" content="<?= $titulo_seo; ?>" />
        <meta property="og:description" content="<?= $descripcion_seo; ?>" />
        <meta property="og:image" content="<?= $imagen_seo; ?>" />
        <meta property="og:site_name" content="<?php echo $url_base; ?>" />
        <link rel="canonical" href="<?= obtenerURL(); ?>">
    <?php } else { ?>
        <META NAME="Title" CONTENT="<?= opciones("seo_titulo"); ?>">
        <META NAME="DC.Title" CONTENT="<?= opciones("seo_titulo"); ?>">
        <META http-equiv="title" CONTENT="<?= opciones("seo_titulo"); ?>">
        <META NAME="DESCRIPTION" CONTENT="<?= opciones("seo_descripcion"); ?>">
        <META NAME="copyright" CONTENT="<?= opciones("nombre_cliente"); ?>">
        <META NAME="Keywords" CONTENT="<?= opciones("seo_keywords"); ?>">
        <META http-equiv="keywords" CONTENT="<?= opciones("seo_keywords"); ?>">
       
        <meta property="og:title" content="<?= opciones("seo_titulo"); ?>" />
        <meta property="og:description" content="<?= opciones("seo_descripcion"); ?>" />
        <meta property="og:image" content="<?= opciones("seo_url_imagen"); ?>" />
        <meta property="og:site_name" content="<?php echo $url_base; ?>" />
        <link rel="canonical" href="<?= obtenerURL(); ?>">
    <?php } ?>


    <!-- FAVICONS -->
    <LINK REL="SHORTCUT ICON" HREF="favicons/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
