<!DOCTYPE html>
<html lang="es">
<head>
    <?php include("includes/metatags.php"); ?>
    <?php include("includes/titulos.php"); ?>
    
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    
    <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>

	<!-- Sweet alert -->
    <script type="text/javascript" src="js/sweetalert.js"></script>
    <link href="css/sweetalert2.min.css" rel="stylesheet">
<?php 
	
	$base = file_get_contents('tienda/base_tienda.css');
	echo '<style type="text/css">';
	echo minify_css($base);
	echo '</style>';

	$main = file_get_contents('css/main.css');
	echo '<style type="text/css">';
	echo minify_css($main);
	echo '</style>';

	$responsive = file_get_contents('css/responsive.css');
	echo '<style type="text/css">';
	echo minify_css($responsive);
	echo '</style>';
	
	/*$baseResp = file_get_contents('tienda/base_responsive.css');
	echo '<style type="text/css">';
	echo minify_css($baseResp);
	echo '</style>';*/

	$ui = file_get_contents('css/jquery-ui.css');
	echo '<style type="text/css">';
	echo minify_css($ui);
	echo '</style>';
?>
	<link rel="stylesheet" type="text/css" href="css/slick.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    
    <script src="js/jquery.cycle2.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script src="js/jquery.ui.core.min.js"></script>
    <script src="js/jquery.ui.widget.min.js"></script>
    <script src="js/jquery.ui.button.min.js"></script>
    <script src="js/jquery.ui.spinner.min.js"></script>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-23584974-78', 'auto');
	ga('require', 'ec');
	ga('send', 'pageview');
	
	function productClick(sku, nombre_producto,categoria, variante, posicion, lista, marca ) {
	  ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'position': posicion
	  });
	  ga('ec:setAction', 'click', {list: lista});
	  ga('send', 'event', 'UX', 'click', 'Results', {
	      hitCallback: function() {
	      }
	  });
	};
  
	function addToCart(sku, nombre_producto,categoria, variante, precio, cantidad, marca) {
	 ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'price': precio,
	    'quantity': cantidad
	  });
	  ga('ec:setAction', 'add');
	  ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
	}
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23584974-78"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23584974-78');
</script>
<!--agrego el chat en el caso que el cliente lo cargo en su pagina de configuracion -->
<?= opciones("chat"); ?>
<meta name="google-site-verification" content="h52iZo-ulqh0TclEFmA1e3uSkDshuxqaPQ4_RWJ_WQY" />

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '116816973034064'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=116816973034064&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

</head>