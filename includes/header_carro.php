<div class="header_carro">
	<div class="container_small grid_hcarro">
		<h1 class="logo">
	        <a href="home">
	            <img src="img/logo.svg">
	        </a>
	    </h1><!--Fin logo-->
	
		<?php if ($op != 'exito' AND $op != 'fracaso'): ?>
			<div class="box_breadcarro">
		    	<a href="javascript:void(0)" class="active">Mi carro</a> |
		    	<a href="javascript:void(0)">comprar</a>
		    </div>
		<?php endif ?>
	</div>
</div>