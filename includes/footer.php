<footer>
    <div class="container grid_footer">
        <div class="col xsmb20">
            <div class="title hidden_movil">Somos Parte del Grupo Oviedo</div>

            <img src="img/logo_footer.png" class="logos_footer">
            <img src="img/ssl.png" class="logos_footer">
                
            <div class="text">Seguridad y Privacidad Garantizada SSL Secure GlobalSign. Comprar en Herramientaschile.cl es 100% seguro.</div>

            <img src="img/ccs.png" style="margin-right: 20px;" class="xsml">
            <img src="img/mts.png" class="xsml">
        </div>
        <div class="col">
            <div class="title btnXs">
                Categorías 
                <i class="material-icons">keyboard_arrow_right</i>
            </div>
            <div class="list_footer list_hidden">
                <?php $lineas_footer = consulta_bd('id, nombre', 'lineas', 'publicado = 1', 'posicion asc'); ?>
                <?php foreach ($lineas_footer as $item): ?>
                    <li><a href="lineas/<?= $item[0] ?>/<?= url_amigables($item[1]) ?>"><?= $item[1] ?></a></li>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col">
            <div class="title btnXs">
                Soporte 
                <i class="material-icons">keyboard_arrow_right</i>
            </div>
            <div class="list_footer list_hidden">
                <li><a href="terminos-y-condiciones">Términos y Condiciones</a></li>
                <li><a href="politicas-de-privacidad">Políticas de privacidad</a></li>
                <li><a href="como-comprar">Cómo Comprar</a></li>
                <li><a href="politicas-de-calidad">Políticas de Calidad</a></li>
                <li><a href="tiendas">Tiendas</a></li>
                <!-- <li><a href="#">Quienes somos</a></li> -->
            </div>
        </div>
        <div class="col xsmb20">
            <div class="title">Contacto</div>
            <div class="list_footer">
                <li><a href="tel:+56939127048" class="numbero_contacto">+56 9 3912 7048</a></li>
                <li><a href="mailto:hablemos@herramientaschile.cl" class="numbero_contacto">hablemos@herramientaschile.cl</a></li>
            </div>

            <div class="title2">Siguenos</div>
            <div class="content_box">
                <a href="https://www.facebook.com/HerramientasChile/" class="box_rrss" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="https://www.instagram.com/herramientaschilehcl/" class="box_rrss" target="_blank"><i class="fab fa-instagram"></i></a>
                <!-- <a class="box_rrss"><i class="fab fa-youtube"></i></a> -->
            </div>

            <div class="title2">Medios de pago</div>
            <div class="content_box">
                <img src="img/webpayfooter.png" style="float: left; margin-right: 15px;">
                <img src="img/mpagofooter.png" style="float: left; margin-top: 8px;">
            </div>
        </div>
    </div>

    <div class="bottom_footer">
        <div class="container grid_btm">
            <div class="left">&copy; Todos los derechos reservados. Herramientaschile.cl</div>
            <div class="right"><img src="img/moldeable.png"></div>
        </div>
    </div>
</footer>