<?php
include("admin/conf.php");
require_once('admin/includes/tienda/cart/inc/functions.inc.php');
include("funciones.php");

if(isset($_GET[op])){
    $op=$_GET[op];
    } else{
        $op="home";
    }
	include("php-html-css-js-minifier.php");
	
include("includes/cookies.php");

?>
<?php 
	ob_start(); # apertura de bufer
	include("includes/head.php");
	$head = ob_get_contents();
	ob_end_clean(); # cierre de bufer
	echo minify_html($head);
?>



<body>
<div class="cont_loading">
	<div class="lds-dual-ring"></div>
</div>

<div id='popUp'></div>
	<?php 
		ob_start(); # apertura de bufer
        include("includes/header.php");
        $header = ob_get_contents();
        ob_end_clean(); # cierre de bufer
        echo minify_html($header);
		
		ob_start(); # apertura de bufer
        include("pags/$op.php");
        $pags = ob_get_contents();
        ob_end_clean(); # cierre de bufer
        echo minify_html($pags);
		
		ob_start(); # apertura de bufer
        include("includes/footer.php");
        $footer = ob_get_contents();
        ob_end_clean(); # cierre de bufer
        echo minify_html($footer);
    ?>
 
<!-- JavaScript -->
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
<script src="js/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(".editarDireccion").fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		$("#nuevaDireccion").fancybox({
			'titlePosition'		: 'inside',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none'
			});
		$(".fancybox").fancybox({
			'titlePosition'		: 'inside',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none'
			});
		
		});
	
</script>   
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.Rut.js"></script>
	<script type="text/javascript" src="admin/js/jquery.numeric.js"></script>
    <script type="text/javascript">$(function(){$("#telefono, #phone, #telefono_checkout").numeric();});</script>
    <script type="text/javascript" src="js/validacionCompraRapida.js"></script>
    <script type="text/javascript" src="js/agrega_desde_ficha.js"></script>
    <script type="text/javascript" src="js/cotiza_desde_ficha.js"></script>
    <?php 
		$codigoDescuento = file_get_contents('js/codigoDescuento.js');
		echo '<script>';
		echo minify_css($codigoDescuento);
		echo '</script>';
	?>
	<script type="text/javascript" src="js/js.cookie.min.js"></script>
    <script type="text/javascript" src="js/funciones.js?v=<?=cacheo()?>"></script>
    <script type="text/javascript">function cerrar(){ $(".fondoPopUp").remove();}</script>
    <script type="text/javascript" src="js/agrega_quita_elimina_ShowCart.js"></script>
	<script type="text/javascript" src="js/agrega_quita_elimina_ShowCartCotizacion.js"></script>
    <script type="text/javascript" src="js/lista_productos.js"></script>
    <script type="text/javascript" src="js/validacionCotizacion.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    
    <?php if($op == "home"){?><script type="text/javascript" src="js/slider-home.js"></script><?php } ?>
    <?php if($op == "ficha"){?><script type="text/javascript" src="js/fixedFicha.js"></script><?php } ?>
    <?php if($op == "carro-resumen"){?>
		<script type="text/javascript" src="js/fixedCarro1.js"></script>
        <script type="text/javascript" src="js/cambiosDePrecio.js"></script>
	<?php } ?>
    
    <?php if ($op == 'home'): ?>
    	<script type="text/javascript" src="js/owl.js"></script>
    	<script type="text/javascript" src="js/sliderBanners.js"></script>
    <?php endif ?>
    
	<?php if ($_GET[msje]) {
		if($_GET[a] == 1){$type = "success";} else {$type = "error";}
		?>
	<script type="text/javascript">
        $(function() { 
				swal('','<?= $_GET[msje];?>','<?= $type; ?>'); 
			});
    </script>     
    <?php } ?>
    
	
   <script type="text/javascript">ga('send', 'pageview');</script> 
    
    
    <?php /*?><div class="cont100">
        <div class="cont100Centro">
            <?php 
                echo '<pre>';
                print_r($_SESSION);
                echo '</pre>';
            ?>
        </div>
    </div><?php */?>
    <?php /*?><div class="cont100">
        <div class="cont100Centro">
            <?php 
                echo '<pre>';
                print_r($_COOKIE);
                echo '</pre>';
            ?>
        </div>
    </div><?php */?>
</body>
</html>
<?php mysqli_close($conexion);?>