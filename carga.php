<?php 
include('admin/conf.php');
require('admin/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
date_default_timezone_set('America/Santiago');

$fecha_hoy = date('Y-m-d H:i:s', time());

/*for ($i=1; $i < 329; $i++) { 
	insert_bd('filtros_productos', 'producto_id, filtros_variante_id', "$i, 1");
}*/


$archivo = "prd.xlsx";

$obj = PHPEXCEL_IOFactory::load($archivo);
$obj->setActiveSheetIndex(2);
$rows = $obj->setActiveSheetIndex(2)->getHighestRow();

$fecha_hoy = date('Y-m-d H:i:s', time());
$contador = 0;
$encontrados = 0;

for ($i=2; $i <= $rows ; $i++) { 
	$sku = trim($obj->getActiveSheet()->getCell('A'.$i)->getCalculatedValue());
	// $ficha_tecnica = trim(nl2br($obj->getActiveSheet()->getCell('J'.$i)->getCalculatedValue()));

	// $descripcion = trim(nl2br($obj->getActiveSheet()->getCell('K'.$i)->getCalculatedValue()));

	// $descripcion = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', html_entity_decode($descripcion));
	
	// $marca_id = trim($obj->getActiveSheet()->getCell('C'.$i)->getCalculatedValue());
	
	$peso = trim($obj->getActiveSheet()->getCell('F'.$i)->getCalculatedValue());
	$alto = trim($obj->getActiveSheet()->getCell('G'.$i)->getCalculatedValue());
	$ancho = trim($obj->getActiveSheet()->getCell('H'.$i)->getCalculatedValue());
	$largo = trim($obj->getActiveSheet()->getCell('I'.$i)->getCalculatedValue());

	$producto = consulta_bd('producto_id, sku', 'productos_detalles', "sku = '$sku'", '');

	if (is_array($producto)) {
		update_bd('productos_detalles', "peso = $peso, alto = $alto, ancho = $ancho, largo = $largo", "sku = '$sku'");
		echo '<li>'.$sku.': Producto en la bd</li>';
		$encontrados++;
	}else{
		echo '<li style="color: red;">'.$sku.': No encontrado</li>';
	}

	$contador++;

};

echo 'Productos agregados: ' . $contador . '<br>';
echo 'Productos encontrados: ' . $encontrados . '<br>';
